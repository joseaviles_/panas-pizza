<?php

use Illuminate\Database\Seeder;
use App\Models\CashRegister;
use App\Models\Category;
use App\Models\Client;
use App\Models\Configuration;
use App\Models\DeliveryCompany;
use App\Models\DeliveryCompanyDriver;
use App\Models\DeliveryType;
use App\Models\PaymentMethod;
use App\Models\PaymentMethodOption;
use App\Models\Product;
use App\Models\Promotion;
use App\Models\RawMaterial;
use App\Models\RawMaterialMovement;
use App\Models\UnprocessedProductsMovement;

class ConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $price=15;
        $exchange_rate=268000;
        $price_bss=$price*$exchange_rate;

    	// Configuration
		Configuration::create(['exchange_rate' => $exchange_rate]);

		// Clients
		$clients=['1'=>['client_number'=>'4450', 'first_name'=>'Miguel', 'last_name'=>'Avilés', 'identity_card'=>'9908338', 'name_business_phone'=>'Miguel - Riberas del Caroní', 'first_phone'=>'04249740557', 'second_phone'=>'04121934884'], '2'=>['client_number'=>'4451', 'first_name'=>'Joselen', 'last_name'=>'Aray', 'identity_card'=>'', 'name_business_phone'=>'Joselen - Curagua', 'first_phone'=>'04129410452', 'second_phone'=>''], '3'=>['client_number'=>'4452', 'first_name'=>'Luis', 'last_name'=>'Parra', 'identity_card'=>'24855325', 'name_business_phone'=>'Luis - Riberas del Caroní', 'first_phone'=>'04263985557', 'second_phone'=>'']];

    	foreach($clients as $client) {
    		Client::create($client);
    	}

    	// Categories
    	$categories=['1'=>['name'=>'Bebida'], '2'=>['name'=>'Extra'], '3'=>['name'=>'Postre'], '4'=>['name'=>'Pizza grande'], '5'=>['name'=>'Pizza mediana'], '6'=>['name'=>'Pizza pequeña']];

    	foreach($categories as $category) {
    		Category::create($category);
    	}

    	// Raw material
		$raws_material=['1'=>['name'=>'Carne', 'unit'=>'Gramos', 'amount'=>0], '2'=>['name'=>'Harina de trigo', 'unit'=>'Gramos', 'amount'=>0], '3'=>['name'=>'Jamón', 'unit'=>'Gramos', 'amount'=>0], '4'=>['name'=>'Maíz', 'unit'=>'Gramos', 'amount'=>0], '5'=>['name'=>'Pepperoni', 'unit'=>'Gramos', 'amount'=>0], '6'=>['name'=>'Pollo', 'unit'=>'Gramos', 'amount'=>0], '7'=>['name'=>'Queso', 'unit'=>'Gramos', 'amount'=>0], '8'=>['name'=>'Levadura', 'unit'=>'Gramos', 'amount'=>0], '9'=>['name'=>'Azúcar', 'unit'=>'Gramos', 'amount'=>0], '10'=>['name'=>'Aceite', 'unit'=>'Litros', 'amount'=>0], '11'=>['name'=>'Tomate', 'unit'=>'Gramos', 'amount'=>0], '12'=>['name'=>'Cebolla', 'unit'=>'Gramos', 'amount'=>0], '13'=>['name'=>'Especias', 'unit'=>'Gramos', 'amount'=>0], '14'=>['name'=>'Aceitunas negras', 'unit'=>'Gramos', 'amount'=>0], '15'=>['name'=>'Albahaca', 'unit'=>'Gramos', 'amount'=>0], '16'=>['name'=>'Caja mediana artesanal 30x30', 'unit'=>'Unidades', 'amount'=>0], '17'=>['name'=>'Caja pequeña artesanal 27x27', 'unit'=>'Unidades', 'amount'=>0], '18'=>['name'=>'Caja grande de cartón corrugado 33x33', 'unit'=>'Unidades', 'amount'=>0], '19'=>['name'=>'Champiñones', 'unit'=>'Gramos', 'amount'=>0], '20'=>['name'=>'Lauren', 'unit'=>'Gramos', 'amount'=>0], '21'=>['name'=>'Orégano', 'unit'=>'Gramos', 'amount'=>0], '22'=>['name'=>'Pimentón', 'unit'=>'Gramos', 'amount'=>0], '23'=>['name'=>'Piña', 'unit'=>'Gramos', 'amount'=>0], '24'=>['name'=>'Sal', 'unit'=>'Gramos', 'amount'=>0], '25'=>['name'=>'Salchichón', 'unit'=>'Gramos', 'amount'=>0], '26'=>['name'=>'Tocineta', 'unit'=>'Gramos', 'amount'=>0]];

    	foreach($raws_material as $raw_material) {
    		$rm=RawMaterial::create($raw_material);
    	}

        // Categories - Raw material
        $categories_raws_material=['1'=>['category_id'=>4, 'raw_material'=>['1'=>['raw_material_id'=>2, 'amount'=>285], '2'=>['raw_material_id'=>8, 'amount'=>10], '3'=>['raw_material_id'=>7, 'amount'=>250], '4'=>['raw_material_id'=>9, 'amount'=>50], '5'=>['raw_material_id'=>10, 'amount'=>0.02], '6'=>['raw_material_id'=>11, 'amount'=>350], '7'=>['raw_material_id'=>12, 'amount'=>50], '8'=>['raw_material_id'=>13, 'amount'=>10]]], '2'=>['category_id'=>5, 'raw_material'=>['1'=>['raw_material_id'=>2, 'amount'=>155], '2'=>['raw_material_id'=>8, 'amount'=>5], '3'=>['raw_material_id'=>7, 'amount'=>160], '4'=>['raw_material_id'=>9, 'amount'=>40], '5'=>['raw_material_id'=>10, 'amount'=>0.01], '6'=>['raw_material_id'=>11, 'amount'=>225], '7'=>['raw_material_id'=>12, 'amount'=>10], '8'=>['raw_material_id'=>13, 'amount'=>5]]], '3'=>['category_id'=>6, 'raw_material'=>['1'=>['raw_material_id'=>2, 'amount'=>100], '2'=>['raw_material_id'=>8, 'amount'=>3.5], '3'=>['raw_material_id'=>7, 'amount'=>110], '4'=>['raw_material_id'=>9, 'amount'=>30], '5'=>['raw_material_id'=>10, 'amount'=>0.01], '6'=>['raw_material_id'=>11, 'amount'=>170], '7'=>['raw_material_id'=>12, 'amount'=>5], '8'=>['raw_material_id'=>13, 'amount'=>2]]]];

        foreach($categories_raws_material as $categories_raw_material) {
            $category=Category::find($categories_raw_material['category_id']);
            foreach($categories_raw_material['raw_material'] as $raw_material) {
                $category->raw_material()->attach($raw_material['raw_material_id'],['amount'=>$raw_material['amount']]);
            }
        }

    	// Products
        $products=['1'=>['name'=>'Coca Cola 2L', 'photo'=>'none', 'real_name_photo'=>'none', 'category_id'=>1, 'price'=>2.5, 'type'=>1, 'amount'=>0], '2'=>['name'=>'Coca Cola 1.5L', 'photo'=>'none', 'real_name_photo'=>'none', 'category_id'=>1, 'price'=>2, 'type'=>1, 'amount'=>0], '3'=>['name'=>'Pepsi Cola 2L', 'photo'=>'none', 'real_name_photo'=>'none', 'category_id'=>1, 'price'=>2.5, 'type'=>1, 'amount'=>0], '4'=>['name'=>'Pepsi Cola 1.5L', 'photo'=>'none', 'real_name_photo'=>'none', 'category_id'=>1, 'price'=>2, 'type'=>1, 'amount'=>0]];

        foreach($products as $product) {
            $prod=Product::create(['name'=>$product['name'], 'photo'=>$product['photo'], 'real_name_photo'=>$product['real_name_photo'], 'category_id'=>$product['category_id'], 'price'=>$product['price'], 'type'=>$product['type'], 'amount'=>$product['amount']]);
        }

    	// Payment methods
    	$payment_methods=['1'=>['name'=>'Transferencia en Bs.S', 'currency'=>'Bs.S', 'type'=>'Electrónico'], '2'=>['name'=>'Pago móvil en Bs.S', 'currency'=>'Bs.S', 'type'=>'Electrónico'], '3'=>['name'=>'Efectivo en $', 'currency'=>'$', 'type'=>'Efectivo'], '4'=>['name'=>'Transferencia en $', 'currency'=>'$', 'type'=>'Electrónico'], '5'=>['name'=>'Efectivo en Bs.S', 'currency'=>'Bs.S', 'type'=>'Efectivo']];

    	foreach($payment_methods as $payment_method) {
    		PaymentMethod::create($payment_method);
    	}

    	// Payment method options
    	$payment_method_options=['1'=>['name'=>'BBVA Provincial', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>1], '2'=>['name'=>'Banco Bicentenario del Pueblo', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>1], '3'=>['name'=>'Banco Caroní', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>1], '4'=>['name'=>'Banco Nacional de Crédito', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>1], '5'=>['name'=>'Banco de Venezuela', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>1], '6'=>['name'=>'Banesco', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>1], '7'=>['name'=>'DELSUR Banco Universal', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>1], '8'=>['name'=>'Mercantil Banco', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>1], '9'=>['name'=>'BBVA Provincial', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>2], '10'=>['name'=>'Banco de Venezuela', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>2], '11'=>['name'=>'Banesco', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>2], '12'=>['name'=>'Neteller', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>4], '13'=>['name'=>'PayPal', 'commission'=>5.4, 'extra_amount'=>0.30, 'payment_method_id'=>4], '14'=>['name'=>'Zelle', 'commission'=>0, 'extra_amount'=>0, 'payment_method_id'=>4]];

    	foreach($payment_method_options as $payment_method_option) {
    		PaymentMethodOption::create($payment_method_option);
    	}

    	// Delivery companies
    	$delivery_companies=['1'=>['name'=>'Pa Yá! Delivery']];

    	foreach($delivery_companies as $delivery_company) {
    		DeliveryCompany::create($delivery_company);
    	}

        // Delivery types
        $delivery_types=['1'=>['name'=>'Delivery corto', 'price'=>2, 'delivery_company_id'=>1], '2'=>['name'=>'Delivery medio', 'price'=>3, 'delivery_company_id'=>1], '3'=>['name'=>'Delivery largo', 'price'=>4, 'delivery_company_id'=>1], '4'=>['name'=>'Delivery doble largo', 'price'=>5, 'delivery_company_id'=>1], '5'=>['name'=>'Delivery XXL', 'price'=>7, 'delivery_company_id'=>1]];

        foreach($delivery_types as $delivery_type) {
            DeliveryType::create($delivery_type);
        }

        // Cash register
        CashRegister::create();

    }
}
