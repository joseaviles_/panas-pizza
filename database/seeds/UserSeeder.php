<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Role::create(['name' => 'admin']);
    	Role::create(['name' => 'seller']);

        $user=User::create([
            'first_name'    => 'Angelo',
            'last_name'     => 'Estrada',
            'identity_card' => '12345678',
            'email'         => 'angeloricardoe@gmail.com',
            'password'      => Hash::make('angelo-1234'),
        ]);

        $user->assignRole('admin');

        $user=User::create([
            'first_name'    => 'José',
            'last_name'     => 'Avilés',
            'identity_card' => '25040903',
            'email'         => 'joseaviles230796@gmail.com',
            'password'      => Hash::make('jose-1234'),
        ]);

        $user->assignRole('admin');
    }
}
