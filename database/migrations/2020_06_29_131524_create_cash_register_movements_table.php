<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashRegisterMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_register_movements', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cash_register_id')->unsigned();
            $table->foreign('cash_register_id')->references('id')->on('cash_registers')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('type');
            $table->string('currency');
            $table->string('movement');
            $table->double('amount');
            $table->bigInteger('sale_id')->unsigned()->nullable();
            $table->foreign('sale_id')->references('id')->on('sales')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_register_movements');
    }
}
