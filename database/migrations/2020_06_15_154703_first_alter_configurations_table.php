<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FirstAlterConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configurations', function (Blueprint $table) {
            $table->string('name')->default('Panas Pizza');
            $table->string('logo')->default('images/panas-pizza.jpg?v=360703934');
            $table->string('real_name_logo')->default('images/panas-pizza.jpg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configurations', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('logo');
            $table->dropColumn('real_name_logo');
        });
    }
}
