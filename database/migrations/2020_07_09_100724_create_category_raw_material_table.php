<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryRawMaterialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_raw_material', function (Blueprint $table) {
            $table->bigInteger('category_id')->unsigned();
            $table->bigInteger('raw_material_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('raw_material_id')->references('id')->on('raw_material')->onUpdate('cascade')->onDelete('cascade');
            $table->double('amount')->default(0);
            $table->primary(['category_id', 'raw_material_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_raw_material');
    }
}
