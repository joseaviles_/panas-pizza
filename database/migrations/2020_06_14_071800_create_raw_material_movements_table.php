<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRawMaterialMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raw_material_movements', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('raw_material_id')->unsigned();
            $table->foreign('raw_material_id')->references('id')->on('raw_material')->onUpdate('cascade')->onDelete('cascade');
            $table->double('amount');
            $table->double('price');
            $table->double('price_bss');
            $table->double('exchange_rate');
            $table->text('concept');
            $table->integer('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raw_material_movements');
    }
}
