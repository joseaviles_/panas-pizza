<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductRawMaterialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_raw_material', function (Blueprint $table) {
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('raw_material_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('raw_material_id')->references('id')->on('raw_material')->onUpdate('cascade')->onDelete('cascade');
            $table->double('amount')->default(0);
            $table->primary(['product_id', 'raw_material_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_raw_material');
    }
}
