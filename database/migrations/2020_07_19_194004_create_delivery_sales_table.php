<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliverySalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_sales', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('delivery_company_id')->unsigned()->nullable();
            $table->foreign('delivery_company_id')->references('id')->on('delivery_companies')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('delivery_type_id')->unsigned()->nullable();
            $table->foreign('delivery_type_id')->references('id')->on('delivery_types')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('delivery_company_driver_id')->unsigned()->nullable();
            $table->foreign('delivery_company_driver_id')->references('id')->on('delivery_company_drivers')->onUpdate('cascade')->onDelete('cascade');
            $table->double('amount')->default(0);
            $table->double('price')->default(0);
            $table->bigInteger('sale_id')->unsigned()->nullable();
            $table->foreign('sale_id')->references('id')->on('sales')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_sales');
    }
}
