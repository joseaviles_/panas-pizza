<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRawMaterialIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raw_material_incomes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('raw_material_movement_id')->unsigned();
            $table->foreign('raw_material_movement_id')->references('id')->on('raw_material_movements')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('raw_material_id')->unsigned();
            $table->foreign('raw_material_id')->references('id')->on('raw_material')->onUpdate('cascade')->onDelete('cascade');
            $table->double('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raw_material_incomes');
    }
}
