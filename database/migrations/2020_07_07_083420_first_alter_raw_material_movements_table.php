<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FirstAlterRawMaterialMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('raw_material_movements', function (Blueprint $table) {
            $table->integer('is_deleted')->default(0);
            $table->bigInteger('sale_id')->unsigned()->nullable();
            $table->foreign('sale_id')->references('id')->on('sales')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('raw_material_movements', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->dropForeign('raw_material_movements_sale_id_foreign');
            $table->dropColumn('sale_id');
        });
    }
}
