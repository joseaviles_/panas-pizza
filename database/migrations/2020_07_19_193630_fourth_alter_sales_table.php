<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FourthAlterSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropForeign('sales_delivery_company_id_foreign');
            $table->dropColumn('delivery_company_id');

            $table->dropForeign('sales_delivery_company_driver_id_foreign');
            $table->dropColumn('delivery_company_driver_id');

            $table->dropForeign('sales_delivery_type_id_foreign');
            $table->dropColumn('delivery_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->bigInteger('delivery_company_id')->unsigned()->nullable();
            $table->foreign('delivery_company_id')->references('id')->on('delivery_companies')->onUpdate('cascade')->onDelete('cascade');

            $table->bigInteger('delivery_company_driver_id')->unsigned()->nullable();
            $table->foreign('delivery_company_driver_id')->references('id')->on('delivery_company_drivers')->onUpdate('cascade')->onDelete('cascade');

            $table->bigInteger('delivery_type_id')->unsigned()->nullable();
            $table->foreign('delivery_type_id')->references('id')->on('delivery_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}
