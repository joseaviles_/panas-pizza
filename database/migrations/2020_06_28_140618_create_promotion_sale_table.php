<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_sale', function (Blueprint $table) {
            $table->bigInteger('promotion_id')->unsigned();
            $table->bigInteger('sale_id')->unsigned();
            $table->foreign('promotion_id')->references('id')->on('promotions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('sale_id')->references('id')->on('sales')->onUpdate('cascade')->onDelete('cascade');
            $table->double('amount');
            $table->double('price');
            $table->text('description')->nullable();
            $table->primary(['promotion_id', 'sale_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_sale');
    }
}
