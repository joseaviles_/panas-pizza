<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            // Sale
            $table->bigInteger('sale_id')->unsigned();
            $table->foreign('sale_id')->references('id')->on('sales')->onUpdate('cascade')->onDelete('cascade');
            // Payment
            $table->bigInteger('payment_method_id')->unsigned();
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('payment_method_option_id')->unsigned()->nullable();
            $table->foreign('payment_method_option_id')->references('id')->on('payment_method_options')->onUpdate('cascade')->onDelete('cascade');
            $table->double('payment_amount_usd');
            $table->double('payment_amount_bss');
            $table->double('payment_exchange_rate');
            $table->text('payment_reference_number')->nullable();
            $table->text('payment_description')->nullable();
            // Change
            $table->bigInteger('change_method_id')->unsigned();
            $table->foreign('change_method_id')->references('id')->on('payment_methods')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('change_method_option_id')->unsigned()->nullable();
            $table->foreign('change_method_option_id')->references('id')->on('payment_method_options')->onUpdate('cascade')->onDelete('cascade');
            $table->double('change_amount_usd');
            $table->double('change_amount_bss');
            $table->double('change_exchange_rate');
            $table->text('change_reference_number')->nullable();
            $table->text('change_description')->nullable();
            $table->integer('pay_the_customer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
