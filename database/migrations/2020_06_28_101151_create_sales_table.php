<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients')->onUpdate('cascade')->onDelete('cascade');
            $table->text('description')->nullable();
            $table->bigInteger('delivery_company_id')->unsigned()->nullable();
            $table->foreign('delivery_company_id')->references('id')->on('delivery_companies')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('delivery_company_driver_id')->unsigned()->nullable();
            $table->foreign('delivery_company_driver_id')->references('id')->on('delivery_company_drivers')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('sale_type');
            $table->double('total');
            $table->double('exchange_rate');
            $table->double('total_bss');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
