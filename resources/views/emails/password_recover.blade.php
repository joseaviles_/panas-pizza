<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
	</head>
	<body>
		
		<p>Hola {{$user->first_name}} {{$user->last_name}}.</p>
		
		<p>Estás recibiendo esta notificación debido a que has solicitado una recuperación de contraseña de tu cuenta de <b>{{$configuration->name}}</b>.</p>

		<p>Por favor, ingresa al siguiente enlace para recuperar tu contraseña: <a href="{{$reset_link}}">Clic aquí</a>.</p>

	</body>
</html>