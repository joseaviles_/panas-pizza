@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-cog"></i> Configuración</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Editar</h3>
                </div>
                <form action="{{route('save_configuration')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <div class="input-group mb-3">
                                        <img src="{{asset($configuration->logo)}}" class="rounded mx-auto" width="50%">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Nombre</small>
                                    <div class="input-group mb-3">
                                        <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{$configuration->name}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-info"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <small class="text-muted">* Código de desbloqueo de operaciones</small>
                                    <div class="input-group mb-3">
                                        <input name="unlock_code" type="text" class="form-control" placeholder="Código de desbloqueo de operaciones" value="{{$configuration->unlock_code}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-lock"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">
                                    <small class="text-muted">Cambiar logo</small>
                                    <div class="custom-file mb-3">
                                        <input name="logo" type="file" accept=".jpeg,.jpg,.png" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile"><i class="fas fa-upload"></i> Opcional</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $(".custom-file-input").on("change", function() { 
                let fileName = $(this).val().split('\\').pop(); 
                $(this).next(".custom-file-label").addClass("selected").html(fileName); 
            });

        });
    </script>
@endsection