@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-store"></i> Ventas</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('sales')}}">Listado</a></li>
                    </ol>
                    @if($user->hasRole('admin'))
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-warning btn-sm" href="{{route('sales_trash',['sale_id'=>$sale->id])}}"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                        </ol>
                    @endif
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-info btn-sm" href="{{route('sales_show',['sale_id'=>$sale->id])}}"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                    </ol>
                    @if($sale->total_paid<$sale->total && $sale->sale_type==2)
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-success btn-sm" href="{{route('payment_collections_pay',['sale_id'=>$sale->id])}}"><i class="fa fa-handshake" aria-hidden="true"></i></a></li>
                        </ol>
                    @endif
                    @if($sale->dispatched==0)
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-dark btn-sm" href="{{route('sales_update_dispatched',['sale_id'=>$sale->id])}}"><i class="fa fa-shipping-fast" aria-hidden="true"></i></a></li>
                        </ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Editar venta con ID {{$sale->id}}</h3>
                </div>
                <form class="form-sale" action="{{route('sales_update')}}" method="post">
                    @csrf
                    <input id="today-sales" type="hidden" value="">
                    <input type="hidden" name="sale_id" value="{{$sale->id}}">
                    <div class="card-body">
                        <b>Vendedor y cliente</b>
                        <div id="info-seller-client"></div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Vendedor</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control-sm" value="{{$user->first_name}} {{$user->last_name}}" readonly>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-info"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(count($clients)>0)
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Cliente</small>
                                        <div class="input-group mb-3">
                                            <select class="form-control form-control-sm" name="client_type" id="client-type">
                                                <option value="1">Seleccionar del listado</option>
                                                <option value="2">Registrar nuevo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="select-client col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">* Seleccionar cliente</small>
                                        <div class="input-group mb-3">
                                            <select class="selectpicker show-tick form-control form-control-sm" name="client_id" id="client_id" data-live-search="true" data-size="5" data-show-subtext="true" required>
                                                @foreach($clients as $client)
                                                    <option {{$sale->client_id==$client->id?'selected':''}} value="{{$client->id}}">{{$client->name_business_phone}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div style="display: none;" class="number-client col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Número de cliente</small>
                                        <div class="input-group mb-3">
                                            <input id="client_number" name="client_number" type="text" class="form-control form-control-sm" placeholder="Número de cliente" value="{{old('client_number')}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-info"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display: none;" class="first-name-client col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Nombre</small>
                                        <div class="input-group mb-3">
                                            <input id="first_name" name="first_name" type="text" class="form-control form-control-sm" placeholder="Nombre" value="{{old('first_name')}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-user"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Número de cliente</small>
                                        <div class="input-group mb-3">
                                            <input name="client_number" id="client_number" type="text" class="form-control form-control-sm" placeholder="Número de cliente" value="{{old('client_number')}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-info"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Nombre</small>
                                        <div class="input-group mb-3">
                                            <input name="first_name" id="first_name" type="text" class="form-control form-control-sm" placeholder="Nombre" value="{{old('first_name')}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-user"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Apellido</small>
                                        <div class="input-group mb-3">
                                            <input name="last_name" id="last_name" type="text" class="form-control form-control-sm" placeholder="Apellido" value="{{old('last_name')}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-user"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @if(count($clients)>0)
                            <div style="display: none;" class="div-fields-client form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-2 col-md-2">              
                                        <small class="text-muted">* Apellido</small>
                                        <div class="input-group mb-3">
                                            <input name="last_name" id="last_name" type="text" class="form-control form-control-sm" placeholder="Apellido" value="{{old('last_name')}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-user"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-2 col-md-2">              
                                        <small class="text-muted">Cédula de identidad</small>
                                        <div class="input-group mb-3">
                                            <input name="identity_card" type="text" class="form-control form-control-sm" placeholder="Opcional" value="{{old('identity_card')}}">
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-id-card"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Nombre registrado en el tlf del negocio</small>
                                        <div class="input-group mb-3">
                                            <input id="name_business_phone" name="name_business_phone" type="text" class="form-control form-control-sm" placeholder="Nombre registrado en el tlf del negocio" value="{{old('name_business_phone')}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-info"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Teléfono #1</small>
                                        <div class="input-group mb-3">
                                            <input id="first_phone" name="first_phone" type="text" class="form-control form-control-sm" placeholder="Teléfono #1" value="{{old('first_phone')}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-phone"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-2 col-md-2">              
                                        <small class="text-muted">Teléfono #2</small>
                                        <div class="input-group mb-3">
                                            <input name="second_phone" type="text" class="form-control form-control-sm" placeholder="Opcional" value="{{old('second_phone')}}">
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-phone"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="div-edit-client form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Número de cliente</small>
                                        <div class="input-group mb-3">
                                            <input name="edit_client_number" id="edit_client_number" type="text" class="form-control form-control-sm" placeholder="Número de cliente" value="{{$sale->client->client_number}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-info"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Nombre</small>
                                        <div class="input-group mb-3">
                                            <input name="edit_first_name" id="edit_first_name" type="text" class="form-control form-control-sm" placeholder="Nombre" value="{{$sale->client->first_name}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-user"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Apellido</small>
                                        <div class="input-group mb-3">
                                            <input name="edit_last_name" id="edit_last_name" type="text" class="form-control form-control-sm" placeholder="Apellido" value="{{$sale->client->last_name}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-user"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">Cédula de identidad</small>
                                        <div class="input-group mb-3">
                                            <input name="edit_identity_card" id="edit_identity_card" type="text" class="form-control form-control-sm" placeholder="Opcional" value="{{$sale->client->identity_card}}">
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-id-card"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="div-edit-client form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-4 col-md-4">              
                                        <small class="text-muted">* Nombre registrado en el tlf del negocio</small>
                                        <div class="input-group mb-3">
                                            <input name="edit_name_business_phone" id="edit_name_business_phone" type="text" class="form-control form-control-sm" placeholder="Nombre registrado en el teléfono del negocio" value="{{$sale->client->name_business_phone}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-info"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4 col-md-4">              
                                        <small class="text-muted">* Teléfono #1</small>
                                        <div class="input-group mb-3">
                                            <input name="edit_first_phone" id="edit_first_phone" type="text" class="form-control form-control-sm" placeholder="Teléfono #1" value="{{$sale->client->first_phone}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-phone"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4 col-md-4">              
                                        <small class="text-muted">Teléfono #2</small>
                                        <div class="input-group mb-3">
                                            <input name="edit_second_phone" id="edit_second_phone" type="text" class="form-control form-control-sm" placeholder="Opcional" value="{{$sale->client->second_phone}}">
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-phone"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">Cédula de identidad</small>
                                        <div class="input-group mb-3">
                                            <input name="identity_card" type="text" class="form-control form-control-sm" placeholder="Opcional" value="{{old('identity_card')}}">
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-id-card"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Nombre registrado en el tlf del negocio</small>
                                        <div class="input-group mb-3">
                                            <input name="name_business_phone" id="name_business_phone" type="text" class="form-control form-control-sm" placeholder="Nombre registrado en el teléfono del negocio" value="{{old('name_business_phone')}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-info"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">* Teléfono #1</small>
                                        <div class="input-group mb-3">
                                            <input name="first_phone" id="first_phone" type="text" class="form-control form-control-sm" placeholder="Teléfono #1" value="{{old('first_phone')}}" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-phone"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-3 col-md-3">              
                                        <small class="text-muted">Teléfono #2</small>
                                        <div class="input-group mb-3">
                                            <input name="second_phone" type="text" class="form-control form-control-sm" placeholder="Opcional" value="{{old('second_phone')}}">
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <span class="fas fa-phone"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <hr>
                        <b>Productos</b>
                        <div id="info-products-promotions"></div>
                        <div id="cols"></div>
                        <div class="text-center mt-3 mb-3">
                            <button type="button" id="add-products" class="btn btn-dark btn-sm"><i class="fas fa-plus"></i> Agregar producto</button>
                        </div>
                        @if(count($promotions)>0)
                            <hr>
                            <b>Promociones</b>
                            <div id="info-promotions"></div>
                            <div id="cols-promotions"></div>
                            <div class="text-center mt-3 mb-3">
                                <button type="button" id="add-promotions" class="btn btn-danger btn-sm"><i class="fas fa-plus"></i> Agregar promoción</button>
                            </div>
                        @endif
                        @if(count($delivery_companies)>0)
                            <hr>
                            <b>Delivery</b>
                            <div id="info-deliveries"></div>
                            <div id="cols-deliveries"></div>
                            <div class="text-center mt-3 mb-3">
                                <button type="button" id="add-deliveries" class="btn btn-info btn-sm"><i class="fas fa-plus"></i> Agregar delivery</button>
                            </div>
                        @endif
                        <hr>
                        <b>Información de la venta</b>
                        <div id="info-errors"></div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">              
                                    <small class="text-muted">Descripción de venta</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control-sm" id="sale_description" name="description" value="{{$sale->description}}" placeholder="Opcional">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Tipo de venta</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control form-control-sm" name="sale_type" id="sale_type">
                                            <option {{$sale->sale_type==1?'selected':''}} value="1">Al contado</option>
                                            <option {{$sale->sale_type==2?'selected':''}} value="2">A crédito</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Descuento en %</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control-sm" name="discount" id="discount" value="{{$sale->discount}}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">Monto total en $ de venta</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control-sm" name="total" id="total" readonly>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-dollar-sign"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Tasa del día de venta</small>
                                    <div class="input-group mb-3">
                                        <input name="exchange_rate" id="exchange_rate" type="text" class="form-control form-control-sm" placeholder="Precio en Bs.S" value="{{$configuration->exchange_rate}}" autocomplete="off" readonly required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas"><b>Bs.S</b></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">Monto total en Bs.S de venta</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control-sm" name="total_bss" id="total_bss" readonly>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas"><b>Bs.S</b></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <b>Métodos de pagos/cambios</b>
                        <div id="info-payments"></div>
                        <div id="cols-payment-methods"></div>
                        <div class="text-center mt-3 mb-3">
                            <button type="button" id="add-payment-methods" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> Agregar método de pago/cambio</button>
                        </div>
                        <button type="button" class="btn btn-primary btn-sale">Editar</button>

					  	<div class="modal fade" id="ticket">
			        		<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
					          	<div class="modal-content">
					            	<div class="modal-header">
						              	<h4 class="modal-title text-center">Ticket</h4>
					              		<button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
						                	<span aria-hidden="true">&times;</span>
						              	</button>
						            </div>
						            <div class="modal-body text-center">
						            	<div class="border">
						            		<p class="mt-0 mb-0">{{$configuration->name}}</p>
						            		<div id="ticket-content"></div>
						            	</div>
						            </div>
						            <div class="modal-footer justify-content-between">
						              	<button type="button" class="btn btn-danger" id="noTicket">Generar venta sin ticket</button>
						              	<button type="button" class="btn btn-success" id="btnImprimir">Generar venta con ticket</button>
						            </div>
					          	</div>
					        </div>
				      	</div>
	                    <div class="d-none">
	                        <h1>Demostrar capacidades de plugin de impresión</h1>
	                        <a href="//parzibyte.me/blog">By Parzibyte</a>
	                        <br>
	                        <a class="btn btn-danger btn-sm" href="../../index.html">Documentación</a>
	                    </div>
	                    <div class="d-none">

	                        <h2>Ajustes de impresora</h2>
	                        <strong>Nombre de impresora seleccionada: </strong>
	                        <p id="impresoraSeleccionada"></p>
	                        <div class="form-group">
	                            <select class="form-control" id="listaDeImpresoras"></select>
	                        </div>
	                        <button class="btn btn-primary btn-sm" id="btnRefrescarLista">Refrescar lista</button>
	                        <button class="btn btn-primary btn-sm" id="btnEstablecerImpresora">Establecer como predeterminada</button>
	                        <h2>Capacidades</h2>
	                        <p>Utiliza el siguiente botón para imprimir un recibo de prueba en la impresora predeterminada:</p>
	                    </div>
	                    <button class="d-none">Imprimir ticket</button>
	                    <div class="d-none">
	                        <h2>Log</h2>
	                        <button class="btn btn-warning btn-sm" id="btnLimpiarLog">Limpiar log</button>
	                        <pre id="estado"></pre>
	                    </div>

                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/printer.js?v=5')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom-printer.js?v=5')}}"></script>
    <script>
        $(document).ready(function() {

            $("#discount").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});
            $("#exchange_rate").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            $("#client-type").change(function () {
                var client_type=$(this).val();

                if(client_type==1) {
                    $(".select-client ").show();
                    $(".number-client").hide();
                    $(".first-name-client").hide();
                    $(".div-fields-client").hide();
                    $(".div-edit-client").show();
                }else {
                    $(".select-client ").hide();
                    $(".number-client").show();
                    $(".first-name-client").show();
                    $(".div-fields-client").show();
                    $(".div-edit-client").hide();
                }

            });

            $("#client_id").change(function () {
                var client_id=$(this).val();
        
                @foreach($clients as $client)
                    if(client_id=='{{$client->id}}') {
                        $("#edit_client_number").val('{{$client->client_number}}');
                        $("#edit_first_name").val('{{$client->first_name}}');
                        $("#edit_last_name").val('{{$client->last_name}}');
                        $("#edit_identity_card").val('{{$client->identity_card}}');
                        $("#edit_name_business_phone").val('{{$client->name_business_phone}}');
                        $("#edit_first_phone").val('{{$client->first_phone}}');
                        $("#edit_second_phone").val('{{$client->second_phone}}');
                    }
                @endforeach

            });

            var cols = $("#cols");
            var i=0;
            
            $("#add-products").click(function() {
                i++;
                var product_id='';
                var amount='1';
                var price='';
                var description='';
                var activate_scroll=1;
                addNew(i, product_id, amount, price, description, activate_scroll);
            });

            @foreach($sale->products as $product)
                i++;
                var product_id='{{$product->id}}';
                var amount='{{$product->pivot->amount}}';
                var price='{{$product->pivot->price}}';
                var description='{{$product->pivot->description}}';
                var activate_scroll=0;
                addNew(i, product_id, amount, price, description, activate_scroll);
            @endforeach

            function addNew(j, product_id, amount, price, description, activate_scroll) {

                var option=``;
                @foreach($products as $product)
                    if(product_id=="{{$product->id}}") {
                        option+=`<option @if($product->available<1) class="special-option" disabled @endif value="{{$product->id}}" selected data-subtext="| Precio: ${{number_format($product->price, 2, ',', '.')}} | Disponible: {{$product->available}}">{{$product->name}}</option>`;
                    }else {
                        option+=`<option @if($product->available<1) class="special-option" disabled @endif value="{{$product->id}}" data-subtext="| Precio: ${{number_format($product->price, 2, ',', '.')}} | Disponible: {{$product->available}}">{{$product->name}}</option>`;
                    }
                @endforeach

                cols.append(`
                    <div class="px-3 mb-0 rounded border" id="div-`+j+`">
                        <div class="color-palette text-right">
                            <button type="button" id="remove-div-`+j+`" class="btn btn-sm btn-warning mt-2"><i class="fa fa-trash"></i></button>
                        </div>
                        <div id="div-error-`+j+`"></div>
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6">
                                <small>* Producto</small>
                                <select class="selectpicker show-tick form-control form-control-sm" name="product_id[]" id="select-`+j+`" data-live-search="true" data-size="5" data-show-subtext="true" required>
                                    `+option+`
                                </select>
                                <small class="text-muted" id="raw-material-`+j+`"></small>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Cantidad</small>
                                <input name="products_amounts[]" id="amount-`+j+`" value="`+amount+`" type="text" class="form-control form-control-sm" placeholder="Cantidad" required>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Precio en $</small>
                                <input name="products_prices[]" id="prices-`+j+`" value="`+price+`" type="text" class="form-control form-control-sm" placeholder="Precio en $" readonly required>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 col-sm-6 col-md-6">
                                <small>Descripción opcional</small>
                                <input name="products_descriptions[]" value="`+description+`" class="form-control form-control-sm form-control form-control-sm-sm" placeholder="Ejemplo: El bacon extra seleccionado va en la pizza equis.">
                            </div>
                        </div>
                        <br>
                    </div>
                `);

                $("#remove-div-"+j).click(function(event) {
                    $("#div-"+j).remove();
                    generateTotal();
                });

                $("#select-"+j).selectpicker();

                $("#select-"+j).change(function () {
                    generateSubPrice($("#select-"+j), $("#amount-"+j), $("#prices-"+j), $("#raw-material-"+j), $("#div-error-"+j));
                });

                $("#amount-"+j).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "0"});

                $("#amount-"+j).keyup(function () {
                    generateSubPrice($("#select-"+j), $("#amount-"+j), $("#prices-"+j), $("#raw-material-"+j), $("#div-error-"+j));
                });

                $("#prices-"+j).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "0"});

                generateSubPrice($("#select-"+j), $("#amount-"+j), $("#prices-"+j), $("#raw-material-"+j), $("#div-error-"+j));

                if(activate_scroll==1) {
                    $('html,body').animate({
                        scrollTop: $("#div-"+j).offset().top-75
                    }, 500);                    
                }

            }

            function generateSubPrice(selector_a, selector_b, selector_c, selector_d, selector_e) {
                var product_id=selector_a.val();
                var name='';
                var price=0;
                var max=0;

                @foreach($products as $product)
                    if(product_id=="{{$product->id}}") {
                        name='{{$product->name}}';
                        price="{{$product->price}}";
                        max="{{$product->available}}";

                        var raw_material='';

                        if('{{$product->type}}'==2) {

                            @php 
                                $count_rm=count($product->raw_material->sortByDesc('pivot.amount'));
                                $add_rm=0;
                            @endphp

                            @foreach($product->raw_material->sortByDesc('pivot.amount') as $rm)

                                @php $add_rm++; @endphp

                                @if($add_rm==$count_rm)
                                    raw_material=raw_material+"{{$rm->name}}.";
                                @else
                                    raw_material=raw_material+"{{$rm->name}}, ";
                                @endif

                            @endforeach

                        }

                        if(raw_material.length>0) {
                            selector_d.text("Materia prima: "+raw_material);
                        }else {
                            selector_d.text(raw_material);
                        }

                    }
                @endforeach

                var re=new RegExp(escapeRegExp('.'),'g');
                var amount=selector_b.val().replace(re,'');
                var amount=amount.replace(/,/g, ".");
                amount=parseFloat(amount);

                if(amount>max) {
                    selector_e.empty();
                    selector_e.append(`
                        <div class="alert alert-danger alert-dismissible mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                            La cantidad máxima disponible del producto `+name+` es de `+max+`.
                        </div>
                    `);

                    $('html,body').animate({
                        scrollTop: selector_e.offset().top-75
                    }, 500);

                    selector_b.val(max);
                    generateSubPrice(selector_a, selector_b, selector_c, selector_d, selector_e);
                    return true;
                }

                selector_c.val(formatNumber((price*amount).toFixed(2)));

                generateTotal();
            }

            @if(count($promotions)>0)
                var cols_promotions = $("#cols-promotions");
                var k=0;
                
                $("#add-promotions").click(function() {
                    k++;
                    var promotion_id='';
                    var amount='1';
                    var price='';
                    var description='';
                    var activate_scroll=1;
                    addNewPromotions(k, promotion_id, amount, price, description, activate_scroll);
                });

                @foreach($sale->promotions as $promotion)
                    k++;
                    var promotion_id='{{$promotion->id}}';
                    var amount='{{$promotion->pivot->amount}}';
                    var price='{{$promotion->pivot->price}}';
                    var description='{{$promotion->pivot->description}}';
                    var activate_scroll=0;
                    addNewPromotions(k, promotion_id, amount, price, description, activate_scroll);
                @endforeach

                function addNewPromotions(l, promotion_id, amount, price, description, activate_scroll) {

                    var option=``;
                    @foreach($promotions as $promotion)
                        if(promotion_id=="{{$promotion->id}}") {
                            option+=`<option @if($promotion->available<1) class="special-option" disabled @endif value="{{$promotion->id}}" selected data-subtext="| Precio: ${{number_format($promotion->price, 2, ',', '.')}} | Disponible: {{$promotion->available}}">{{$promotion->name}}</option>`;
                        }else {
                            option+=`<option @if($promotion->available<1) class="special-option" disabled @endif value="{{$promotion->id}}" data-subtext="| Precio: ${{number_format($promotion->price, 2, ',', '.')}} | Disponible: {{$promotion->available}}">{{$promotion->name}}</option>`;
                        }
                    @endforeach

                    cols_promotions.append(`
                        <div class="px-3 mb-0 rounded border" id="div-promo-`+l+`">
                            <div class="color-palette text-right">
                                <button type="button" id="remove-div-promo-`+l+`" class="btn btn-sm btn-warning mt-2"><i class="fa fa-trash"></i></button>
                            </div>
                            <div id="div-error-promo-`+l+`"></div>
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">
                                    <small>* Promoción</small>
                                    <select class="selectpicker show-tick form-control form-control-sm" name="promotion_id[]" id="select-promo-`+l+`" data-live-search="true" data-size="5" data-show-subtext="true" required>
                                        `+option+`
                                    </select>
                                    <small class="text-muted" id="promo-products-`+l+`"></small>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">
                                    <small>* Cantidad</small>
                                    <input name="promotions_amounts[]" id="amount-promo-`+l+`" value="`+amount+`" type="text" class="form-control form-control-sm" placeholder="Cantidad" required>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">
                                    <small>* Precio en $</small>
                                    <input name="promotions_prices[]" id="prices-promo-`+l+`" value="`+price+`" type="text" class="form-control form-control-sm" placeholder="Precio en $" readonly required>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-12 col-sm-6 col-md-6">
                                    <small>Descripción opcional</small>
                                    <input name="promotions_descriptions[]" value="`+description+`" class="form-control form-control-sm form-control form-control-sm-sm" placeholder="Ejemplo: Una de las 3 pizzas de la promoción va sin cebolla.">
                                </div>
                            </div>
                            <br>
                        </div>
                    `);

                    $("#remove-div-promo-"+l).click(function(event) {
                        $("#div-promo-"+l).remove();
                        generateTotal();
                    });

                    $("#select-promo-"+l).selectpicker();

                    $("#select-promo-"+l).change(function () {
                        generateSubPromotionPrice($("#select-promo-"+l), $("#amount-promo-"+l), $("#prices-promo-"+l), $("#promo-products-"+l), $("#div-error-promo-"+l));
                    });

                    $("#amount-promo-"+l).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "0"});

                    $("#amount-promo-"+l).keyup(function () {
                        generateSubPromotionPrice($("#select-promo-"+l), $("#amount-promo-"+l), $("#prices-promo-"+l), $("#promo-products-"+l), $("#div-error-promo-"+l));
                    });

                    $("#prices-promo-"+l).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "0"});

                    generateSubPromotionPrice($("#select-promo-"+l), $("#amount-promo-"+l), $("#prices-promo-"+l), $("#promo-products-"+l), $("#div-error-promo-"+l));

                    if(activate_scroll==1) {
                        $('html,body').animate({
                            scrollTop: $("#div-promo-"+l).offset().top-75
                        }, 500);                        
                    }

                }

                function generateSubPromotionPrice(selector_a, selector_b, selector_c, selector_d, selector_e) {
                    var promotion_id=selector_a.val();
                    var name='';
                    var price=0;
                    var max=0;

                    var products='';

                    @foreach($promotions as $promotion)
                        if(promotion_id=="{{$promotion->id}}") {
                            name='{{$promotion->name}}';
                            price="{{$promotion->price}}";
                            max="{{$promotion->available}}";

                            @php 
                                $count_prod=count($promotion->products);
                                $add_prod=0;
                            @endphp

                            @foreach($promotion->products->sortByDesc('pivot.amount') as $product)

                                @php $add_prod++; @endphp

                                @if($add_prod==$count_prod)
                                    products=products+"{{$product->name}}"+": "+"{{$product->pivot->amount}}.";
                                @else
                                    products=products+"{{$product->name}}"+": "+"{{$product->pivot->amount}}, ";
                                @endif

                            @endforeach
                            selector_d.text("Productos: "+products);
                        }
                    @endforeach

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var amount=selector_b.val().replace(re,'');
                    var amount=amount.replace(/,/g, ".");
                    amount=parseFloat(amount);

                    if(amount>max) {
                        selector_e.empty();
                        selector_e.append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                La cantidad máxima disponible de la promoción `+name+` es de `+max+`.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: selector_e.offset().top-75
                        }, 500);

                        selector_b.val(max);
                        generateSubPromotionPrice(selector_a, selector_b, selector_c, selector_d, selector_e);
                        return true;
                    }

                    selector_c.val(formatNumber((price*amount).toFixed(2)));

                    generateTotal();
                }
            @endif

            @if(count($delivery_companies)>0)
                var cols_deliveries = $("#cols-deliveries");
                var e=0;

                $("#add-deliveries").click(function() {
                    e++;
                    var delivery_company_id='';
                    var delivery_type_id='';
                    var delivery_company_driver_id='';
                    var service_quantity='1';
                    var activate_scroll=0;
                    addNewDeliveries(e, delivery_company_id, delivery_type_id, delivery_company_driver_id, service_quantity, activate_scroll);
                });

                @foreach($sale->delivery_sales as $delivery_sale)
                    e++;
                    var delivery_company_id='{{$delivery_sale->delivery_company_id}}';
                    var delivery_type_id='{{$delivery_sale->delivery_type_id}}';
                    var delivery_company_driver_id='{{$delivery_sale->delivery_company_driver_id}}';
                    var service_quantity='{{$delivery_sale->amount}}';
                    var activate_scroll=0;
                    addNewDeliveries(e, delivery_company_id, delivery_type_id, delivery_company_driver_id, service_quantity, activate_scroll);
                @endforeach

                function addNewDeliveries(f, delivery_company_id, delivery_type_id, delivery_company_driver_id, service_quantity, activate_scroll) {

                    var option_delivery_company_id=``;
                    @foreach($delivery_companies as $delivery_company)
                        if(delivery_company_id=="{{$delivery_company->id}}") {
                            option_delivery_company_id+=`<option selected value="{{$delivery_company->id}}">{{$delivery_company->name}}</option>`;
                        }else {
                            option_delivery_company_id+=`<option value="{{$delivery_company->id}}">{{$delivery_company->name}}</option>`;
                        }
                    @endforeach

                    cols_deliveries.append(`
                        <div class="px-3 mb-0 rounded border" id="div-delivery-`+f+`">
                            <div class="color-palette text-right">
                                <button type="button" id="remove-div-delivery-`+f+`" class="btn btn-sm btn-warning mt-2"><i class="fa fa-trash"></i></button>
                            </div>
                            <div id="div-error-delivery-`+f+`"></div>
                            <div class="row">
                                <div class="col-12 col-sm-3 col-md-3">
                                    <small>* Empresa</small>
                                    <select class="form-control form-control-sm form-control form-control-sm-sm" id="delivery-company-id-`+f+`" name="delivery_company_id[]" required>
                                            `+option_delivery_company_id+`
                                    </select>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">
                                    <small>* Tipo</small>
                                    <select class="form-control form-control-sm form-control form-control-sm-sm" id="delivery-type-id-`+f+`" name="delivery_type_id[]" required>
                                        <option value="NULL">N/A</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">
                                    <small class="text-muted">* Conductor</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control form-control-sm form-control form-control-sm-sm" id="delivery-driver-id-`+f+`" name="delivery_company_driver_id[]" required>
                                            <option value="NULL">N/A</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">
                                    <small>* Cantidad del servicio</small>
                                    <input class="form-control form-control-sm form-control form-control-sm-sm" id="amount-delivery-`+f+`" name="services_quantity[]" placeholder="Cantidad" value="`+service_quantity+`" required>
                                </div>
                            </div>
                            <br>
                        </div>
                    `);

                    $("#remove-div-delivery-"+f).click(function(event) {
                        $("#div-delivery-"+f).remove();
                        generateTotal();
                    });

                    $("#delivery-company-id-"+f).change(function () {
                        getDeliveryTypes($("#delivery-company-id-"+f), $("#delivery-type-id-"+f), delivery_type_id);
                        getDeliveryCompanyDrivers($("#delivery-company-id-"+f), $("#delivery-driver-id-"+f), delivery_company_driver_id);
                        generateTotal();
                    });

                    $("#delivery-type-id-"+f).change(function () {
                        generateTotal();
                    });

                    $("#amount-delivery-"+f).keyup(function () {
                        generateTotal();
                    });

                    $("#amount-delivery-"+f).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "0"});
                    
                    getDeliveryTypes($("#delivery-company-id-"+f), $("#delivery-type-id-"+f), delivery_type_id);
                    getDeliveryCompanyDrivers($("#delivery-company-id-"+f), $("#delivery-driver-id-"+f), delivery_company_driver_id);
                    generateTotal();

                    if(activate_scroll==1) {
                        $('html,body').animate({
                            scrollTop: $("#div-delivery-"+f).offset().top-75
                        }, 500);
                    }
                }

            @endif

            function getDeliveryCompanyDrivers(selector_a, selector_b, delivery_company_driver_id) {

                selector_b.empty();

                if(selector_a.val()!="NULL") {

                    var route="/sales/delivery_company_drivers/"+selector_a.val();
                    $.get(route,{"_token":"{{csrf_token()}}"}, 
                    function(data) {

                        var delivery_company_drivers=data.delivery_company_drivers;

                        selector_b.append('<option value="NULL">N/A</option>');

                        if(delivery_company_drivers!=null) {
                            
                            $.each(delivery_company_drivers,function(key, value) {

                                if(delivery_company_driver_id==value.id)
                                    selector_b.append('<option selected value="'+value.id+'">'+value.name+'</option>');
                                else
                                    selector_b.append('<option value="'+value.id+'">'+value.name+'</option>');

                            });

                        }

                    });

                }else {
                    selector_b.append('<option value="NULL">N/A</option>');
                }
                
            }

            function getDeliveryTypes(selector_a, selector_b, delivery_type_id) {

                selector_b.empty();

                if(selector_a.val()!="NULL") {

                    var route="/sales/delivery_types/"+selector_a.val();
                    $.get(route,{"_token":"{{csrf_token()}}"}, 
                    function(data) {

                        var delivery_types=data.delivery_types;

                        selector_b.append('<option value="NULL">N/A</option>');

                        if(delivery_types!=null) {
                            $.each(delivery_types,function(key, value) {
                                
                                if(delivery_type_id==value.id) {
                                    selector_b.append('<option selected value="'+value.id+'">'+value.name+': '+value.price+'$</option>');

                                    var products_prices=document.getElementsByName('products_prices[]');
                                    var promotions_prices=document.getElementsByName('promotions_prices[]');
                                    var re=new RegExp(escapeRegExp('.'),'g');
                                    var total=0;

                                    for(p=0; p<products_prices.length; p++) {

                                        var price=products_prices[p].value.replace(re,'');
                                        price=price.replace(/,/g, ".");
                                        price=parseFloat(price);
                                        total+=price;

                                    }

                                    for(q=0; q<promotions_prices.length; q++) {

                                        var price=promotions_prices[q].value.replace(re,'');
                                        price=price.replace(/,/g, ".");
                                        price=parseFloat(price);
                                        total+=price;

                                    }

                                    var discount=$('#discount').val().replace(re,'');
                                    var discount=discount.replace(/,/g, ".");
                                    discount=parseFloat(discount);

                                    // Deliveries
                                    var delivery_price=0;
                                    var delivery_type_ids=document.getElementsByName('delivery_type_id[]');
                                    var services_quantity=document.getElementsByName('services_quantity[]');

                                    for(g=0; g<delivery_type_ids.length; g++) {
                                        @foreach($delivery_types as $delivery_type)
                                            if(delivery_type_ids[g].value=='{{$delivery_type->id}}') {
                                                var service_quantity=services_quantity[g].value.replace(re,'');
                                                var service_quantity=service_quantity.replace(/,/g, ".");
                                                service_quantity=parseFloat(service_quantity);
                                                delivery_price+=parseFloat('{{$delivery_type->price}}'*service_quantity);
                                            }
                                        @endforeach
                                    }

                                    total+=delivery_price;
                                    total=parseFloat((total-(total*(discount/100))).toFixed(2));

                                    var exchange_rate=$('#exchange_rate').val().replace(re,'');
                                    var exchange_rate=exchange_rate.replace(/,/g, ".");
                                    exchange_rate=parseFloat(exchange_rate);

                                    var total_bss=total*exchange_rate;
                                    total_bss=formatNumber(total_bss.toFixed(2));
                                    $("#total_bss").val(total_bss);

                                    $("#total").val(formatNumber(total.toFixed(2)));

                                }else {
                                    selector_b.append('<option value="'+value.id+'">'+value.name+': '+value.price+'$</option>');
                                }

                            });
                        }

                    });

                }else {
                    selector_b.append('<option value="NULL">N/A</option>');
                }
                
            }

            $("#delivery_type_id").change(function () {
                generateTotal();
            });

            generateTotal();

            function escapeRegExp(string) {
                return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); 
            }

            function formatNumber(num) {
                return num.toString().replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1,$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
            }

            function generateTotal() {
                var products_prices=document.getElementsByName('products_prices[]');
                var promotions_prices=document.getElementsByName('promotions_prices[]');
                var re=new RegExp(escapeRegExp('.'),'g');
                var total=0;

                for(p=0; p<products_prices.length; p++) {

                    var price=products_prices[p].value.replace(re,'');
                    price=price.replace(/,/g, ".");
                    price=parseFloat(price);
                    total+=price;

                }

                for(q=0; q<promotions_prices.length; q++) {

                    var price=promotions_prices[q].value.replace(re,'');
                    price=price.replace(/,/g, ".");
                    price=parseFloat(price);
                    total+=price;

                }

                var discount=$('#discount').val().replace(re,'');
                var discount=discount.replace(/,/g, ".");
                discount=parseFloat(discount);

                // Deliveries
                var delivery_price=0;
                var delivery_type_ids=document.getElementsByName('delivery_type_id[]');
                var services_quantity=document.getElementsByName('services_quantity[]');

                for(g=0; g<delivery_type_ids.length; g++) {
                    @foreach($delivery_types as $delivery_type)
                        if(delivery_type_ids[g].value=='{{$delivery_type->id}}') {
                            var service_quantity=services_quantity[g].value.replace(re,'');
                            var service_quantity=service_quantity.replace(/,/g, ".");
                            service_quantity=parseFloat(service_quantity);
                            delivery_price+=parseFloat('{{$delivery_type->price}}'*service_quantity);
                        }
                    @endforeach
                }

                total+=delivery_price;
                total=parseFloat((total-(total*(discount/100))).toFixed(2));

                var exchange_rate=$('#exchange_rate').val().replace(re,'');
                var exchange_rate=exchange_rate.replace(/,/g, ".");
                exchange_rate=parseFloat(exchange_rate);

                var total_bss=total*exchange_rate;
                total_bss=formatNumber(total_bss.toFixed(2));
                $("#total_bss").val(total_bss);

                $("#total").val(formatNumber(total.toFixed(2)));
            }

            $('#discount').keyup(function () {
                var re=new RegExp(escapeRegExp('.'),'g');
                var discount=$('#discount').val().replace(re,'');
                var discount=discount.replace(/,/g, ".");
                discount=parseFloat(discount);

                if(discount>100) {
                    $('#info-errors').empty();
                    $('#info-errors').append(`
                        <div class="alert alert-danger alert-dismissible mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                            El descuento máximo es de 100%.
                        </div>
                    `);

                    $('html,body').animate({
                        scrollTop: $('#info-errors').offset().top-85
                    }, 500);

                    $('#discount').val(100);
                }

                generateTotal();
            });

            $('#exchange_rate').keyup(function () {
                var re=new RegExp(escapeRegExp('.'),'g');
                var exchange_rate=$(this).val().replace(re,'');
                var exchange_rate=exchange_rate.replace(/,/g, ".");
                exchange_rate=parseFloat(exchange_rate);

                var total=$('#total').val().replace(re,'');
                var total=total.replace(/,/g, ".");
                total=parseFloat(total);
                total*=exchange_rate;
                total=formatNumber(total.toFixed(2));
                $("#total_bss").val(total);
            });

            var cols_payment_methods = $("#cols-payment-methods");
            var x=0;
            
            $("#add-payment-methods").click(function() {
                x++;
                var payment_method_id="";
                var payment_method_option_id="";
                var payment_amount_usd="0";
                var payment_amount_bss="0";
                var payment_exchange_rate="";
                var payment_reference_number="";
                var payment_description="";
                var change_method_id="";
                var change_method_option_id="";
                var change_amount_usd="0";
                var change_amount_bss="0";
                var change_exchange_rate="";
                var change_reference_number="";
                var change_description="";
                var pay_the_customer=0;
                var activate_scroll=1;
                addNewPaymentMethods(x, payment_method_id, payment_method_option_id, payment_amount_usd, payment_amount_bss, payment_exchange_rate, payment_reference_number, payment_description, change_method_id, change_method_option_id, change_amount_usd, change_amount_bss, change_exchange_rate, change_reference_number, change_description, pay_the_customer, activate_scroll);
            });

            @foreach($sale->payments as $payment)
                x++;
                var payment_method_id="{{$payment->payment_method_id}}";
                var payment_method_option_id="{{$payment->payment_method_option_id}}";
                var payment_amount_usd="{{$payment->payment_amount_usd}}";
                var payment_amount_bss="{{$payment->payment_amount_bss}}";
                var payment_exchange_rate="{{$payment->payment_exchange_rate}}";
                var payment_reference_number="{{$payment->payment_reference_number}}";
                var payment_description="{{$payment->payment_description}}";
                var change_method_id="{{$payment->change_method_id}}";
                var change_method_option_id="{{$payment->change_method_option_id}}";
                var change_amount_usd="{{$payment->change_amount_usd}}";
                var change_amount_bss="{{$payment->change_amount_bss}}";
                var change_exchange_rate="{{$payment->change_exchange_rate}}";
                var change_reference_number="{{$payment->change_reference_number}}";
                var change_description="{{$payment->change_description}}";
                var pay_the_customer="{{$payment->pay_the_customer}}";
                var activate_scroll=0;
                addNewPaymentMethods(x, payment_method_id, payment_method_option_id, payment_amount_usd, payment_amount_bss, payment_exchange_rate, payment_reference_number, payment_description, change_method_id, change_method_option_id, change_amount_usd, change_amount_bss, change_exchange_rate, change_reference_number, change_description, pay_the_customer, activate_scroll);
            @endforeach

            function addNewPaymentMethods(x, payment_method_id, payment_method_option_id, payment_amount_usd, payment_amount_bss, payment_exchange_rate, payment_reference_number, payment_description, change_method_id, change_method_option_id, change_amount_usd, change_amount_bss, change_exchange_rate, change_reference_number, change_description, pay_the_customer, activate_scroll) {

                var option=``;
                @foreach($payment_methods as $payment_method)
                    if(payment_method_id=="{{$payment_method->id}}") {
                        option+=`<option selected value="{{$payment_method->id}}">{{$payment_method->name}}</option>`;
                    }else {
                        option+=`<option value="{{$payment_method->id}}">{{$payment_method->name}}</option>`;
                    }
                @endforeach

                var change_option=``;
                @foreach($payment_methods as $payment_method)
                    if(change_method_id=="{{$payment_method->id}}") {
                        change_option+=`<option selected value="{{$payment_method->id}}">{{$payment_method->name}}</option>`;
                    }else {
                        change_option+=`<option value="{{$payment_method->id}}">{{$payment_method->name}}</option>`;
                    }
                @endforeach

                cols_payment_methods.append(`

                    <div class="px-3 mb-5 rounded border" id="div-pay-`+x+`">
                        <div class="color-palette text-right">
                            <button type="button" id="remove-div-pay-`+x+`" class="btn btn-sm btn-warning mt-2"><i class="fa fa-trash"></i></button>
                        </div>
                        <b>Pago del cliente</b>
                        <div class="row">
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Método del pago</small>
                                <select id="select-pay-`+x+`" class="form-control form-control-sm" name="payment_method_id[]" required>
                                    `+option+`
                                </select>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Banco o procesador del pago</small>
                                <select id="select-pay-option-`+x+`" class="form-control form-control-sm" name="payment_method_option_id[]" required>
                                </select>
                                <small class="text-muted" id="text-info-`+x+`"></small>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Monto en $ del pago</small>
                                <div class="input-group">
                                    <input id="amount-usd-`+x+`" type="text" class="form-control form-control-sm" placeholder="Monto en $ del pago" name="payment_amount_usd[]" value="`+payment_amount_usd+`" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-dollar-sign"></span>
                                        </div>
                                    </div>
                                </div>
                                <small style="color: red !important; font-weight: bold !important;" id="text-com-extra-`+x+`"></small>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Monto en Bs.S del pago</small>
                                <div class="input-group mb-3">
                                    <input id="amount-bss-`+x+`" type="text" class="form-control form-control-sm" placeholder="Monto en Bs.S del pago" name="payment_amount_bss[]" value="`+payment_amount_bss+`" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas"><b>Bs.S</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Tasa del pago</small>
                                <div class="input-group mb-3">
                                    <input id="pay-exchange-rate-`+x+`" type="text" class="form-control form-control-sm" placeholder="Tasa del pago" value="{{$configuration->exchange_rate}}" name="payment_exchange_rate[]" autocomplete="off" readonly required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas"><b>Bs.S</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>Nro. de referencia del pago</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control form-control-sm" name="payment_reference_number[]" value="`+payment_reference_number+`" placeholder="Opcional">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <small>Descripción del pago</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control form-control-sm" name="payment_descriptions[]" value="`+payment_description+`" placeholder="Opcional">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <b>Cambio o vuelto del vendedor</b>
                        <br><small class="text-muted">Si el pago no necesita un cambio o vuelto, deja el monto en $ y en Bs.S en 0,00.</small>
                        <div class="row">
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Método del cambio</small>
                                <select id="select-change-`+x+`" class="form-control form-control-sm" name="change_method_id[]" required>
                                     `+change_option+`
                                </select>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Banco o procesador del cambio</small>
                                <select id="select-change-option-`+x+`" class="form-control form-control-sm" name="change_method_option_id[]" required>
                                </select>
                                <small class="text-muted" id="change-info-`+x+`">Hola</small>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Monto en $ del cambio</small>
                                <div class="input-group">
                                    <input id="change-usd-`+x+`" type="text" class="form-control form-control-sm" placeholder="Monto en $ del cambio" name="change_amount_usd[]" value="`+change_amount_usd+`" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-dollar-sign"></span>
                                        </div>
                                    </div>
                                </div>
                                <small style="color: red !important; font-weight: bold !important;" id="change-com-extra-`+x+`"></small>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Monto en Bs.S del cambio</small>
                                <div class="input-group mb-3">
                                    <input id="change-bss-`+x+`" type="text" class="form-control form-control-sm" placeholder="Monto en Bs.S del cambio" name="change_amount_bss[]" value="`+change_amount_bss+`" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas"><b>Bs.S</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Tasa del cambio</small>
                                <div class="input-group mb-3">
                                    <input id="change-exchange-rate-`+x+`" type="text" class="form-control form-control-sm" placeholder="Tasa del cambio" value="{{$configuration->exchange_rate}}" name="change_exchange_rate[]" autocomplete="off" readonly required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas"><b>Bs.S</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>Nro. de referencia del cambio</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control form-control-sm" name="change_reference_number[]" value="`+change_reference_number+`" placeholder="Opcional">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <small>Descripción del cambio</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control form-control-sm" name="change_descriptions[]" value="`+change_description+`" placeholder="Opcional">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="pay_the_customers[]" type="hidden" value="`+pay_the_customer+`" id="pay-credit-`+x+`">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="0" id="check-`+x+`">
                            <label class="form-check-label" for="check-`+x+`">
                                Abonar al cliente
                            </label>
                        </div>
                        <br>
                    </div>

                    <div class="modal fade" id="unlock-credit-`+x+`" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title secondary_color">Abonar al cliente</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p class="text-center"><small>¡Ingresa el código de desbloqueo para abonar el cambio o vuelto!</small></p>
                                            <input id="credit-unlock-code-`+x+`" type="password" class="form-control form-control-sm" placeholder="Código de desbloqueo"/>
                                            <div class="text-center">
                                                <small id="info-code-`+x+`" style="color: red !important; font-weight: bold !important;"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="btn-credit-`+x+`" type="button" class="btn btn-success">Abonar</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                `);
                
                var verify_check=false;

                if(pay_the_customer==1) {
                    verify_check=true;
                    $("#check-"+x).prop("checked", true);
                }

                $("#check-"+x).change(function () {

                    $("#info-code-"+x).text('');

                    if(!verify_check) {
                        $("#check-"+x).prop("checked", false);
                        $("#credit-unlock-code-"+x).val("");
                        $("#unlock-credit-"+x).modal('show');
                    }else {

                        if($("#check-"+x).prop("checked")) {
                            $("#check-"+x).val(1)
                            $("#pay-credit-"+x).val(1);
                        }else {
                            $("#check-"+x).val(0);
                            $("#pay-credit-"+x).val(0);
                        }

                    }

                });

                $("#btn-credit-"+x).click(function(event) {
                    
                    $("#info-code-"+x).text('');
                    
                    var credit_unlock_code=$("#credit-unlock-code-"+x).val();

                    if(credit_unlock_code.length==0) {

                        $("#info-code-"+x).text("Ingresa el código de desbloqueo...");
                        $("#check-"+x).prop("checked", false); 

                    }else if(credit_unlock_code!="{{$configuration->unlock_code}}") {

                        $("#info-code-"+x).text("Código de desbloqueo incorrecto...");
                        $("#check-"+x).prop("checked", false);

                    }else if(credit_unlock_code=="{{$configuration->unlock_code}}") {
                        
                        $("#check-"+x).val(1)
                        $("#pay-credit-"+x).val(1);
                        verify_check=true;
                        $("#credit-unlock-code-"+x).val("");
                        $("#unlock-credit-"+x).modal('hide');
                        $("#check-"+x).prop("checked", true);

                    }

                });

                $("#remove-div-pay-"+x).click(function(event) {
                    $("#div-pay-"+x).remove();
                    $("#unlock-credit-"+x).remove();
                });

                // PAYMENTS FUNCTIONS
                $("#select-pay-"+x).change(function () {
                    $("#text-com-extra-"+x).text('');

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var amount=$("#amount-usd-"+x).val().replace(re,'');
                    var amount=amount.replace(/,/g, ".");
                    amount=parseFloat(amount);
                    getPaymentMethodOptions($("#select-pay-"+x), $("#select-pay-option-"+x), $("#text-info-"+x), $("#text-com-extra-"+x), amount, payment_method_option_id);
                });

                $("#amount-usd-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                $("#amount-bss-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                $("#pay-exchange-rate-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                var re=new RegExp(escapeRegExp('.'),'g');
                var amount=$("#amount-usd-"+x).val().replace(re,'');
                var amount=amount.replace(/,/g, ".");
                amount=parseFloat(amount);
                getPaymentMethodOptions($("#select-pay-"+x), $("#select-pay-option-"+x), $("#text-info-"+x), $("#text-com-extra-"+x), amount, payment_method_option_id);

                $("#select-pay-option-"+x).change(function () {
                    $("#text-com-extra-"+x).text('');

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var amount=$("#amount-usd-"+x).val().replace(re,'');
                    var amount=amount.replace(/,/g, ".");
                    amount=parseFloat(amount);
                    generateComissionExtraAmount($("#select-pay-option-"+x), $("#text-info-"+x), $("#text-com-extra-"+x), amount);
                });

                // CHANGES FUNCTIONS
                $("#select-change-"+x).change(function () {
                    $("#change-com-extra-"+x).text('');

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var change=$("#change-usd-"+x).val().replace(re,'');
                    var change=change.replace(/,/g, ".");
                    change=parseFloat(change);
                    getPaymentMethodOptions($("#select-change-"+x), $("#select-change-option-"+x), $("#change-info-"+x), $("#change-com-extra-"+x), change, change_method_option_id);
                });

                $("#change-usd-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                $("#change-bss-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                $("#change-exchange-rate-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                var re=new RegExp(escapeRegExp('.'),'g');
                var change=$("#change-usd-"+x).val().replace(re,'');
                var change=change.replace(/,/g, ".");
                change=parseFloat(change);
                getPaymentMethodOptions($("#select-change-"+x), $("#select-change-option-"+x), $("#change-info-"+x), $("#change-com-extra-"+x), change, change_method_option_id);

                $("#select-change-option-"+x).change(function () {
                    $("#change-com-extra-"+x).text('');

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var change=$("#change-usd-"+x).val().replace(re,'');
                    var change=change.replace(/,/g, ".");
                    change=parseFloat(change);
                    generateComissionExtraAmount($("#select-change-option-"+x), $("#change-info-"+x), $("#change-com-extra-"+x), change);
                });

                // AMOUNTS AND CHANGES KEYUP
                $("#amount-usd-"+x).keyup(function () {

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var exchange_rate=$("#pay-exchange-rate-"+x).val().replace(re,'');
                    var exchange_rate=exchange_rate.replace(/,/g, ".");
                    exchange_rate=parseFloat(exchange_rate);

                    var amount=$(this).val().replace(re,'');
                    var amount=amount.replace(/,/g, ".");
                    amount=parseFloat(amount);

                    generateComissionExtraAmount($("#select-pay-option-"+x), $("#text-info-"+x), $("#text-com-extra-"+x), amount);
                    
                    amount*=exchange_rate;
                    amount=formatNumber(amount.toFixed(2));
                    $("#amount-bss-"+x).val(amount);
                });

                $("#amount-bss-"+x).keyup(function () {
                    var re=new RegExp(escapeRegExp('.'),'g');
                    var exchange_rate=$("#pay-exchange-rate-"+x).val().replace(re,'');
                    var exchange_rate=exchange_rate.replace(/,/g, ".");
                    exchange_rate=parseFloat(exchange_rate);
                    
                    var amount_bss=$(this).val().replace(re,'');
                    var amount_bss=amount_bss.replace(/,/g, ".");
                    amount_bss=parseFloat(amount_bss);
                    amount_bss/=exchange_rate;

                    generateComissionExtraAmount($("#select-pay-option-"+x), $("#text-info-"+x), $("#text-com-extra-"+x), amount_bss);

                    amount_bss=formatNumber(amount_bss.toFixed(2));
                    $("#amount-usd-"+x).val(amount_bss);
                });

                $("#change-usd-"+x).keyup(function () {
                    var re=new RegExp(escapeRegExp('.'),'g');
                    var exchange_rate=$("#change-exchange-rate-"+x).val().replace(re,'');
                    var exchange_rate=exchange_rate.replace(/,/g, ".");
                    exchange_rate=parseFloat(exchange_rate);

                    var change=$(this).val().replace(re,'');
                    var change=change.replace(/,/g, ".");
                    change=parseFloat(change);

                    generateComissionExtraAmount($("#select-change-option-"+x), $("#change-info-"+x), $("#change-com-extra-"+x), change);

                    change*=exchange_rate;
                    change=formatNumber(change.toFixed(2));
                    $("#change-bss-"+x).val(change);
                });

                $("#change-bss-"+x).keyup(function () {
                    var re=new RegExp(escapeRegExp('.'),'g');
                    var exchange_rate=$("#change-exchange-rate-"+x).val().replace(re,'');
                    var exchange_rate=exchange_rate.replace(/,/g, ".");
                    exchange_rate=parseFloat(exchange_rate);
                    
                    var change_bss=$(this).val().replace(re,'');
                    var change_bss=change_bss.replace(/,/g, ".");
                    change_bss=parseFloat(change_bss);
                    change_bss/=exchange_rate;

                    generateComissionExtraAmount($("#select-change-option-"+x), $("#change-info-"+x), $("#change-com-extra-"+x), change_bss);

                    change_bss=formatNumber(change_bss.toFixed(2));
                    $("#change-usd-"+x).val(change_bss);
                });

                if(activate_scroll==1) {
                    $('html,body').animate({
                        scrollTop: $("#div-pay-"+x).offset().top-75
                    }, 500);
                }

            }

            function generateComissionExtraAmount(selector_a, selector_b, selector_c, amount) {
                var select_pay_option=selector_a.val();
                var payment_method_option_name='';
                var commission=parseFloat(0);
                var extra_amount=parseFloat(0);

                @foreach($payment_method_options as $payment_method_option)
                    
                    if(select_pay_option=='{{$payment_method_option->id}}') {
                        payment_method_option_name='{{$payment_method_option->name}}';
                        commission='{{$payment_method_option->commission}}';
                        extra_amount='{{$payment_method_option->extra_amount}}';
                    }

                @endforeach

                var new_comission=parseFloat(commission);
                var new_extra_amount=parseFloat(extra_amount);

                if(new_comission>0 || new_extra_amount>0) {
                    generateComExtraFinal(new_comission, new_extra_amount, amount, selector_c);
                }

                selector_b.text("Comisión: "+formatNumber(new_comission.toFixed(2))+"% | Monto extra: "+formatNumber(new_extra_amount.toFixed(2)));
            }

            function getPaymentMethodOptions(selector_a, selector_b, selector_c, selector_d, amount, method_option_id) {

                selector_b.empty();

                var route="/sales/payment_method_options/"+selector_a.val();
                $.get(route,{"_token":"{{csrf_token()}}"}, 
                function(data) {

                    var payment_method_options=data.payment_method_options;

                    if(payment_method_options!=null) {
                        
                        $.each(payment_method_options,function(key, value) {

                            if(method_option_id==value.id)
                                selector_b.append('<option selected value="'+value.id+'">'+value.name+'</option>');
                            else
                                selector_b.append('<option value="'+value.id+'">'+value.name+'</option>');  

                        });

                    }

                    if(payment_method_options.length===0) {
                        selector_b.append('<option value="NULL">N/A</option>');
                    }

                    generateComissionExtraAmount(selector_b, selector_c, selector_d, amount);
                });
                
            }

            function generateComExtraFinal(commission, extra, amount, selector_a) {
                var final_amount=(amount+extra)/(1-(commission/100));
                selector_a.text("Monto neto: "+formatNumber(final_amount.toFixed(2))+"$");
            }

            $(".btn-sale").click(function () {

                var count_clients="{{count($clients)}}";
                var client_type=$("#client-type").val();

                if(count_clients==0 || client_type==2) {

                    if($("#client_number").val().length==0) {

                        $('#info-seller-client').empty();
                        $('#info-seller-client').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El número de cliente es un campo obligatorio.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-seller-client').offset().top-85
                        }, 500);

                        return true;
                    }

                    if($("#first_name").val().length==0) {

                        $('#info-seller-client').empty();
                        $('#info-seller-client').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El nombre del cliente es un campo obligatorio.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-seller-client').offset().top-85
                        }, 500);

                        return true;
                    }

                    if($("#last_name").val().length==0) {

                        $('#info-seller-client').empty();
                        $('#info-seller-client').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El apellido del cliente es un campo obligatorio.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-seller-client').offset().top-85
                        }, 500);

                        return true;
                    }

                    if($("#name_business_phone").val().length==0) {

                        $('#info-seller-client').empty();
                        $('#info-seller-client').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El nombre del cliente registrado en el tlf del negocio es un campo obligatorio.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-seller-client').offset().top-85
                        }, 500);

                        return true;
                    }

                    if($("#first_phone").val().length==0) {

                        $('#info-seller-client').empty();
                        $('#info-seller-client').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El teléfono #1 del cliente es un campo obligatorio.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-seller-client').offset().top-85
                        }, 500);

                        return true;
                    }

                }else if(client_type==1) {

                    if($("#edit_client_number").val().length==0) {
                        
                        $('#info-seller-client').empty();
                        $('#info-seller-client').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El número de cliente es un campo obligatorio.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-seller-client').offset().top-85
                        }, 500);

                        return true;
                    }

                    if($("#edit_first_name").val().length==0) {

                        $('#info-seller-client').empty();
                        $('#info-seller-client').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El nombre del cliente es un campo obligatorio.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-seller-client').offset().top-85
                        }, 500);

                        return true;
                    }

                    if($("#edit_last_name").val().length==0) {

                        $('#info-seller-client').empty();
                        $('#info-seller-client').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El apellido del cliente es un campo obligatorio.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-seller-client').offset().top-85
                        }, 500);

                        return true;
                    }

                    if($("#edit_name_business_phone").val().length==0) {

                        $('#info-seller-client').empty();
                        $('#info-seller-client').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El nombre del cliente registrado en el tlf del negocio es un campo obligatorio.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-seller-client').offset().top-85
                        }, 500);

                        return true;
                    }

                    if($("#edit_first_phone").val().length==0) {

                        $('#info-seller-client').empty();
                        $('#info-seller-client').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El teléfono #1 del cliente es un campo obligatorio.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-seller-client').offset().top-85
                        }, 500);

                        return true;
                    }

                }
                
                var product_id=document.getElementsByName('product_id[]');
                var products_amounts=document.getElementsByName('products_amounts[]');

                var promotion_id=document.getElementsByName('promotion_id[]');
                var promotions_amounts=document.getElementsByName('promotions_amounts[]');

                if(products_amounts.length==0 && promotions_amounts.length==0) {

                    $('#info-products-promotions').empty();
                    $('#info-products-promotions').append(`
                        <div class="alert alert-danger alert-dismissible mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                            Debes agregar como mínimo un producto o una promoción para generar una venta.
                        </div>
                    `);

                    $('html,body').animate({
                        scrollTop: $('#info-products-promotions').offset().top-85
                    }, 500);

                    return true;
                }

                var verify=true;
                var verify_products=true;
                var check_products=[];
                var verify_promotions=true;
                var check_promotions=[];

                for(t=0; t<products_amounts.length; t++) {

                    if($.inArray(product_id[t].value, check_products)==-1) {
                        check_products.push(product_id[t].value);
                    }else {
                        verify_products=false;
                        break;
                    }
                    
                    if(products_amounts[t].value==0 || products_amounts[t].value.length==0) {

                        $('#info-products-promotions').empty();
                        $('#info-products-promotions').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                Recuerda agregar las cantidades de los productos a vender. No pueden quedar en blanco o en cero (0).
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-products-promotions').offset().top-85
                        }, 500);

                        verify=false;
                        break;
                    }

                }

                if(!verify_products) {

                    $('#info-products-promotions').empty();
                    $('#info-products-promotions').append(`
                        <div class="alert alert-danger alert-dismissible mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                            Agregaste dos veces o más el mismo producto en la venta.
                        </div>
                    `);

                    $('html,body').animate({
                        scrollTop: $('#info-products-promotions').offset().top-85
                    }, 500);

                    return false;
                }

                for(s=0; s<promotions_amounts.length; s++) {
                    
                    if($.inArray(promotion_id[s].value, check_promotions)==-1) {
                        check_promotions.push(promotion_id[s].value);
                    }else {
                        verify_promotions=false;
                        break;
                    }

                    if(promotions_amounts[s].value==0 || promotions_amounts[s].value.length==0) {

                        $('#info-promotions').empty();
                        $('#info-promotions').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                Recuerda agregar las cantidades de las promociones a vender. No pueden quedar en blanco o en cero (0).
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-promotions').offset().top-85
                        }, 500);

                        verify=false;
                        break;
                    }

                }

                if(!verify_promotions) {

                    $('#info-promotions').empty();
                    $('#info-promotions').append(`
                        <div class="alert alert-danger alert-dismissible mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                            Agregaste dos veces o más la misma promoción en la venta.
                        </div>
                    `);

                    $('html,body').animate({
                        scrollTop: $('#info-promotions').offset().top-85
                    }, 500);

                    return false;
                }

                var re=new RegExp(escapeRegExp('.'),'g');
                var exchange_rate=$('#exchange_rate').val().replace(re,'');
                var exchange_rate=exchange_rate.replace(/,/g, ".");
                exchange_rate=parseFloat(exchange_rate);

                if(exchange_rate==0 || !exchange_rate) {
                    //alert("Recuerda agregar la tasa del día.");
                    return true;
                }

                var services_quantity=document.getElementsByName('services_quantity[]');

                for(s=0; s<services_quantity.length; s++) {

                    if(services_quantity[s].value==0 || services_quantity[s].value.length==0) {

                        $('#info-deliveries').empty();
                        $('#info-deliveries').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                Recuerda agregar las cantidades de los servicios de delivery. No pueden quedar en blanco o en cero (0).
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-deliveries').offset().top-85
                        }, 500);

                        verify=false;
                        break;
                    }

                }

                if(!verify)
                    return false;

                var verify_payment_usd=true;
                var total_amount_usd=0;
                var payment_amount_usd=document.getElementsByName('payment_amount_usd[]');

                for(t=0; t<payment_amount_usd.length; t++) {

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var verify_amount_pay_usd=payment_amount_usd[t].value.replace(re,'');
                    var verify_amount_pay_usd=verify_amount_pay_usd.replace(/,/g, ".");
                    verify_amount_pay_usd=parseFloat(verify_amount_pay_usd);

                    if(payment_amount_usd[t].value.length==0) {
                        verify_payment_usd=false;
                        break;
                    }

                    var amount=payment_amount_usd[t].value.replace(re,'');
                    amount=amount.replace(/,/g, ".");
                    amount=parseFloat(amount);
                    total_amount_usd+=amount;

                }

                if(!verify_payment_usd) {

                    $('#info-payments').empty();
                    $('#info-payments').append(`
                        <div class="alert alert-danger alert-dismissible mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                            Recuerda agregar el/los monto(s) en $ del pago. No pueden quedar en blanco, debes agregar como mínimo la cantidad de 0,00$.
                        </div>
                    `);

                    $('html,body').animate({
                        scrollTop: $('#info-payments').offset().top-85
                    }, 500);

                    return true;
                }

                var total_change_usd=0;
                var change_amount_usd=document.getElementsByName('change_amount_usd[]');
                var change_amount_bss=document.getElementsByName('change_amount_bss[]');
                var change_method_ids=document.getElementsByName('change_method_id[]');
                var pay_the_customers=document.getElementsByName('pay_the_customers[]');
                var verify_change_usd_bss=true;

                for(t=0; t<change_amount_usd.length; t++) {

                    var change=change_amount_usd[t].value.replace(re,'');
                    change=change.replace(/,/g, ".");
                    change=parseFloat(change);

                    if(!change)
                            change=0;

                    total_change_usd+=change;

                    var change_bss=change_amount_bss[t].value.replace(re,'');
                    change_bss=change_bss.replace(/,/g, ".");
                    change_bss=parseFloat(change_bss);

                    if(!change_bss)
                            change_bss=0;

                    var change_method_id=change_method_ids[t].value;

                    @foreach($payment_methods as $payment_method)

                        var pm_id='{{$payment_method->id}}';
                        var currency='{{$payment_method->currency}}';
                        var type='{{$payment_method->type}}';
                        var dollar_amount=parseFloat('{{$cash_register->dollar_amount}}');
                        var bolivars_amount=parseFloat('{{$cash_register->bolivars_amount}}');

                        if(change_method_id==pm_id && currency=="$" && type=="Efectivo" && dollar_amount<change && pay_the_customers[t].value==0) {
                            verify_change_usd_bss=false;

                            $('#info-payments').empty();
                            $('#info-payments').append(`
                                <div class="alert alert-danger alert-dismissible mt-1">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                    Actualmente tienes en caja `+formatNumber(dollar_amount.toFixed(2))+`$ en efectivo, no puedes dar un cambio de `+formatNumber(change.toFixed(2))+`$ en efectivo.
                                </div>
                            `);

                            $('html,body').animate({
                                scrollTop: $('#info-payments').offset().top-85
                            }, 500);

                        }

                        if(change_method_id==pm_id && currency=="Bs.S" && type=="Efectivo" && bolivars_amount<change_bss && pay_the_customers[t].value==0) {
                            verify_change_usd_bss=false;

                            $('#info-payments').empty();
                            $('#info-payments').append(`
                                <div class="alert alert-danger alert-dismissible mt-1">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                    Actualmente tienes en caja `+formatNumber(bolivars_amount.toFixed(2))+` Bs.S en efectivo, no puedes dar un cambio de `+formatNumber(change_bss.toFixed(2))+` Bs.S en efectivo.
                                </div>
                            `);

                            $('html,body').animate({
                                scrollTop: $('#info-payments').offset().top-85
                            }, 500);
                            
                        }

                    @endforeach

                }

                if(!verify_change_usd_bss) {
                    return true;
                }

                var sale_type=$("#sale_type").val();
                var total=$('#total').val().replace(re,'');
                total=total.replace(/,/g, ".");
                total=parseFloat(total);

                // Round prices
                total_amount_usd=parseFloat(total_amount_usd.toFixed(2));
                total=parseFloat(total.toFixed(2));
                missing_usd=parseFloat((total-total_amount_usd).toFixed(2));
                total_change_usd=parseFloat(total_change_usd.toFixed(2));
                change_or_come_back=parseFloat((total_amount_usd-total).toFixed(2));

                if(sale_type==1) {

                    if(total_amount_usd<total) {

                        $('#info-payments').empty();
                        $('#info-payments').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                Si eliges un tipo de venta al contado, recuerda ingresar el/los pago(s) necesarios para cubrir el monto total de la venta: `+formatNumber(total.toFixed(2))+`$. Actualmente te falta por ingresar `+formatNumber(missing_usd.toFixed(2))+`$.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-payments').offset().top-85
                        }, 500);

                        return true;

                    }else if(total_amount_usd>total) {

                        if(!total_change_usd)
                            total_change_usd=0;

                        if(total_change_usd<change_or_come_back) {

                            $('#info-payments').empty();
                            $('#info-payments').append(`
                                <div class="alert alert-danger alert-dismissible mt-1">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                    El/los pago(s) ingresado(s) suma(n) un monto total de: `+formatNumber(total_amount_usd.toFixed(2))+`$ y el monto total de la venta es de `+formatNumber(total.toFixed(2))+`$. Deberías dar un cambio al cliente de: `+(formatNumber(change_or_come_back.toFixed(2)))+`$.
                                </div>
                            `);

                            $('html,body').animate({
                                scrollTop: $('#info-payments').offset().top-85
                            }, 500);

                            return true;

                        }else if(total_change_usd>change_or_come_back) {

                            $('#info-payments').empty();
                            $('#info-payments').append(`
                                <div class="alert alert-danger alert-dismissible mt-1">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                    El/los pago(s) ingresado(s) suma(n) un monto total de: `+formatNumber(total_amount_usd.toFixed(2))+`$ y el monto total de la venta es de `+formatNumber(total.toFixed(2))+`$. Deberías dar un cambio al cliente de: `+(formatNumber(change_or_come_back.toFixed(2)))+`$ y estás dando un cambio de: `+formatNumber(total_change_usd.toFixed(2))+`$.
                                </div>
                            `);

                            $('html,body').animate({
                                scrollTop: $('#info-payments').offset().top-85
                            }, 500);

                            return true;

                        }

                    }else if(total_amount_usd==total) {

                        if(!total_change_usd)
                            total_change_usd=0;

                        if(total_change_usd>0) {

                            $('#info-payments').empty();
                            $('#info-payments').append(`
                                <div class="alert alert-danger alert-dismissible mt-1">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                    El/los pago(s) ingresado(s) suma(n) un monto total de: `+formatNumber(total_amount_usd.toFixed(2))+`$ y el monto total de la venta es de `+formatNumber(total.toFixed(2))+`$. No deberías dar un cambio al cliente.
                                </div>
                            `);

                            $('html,body').animate({
                                scrollTop: $('#info-payments').offset().top-85
                            }, 500);

                            return true;

                        }

                    }

                }else {

                    if(!total_change_usd)
                        total_change_usd=0;

                    if(total_amount_usd>=total) {

                        $('#info-payments').empty();
                        $('#info-payments').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                Deberías elegir un tipo de venta al contado, ya que el/los pago(s) ingresado(s) suma(n) un monto total de: `+formatNumber(total_amount_usd.toFixed(2))+`$ y el monto total de la venta es de `+formatNumber(total.toFixed(2))+`$. En este caso el cliente estaría pagando el monto total de la venta.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-payments').offset().top-85
                        }, 500);

                        return true;

                    }else if(total_amount_usd<total && total_change_usd!=0) {

                        $('#info-payments').empty();
                        $('#info-payments').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El/los pago(s) ingresado(s) suma(n) un monto total de: `+formatNumber(total_amount_usd.toFixed(2))+`$ y el monto total de la venta es de `+formatNumber(total.toFixed(2))+`$. No deberías dar un cambio al cliente de: `+(formatNumber(total_change_usd.toFixed(2)))+`$.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-payments').offset().top-85
                        }, 500);

                        return true;

                    }

                }

                var discount=$('#discount').val();

                if(!discount) {

                    $('#info-errors').empty();
                    $('#info-errors').append(`
                        <div class="alert alert-danger alert-dismissible mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                            El descuento mínimo es de 0%.
                        </div>
                    `);

                    $('html,body').animate({
                        scrollTop: $('#info-errors').offset().top-85
                    }, 500);

                    return true;
                }
                
                if(verify) {

                    $('#ticket').modal('show');
                    $('#ticket-content').empty();

                    var client="";

				    // Client
				    var count_clients="{{count($clients)}}";
				    var client_type=$("#client-type").val();

				    if(count_clients==0 || client_type==2) {

				        if($("#name_business_phone").val().length>0) {
				            client='<p class="mt-0 mb-0">CLIENTE: '+$("#name_business_phone").val()+'</p>';
				        }

				    }else if(client_type==1) {

				        if($("#edit_name_business_phone").val().length>0) {
				        	client='<p class="mt-0 mb-0">CLIENTE: '+$("#edit_name_business_phone").val()+'</p>';
				        }
				    }

				    if($("#today-sales").val().length>0) {

				    	$('#ticket-content').append(`
	                    	<p class="mt-0 mb-0">VENTA DEL DIA: #`+$("#today-sales").val()+`</p>
	                    	`+client+`
	                	`);

					}else {

				    	$('#ticket-content').append(`
	                    	`+client+`
	                	`);
										    	
					}


				    $('#ticket-content').append(`
                    	<br><p class="mt-0 mb-0">PEDIDO:</p>
                	`);

				    var product_id=document.getElementsByName('product_id[]');
				    var products_amounts=document.getElementsByName('products_amounts[]');
				    var products_descriptions=document.getElementsByName('products_descriptions[]');

				    if(product_id.length>0) {
				        for(p=0; p<product_id.length; p++) {
				            
				            var ticket_product="- "+products_amounts[p].value+" "+$("#"+product_id[p].id+" option:selected").text();

						    $('#ticket-content').append(`
		                    	<p class="mt-0 mb-0">`+ticket_product+`</p>
		                	`);

				            if(products_descriptions[p].value.length>0) {
				                var description_ticket_product=" - Descripcion del producto: "+products_descriptions[p].value;

							    $('#ticket-content').append(`
			                    	<p class="mt-0 mb-0">`+description_ticket_product+`</p>
			                	`);
				            }

						    $('#ticket-content').append(`
		                    	<br>
		                	`);

				        }
				    }

				    // Promotions
				    var promotion_id=document.getElementsByName('promotion_id[]');
				    var promotions_amounts=document.getElementsByName('promotions_amounts[]');
				    var promotions_descriptions=document.getElementsByName('promotions_descriptions[]');

				    if(promotion_id.length>0) {
				        for(p=0; p<promotion_id.length; p++) {
				            
	            			var ticket_promotion="- "+promotions_amounts[p].value+" "+$("#"+promotion_id[p].id+" option:selected").text();

						    $('#ticket-content').append(`
		                    	<p class="mt-0 mb-0">`+ticket_promotion+`</p>
		                	`);

				            if(promotions_descriptions[p].value.length>0) {
		                		var description_ticket_promotion=" - Descripcion de promoción: "+promotions_descriptions[p].value;

							    $('#ticket-content').append(`
			                    	<p class="mt-0 mb-0">`+description_ticket_promotion+`</p>
			                	`);
				            }

						    $('#ticket-content').append(`
		                    	<br>
		                	`);

				        }
				    }

				    //Description
				    if($("#sale_description").val().length>0) {
					    $('#ticket-content').append(`
	                    	<p class="mt-0 mb-0">DIRECCIÓN:</p>
	                	`);
					    $('#ticket-content').append(`
	                    	<p class="mt-0 mb-0">`+$("#sale_description").val()+`</p>
	                	`);
				    }

				    // Phone
				    var count_clients="{{count($clients)}}";
				    var client_type=$("#client-type").val();

				    if(count_clients==0 || client_type==2) {

				        if($("#first_phone").val().length>0) {
						    $('#ticket-content').append(`
		                    	<p class="mt-0 mb-0">TLF #1:</p>
		                	`);
						    $('#ticket-content').append(`
		                    	<p class="mt-0 mb-0">`+$("#first_phone").val()+`</p>
		                	`);
				        }

				        if($("#second_phone").val().length>0) {
			    			$('#ticket-content').append(`
		                    	<p class="mt-0 mb-0">TLF #2:</p>
		                	`);
						    $('#ticket-content').append(`
		                    	<p class="mt-0 mb-0">`+$("#second_phone").val()+`</p>
		                	`);
				        }

				    }else if(client_type==1) {

				        if($("#edit_first_phone").val().length>0) {
						    $('#ticket-content').append(`
		                    	<p class="mt-0 mb-0">TLF #1:</p>
		                	`);
						    $('#ticket-content').append(`
		                    	<p class="mt-0 mb-0">`+$("#edit_first_phone").val()+`</p>
		                	`);
				        }

				        if($("#edit_second_phone").val().length>0) {
			    			$('#ticket-content').append(`
		                    	<p class="mt-0 mb-0">TLF #2:</p>
		                	`);
						    $('#ticket-content').append(`
		                    	<p class="mt-0 mb-0">`+$("#edit_second_phone").val()+`</p>
		                	`);
				        }

				    }
                    
                }
            });

			$("#noTicket").click(function () {
				$("#noTicket").attr("disabled", true);
				$("#btnImprimir").attr("disabled", true);
			    $("#add-products").attr("disabled", true);
			    $("#add-promotions").attr("disabled", true);
			    $("#add-deliveries").attr("disabled", true);
			    $("#add-payment-methods").attr("disabled", true);
			    $(".btn-sale").attr("disabled", true);
			    $(".form-sale").submit();
			});

			$("#btnImprimir").click(function () {
				$("#noTicket").attr("disabled", true);
				$("#btnImprimir").attr("disabled", true);
			    $("#add-products").attr("disabled", true);
			    $("#add-promotions").attr("disabled", true);
			    $("#add-deliveries").attr("disabled", true);
			    $("#add-payment-methods").attr("disabled", true);
			    $(".btn-sale").attr("disabled", true);
			});

        });
    </script>
@endsection