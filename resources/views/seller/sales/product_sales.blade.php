@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-chart-line"></i> Reportes <small class="text-muted">| Productos más vendidos | {{$product->name}}</small></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_new_report',['type'=>'most_selled_products'])}}">Volver</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="table-responsive">
                <table id="table-list" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Fecha de registro</th>
                            <th>Vendedor</th>
                            <th>Cliente</th>
                            <th>Total en $</th>
                            <th>Pagado en $</th>
                            <th>Pendiente en $</th>
                            <th>Tipo</th>
                            <th>¿Abonado?</th>
                            <th>¿Despachada?</th>
                            <th></th>
                        </tr>
                    </thead>                            
                    <tbody>
                        @php $count=0; @endphp
                        @foreach($sales as $sale)
                            @php $count++; @endphp
                            <tr>
                                <td>{{$count}}</td>
                                <td>{{$sale->id}}</td>
                                <td>{{$sale->created_at->format('d-m-Y h:i:s a')}}</td>
                                <td>{{$sale->user->first_name.' '.$sale->user->last_name}}</td>
                                <td>{{$sale->client->name_business_phone}}</td>
                                <td>{{number_format($sale->total, 2, ',', '.')}}</td>
                                <td>{{number_format($sale->total_paid, 2, ',', '.')}}</td>
                                <td>{{($sale->total-$sale->total_paid)<0?'0.00':number_format($sale->total-$sale->total_paid, 2, ',', '.')}}</td>
                                <td>{{$sale->sale_type==1?'Al contado':'A crédito'}}</td>
                                <td>{{($sale->total-$sale->total_paid)<0?'Si':'No'}}</td>
                                <td>{{$sale->dispatched==1?'Si':'No'}}</td>
                                <td><a target="_blank" style="color: #17a2b8 !important;" href="{{route('sales_show',['sale_id'=>$sale->id])}}"><i class="fa fa-search" aria-hidden="true"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            $("#table-list").DataTable( {
                "columnDefs": [{ "orderable": false, "targets": -1 }],
                "iDisplayLength": 100,
                "order": [[ 0, "desc" ]],
                "language": {
                    "sProcessing":     "Procesando ventas...",
                    "sLengthMenu":     "Mostrar _MENU_ ventas",
                    "sZeroRecords":    "No se encontraron ventas",
                    "sEmptyTable":     "Ninguna venta disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ ventas de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ninguna venta",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ ventas)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando ventas...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );
            
        });
    </script>
@endsection