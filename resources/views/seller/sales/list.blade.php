@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    @if($route=='sales_history')
                        <h1 class="m-0 text-dark"><i class="nav-icon fas fa-store"></i> Ventas | <small class="text-muted">Historial</small></h1>
                    @else
                        <h1 class="m-0 text-dark"><i class="nav-icon fas fa-store"></i> Ventas | <small class="text-muted">Hoy {{date('d-m-Y')}}</small></h1>
                    @endif
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('sales_create')}}">Registrar</a></li>
                    </ol>
                    @if($route=='sales_history')
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-primary btn-sm" href="{{route('sales')}}">Hoy {{date('d-m-Y')}}</a></li>
                        </ol>
                    @else
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-primary btn-sm" href="{{route('sales_history')}}">Historial</a></li>
                        </ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="table-responsive">
                <table id="table-list" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Fecha de registro</th>
                            <th>Vendedor</th>
                            <th>Cliente</th>
                            <th>Total en $</th>
                            <th>Pagado en $</th>
                            <th>Pendiente en $</th>
                            <th>Tipo</th>
                            <th>¿Abonado?</th>
                            <th>¿Despachada?</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            @if($route=='sales_history')
                $("#table-list").DataTable( {
                    "ajax": "{{route('get_sales_history')}}",
                    "columnDefs": [{ "orderable": false, "targets": -1 }],
                    "iDisplayLength": 100,
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "sProcessing":     "Procesando ventas...",
                        "sLengthMenu":     "Mostrar _MENU_ ventas",
                        "sZeroRecords":    "No se encontraron ventas",
                        "sEmptyTable":     "Ninguna venta disponible en esta tabla",
                        "sInfo":           "Mostrando de _START_ a _END_ ventas de un total de _TOTAL_",
                        "sInfoEmpty":      "No se ha registrado ninguna venta",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ ventas)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando ventas...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "decimal": ",",
                        "thousands": "."
                    }
                } );

            @else
                $("#table-list").DataTable( {
                    "ajax": "{{route('get_sales')}}",
                    "columnDefs": [{ "orderable": false, "targets": -1 }],
                    "iDisplayLength": 100,
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "sProcessing":     "Procesando ventas...",
                        "sLengthMenu":     "Mostrar _MENU_ ventas",
                        "sZeroRecords":    "No se encontraron ventas",
                        "sEmptyTable":     "Ninguna venta disponible en esta tabla",
                        "sInfo":           "Mostrando de _START_ a _END_ ventas de un total de _TOTAL_",
                        "sInfoEmpty":      "No se ha registrado ninguna venta",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ ventas)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando ventas...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "decimal": ",",
                        "thousands": "."
                    }
                } );
            @endif
            
        });
    </script>
@endsection