@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-store"></i> Ventas</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('sales')}}">Listado</a></li>
                    </ol>
                    @if($user->hasRole('admin'))
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-warning btn-sm" href="{{route('sales_trash',['sale_id'=>$sale->id])}}"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                        </ol>
                    @endif
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-primary btn-sm" href="{{route('sales_edit',['sale_id'=>$sale->id])}}"><i class="fa fa-edit" aria-hidden="true"></i></a></li>
                    </ol>
                    @if($sale->total_paid<$sale->total && $sale->sale_type==2)
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-success btn-sm" href="{{route('payment_collections_pay',['sale_id'=>$sale->id])}}"><i class="fa fa-handshake" aria-hidden="true"></i></a></li>
                        </ol>
                    @endif
                    @if($sale->dispatched==0)
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-dark btn-sm" href="{{route('sales_update_dispatched',['sale_id'=>$sale->id])}}"><i class="fa fa-shipping-fast" aria-hidden="true"></i></a></li>
                        </ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @include("seller.sales.includes.invoice")
        </div>
    </section>
@endsection