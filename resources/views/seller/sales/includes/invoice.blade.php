<div class="invoice p-3 mb-3">
    <div class="row">
        <div class="col-12">
            <h4>
                <img class="rounded mx-auto" width="50px" src="{{asset($configuration->logo)}}" alt="Dashboard 789">
                {{$configuration->name}}
            </h4>
        </div>
    </div>
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            Vendedor
            <address>
                <strong>{{$sale->user->first_name}} {{$sale->user->last_name}}</strong><br>
                <small>
                    Cédula de identidad: {{$sale->user->identity_card}}<br>
                    Correo electrónico: {{$sale->user->email}}
                </small>
            </address>
        </div>
        <div class="col-sm-4 invoice-col">
            Cliente
            <address>
                <strong>{{$sale->client->first_name}} {{$sale->client->last_name}}</strong><br>
                <small>
                    Nombre en el tlf del negocio: {{$sale->client->name_business_phone}}<br>
                    @if($sale->client->first_phone && $sale->client->second_phone)
                        Teléfono(s): {{$sale->client->first_phone}} | {{$sale->client->second_phone}}
                    @else
                        Teléfono: {{$sale->client->first_phone}}
                    @endif
                </small>
            </address>
        </div>
        <div class="col-sm-4 invoice-col">
            <small>
                <b>ID de venta:</b> {{$sale->id}}<br>
                <b>Tipo de venta:</b> {{$sale->sale_type==1?'Al contado':'A crédito'}}<br>
                <b>Fecha de registro:</b> {{$sale->created_at->format('d-m-Y h:i:s a')}}<br>
                <b>Descripción de venta:</b> {{$sale->description==null?'N/A':$sale->description}}<br>
                <b>¿Despachada?</b> @if($sale->dispatched==1) Si @else No @endif<br>
                <!--@if($sale->delivery_company_id)
                    <br><b>Delivery:</b> {{$sale->delivery_company->name}} @if($sale->delivery_company_driver_id) | {{$sale->delivery_company_driver->name}} @endif

                    @if($sale->delivery_type_id)
                        <br><b>Tipo de delivery:</b> {{$sale->delivery_type->name}} | {{number_format($sale->delivery_amount, 2, ',', '.')}}$
                    @endif
                @endif-->
            </small>
        </div>
    </div>
    @php $price=0; @endphp
    @if(count($sale->products)>0)
        <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Descripción opcional</th>
                            <th>Cantidad</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sale->products as $product)
                            <tr>
                                <td>{{$product->name}}</td>
                                <td>{{$product->pivot->description==null?'N/A':$product->pivot->description}}</td>
                                <td>{{$product->pivot->amount}}</td>
                                <td>{{number_format($product->pivot->price, 2, ',', '.')}}$ @php $price+=$product->pivot->price; @endphp</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
    @endif
    @if(count($sale->promotions)>0)
        <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Promoción</th>
                            <th>Descripción opcional</th>
                            <th>Cantidad</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sale->promotions as $promotion)
                            <tr>
                                <td>{{$promotion->name}}</td>
                                <td>{{$promotion->pivot->description==null?'N/A':$promotion->pivot->description}}</td>
                                <td>{{$promotion->pivot->amount}}</td>
                                <td>{{number_format($promotion->pivot->price, 2, ',', '.')}}$ @php $price+=$promotion->pivot->price; @endphp</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
    @endif
    @if(count($sale->delivery_sales)>0)
        <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Empresa</th>
                            <th>Tipo</th>
                            <th>Conductor</th>
                            <th>Cantidad</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sale->delivery_sales as $delivery_sale)
                            <tr>
                                <td>{{$delivery_sale->delivery_company->name}}</td>
                                <td>{{$delivery_sale->delivery_type_id==null?'N/A':$delivery_sale->delivery_type->name}}</td>
                                <td>{{$delivery_sale->delivery_company_driver==null?'N/A':$delivery_sale->delivery_company_driver->name}}</td>
                                <td>{{number_format($delivery_sale->amount, 0, ',', '.')}}</td>
                                <td>{{number_format($delivery_sale->amount*$delivery_sale->price, 2, ',', '.')}}$</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
    @endif
    <div class="row">
        <div class="col-12 col-sm-8 col-md-8">
            <p class="lead mb-0">Métodos de pagos/cambios</p>
            <small>
                <b>Pagado en $:</b> {{number_format($sale->total_paid, 2, ',', '.')}}$ 
                | <b>Pendiente en $:</b> {{($sale->total-$sale->total_paid)<0?'0.00$':number_format($sale->total-$sale->total_paid, 2, ',', '.').'$'}}
                | <b>Abonado en $:</b> {{($sale->total-$sale->total_paid)<0?number_format(-1*($sale->total-$sale->total_paid), 2, ',', '.').'$':'0.00$'}}
            </small>
            @if(count($sale->payments)>0)
                @foreach($sale->payments as $payment)
                    <div class="card border">
                        <div class="card-header pt-1 pb-1">
                            <h3 class="card-title">
                                <small>ID del pago/cambio: {{$payment->id}}</small>
                            </h3>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="card-body pt-1 pb-1">
                                    <small>
                                        <dl>
                                            <dt class="text-muted">Pago del cliente</dt>
                                            <hr class="mt-0 mb-0">
                                            <dd class="mb-0"><b>Método:</b> {{$payment->payment_method->name}}</dd>
                                            <dd class="mb-0"><b>Banco o procesador:</b> {{$payment->payment_method_option_id==null?'N/A':$payment->payment_method_option->name}}</dd>
                                            <dd class="mb-0"><b>Monto en $:</b> {{number_format($payment->payment_amount_usd, 2, ',', '.')}}</dd>
                                            <dd class="mb-0"><b>Monto en Bs.S:</b> {{number_format($payment->payment_amount_bss, 2, ',', '.')}}</dd>
                                            <dd class="mb-0"><b>Tasa en Bs.S:</b> {{number_format($payment->payment_exchange_rate, 2, ',', '.')}}</dd>
                                            <dd class="mb-0"><b>Nro. de referencia:</b> {{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</dd>
                                            <dd class="mb-0"><b>Descripción:</b> {{$payment->payment_description==null?'N/A':$payment->payment_description}}</dd>
                                        </dl>
                                    </small>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="card-body pt-1 pb-1">
                                    <small>
                                        <dl>
                                            <dt class="text-muted">Cambio o vuelto del vendedor</dt>
                                            <hr class="mt-0 mb-0">
                                            @if($payment->change_amount_usd!=0)
                                                <dd class="mb-0"><b>Método:</b> {{$payment->change_method->name}}</dd>
                                                <dd class="mb-0"><b>Banco o procesador:</b> {{$payment->change_method_option_id==null?'N/A':$payment->change_method_option->name}}</dd>
                                                <dd class="mb-0"><b>Monto en $:</b> {{number_format($payment->change_amount_usd, 2, ',', '.')}}</dd>
                                                <dd class="mb-0"><b>Monto en Bs.S:</b> {{number_format($payment->change_amount_bss, 2, ',', '.')}}</dd>
                                                <dd class="mb-0"><b>Tasa en Bs.S:</b> {{number_format($payment->change_exchange_rate, 2, ',', '.')}}</dd>
                                                <dd class="mb-0"><b>Nro. de referencia:</b> {{$payment->change_reference_number==null?'N/A':$payment->change_reference_number}}</dd>
                                                <dd class="mb-0"><b>Descripción:</b> {{$payment->change_description==null?'N/A':$payment->change_description}}</dd>
                                                <dd class="mb-0"><b>¿Abonado al cliente?</b> {{$payment->pay_the_customer==0?'No':'Si'}}</dd>
                                            @else
                                                <p>N/A</p>
                                            @endif
                                        </dl>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <p>¡Aún no se han realizado pagos!</p>
            @endif
        </div>
        <div class="col-12 col-sm-4 col-md-4">
            <p class="lead">Totales</p>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Subtotal:</th>
                        <td>{{number_format($price, 2, ',', '.')}}$</td>
                    </tr>
                    <tr>
                        <th>Delivery:</th>
                        <td>{{number_format($sale->delivery_amount, 2, ',', '.')}}$</td>
                    </tr>
                    <tr>
                        <th>Descuento:</th>
                        <td>-{{number_format($price*($sale->discount/100), 2, ',', '.')}}$ | {{number_format($sale->discount, 2, ',', '.')}}%</td>
                    </tr>
                    <tr>
                        <th>Total en $:</th>
                        <td>{{number_format($sale->total, 2, ',', '.')}}$</td>
                    </tr>
                    <tr>
                        <th>Total en Bs.S:</th>
                        <td>{{number_format($sale->total_bss, 2, ',', '.')}} Bs.S <br> <small class="text-muted">Tasa de la venta: {{number_format($sale->exchange_rate, 2, ',', '.')}} Bs.S</small></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>