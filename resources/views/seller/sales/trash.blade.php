@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-store"></i> Ventas</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('sales')}}">Listado</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-primary btn-sm" href="{{route('sales_edit',['sale_id'=>$sale->id])}}"><i class="fa fa-edit" aria-hidden="true"></i></a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-info btn-sm" href="{{route('sales_show',['sale_id'=>$sale->id])}}"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                    </ol>
                    @if($sale->total_paid<$sale->total && $sale->sale_type==2)
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-success btn-sm" href="{{route('payment_collections_pay',['sale_id'=>$sale->id])}}"><i class="fa fa-handshake" aria-hidden="true"></i></a></li>
                        </ol>
                    @endif
                    @if($sale->dispatched==0)
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-dark btn-sm" href="{{route('sales_update_dispatched',['sale_id'=>$sale->id])}}"><i class="fa fa-shipping-fast" aria-hidden="true"></i></a></li>
                        </ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Eliminar</h3>
                </div>
                <form class="form-delete" action="{{route('sales_delete')}}" method="post">
                    @csrf
                    <input type="hidden" name="sale_id" value="{{$sale->id}}"/>
                    <div class="card-body text-center">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-12">
                                    <p class="text-muted">¿Está seguro que desea eliminar la venta con <b>ID {{$sale->id}}</b>?</p>
                                    <p class="text-muted">Escriba <b>eliminar</b> en el campo para confirmar</p>
                                    <div class="input-group mb-3">
                                        <input id="confirm_delete" type="text" class="form-control offset-sm-3 col-sm-6" placeholder="eliminar" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-trash"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button id="delete_button" type="button" class="btn btn-warning" disabled>Eliminar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {
            $("#confirm_delete").keyup(function () {
                if($(this).val().toLowerCase()=="eliminar"){
                    $("#delete_button").prop("disabled",false);
                } else {
                    $("#delete_button").prop("disabled",true);
                }
            })
            $("#delete_button").click(function(event) {
                if($("#confirm_delete").val().toLowerCase()=="eliminar"){
                    $("#delete_button").prop("disabled",true);
                    $(".form-delete").submit();
                }
            });
        });
    </script>
@endsection