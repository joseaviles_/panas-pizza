@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    @if($initial_date && $final_date)
                        <h1 class="m-0 text-dark"><i class="nav-icon fas fa-percentage"></i> Ventas con descuento</h1>
                        <small class="text-muted">Desde el <b>{{DateTime::createFromFormat('Y-m-d',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d',$final_date)->format('d-m-Y')}}</b></small>
                    @else
                        <h1 class="m-0 text-dark"><i class="nav-icon fas fa-percentage"></i> Ventas con descuento</h1>
                        <small class="text-muted"><b>Historial</b></small>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form id="sales" class="card card-primary" action="{{route('discount_sales')}}" method="get">
                <div class="card-header">
                    <h3 class="card-title">¡Consulta las ventas con descuento!</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body p-2">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6">              
                                <small class="text-muted">Fecha inicial</small>
                                <div class="input-group mb-3">
                                    <input name="initial_date" type="date" class="form-control" value="{{$initial_date}}" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">              
                                <small class="text-muted">Fecha final</small>
                                <div class="input-group mb-3">
                                    <input name="final_date" type="date" class="form-control" value="{{$final_date}}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                        <a class="btn btn-danger btn-sm mt-1" href="{{route('discount_sales')}}">Eliminar filtro</a>
                    </div>
                </div>
            </form>  
            <div class="table-responsive">
                <table id="table-list" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Fecha de registro</th>
                            <th>Vendedor</th>
                            <th>Cliente</th>
                            <th>Descuento %</th>
                            <th>Total en $</th>
                            <th>Pagado en $</th>
                            <th>Pendiente en $</th>
                            <th>Tipo</th>
                            <th>¿Abonado?</th>
                            <th>¿Despachada?</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            @if($initial_date && $final_date)
                var initial_date='{{$initial_date}}';
                var final_date='{{$final_date}}';
                var route= "/sales/discount/get?initial_date="+initial_date+"&final_date="+final_date;
                $("#table-list").DataTable( {
                    "ajax": route,
                    "columnDefs": [{ "orderable": false, "targets": -1 }],
                    "iDisplayLength": 100,
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "sProcessing":     "Procesando ventas...",
                        "sLengthMenu":     "Mostrar _MENU_ ventas",
                        "sZeroRecords":    "No se encontraron ventas",
                        "sEmptyTable":     "Ninguna venta disponible en esta tabla",
                        "sInfo":           "Mostrando de _START_ a _END_ ventas de un total de _TOTAL_",
                        "sInfoEmpty":      "No se ha registrado ninguna venta",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ ventas)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando ventas...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "decimal": ",",
                        "thousands": "."
                    }
                } );
            @else
                $("#table-list").DataTable( {
                    "ajax": "{{route('get_discount_sales')}}",
                    "columnDefs": [{ "orderable": false, "targets": -1 }],
                    "iDisplayLength": 100,
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "sProcessing":     "Procesando ventas...",
                        "sLengthMenu":     "Mostrar _MENU_ ventas",
                        "sZeroRecords":    "No se encontraron ventas",
                        "sEmptyTable":     "Ninguna venta disponible en esta tabla",
                        "sInfo":           "Mostrando de _START_ a _END_ ventas de un total de _TOTAL_",
                        "sInfoEmpty":      "No se ha registrado ninguna venta",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ ventas)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando ventas...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "decimal": ",",
                        "thousands": "."
                    }
                } );
            @endif
            
        });
    </script>
@endsection