@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-cash-register"></i> Caja</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('cash_register')}}">Listado</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <p>Disponible en $: <b>{{number_format($cash_register->dollar_amount, 2, ',', '.')}}</b> - Disponible en Bs.S: <b>{{number_format($cash_register->bolivars_amount, 2, ',', '.')}}</b></p>
            <div class="card card-danger">
                <div class="card-header">
                    <h3 class="card-title">Retirar efectivo</h3>
                </div>
                <form action="{{route('cash_register_post_withdraw_cash')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">              
                                    <small class="text-muted">* Moneda</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" name="currency" required>
                                            <option @if(old('currency')=="$") selected @endif value="$">$ - Dólares</option>
                                            <option @if(old('currency')=="Bs.S") selected @endif value="Bs.S">Bs.S - Bolívares Soberanos</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6">              
                                    <small class="text-muted">* Monto</small>
                                    <div class="input-group mb-3">
                                        <input name="amount" id="amount" type="text" class="form-control" placeholder="Monto" value="{{old('amount')}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span><b>$ o Bs.S</b></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-danger">Retirar efectivo</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#amount').autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});
        });
    </script>
@endsection