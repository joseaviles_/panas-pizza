@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-cash-register"></i> Caja</h1>
                </div>
                <div class="col-sm-6">
                    @if($user->hasRole('admin'))
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('cash_register_withdraw_cash')}}">Retirar efectivo</a></li>
                        </ol>
                    @endif
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-success btn-sm" href="{{route('cash_register_create')}}">Ingresar efectivo</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <p>Disponible en $: <b>{{number_format($cash_register->dollar_amount, 2, ',', '.')}}</b> - Disponible en Bs.S: <b>{{number_format($cash_register->bolivars_amount, 2, ',', '.')}}</b></p>
            <div class="table-responsive">
                <table id="table-list" class="table table-bordered table-sm display table-hover font-12" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Fecha de registro</th>
                            <th>Tipo</th>
                            <th>Movimiento</th>
                            <th>Monto</th>
                            <th>Moneda</th>
                            <th>Venta</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            $("#table-list").DataTable( {
                "ajax": "{{route('get_cash_register')}}",
                "iDisplayLength": 25,
                "order": [[ 0, "desc" ]],
                "language": {
                    "sProcessing":     "Procesando movimientos...",
                    "sLengthMenu":     "Mostrar _MENU_ movimientos",
                    "sZeroRecords":    "No se encontraron movimientos",
                    "sEmptyTable":     "Ningún movimiento disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ movimientos de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ningún movimiento",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ movimientos)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando movimientos...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );
            
        });
    </script>
@endsection