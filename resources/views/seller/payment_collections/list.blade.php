@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-handshake"></i> Cobranzas</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="table-responsive">
                <table id="table-list" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Fecha de registro</th>
                            <th>Vendedor</th>
                            <th>Cliente</th>
                            <th>Total en $</th>
                            <th>Pagado en $</th>
                            <th>Pendiente en $</th>
                            <th>Tipo</th>
                            <th>¿Abonado?</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            $("#table-list").DataTable( {
                "ajax": "{{route('get_payment_collections')}}",
                "iDisplayLength": 100,
                "order": [[ 0, "desc" ]],
                "language": {
                    "sProcessing":     "Procesando ventas...",
                    "sLengthMenu":     "Mostrar _MENU_ ventas",
                    "sZeroRecords":    "No se encontraron ventas",
                    "sEmptyTable":     "Ninguna venta disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ ventas de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ninguna venta",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ ventas)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando ventas...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );
            
        });
    </script>
@endsection