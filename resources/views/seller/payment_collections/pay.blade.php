@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-handshake"></i> Cobranzas</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('payment_collections')}}">Listado</a></li>
                    </ol>
                    @if($user->hasRole('admin'))
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-warning btn-sm" href="{{route('sales_trash',['sale_id'=>$sale->id])}}"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                        </ol>
                    @endif
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-primary btn-sm" href="{{route('sales_edit',['sale_id'=>$sale->id])}}"><i class="fa fa-edit" aria-hidden="true"></i></a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-info btn-sm" href="{{route('sales_show',['sale_id'=>$sale->id])}}"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                    </ol>
                    @if($sale->dispatched==0)
                        <ol class="breadcrumb float-sm-right ml-1 mt-1">
                            <li class="breadcrumb-item"><a class="btn btn-dark btn-sm" href="{{route('sales_update_dispatched',['sale_id'=>$sale->id])}}"><i class="fa fa-shipping-fast" aria-hidden="true"></i></a></li>
                        </ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Pagar venta con ID {{$sale->id}}</h3>
                </div>
                <form class="form-pay" action="{{route('payment_collections_post_pay')}}" method="post">
                    @csrf
                    <input type="hidden" name="sale_id" value="{{$sale->id}}">
                    <div class="card-body">
                        <b>Métodos de pagos/cambios</b>
                        <div id="info-payments"></div>
                        <br><small>
                            <b>Pagado en $:</b> {{number_format($sale->total_paid, 2, ',', '.')}}$ 
                            | <b>Pendiente en $:</b> {{($sale->total-$sale->total_paid)<0?'0.00$':number_format($sale->total-$sale->total_paid, 2, ',', '.').'$'}}
                            | <b>Abonado en $:</b> {{($sale->total-$sale->total_paid)<0?number_format(-1*($sale->total-$sale->total_paid), 2, ',', '.').'$':'0.00$'}}
                        </small>
                        <div id="cols-payment-methods"></div>
                        <div class="text-center mt-3 mb-3">
                            <button type="button" id="add-payment-methods" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> Agregar método de pago/cambio</button>
                        </div>
                        <button type="button" class="btn btn-primary btn-pay">Pagar</button>
                    </div>
                </form>
            </div>
            @include("seller.sales.includes.invoice")
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script>
        $(document).ready(function() {

            var cols_payment_methods = $("#cols-payment-methods");
            var x=0;
            
            $("#add-payment-methods").click(function() {
                x++;
                addNewPaymentMethods(x);
            });

            function addNewPaymentMethods(x) {

                cols_payment_methods.append(`

                    <div class="px-3 mb-5 rounded border" id="div-pay-`+x+`">
                        <div class="color-palette text-right">
                            <button type="button" id="remove-div-pay-`+x+`" class="btn btn-sm btn-warning mt-2"><i class="fa fa-trash"></i></button>
                        </div>
                        <b>Pago del cliente</b>
                        <div class="row">
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Método del pago</small>
                                <select id="select-pay-`+x+`" class="form-control" name="payment_method_id[]" required>
                                    @foreach($payment_methods as $payment_method)
                                        <option value="{{$payment_method->id}}">{{$payment_method->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Banco o procesador del pago</small>
                                <select id="select-pay-option-`+x+`" class="form-control" name="payment_method_option_id[]" required>
                                </select>
                                <small class="text-muted" id="text-info-`+x+`"></small>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Monto en $ del pago</small>
                                <div class="input-group">
                                    <input id="amount-usd-`+x+`" type="text" class="form-control" placeholder="Monto en $ del pago" name="payment_amount_usd[]" value="0" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-dollar-sign"></span>
                                        </div>
                                    </div>
                                </div>
                                <small style="color: red !important; font-weight: bold !important;" id="text-com-extra-`+x+`"></small>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Monto en Bs.S del pago</small>
                                <div class="input-group mb-3">
                                    <input id="amount-bss-`+x+`" type="text" class="form-control" placeholder="Monto en Bs.S del pago" name="payment_amount_bss[]" value="0" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span><b>Bs.S</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Tasa del pago</small>
                                <div class="input-group mb-3">
                                    <input id="pay-exchange-rate-`+x+`" type="text" class="form-control" placeholder="Tasa del pago" value="{{$configuration->exchange_rate}}" name="payment_exchange_rate[]" autocomplete="off" readonly required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span><b>Bs.S</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>Nro. de referencia del pago</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="payment_reference_number[]" placeholder="Opcional">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <small>Descripción del pago</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="payment_descriptions[]" placeholder="Opcional">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <b>Cambio o vuelto del vendedor</b>
                        <br><small class="text-muted">Si el pago no necesita un cambio o vuelto, deja el monto en $ y en Bs.S en 0,00.</small>
                        <div class="row">
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Método del cambio</small>
                                <select id="select-change-`+x+`" class="form-control" name="change_method_id[]" required>
                                    @foreach($payment_methods as $payment_method)
                                        <option value="{{$payment_method->id}}">{{$payment_method->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Banco o procesador del cambio</small>
                                <select id="select-change-option-`+x+`" class="form-control" name="change_method_option_id[]" required>
                                </select>
                                <small class="text-muted" id="change-info-`+x+`">Hola</small>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Monto en $ del cambio</small>
                                <div class="input-group">
                                    <input id="change-usd-`+x+`" type="text" class="form-control" placeholder="Monto en $ del cambio" name="change_amount_usd[]" value="0" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-dollar-sign"></span>
                                        </div>
                                    </div>
                                </div>
                                <small style="color: red !important; font-weight: bold !important;" id="change-com-extra-`+x+`"></small>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Monto en Bs.S del cambio</small>
                                <div class="input-group mb-3">
                                    <input id="change-bss-`+x+`" type="text" class="form-control" placeholder="Monto en Bs.S del cambio" name="change_amount_bss[]" value="0" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span><b>Bs.S</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>* Tasa del cambio</small>
                                <div class="input-group mb-3">
                                    <input id="change-exchange-rate-`+x+`" type="text" class="form-control" placeholder="Tasa del cambio" value="{{$configuration->exchange_rate}}" name="change_exchange_rate[]" autocomplete="off" readonly required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span><b>Bs.S</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small>Nro. de referencia del cambio</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="change_reference_number[]" placeholder="Opcional">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <small>Descripción del cambio</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="change_descriptions[]" placeholder="Opcional">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="pay_the_customers[]" type="hidden" value="0" id="pay-credit-`+x+`">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="0" id="check-`+x+`">
                            <label class="form-check-label" for="check-`+x+`">
                                Abonar al cliente
                            </label>
                        </div>
                        <br>
                    </div>

                    <div class="modal fade" id="unlock-credit-`+x+`" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title secondary_color">Abonar al cliente</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p class="text-center"><small>¡Ingresa el código de desbloqueo para abonar el cambio o vuelto!</small></p>
                                            <input id="credit-unlock-code-`+x+`" type="password" class="form-control" placeholder="Código de desbloqueo"/>
                                            <div class="text-center">
                                                <small id="info-code-`+x+`" style="color: red !important; font-weight: bold !important;"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="btn-credit-`+x+`" type="button" class="btn btn-success">Abonar</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                `);
                
                var verify_check=false;

                $("#check-"+x).change(function () {

                    $("#info-code-"+x).text('');

                    if(!verify_check) {
                        $("#check-"+x).prop("checked", false);
                        $("#credit-unlock-code-"+x).val("");
                        $("#unlock-credit-"+x).modal('show');
                    }else {

                        if($("#check-"+x).prop("checked")) {
                            $("#check-"+x).val(1)
                            $("#pay-credit-"+x).val(1);
                        }else {
                            $("#check-"+x).val(0);
                            $("#pay-credit-"+x).val(0);
                        }

                    }

                });

                $("#btn-credit-"+x).click(function(event) {
                    
                    $("#info-code-"+x).text('');
                    
                    var credit_unlock_code=$("#credit-unlock-code-"+x).val();

                    if(credit_unlock_code.length==0) {

                        $("#info-code-"+x).text("Ingresa el código de desbloqueo...");
                        $("#check-"+x).prop("checked", false); 

                    }else if(credit_unlock_code!="{{$configuration->unlock_code}}") {

                        $("#info-code-"+x).text("Código de desbloqueo incorrecto...");
                        $("#check-"+x).prop("checked", false);

                    }else if(credit_unlock_code=="{{$configuration->unlock_code}}") {
                        
                        $("#check-"+x).val(1)
                        $("#pay-credit-"+x).val(1);
                        verify_check=true;
                        $("#credit-unlock-code-"+x).val("");
                        $("#unlock-credit-"+x).modal('hide');
                        $("#check-"+x).prop("checked", true);

                    }

                });

                $("#remove-div-pay-"+x).click(function(event) {
                    $("#div-pay-"+x).remove();
                    $("#unlock-credit-"+x).remove();
                });

                // PAYMENTS FUNCTIONS
                $("#select-pay-"+x).change(function () {
                    $("#text-com-extra-"+x).text('');

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var amount=$("#amount-usd-"+x).val().replace(re,'');
                    var amount=amount.replace(/,/g, ".");
                    amount=parseFloat(amount);
                    getPaymentMethodOptions($("#select-pay-"+x), $("#select-pay-option-"+x), $("#text-info-"+x), $("#text-com-extra-"+x), amount);
                });

                $("#amount-usd-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                $("#amount-bss-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                $("#pay-exchange-rate-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                var re=new RegExp(escapeRegExp('.'),'g');
                var amount=$("#amount-usd-"+x).val().replace(re,'');
                var amount=amount.replace(/,/g, ".");
                amount=parseFloat(amount);
                getPaymentMethodOptions($("#select-pay-"+x), $("#select-pay-option-"+x), $("#text-info-"+x), $("#text-com-extra-"+x), amount);

                $("#select-pay-option-"+x).change(function () {
                    $("#text-com-extra-"+x).text('');

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var amount=$("#amount-usd-"+x).val().replace(re,'');
                    var amount=amount.replace(/,/g, ".");
                    amount=parseFloat(amount);
                    generateComissionExtraAmount($("#select-pay-option-"+x), $("#text-info-"+x), $("#text-com-extra-"+x), amount);
                });

                // CHANGES FUNCTIONS
                $("#select-change-"+x).change(function () {
                    $("#change-com-extra-"+x).text('');

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var change=$("#change-usd-"+x).val().replace(re,'');
                    var change=change.replace(/,/g, ".");
                    change=parseFloat(change);
                    getPaymentMethodOptions($("#select-change-"+x), $("#select-change-option-"+x), $("#change-info-"+x), $("#change-com-extra-"+x), change);
                });

                $("#change-usd-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                $("#change-bss-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                $("#change-exchange-rate-"+x).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                var re=new RegExp(escapeRegExp('.'),'g');
                var change=$("#change-usd-"+x).val().replace(re,'');
                var change=change.replace(/,/g, ".");
                change=parseFloat(change);
                getPaymentMethodOptions($("#select-change-"+x), $("#select-change-option-"+x), $("#change-info-"+x), $("#change-com-extra-"+x), change);

                $("#select-change-option-"+x).change(function () {
                    $("#change-com-extra-"+x).text('');

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var change=$("#change-usd-"+x).val().replace(re,'');
                    var change=change.replace(/,/g, ".");
                    change=parseFloat(change);
                    generateComissionExtraAmount($("#select-change-option-"+x), $("#change-info-"+x), $("#change-com-extra-"+x), change);
                });

                // AMOUNTS AND CHANGES KEYUP
                $("#amount-usd-"+x).keyup(function () {

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var exchange_rate=$("#pay-exchange-rate-"+x).val().replace(re,'');
                    var exchange_rate=exchange_rate.replace(/,/g, ".");
                    exchange_rate=parseFloat(exchange_rate);

                    var amount=$(this).val().replace(re,'');
                    var amount=amount.replace(/,/g, ".");
                    amount=parseFloat(amount);

                    generateComissionExtraAmount($("#select-pay-option-"+x), $("#text-info-"+x), $("#text-com-extra-"+x), amount);
                    
                    amount*=exchange_rate;
                    amount=formatNumber(amount.toFixed(2));
                    $("#amount-bss-"+x).val(amount);
                });

                $("#amount-bss-"+x).keyup(function () {
                    var re=new RegExp(escapeRegExp('.'),'g');
                    var exchange_rate=$("#pay-exchange-rate-"+x).val().replace(re,'');
                    var exchange_rate=exchange_rate.replace(/,/g, ".");
                    exchange_rate=parseFloat(exchange_rate);
                    
                    var amount_bss=$(this).val().replace(re,'');
                    var amount_bss=amount_bss.replace(/,/g, ".");
                    amount_bss=parseFloat(amount_bss);
                    amount_bss/=exchange_rate;

                    generateComissionExtraAmount($("#select-pay-option-"+x), $("#text-info-"+x), $("#text-com-extra-"+x), amount_bss);

                    amount_bss=formatNumber(amount_bss.toFixed(2));
                    $("#amount-usd-"+x).val(amount_bss);
                });

                $("#change-usd-"+x).keyup(function () {
                    var re=new RegExp(escapeRegExp('.'),'g');
                    var exchange_rate=$("#change-exchange-rate-"+x).val().replace(re,'');
                    var exchange_rate=exchange_rate.replace(/,/g, ".");
                    exchange_rate=parseFloat(exchange_rate);

                    var change=$(this).val().replace(re,'');
                    var change=change.replace(/,/g, ".");
                    change=parseFloat(change);

                    generateComissionExtraAmount($("#select-change-option-"+x), $("#change-info-"+x), $("#change-com-extra-"+x), change);

                    change*=exchange_rate;
                    change=formatNumber(change.toFixed(2));
                    $("#change-bss-"+x).val(change);
                });

                $("#change-bss-"+x).keyup(function () {
                    var re=new RegExp(escapeRegExp('.'),'g');
                    var exchange_rate=$("#change-exchange-rate-"+x).val().replace(re,'');
                    var exchange_rate=exchange_rate.replace(/,/g, ".");
                    exchange_rate=parseFloat(exchange_rate);
                    
                    var change_bss=$(this).val().replace(re,'');
                    var change_bss=change_bss.replace(/,/g, ".");
                    change_bss=parseFloat(change_bss);
                    change_bss/=exchange_rate;

                    generateComissionExtraAmount($("#select-change-option-"+x), $("#change-info-"+x), $("#change-com-extra-"+x), change_bss);

                    change_bss=formatNumber(change_bss.toFixed(2));
                    $("#change-usd-"+x).val(change_bss);
                });

                $('html,body').animate({
                    scrollTop: $("#div-pay-"+x).offset().top-75
                }, 1000);

            }

            function generateComissionExtraAmount(selector_a, selector_b, selector_c, amount) {
                var select_pay_option=selector_a.val();
                var payment_method_option_name='';
                var commission=parseFloat(0);
                var extra_amount=parseFloat(0);

                @foreach($payment_method_options as $payment_method_option)
                    
                    if(select_pay_option=='{{$payment_method_option->id}}') {
                        payment_method_option_name='{{$payment_method_option->name}}';
                        commission='{{$payment_method_option->commission}}';
                        extra_amount='{{$payment_method_option->extra_amount}}';
                    }

                @endforeach

                var new_comission=parseFloat(commission);
                var new_extra_amount=parseFloat(extra_amount);

                if(new_comission>0 || new_extra_amount>0) {
                    generateComExtraFinal(new_comission, new_extra_amount, amount, selector_c);
                }

                selector_b.text("Comisión: "+formatNumber(new_comission.toFixed(2))+"% | Monto extra: "+formatNumber(new_extra_amount.toFixed(2)));
            }

            function getPaymentMethodOptions(selector_a, selector_b, selector_c, selector_d, amount) {

                selector_b.empty();

                var route="/sales/payment_method_options/"+selector_a.val();
                $.get(route,{"_token":"{{csrf_token()}}"}, 
                function(data) {

                    var payment_method_options=data.payment_method_options;

                    if(payment_method_options!=null) {
                        
                        $.each(payment_method_options,function(key, value) {
                            selector_b.append('<option value="'+value.id+'">'+value.name+'</option>');
                        });

                    }

                    if(payment_method_options.length===0) {
                        selector_b.append('<option value="NULL">N/A</option>');
                    }

                    generateComissionExtraAmount(selector_b, selector_c, selector_d, amount);
                });
                
            }

            function generateComExtraFinal(commission, extra, amount, selector_a) {
                var final_amount=(amount+extra)/(1-(commission/100));
                selector_a.text("Monto neto: "+formatNumber(final_amount.toFixed(2))+"$");
            }

            function escapeRegExp(string) {
                return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); 
            }

            function formatNumber(num) {
                return num.toString().replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1,$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
            }

            $(".btn-pay").click(function () {

                var verify_payment_usd=true;
                var total_amount_usd=0;
                var payment_amount_usd=document.getElementsByName('payment_amount_usd[]');

                if(payment_amount_usd.length==0) {

                    $('#info-payments').empty();
                    $('#info-payments').append(`
                        <div class="alert alert-danger alert-dismissible mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                            Debes agregar como mínimo un método de pago/cambio.
                        </div>
                    `);

                    $('html,body').animate({
                        scrollTop: $('#info-payments').offset().top-85
                    }, 500);

                    return true;
                }

                for(t=0; t<payment_amount_usd.length; t++) {

                    var re=new RegExp(escapeRegExp('.'),'g');
                    var verify_amount_pay_usd=payment_amount_usd[t].value.replace(re,'');
                    var verify_amount_pay_usd=verify_amount_pay_usd.replace(/,/g, ".");
                    verify_amount_pay_usd=parseFloat(verify_amount_pay_usd);

                    if(payment_amount_usd[t].value.length==0) {
                        verify_payment_usd=false;
                        break;
                    }

                    var amount=payment_amount_usd[t].value.replace(re,'');
                    amount=amount.replace(/,/g, ".");
                    amount=parseFloat(amount);
                    total_amount_usd+=amount;

                }

                if(!verify_payment_usd) {

                    $('#info-payments').empty();
                    $('#info-payments').append(`
                        <div class="alert alert-danger alert-dismissible mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                            Recuerda agregar el/los monto(s) en $ del pago. No pueden quedar en blanco, debes agregar como mínimo la cantidad de 0,00$.
                        </div>
                    `);

                    $('html,body').animate({
                        scrollTop: $('#info-payments').offset().top-85
                    }, 500);

                    return true;
                }

                var total_change_usd=0;
                var change_amount_usd=document.getElementsByName('change_amount_usd[]');
                var change_amount_bss=document.getElementsByName('change_amount_bss[]');
                var change_method_ids=document.getElementsByName('change_method_id[]');
                var pay_the_customers=document.getElementsByName('pay_the_customers[]');
                var verify_change_usd_bss=true;

                for(t=0; t<change_amount_usd.length; t++) {

                    var change=change_amount_usd[t].value.replace(re,'');
                    change=change.replace(/,/g, ".");
                    change=parseFloat(change);

                    if(!change)
                            change=0;

                    total_change_usd+=change;

                    var change_bss=change_amount_bss[t].value.replace(re,'');
                    change_bss=change_bss.replace(/,/g, ".");
                    change_bss=parseFloat(change_bss);

                    if(!change_bss)
                            change_bss=0;

                    var change_method_id=change_method_ids[t].value;

                    @foreach($payment_methods as $payment_method)

                        var pm_id='{{$payment_method->id}}';
                        var currency='{{$payment_method->currency}}';
                        var type='{{$payment_method->type}}';
                        var dollar_amount=parseFloat('{{$cash_register->dollar_amount}}');
                        var bolivars_amount=parseFloat('{{$cash_register->bolivars_amount}}');

                        if(change_method_id==pm_id && currency=="$" && type=="Efectivo" && dollar_amount<change && pay_the_customers[t].value==0) {
                            verify_change_usd_bss=false;

                            $('#info-payments').empty();
                            $('#info-payments').append(`
                                <div class="alert alert-danger alert-dismissible mt-1">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                    Actualmente tienes en caja `+formatNumber(dollar_amount.toFixed(2))+`$ en efectivo, no puedes dar un cambio de `+formatNumber(change.toFixed(2))+`$ en efectivo.
                                </div>
                            `);

                            $('html,body').animate({
                                scrollTop: $('#info-payments').offset().top-85
                            }, 500);

                        }

                        if(change_method_id==pm_id && currency=="Bs.S" && type=="Efectivo" && bolivars_amount<change_bss && pay_the_customers[t].value==0) {
                            verify_change_usd_bss=false;

                            $('#info-payments').empty();
                            $('#info-payments').append(`
                                <div class="alert alert-danger alert-dismissible mt-1">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                    Actualmente tienes en caja `+formatNumber(bolivars_amount.toFixed(2))+` Bs.S en efectivo, no puedes dar un cambio de `+formatNumber(change_bss.toFixed(2))+` Bs.S en efectivo.
                                </div>
                            `);

                            $('html,body').animate({
                                scrollTop: $('#info-payments').offset().top-85
                            }, 500);

                        }

                    @endforeach

                }

                if(!verify_change_usd_bss) {
                    return true;
                }

                var sale_type='{{$sale->sale_type}}';
                var total='{{($sale->total-$sale->total_paid)<0?"0":number_format($sale->total-$sale->total_paid, 2, ",", ".")}}';
                total=total.replace(/,/g, ".");
                total=parseFloat(total);

                // Round prices
                total_amount_usd=parseFloat(total_amount_usd.toFixed(2));
                total=parseFloat(total.toFixed(2));
                missing_usd=parseFloat((total-total_amount_usd).toFixed(2));
                total_change_usd=parseFloat(total_change_usd.toFixed(2));
                change_or_come_back=parseFloat((total_amount_usd-total).toFixed(2));

                if(!total_change_usd)
                    total_change_usd=0;

                if(total_amount_usd>total) {

                    if(!total_change_usd)
                        total_change_usd=0;

                    if(total_change_usd<change_or_come_back && (total_amount_usd-total_change_usd)>total) {

                        $('#info-payments').empty();
                        $('#info-payments').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El/los pago(s) ingresado(s) suma(n) un monto total de: `+formatNumber(total_amount_usd.toFixed(2))+`$ y el monto pendiente de la venta es de `+formatNumber(total.toFixed(2))+`$. Deberías dar un cambio al cliente de: `+(formatNumber(change_or_come_back.toFixed(2)))+`$.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-payments').offset().top-85
                        }, 500);

                        return true;

                    }else if(total_change_usd>change_or_come_back && (total_amount_usd-total_change_usd)<=0) {

                        $('#info-payments').empty();
                        $('#info-payments').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El/los pago(s) ingresado(s) suma(n) un monto total de: `+formatNumber(total_amount_usd.toFixed(2))+`$ y el monto pendiente de la venta es de `+formatNumber(total.toFixed(2))+`$. Deberías dar un cambio al cliente de: `+(formatNumber(change_or_come_back.toFixed(2)))+`$ y estás dando un cambio de: `+formatNumber(total_change_usd.toFixed(2))+`$.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-payments').offset().top-85
                        }, 500);

                        return true;

                    }

                }else if(total_amount_usd==total) {

                    if(!total_change_usd)
                        total_change_usd=0;

                    if(total_change_usd>0) {

                        $('#info-payments').empty();
                        $('#info-payments').append(`
                            <div class="alert alert-danger alert-dismissible mt-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                                El/los pago(s) ingresado(s) suma(n) un monto total de: `+formatNumber(total_amount_usd.toFixed(2))+`$ y el monto pendiente de la venta es de `+formatNumber(total.toFixed(2))+`$. No deberías dar un cambio al cliente.
                            </div>
                        `);

                        $('html,body').animate({
                            scrollTop: $('#info-payments').offset().top-85
                        }, 500);

                        return true;

                    }

                }/*else if(total_amount_usd<total && total_change_usd!=0) {

                    $('#info-payments').empty();
                    $('#info-payments').append(`
                        <div class="alert alert-danger alert-dismissible mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> ¡Alerta!</h5>
                            El/los pago(s) ingresado(s) suma(n) un monto total de: `+formatNumber(total_amount_usd.toFixed(2))+`$ y el monto pendiente de la venta es de `+formatNumber(total.toFixed(2))+`$. No deberías dar un cambio al cliente de: `+(formatNumber(total_change_usd.toFixed(2)))+`$.
                        </div>
                    `);

                    $('html,body').animate({
                        scrollTop: $('#info-payments').offset().top-85
                    }, 500);

                    return true;

                }*/

                $("#add-payment-methods").attr("disabled", true);
                $(".btn-pay").attr("disabled", true);
                $(".form-pay").submit();
            });

        });
    </script>
@endsection