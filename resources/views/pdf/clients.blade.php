<!DOCTYPE html>
<html>
<head>
	<title>Clientes</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
	    </div>
	    <div>
	        <div>
	            <strong>Clientes</strong>
	        </div>
	    </div>
        <br>
        <table style="font-size: 12px !important;">
            <thead>
                <tr>
                    <th>Número de cliente</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Cédula de identidad</th>
                    <th>Nombre registrado en el teléfono del negocio</th>
                    <th>Teléfono(s)</th>
                    <th>Fecha de registro</th>
                </tr>
            </thead>
            <tbody>
                @foreach($clients as $client)

                    @php
                        $phones=$client->first_phone;
                        if($client->second_phone)
                            $phones='Tlf #1: '.$phones.' | Tlf #2: '.$client->second_phone;
                    @endphp
                    <tr>
                        <td>{{$client->client_number}}</td>
                        <td>{{$client->first_name}}</td>
                        <td>{{$client->last_name}}</td>
                        <td>{{$client->identity_card==null?'N/A':$client->identity_card}}</td>
                        <td>{{$client->name_business_phone}}</td>
                        <td>{{$phones}}</td>
                        <td>{{$client->created_at->format('d-m-Y h:i:s a')}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>