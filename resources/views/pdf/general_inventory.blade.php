<!DOCTYPE html>
<html>
<head>
	<title>Inventario general</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
	    </div>
	    <div>
	        <div>
	            <strong>Inventario general</strong>
	        </div>
	    </div>
        <br>
        <b>Materias primas</b>
        <table style="font-size: 12px !important;">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Unidad</th>
                    <th>Stock</th>
                </tr>
            </thead>
            <tbody>
                @foreach($raw_material as $rm)
                    <tr>
                        <td>{{$rm->name}}</td>
                        <td>{{$rm->unit}}</td>
                        <td>{{number_format($rm->amount, 2, ',', '.')}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <b>Productos sin elaboración</b>
        <table style="font-size: 12px !important;">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Categoría</th>
                    <th>Stock</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->name}}</td>
                        <td>{{$product->category->name}}</td>
                        <td>{{number_format($product->amount, 0, ',', '.')}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>