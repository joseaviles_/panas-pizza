<!DOCTYPE html>
<html>
<head>
	<title>Categorías</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
	    </div>
	    <div>
	        <div>
	            <strong>Categorías</strong>
	        </div>
	    </div>
        <br>
        <table style="font-size: 12px !important;">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Fecha de registro</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->name}}</td>
                        <td>{{$category->created_at->format('d-m-Y h:i:s a')}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>