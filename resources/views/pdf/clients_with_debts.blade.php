<!DOCTYPE html>
<html>
<head>
	<title>Clientes con deudas</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
	    </div>
	    <div>
	        <div>
	            <strong>Clientes con deudas</strong>
	        </div>
	    </div>
        <br>
        <table style="font-size: 12px !important;">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Fecha de registro</th>
                    <th>Vendedor</th>
                    <th>Cliente</th>
                    <th>Total en $</th>
                    <th>Pagado en $</th>
                    <th>Pendiente en $</th>
                    <th>Tipo</th>
                    <th>¿Abonado?</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sales as $sale)
                    @if($sale->total_paid<$sale->total)
                        <tr>
                            <td>{{$sale->id}}</td>
                            <td>{{$sale->created_at->format('d-m-Y h:i:s a')}}</td>
                            <td>{{$sale->user->first_name.' '.$sale->user->last_name}}</td>
                            <td>{{$sale->client->name_business_phone}}</td>
                            <td>{{number_format($sale->total, 2, ',', '.')}}</td>
                            <td>{{number_format($sale->total_paid, 2, ',', '.')}}</td>
                            <td>{{($sale->total-$sale->total_paid)<0?'0.00':number_format($sale->total-$sale->total_paid, 2, ',', '.')}}</td>
                            <td>{{$sale->sale_type==1?'Al contado':'A crédito'}}</td>
                            <td>{{($sale->total-$sale->total_paid)<0?'Si':'No'}}</td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
	</div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>