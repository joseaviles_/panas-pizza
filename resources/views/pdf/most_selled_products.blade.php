<!DOCTYPE html>
<html>
<head>
	<title>Productos más vendidos</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
	    </div>
	    <div>
	        <div>
	            <strong>Productos más vendidos</strong>
	            <small>
                    @if($initial_date!='none' && $final_date!='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @elseif($initial_date!='none' && $final_date=='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                    @elseif($initial_date=='none' && $final_date!='none')
                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @else
                        | Historial completo
                    @endif
                </small>
	        </div>
	    </div>
        <div style="margin-top: 25px !important;">
            <div>
            	<table>
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Ventas</th>
                            <th>Generado en $</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php 
                            $categories=[];
                            $categories_prices=[];
                        @endphp
                        @foreach($products as $product)
                            <tr>
                                <td>{{$product->name}}</td>
                                <td>{{number_format($product->product_sale_amount, 0, ',', '.')}}</td>
                                <td>{{number_format($product->product_sale_price, 2, ',', '.')}}</td>
                            </tr>

                            @if(!isset($categories[App\Models\Product::where('id',$product->id)->first()->category->name]))
                                @php
                                    $categories[App\Models\Product::where('id',$product->id)->first()->category->name]=0;
                                @endphp
                            @endif
                            @php
                                $categories[App\Models\Product::where('id',$product->id)->first()->category->name]+=$product->product_sale_amount;
                            @endphp

                            @if(!isset($categories_prices[App\Models\Product::where('id',$product->id)->first()->category->name]))
                                @php
                                    $categories_prices[App\Models\Product::where('id',$product->id)->first()->category->name]=0;
                                @endphp
                            @endif
                            @php
                                $categories_prices[App\Models\Product::where('id',$product->id)->first()->category->name]+=$product->product_sale_price;
                            @endphp

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <br><br>
        <div>
            <div>
                <strong>Promociones más vendidas</strong>
                <small>
                    @if($initial_date!='none' && $final_date!='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @elseif($initial_date!='none' && $final_date=='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                    @elseif($initial_date=='none' && $final_date!='none')
                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @else
                        | Historial completo
                    @endif
                </small>
            </div>
        </div>
        <div style="margin-top: 25px !important;">
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Promoción</th>
                            <th>Ventas</th>
                            <th>Generado en $</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($promotions as $promotion)
                            <tr>
                                <td>{{$promotion->name}}</td>
                                <td>{{number_format($promotion->promotion_sale_amount, 0, ',', '.')}}</td>
                                <td>{{number_format($promotion->promotion_sale_price, 2, ',', '.')}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <br><br>
        <div>
            <div>
                <strong>Categorías más vendidas</strong>
                <small>
                    @if($initial_date!='none' && $final_date!='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @elseif($initial_date!='none' && $final_date=='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                    @elseif($initial_date=='none' && $final_date!='none')
                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @else
                        | Historial completo
                    @endif
                </small>
            </div>
        </div>
        <div style="margin-top: 25px !important;">
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Categoría</th>
                            <th>Ventas</th>
                            <th>Generado en $</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php arsort($categories); @endphp
                        @foreach($categories as $key=> $sales)
                            <tr>
                                <td>{{$key}}</td>
                                <td>{{number_format($sales, 0, ',', '.')}}</td>
                                <td>{{number_format($categories_prices[$key], 2, ',', '.')}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>