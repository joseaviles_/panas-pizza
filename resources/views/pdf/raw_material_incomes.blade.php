<!DOCTYPE html>
<html>
<head>
	<title>Productos generados de las entradas de materias primas</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
    <div>
        <div>
            <div>
                <h4>
                    <img width="50px" src="{{asset($configuration->logo)}}" alt="{{$configuration->name}}">
                    {{$configuration->name}}
                </h4>
            </div>
        </div>
        <div>
            <div>
                <strong>Productos generados de las entradas de materias primas</strong>
                <small>
                    @if($initial_date!='none' && $final_date!='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @elseif($initial_date!='none' && $final_date=='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                    @elseif($initial_date=='none' && $final_date!='none')
                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @else
                        | Historial completo
                    @endif
                </small>
            </div>
        </div>
        @foreach($raw_material_incomes as $raw_material_income)
            <div>
                <p>
                    <b>Entrada de {{number_format($raw_material_income->raw_material_movement->amount, 2, ',', '.')}} {{$raw_material_income->raw_material->unit}} de {{$raw_material_income->raw_material->name}} el {{$raw_material_income->created_at->format('d-m-Y h:i:sa')}} <small>| Actualmente quedan {{number_format($raw_material_income->amount, 2, ',', '.')}} {{$raw_material_income->raw_material->unit}}</small></b>
                </p>
                <div>
                    @php
                        $total_array_product=0; 
                        $total_array_raw_material_used=0;
                    @endphp
                    <div>
                        <table>
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>{{$raw_material_income->raw_material->unit}} usados(as)</th>
                                    <th>Cantidad generada</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($raw_material_income->array_products as $key=> $array_product)
                                    <tr>
                                        <td>{{$key}}</td>
                                        <td>{{number_format($raw_material_income->array_raw_material_used[$key], 2, ',', '.')}}</td>
                                        <td>{{number_format($array_product, 0, ',', '.')}}</td>
                                    </tr>
                                    @php
                                        $total_array_product+=$array_product; 
                                        $total_array_raw_material_used+=$raw_material_income->array_raw_material_used[$key];
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <small>
                    <p><b>Total de {{$raw_material_income->raw_material->unit}} de {{$raw_material_income->raw_material->name}} usados(as):</b> {{number_format($total_array_raw_material_used, 2, ',', '.')}}</p>
                    <p><b>Total de {{$raw_material_income->raw_material->unit}} de {{$raw_material_income->raw_material->name}} restantes:</b> {{number_format($raw_material_income->amount, 2, ',', '.')}} <b>=</b> ({{number_format($raw_material_income->raw_material_movement->amount, 2, ',', '.')}}-{{number_format($total_array_raw_material_used, 2, ',', '.')}})</p>
                    <p><b>Total de productos de generados:</b> {{number_format($total_array_product, 0, ',', '.')}}</p>
                </small>
                <hr>
            </div>
        @endforeach
    </div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>