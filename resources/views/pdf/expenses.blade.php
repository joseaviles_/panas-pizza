<!DOCTYPE html>
<html>
<head>
	<title>Gastos</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
	    </div>
        @php
            $total_usd_rm=0;
            $total_bss_rm=0;
            $total_usd_products=0;
            $total_bss_products=0;
        @endphp
        <div>
            <div>
                <strong>Gastos en materia prima</strong>
                <small>
                    @if($initial_date!='none' && $final_date!='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @elseif($initial_date!='none' && $final_date=='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                    @elseif($initial_date=='none' && $final_date!='none')
                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @else
                        | Historial completo
                    @endif
                </small>
            </div>
            <div>
                <table>
                    <thead>
                            <tr>
                                <th>Fecha de registro</th>
                                <th>Materia prima</th>
                                <th>Cantidad</th>
                                <th>Unidad</th>
                                <th>Precio en $</th>
                                <th>Precio en Bs.S</th>
                                <th>Tasa en Bs.S</th>
                                <th>Concepto</th>
                            </tr>
                    </thead>
                    <tbody>
                        @foreach($raw_material_movements as $raw_material_movement)
                            <tr>
                                <td>{{$raw_material_movement->created_at->format('d-m-Y h:i:s a')}}</td>
                                <td>{{$raw_material_movement->raw_material->name}}</td>
                                <td>{{number_format($raw_material_movement->amount, 2, ',', '.')}}</td>
                                <td>{{$raw_material_movement->raw_material->unit}}</td>
                                <td>{{number_format($raw_material_movement->price, 2, ',', '.')}}</td>
                                <td>{{number_format($raw_material_movement->price_bss, 2, ',', '.')}}</td>
                                <td>{{number_format($raw_material_movement->exchange_rate, 2, ',', '.')}}</td>
                                <td>{{$raw_material_movement->concept}}</td>
                            </tr>
                            @php
                                $total_usd_rm+=$raw_material_movement->price;
                                $total_bss_rm+=$raw_material_movement->price_bss;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            <p>Total de gastos en materia prima en $: <b>{{number_format($total_usd_rm, 2, ',', '.')}}</b> | Total de gastos en materia prima en Bs.S: <b>{{number_format($total_bss_rm, 2, ',', '.')}}</b></p>
        </div>
        <hr>
        <div>
            <div>
                <strong>Gastos en productos sin elaboración</strong>
                <small>
                    @if($initial_date!='none' && $final_date!='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @elseif($initial_date!='none' && $final_date=='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                    @elseif($initial_date=='none' && $final_date!='none')
                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @else
                        | Historial completo
                    @endif
                </small>
            </div>
            <div>
                <table>
                    <thead>
                            <tr>
                                <th>Fecha de registro</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Precio en $</th>
                                <th>Precio en Bs.S</th>
                                <th>Tasa en Bs.S</th>
                                <th>Concepto</th>
                            </tr>
                    </thead>
                    <tbody>
                        @foreach($unprocessed_products_movements as $unprocessed_product_movement)
                            <tr>
                                <td>{{$unprocessed_product_movement->created_at->format('d-m-Y h:i:s a')}}</td>
                                <td>{{$unprocessed_product_movement->product->name}}</td>
                                <td>{{number_format($unprocessed_product_movement->amount, 2, ',', '.')}}</td>
                                <td>{{number_format($unprocessed_product_movement->price, 2, ',', '.')}}</td>
                                <td>{{number_format($unprocessed_product_movement->price_bss, 2, ',', '.')}}</td>
                                <td>{{number_format($unprocessed_product_movement->exchange_rate, 2, ',', '.')}}</td>
                                <td>{{$unprocessed_product_movement->concept}}</td>
                            </tr>
                            @php
                                $total_usd_products+=$unprocessed_product_movement->price;
                                $total_bss_products+=$unprocessed_product_movement->price_bss;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            <p>Total de gastos en productos en $: <b>{{number_format($total_usd_products, 2, ',', '.')}}</b> | Total de gastos en productos en Bs.S: <b>{{number_format($total_bss_products, 2, ',', '.')}}</b></p>
        </div>
	</div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>