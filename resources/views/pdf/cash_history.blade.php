<!DOCTYPE html>
<html>
<head>
	<title>Historial de caja</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
	    </div>
	    <div>
	        <div>
	            <strong>Historial de caja</strong>
	        </div>
	    </div>
        <br>
        <table style="font-size: 12px !important;">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Fecha de registro</th>
                    <th>Tipo</th>
                    <th>Movimiento</th>
                    <th>Monto</th>
                    <th>Moneda</th>
                    <th>Venta</th>
                </tr>
            </thead>
            <tbody>
                @foreach($movements as $movement)
                    @php
                        $sale='N/A';
                        if($movement->sale_id)
                            $sale='ID de venta: '.$movement->sale->id;
                    @endphp
                    <tr>
                        <td>{{$movement->id}}</td>
                        <td>{{$movement->created_at->format('d-m-Y h:i:s a')}}</td>
                        <td>{{$movement->type==1?'Entrada':'Salida', $movement->movement}}</td>
                        <td>{{$movement->movement}}</td>
                        <td>{{number_format($movement->amount, 2, ',', '.')}}</td>
                        <td>{{$movement->currency}}</td>
                        <td>{{$sale}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>