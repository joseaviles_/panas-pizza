<!DOCTYPE html>
<html>
<head>
	<title>Entregas de las empresas de delivery</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
	    </div>
	    <div>
	        <div>
	            <strong>Entregas de las empresas de delivery</strong>
                <small>
                    @if($initial_date!='none' && $final_date!='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @elseif($initial_date!='none' && $final_date=='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                    @elseif($initial_date=='none' && $final_date!='none')
                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @else
                        | Historial completo
                    @endif
                </small>
	        </div>
	    </div>

        @php
            $final_delivery_sales=0;
            $final_delivery_amount_usd=0;
            $final_delivery_amount_bss=0;
        @endphp

        @foreach($delivery_companies as $delivery_company)
            <div>
                <p>
                    <b>{{$delivery_company->name}}</b>                       
                    <small>
                        @if($initial_date!='none' && $final_date!='none')
                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                        @elseif($initial_date!='none' && $final_date=='none')
                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                        @elseif($initial_date=='none' && $final_date!='none')
                            | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                        @else
                            | Historial completo
                        @endif
                    </small>
                </p>

                <div>
                    <table>
                        <thead>                  
                            <tr>
                                <th>Total de entregas</th>
                                <th>Total de generado en $</th>
                                <th>Total de generado en Bs.S</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    @if($initial_date!='none' && $final_date!='none')
                                        
                                        {{number_format($delivery_company->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}
                                        
                                        @php
                                            $final_delivery_sales+=$delivery_company->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('amount');
                                        @endphp

                                    @elseif($initial_date!='none' && $final_date=='none')

                                        {{number_format($delivery_company->delivery_sales->where('created_at','>=',$initial_date)->sum('amount'), 0, ',', '.')}}

                                        @php
                                            $final_delivery_sales+=$delivery_company->delivery_sales->where('created_at','>=',$initial_date)->sum('amount');
                                        @endphp

                                    @elseif($initial_date=='none' && $final_date!='none')

                                        {{number_format($delivery_company->delivery_sales->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}

                                        @php
                                            $final_delivery_sales+=$delivery_company->delivery_sales->where('created_at','<=',$final_date)->sum('amount');
                                        @endphp

                                    @else

                                        {{number_format($delivery_company->delivery_sales->sum('amount'), 0, ',', '.')}}

                                        @php
                                            $final_delivery_sales+=$delivery_company->delivery_sales->sum('amount');
                                        @endphp

                                    @endif
                                </td>
                                <td>
                                    
                                    @php 
                                        $total_delivery_amount_usd=0; 
                                        $total_delivery_amount_bss=0; 
                                    @endphp

                                    @if($initial_date!='none' && $final_date!='none')
                                        
                                        @foreach($delivery_company->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $delivery_sale)
                                            @php 
                                                $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                            @endphp
                                        @endforeach

                                        {{number_format($total_delivery_amount_usd, 2, ',', '.')}}

                                        @php
                                            $final_delivery_amount_usd+=$total_delivery_amount_usd;
                                            $final_delivery_amount_bss+=$total_delivery_amount_bss;
                                        @endphp

                                    @elseif($initial_date!='none' && $final_date=='none')

                                        @foreach($delivery_company->delivery_sales->where('created_at','>=',$initial_date) as $delivery_sale)
                                            @php 
                                                $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                            @endphp
                                        @endforeach

                                        {{number_format($total_delivery_amount_usd, 2, ',', '.')}}

                                        @php
                                            $final_delivery_amount_usd+=$total_delivery_amount_usd;
                                            $final_delivery_amount_bss+=$total_delivery_amount_bss;
                                        @endphp

                                    @elseif($initial_date=='none' && $final_date!='none')

                                        @foreach($delivery_company->delivery_sales->where('created_at','<=',$final_date) as $delivery_sale)
                                            @php 
                                                $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                            @endphp
                                        @endforeach

                                        {{number_format($total_delivery_amount_usd, 2, ',', '.')}}

                                        @php
                                            $final_delivery_amount_usd+=$total_delivery_amount_usd;
                                            $final_delivery_amount_bss+=$total_delivery_amount_bss;
                                        @endphp

                                    @else

                                        @foreach($delivery_company->delivery_sales as $delivery_sale)
                                            @php 
                                                $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                            @endphp
                                        @endforeach

                                        {{number_format($total_delivery_amount_usd, 2, ',', '.')}}

                                        @php
                                            $final_delivery_amount_usd+=$total_delivery_amount_usd;
                                            $final_delivery_amount_bss+=$total_delivery_amount_bss;
                                        @endphp

                                    @endif

                                </td>
                                <td>
                                    {{number_format($total_delivery_amount_bss, 2, ',', '.')}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                <div>
                    <div>
                        <p>
                            <small>
                                <b>Tipos de delivery</b> 
                            </small>
                        </p>
                        <div>
                            <table>
                                <thead>                  
                                    <tr>
                                        <th>Tipo de delivery</th>
                                        <th>Total de entregas</th>
                                        <th>Total de generado en $</th>
                                        <th>Total de generado en Bs.S</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($delivery_company->delivery_types->sortBy('price') as $delivery_type)
                                        <tr>
                                            <td>{{$delivery_type->name}}: {{number_format($delivery_type->price, 2, ',', '.')}}$</td>
                                            <td>
                                                @if($initial_date!='none' && $final_date!='none')
                                                    {{number_format($delivery_type->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}
                                                @elseif($initial_date!='none' && $final_date=='none')
                                                    {{number_format($delivery_type->delivery_sales->where('created_at','>=',$initial_date)->sum('amount'), 0, ',', '.')}}
                                                @elseif($initial_date=='none' && $final_date!='none')
                                                    {{number_format($delivery_type->delivery_sales->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}
                                                @else
                                                    {{number_format($delivery_type->delivery_sales->sum('amount'), 0, ',', '.')}}
                                                @endif
                                            </td>
                                            <td>
                                                
                                                @php 
                                                    $total_delivery_amount_usd=0; 
                                                    $total_delivery_amount_bss=0; 
                                                @endphp

                                                @if($initial_date!='none' && $final_date!='none')
                                                    @foreach($delivery_type->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $delivery_sale)
                                                        @php 
                                                            $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                            $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                        @endphp
                                                    @endforeach
                                                    {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                @elseif($initial_date!='none' && $final_date=='none')
                                                    @foreach($delivery_type->delivery_sales->where('created_at','>=',$initial_date) as $delivery_sale)
                                                        @php 
                                                            $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                            $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                        @endphp
                                                    @endforeach
                                                    {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                @elseif($initial_date=='none' && $final_date!='none')
                                                    @foreach($delivery_type->delivery_sales->where('created_at','<=',$final_date) as $delivery_sale)
                                                        @php 
                                                            $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                            $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                        @endphp
                                                    @endforeach
                                                    {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                @else
                                                    @foreach($delivery_type->delivery_sales as $delivery_sale)
                                                        @php 
                                                            $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                            $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                        @endphp
                                                    @endforeach
                                                    {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                @endif

                                            </td>
                                            <td>
                                                {{number_format($total_delivery_amount_bss, 2, ',', '.')}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <p>
                            <small>
                                <b>Conductores</b> 
                            </small>
                        </p>
                        <div>
                            <table>
                                <thead>                  
                                    <tr>
                                        <th>Conductor</th>
                                        <th>Total de entregas</th>
                                        <th>Total de generado en $</th>
                                        <th>Total de generado en Bs.S</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($delivery_company->delivery_company_drivers->sortBy('name') as $delivery_company_driver)
                                        <tr>
                                            <td>{{$delivery_company_driver->name}}</td>
                                            <td>
                                                @if($initial_date!='none' && $final_date!='none')
                                                    {{number_format($delivery_company_driver->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}
                                                @elseif($initial_date!='none' && $final_date=='none')
                                                    {{number_format($delivery_company_driver->delivery_sales->where('created_at','>=',$initial_date)->sum('amount'), 0, ',', '.')}}
                                                @elseif($initial_date=='none' && $final_date!='none')
                                                    {{number_format($delivery_company_driver->delivery_sales->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}
                                                @else
                                                    {{number_format($delivery_company_driver->delivery_sales->sum('amount'), 0, ',', '.')}}
                                                @endif
                                            </td>
                                            <td>
                                                
                                                @php 
                                                    $total_delivery_amount_usd=0; 
                                                    $total_delivery_amount_bss=0; 
                                                @endphp

                                                @if($initial_date!='none' && $final_date!='none')
                                                    @foreach($delivery_company_driver->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $delivery_sale)
                                                        @php 
                                                            $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                            $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                        @endphp
                                                    @endforeach
                                                    {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                @elseif($initial_date!='none' && $final_date=='none')
                                                    @foreach($delivery_company_driver->delivery_sales->where('created_at','>=',$initial_date) as $delivery_sale)
                                                        @php 
                                                            $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                            $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                        @endphp
                                                    @endforeach
                                                    {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                @elseif($initial_date=='none' && $final_date!='none')
                                                    @foreach($delivery_company_driver->delivery_sales->where('created_at','<=',$final_date) as $delivery_sale)
                                                        @php 
                                                            $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                            $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                        @endphp
                                                    @endforeach
                                                    {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                @else
                                                    @foreach($delivery_company_driver->delivery_sales as $delivery_sale)
                                                        @php 
                                                            $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                            $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                        @endphp
                                                    @endforeach
                                                    {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                @endif

                                            </td>
                                            <td>
                                                {{number_format($total_delivery_amount_bss, 2, ',', '.')}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <br><hr>
        @endforeach
        <p>
            <b>Sumatoria global</b>                       
            <small>
                @if($initial_date!='none' && $final_date!='none')
                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                @elseif($initial_date!='none' && $final_date=='none')
                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                @elseif($initial_date=='none' && $final_date!='none')
                    | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                @else
                    | Historial completo
                @endif
            </small>
        </p>
        <table>
            <thead>                  
                <tr>
                    <th>Total de entregas</th>
                    <th>Total de generado en $</th>
                    <th>Total de generado en Bs.S</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        {{number_format($final_delivery_sales, 0, ',', '.')}}
                    </td>
                    <td>
                        {{number_format($final_delivery_amount_usd, 2, ',', '.')}}
                    </td>
                    <td>
                        {{number_format($final_delivery_amount_bss, 2, ',', '.')}}
                    </td>
                </tr>
            </tbody>
        </table>

	</div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>