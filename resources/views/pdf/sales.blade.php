<!DOCTYPE html>
<html>
<head>
	<title>Ventas</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>			
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
	    </div>
	    <div>
	        <div>
	            <strong>Ventas</strong>
	            <small>
	            	@if($initial_date!='none' && $final_date!='none')
		            	| Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
	            	@elseif($initial_date!='none' && $final_date=='none')
		            	| Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
	            	@elseif($initial_date=='none' && $final_date!='none')
	            		| Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
            		@else
            			| Historial completo
	            	@endif

					@php
						$pending=0;
					@endphp
						@foreach($sales as $sale)
						@php 
							($sale->total-$sale->total_paid)<0?$pending+=0:$pending+=($sale->total-$sale->total_paid);
						@endphp
					@endforeach

		        	<br><b>Ventas:</b> {{count($sales)}} | <b>Total en $:</b> {{number_format($sales->sum('total'), 2, ',', '.')}} | <b>Pendiente en $:</b> {{number_format($pending, 2, ',', '.')}} | <b>Ingreso total en $:</b> {{number_format($sales->sum('total')-$pending, 2, ',', '.')}}
		        	<br>Total en $ sin delivery: {{number_format($sales->sum('total'), 2, ',', '.')}}$ <b>(Total)</b> - {{number_format($sales->sum('delivery_amount'), 2, ',', '.')}}$ <b>(Delivery)</b> = <b>{{number_format($sales->sum('total')-$sales->sum('delivery_amount'), 2, ',', '.')}}$</b>

                </small>
	        </div>
	    </div>
        <div style="margin-top: 25px !important;">
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Fecha de registro</th>
                            <th>Vendedor</th>
                            <th>Cliente</th>
                            <th>Total en $</th>
                            <th>Pendiente en $</th>
                            <th>Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sales as $sale)
                            <tr>
                                <td>{{$sale->id}}</td>
                                <td>{{$sale->created_at->format('d-m-Y h:i:s a')}}</td>
                                <td>{{$sale->user->first_name.' '.$sale->user->last_name}}</td>
                                <td>{{$sale->client->name_business_phone}}</td>
                                <td>{{number_format($sale->total, 2, ',', '.')}}</td>
                                <td>{{($sale->total-$sale->total_paid)<0?'0.00':number_format($sale->total-$sale->total_paid, 2, ',', '.')}}</td>
                                <td>{{$sale->sale_type==1?'Al contado':'A crédito'}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>