<!DOCTYPE html>
<html>
<head>
	<title>Entradas/salidas de materias primas y de productos sin elaboración</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
	    </div>
	</div>
    @if($raw_material)
        <div>
            <p>
                <b>Entradas de {{$raw_material->name}}</b>                       
                <small>
                    @if($initial_date!='none' && $final_date!='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @elseif($initial_date!='none' && $final_date=='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                    @elseif($initial_date=='none' && $final_date!='none')
                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @else
                        | Historial completo
                    @endif
                </small>
            </p>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Fecha de registro</th>
                            <th>Tipo</th>
                            <th>Stock en {{$raw_material->unit}}</th>
                            <th>Cantidad en {{$raw_material->unit}}</th>
                            <th>Precio en $</th>
                            <th>Precio en Bs.S</th>
                            <th>Tasa en Bs.S</th>
                            <th>Concepto</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($raw_material_movements as $raw_material_movement)
                            @if($raw_material_movement->type==1)
                                <tr>
                                    <td>{{$raw_material_movement->created_at->format('d-m-Y h:i:s a')}}</td>
                                    <td>{{$raw_material_movement->type==1?'Entrada':'Salida'}}</td>
                                    <td>{{number_format($raw_material_movement->new_amount, 2, ',', '.')}}</td>
                                    <td>{{number_format($raw_material_movement->amount, 2, ',', '.')}}</td>
                                    <td>{{$raw_material_movement->price==0?'N/A':number_format($raw_material_movement->price, 2, ',', '.')}}</td>
                                    <td>{{$raw_material_movement->price_bss==0?'N/A':number_format($raw_material_movement->price_bss, 2, ',', '.')}}</td>
                                    <td>{{$raw_material_movement->exchange_rate==0?'N/A':number_format($raw_material_movement->exchange_rate, 2, ',', '.')}}</td>
                                    <td>{!!$raw_material_movement->concept!!}</td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <p>
                <b>Salidas de {{$raw_material->name}}</b>                       
                <small>
                    @if($initial_date!='none' && $final_date!='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @elseif($initial_date!='none' && $final_date=='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                    @elseif($initial_date=='none' && $final_date!='none')
                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @else
                        | Historial completo
                    @endif
                </small>
            </p>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Fecha de registro</th>
                            <th>Tipo</th>
                            <th>Stock en {{$raw_material->unit}}</th>
                            <th>Cantidad en {{$raw_material->unit}}</th>
                            <th>Precio en $</th>
                            <th>Precio en Bs.S</th>
                            <th>Tasa en Bs.S</th>
                            <th>Concepto</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($raw_material_movements as $raw_material_movement)
                            @if($raw_material_movement->type==2)
                                <tr>
                                    <td>{{$raw_material_movement->created_at->format('d-m-Y h:i:s a')}}</td>
                                    <td>{{$raw_material_movement->type==1?'Entrada':'Salida'}}</td>
                                    <td>{{number_format($raw_material_movement->new_amount, 2, ',', '.')}}</td>
                                    <td>{{number_format($raw_material_movement->amount, 2, ',', '.')}}</td>
                                    <td>{{$raw_material_movement->price==0?'N/A':number_format($raw_material_movement->price, 2, ',', '.')}}</td>
                                    <td>{{$raw_material_movement->price_bss==0?'N/A':number_format($raw_material_movement->price_bss, 2, ',', '.')}}</td>
                                    <td>{{$raw_material_movement->exchange_rate==0?'N/A':number_format($raw_material_movement->exchange_rate, 2, ',', '.')}}</td>
                                    <td>{!!$raw_material_movement->concept!!}</td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <div>
            <p>
                <b>Entradas de {{$product->name}}</b>                       
                <small>
                    @if($initial_date!='none' && $final_date!='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @elseif($initial_date!='none' && $final_date=='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                    @elseif($initial_date=='none' && $final_date!='none')
                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @else
                        | Historial completo
                    @endif
                </small>
            </p>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Fecha de registro</th>
                            <th>Tipo</th>
                            <th>Stock</th>
                            <th>Cantidad</th>
                            <th>Precio en $</th>
                            <th>Precio en Bs.S</th>
                            <th>Tasa en Bs.S</th>
                            <th>Concepto</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($unprocessed_products_movements as $unprocessed_product_movement)
                            @if($unprocessed_product_movement->type==1)
                                <tr>
                                    <td>{{$unprocessed_product_movement->created_at->format('d-m-Y h:i:s a')}}</td>
                                    <td>{{$unprocessed_product_movement->type==1?'Entrada':'Salida'}}</td>
                                    <td>{{number_format($unprocessed_product_movement->new_amount, 0, ',', '.')}}</td>
                                    <td>{{number_format($unprocessed_product_movement->amount, 0, ',', '.')}}</td>
                                    <td>{{$unprocessed_product_movement->price==0?'N/A':number_format($unprocessed_product_movement->price, 2, ',', '.')}}</td>
                                    <td>{{$unprocessed_product_movement->price_bss==0?'N/A':number_format($unprocessed_product_movement->price_bss, 2, ',', '.')}}</td>
                                    <td>{{$unprocessed_product_movement->price==0?'N/A':number_format($unprocessed_product_movement->exchange_rate, 2, ',', '.')}}</td>
                                    <td>{!!$unprocessed_product_movement->concept!!}</td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <p>
                <b>Salidas de {{$product->name}}</b>                       
                <small>
                    @if($initial_date!='none' && $final_date!='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @elseif($initial_date!='none' && $final_date=='none')
                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                    @elseif($initial_date=='none' && $final_date!='none')
                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                    @else
                        | Historial completo
                    @endif
                </small>
            </p>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Fecha de registro</th>
                            <th>Tipo</th>
                            <th>Stock</th>
                            <th>Cantidad</th>
                            <th>Precio en $</th>
                            <th>Precio en Bs.S</th>
                            <th>Tasa en Bs.S</th>
                            <th>Concepto</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($unprocessed_products_movements as $unprocessed_product_movement)
                            @if($unprocessed_product_movement->type==2)
                                <tr>
                                    <td>{{$unprocessed_product_movement->created_at->format('d-m-Y h:i:s a')}}</td>
                                    <td>{{$unprocessed_product_movement->type==1?'Entrada':'Salida'}}</td>
                                    <td>{{number_format($unprocessed_product_movement->new_amount, 0, ',', '.')}}</td>
                                    <td>{{number_format($unprocessed_product_movement->amount, 0, ',', '.')}}</td>
                                    <td>{{$unprocessed_product_movement->price==0?'N/A':number_format($unprocessed_product_movement->price, 2, ',', '.')}}</td>
                                    <td>{{$unprocessed_product_movement->price_bss==0?'N/A':number_format($unprocessed_product_movement->price_bss, 2, ',', '.')}}</td>
                                    <td>{{$unprocessed_product_movement->price==0?'N/A':number_format($unprocessed_product_movement->exchange_rate, 2, ',', '.')}}</td>
                                    <td>{!!$unprocessed_product_movement->concept!!}</td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>