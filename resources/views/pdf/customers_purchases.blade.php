<!DOCTYPE html>
<html>
<head>
	<title>Clientes con mas compras</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
            <table>
                <thead>
                    <tr>
                        <th>Número de cliente</th>
                        <th>Nombre registrado en el teléfono del negocio</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Total gastado en $</th>
                        <th>Compras</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($clients as $client)
                        <tr>
                            <td>{{$client->client_number}}</td>
                            <td>{{$client->name_business_phone}}</td>
                            <td>{{$client->first_name}}</td>
                            <td>{{$client->last_name}}</td>
                            <td>{{number_format($client->total, 2, ',', '.')}}</td>
                            <td>{{number_format($client->purchases, 0, ',', '.')}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
	    </div>

	</div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>