<!DOCTYPE html>
<html>
<head>
	<title>Pagos/cambios de los métodos de pago</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<div>
	    <div>
	        <div>
	            <h4>
	                <img width="50px" src="{{asset($configuration->logo)}}" alt="{{asset($configuration->name)}}">
	                {{$configuration->name}}
	            </h4>
	        </div>
	    </div>
	    <div>
	        <div>
	            <strong>Pagos/cambios de los métodos de pago</strong>
	        </div>
	    </div>
        @foreach($payment_methods as $payment_method)
            <div>
                <p>
                    <b>{{$payment_method->name}}</b>                       
                    <small>
                        @if($initial_date!='none' && $final_date!='none')
                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                        @elseif($initial_date!='none' && $final_date=='none')
                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                        @elseif($initial_date=='none' && $final_date!='none')
                            | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                        @else
                            | Historial completo
                        @endif
                    </small>
                </p>
                @if($payment_method->currency=='$')
                    @if(count($payment_method->payment_method_options)>0)
                        
                        <div>
                            <div>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Bancos o procesadores de pago</th>
                                                <th>Pagos en $</th>
                                                <th>Equivalente en Bs.S</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($payment_method->payment_method_options as $payment_method_option)
                                                <tr>
                                                    <td>{{$payment_method_option->name}}</td>
                                                    <td>
                                                        @if($initial_date!='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                        @elseif($initial_date!='none' && $final_date=='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                        @elseif($initial_date=='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                        @else
                                                            {{number_format($payment_method_option->payments->sum('payment_amount_usd'), 2, ',', '.')}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($initial_date!='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                        @elseif($initial_date!='none' && $final_date=='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                        @elseif($initial_date=='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                        @else
                                                            {{number_format($payment_method_option->payments->sum('payment_amount_bss'), 2, ',', '.')}}
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <th>Total</th>
                                                <th>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->payments->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @endif
                                                </th>
                                                <th>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->payments->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @endif
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Bancos o procesadores de cambios</th>
                                                <th>Cambios en $</th>
                                                <th>Equivalente en Bs.S</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($payment_method->payment_method_options as $payment_method_option)
                                                <tr>
                                                    <td>{{$payment_method_option->name}}</td>
                                                    <td>
                                                        @if($initial_date!='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                        @elseif($initial_date!='none' && $final_date=='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                        @elseif($initial_date=='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                        @else
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->sum('change_amount_usd'), 2, ',', '.')}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($initial_date!='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                        @elseif($initial_date!='none' && $final_date=='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                        @elseif($initial_date=='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                        @else
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->sum('change_amount_bss'), 2, ',', '.')}}
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <th>Total</th>
                                                <th>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @endif
                                                </th>
                                                <th>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @endif
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        @foreach($payment_method->payment_method_options as $payment_method_option)
                            <div>
                                <div>
                                    <p>
                                        <small>
                                            <b>Pagos en {{$payment_method_option->name}}</b>
                                            @if($initial_date!='none' && $final_date!='none')
                                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                            @elseif($initial_date!='none' && $final_date=='none')
                                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                            @elseif($initial_date=='none' && $final_date!='none')
                                                | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                            @else
                                                | Historial completo
                                            @endif
                                        </small>
                                    </p>
                                    <div>
                                        <table>
                                            <thead>                  
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>Pago en $</th>
                                                    <th>Equivalente en Bs.S</th>
                                                    <th>Cliente</th>
                                                    <th>Venta</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @if($initial_date!='none' && $final_date!='none')

                                                    @foreach($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $payment)
                                                        <tr>
                                                            <td>
                                                                {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$payment->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$payment->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @elseif($initial_date!='none' && $final_date=='none')

                                                    @foreach($payment_method_option->payments->where('created_at','>=',$initial_date) as $payment)
                                                        <tr>
                                                            <td>
                                                                {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$payment->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$payment->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @elseif($initial_date=='none' && $final_date!='none')

                                                    @foreach($payment_method_option->payments->where('created_at','<=',$final_date) as $payment)
                                                        <tr>
                                                            <td>
                                                                {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$payment->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$payment->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @else

                                                    @foreach($payment_method_option->payments as $payment)
                                                        <tr>
                                                            <td>
                                                                {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$payment->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$payment->sale_id}}
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div>
                                    <p>
                                        <small>
                                            <b>Cambios en {{$payment_method_option->name}}</b>
                                            @if($initial_date!='none' && $final_date!='none')
                                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                            @elseif($initial_date!='none' && $final_date=='none')
                                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                            @elseif($initial_date=='none' && $final_date!='none')
                                                | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                            @else
                                                | Historial completo
                                            @endif
                                        </small>
                                    </p>
                                    <div>
                                        <table>
                                            <thead>                  
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>Cambio en $</th>
                                                    <th>Equivalente en Bs.S</th>
                                                    <th>Cliente</th>
                                                    <th>Venta</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @if($initial_date!='none' && $final_date!='none')

                                                    @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('change_amount_usd','<>',0) as $change)
                                                        <tr>
                                                            <td>
                                                                {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$change->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$change->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @elseif($initial_date!='none' && $final_date=='none')

                                                    @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('change_amount_usd','<>',0) as $change)
                                                        <tr>
                                                            <td>
                                                                {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$change->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$change->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @elseif($initial_date=='none' && $final_date!='none')

                                                    @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('change_amount_usd','<>',0) as $change)
                                                        <tr>
                                                            <td>
                                                                {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$change->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$change->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @else

                                                    @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('change_amount_usd','<>',0) as $change)
                                                        <tr>
                                                            <td>
                                                                {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$change->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$change->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    @else

                        <div>
                            <div>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Pagos en $</th>
                                                <th>Equivalente en Bs.S</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->payments->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->payments->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Cambios en $</th>
                                                <th>Equivalente en Bs.S</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('pay_the_customer',0)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('pay_the_customer',0)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div>
                                <p>
                                    <small>
                                        <b>Pagos en $</b>
                                        @if($initial_date!='none' && $final_date!='none')
                                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                        @elseif($initial_date!='none' && $final_date=='none')
                                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                        @elseif($initial_date=='none' && $final_date!='none')
                                            | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                        @else
                                            | Historial completo
                                        @endif
                                    </small>
                                </p>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Fecha</th>
                                                <th>Pago en $</th>
                                                <th>Equivalente en Bs.S</th>
                                                <th>Cliente</th>
                                                <th>Venta</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @if($initial_date!='none' && $final_date!='none')

                                                @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $payment)
                                                    <tr>
                                                        <td>
                                                            {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$payment->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$payment->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @elseif($initial_date!='none' && $final_date=='none')

                                                @foreach($payment_method->payments->where('created_at','>=',$initial_date) as $payment)
                                                    <tr>
                                                        <td>
                                                            {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$payment->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$payment->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @elseif($initial_date=='none' && $final_date!='none')

                                                @foreach($payment_method->payments->where('created_at','<=',$final_date) as $payment)
                                                    <tr>
                                                        <td>
                                                            {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$payment->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$payment->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @else

                                                @foreach($payment_method->payments as $payment)
                                                    <tr>
                                                        <td>
                                                            {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$payment->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$payment->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div>
                                <p>
                                    <small>
                                        <b>Cambios en $</b>
                                        @if($initial_date!='none' && $final_date!='none')
                                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                        @elseif($initial_date!='none' && $final_date=='none')
                                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                        @elseif($initial_date=='none' && $final_date!='none')
                                            | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                        @else
                                            | Historial completo
                                        @endif
                                    </small>
                                </p>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Fecha</th>
                                                <th>Cambio en $</th>
                                                <th>Equivalente en Bs.S</th>
                                                <th>Cliente</th>
                                                <th>Venta</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @if($initial_date!='none' && $final_date!='none')

                                                @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                    <tr>
                                                        <td>
                                                            {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$change->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$change->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @elseif($initial_date!='none' && $final_date=='none')

                                                @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                    <tr>
                                                        <td>
                                                            {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$change->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$change->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @elseif($initial_date=='none' && $final_date!='none')

                                                @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                    <tr>
                                                        <td>
                                                            {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$change->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$change->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @else

                                                @foreach($payment_method->changes->where('pay_the_customer',0)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                    <tr>
                                                        <td>
                                                            {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$change->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$change->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        @php 
                            $sales_usd_total=0; 
                            $sales_bss_total=0; 
                        @endphp

                        @if($initial_date!='none' && $final_date!='none')
                            
                            @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null) as $sale_payment)

                                @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                    @php 
                                        $sales_usd_total+=$sale_payment->sale->total;
                                        $sales_bss_total+=$sale_payment->sale->total_bss; 
                                    @endphp
                                @else

                                    @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                        @php
                                            $sales_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                            $sales_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                        @endphp
                                    @else
                                        @php
                                            $sales_usd_total+=($sale_payment->payment_amount_usd);
                                            $sales_bss_total+=($sale_payment->payment_amount_bss);
                                        @endphp
                                    @endif

                                @endif  

                            @endforeach

                        @elseif($initial_date!='none' && $final_date=='none')

                            @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('sale_id','<>',null) as $sale_payment)

                                @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                    @php 
                                        $sales_usd_total+=$sale_payment->sale->total;
                                        $sales_bss_total+=$sale_payment->sale->total_bss; 
                                    @endphp
                                @else

                                    @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                        @php
                                            $sales_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                            $sales_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                        @endphp
                                    @else
                                        @php
                                            $sales_usd_total+=($sale_payment->payment_amount_usd);
                                            $sales_bss_total+=($sale_payment->payment_amount_bss);
                                        @endphp
                                    @endif

                                @endif  

                            @endforeach

                        @elseif($initial_date=='none' && $final_date!='none')

                            @foreach($payment_method->payments->where('created_at','<=',$final_date)->where('sale_id','<>',null) as $sale_payment)

                                @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                    @php 
                                        $sales_usd_total+=$sale_payment->sale->total;
                                        $sales_bss_total+=$sale_payment->sale->total_bss; 
                                    @endphp
                                @else

                                    @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                        @php
                                            $sales_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                            $sales_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                        @endphp
                                    @else
                                        @php
                                            $sales_usd_total+=($sale_payment->payment_amount_usd);
                                            $sales_bss_total+=($sale_payment->payment_amount_bss);
                                        @endphp
                                    @endif

                                @endif  

                            @endforeach

                        @else

                            @foreach($payment_method->payments->where('sale_id','<>',null) as $sale_payment)

                                @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                    @php 
                                        $sales_usd_total+=$sale_payment->sale->total;
                                        $sales_bss_total+=$sale_payment->sale->total_bss; 
                                    @endphp
                                @else

                                    @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                        @php
                                            $sales_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                            $sales_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                        @endphp
                                    @else
                                        @php
                                            $sales_usd_total+=($sale_payment->payment_amount_usd);
                                            $sales_bss_total+=($sale_payment->payment_amount_bss);
                                        @endphp
                                    @endif

                                @endif  

                            @endforeach

                        @endif

                        <br>
                        <div>
                            <div>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Ingresos netos de ventas en $</th>
                                                <th>Equivalente en Bs.S</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    {{number_format($sales_usd_total, 2, ',', '.')}}
                                                </td>
                                                <td>
                                                    {{number_format($sales_bss_total, 2, ',', '.')}}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    @endif
                @elseif($payment_method->currency=='Bs.S')
                    @if(count($payment_method->payment_method_options)>0)
                        
                        <div>
                            <div>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Bancos o procesadores de pago</th>
                                                <th>Pagos en Bs.S</th>
                                                <th>Equivalente en $</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($payment_method->payment_method_options as $payment_method_option)
                                                <tr>
                                                    <td>{{$payment_method_option->name}}</td>
                                                    <td>
                                                        @if($initial_date!='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                        @elseif($initial_date!='none' && $final_date=='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                        @elseif($initial_date=='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                        @else
                                                            {{number_format($payment_method_option->payments->sum('payment_amount_bss'), 2, ',', '.')}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($initial_date!='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                        @elseif($initial_date!='none' && $final_date=='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                        @elseif($initial_date=='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->payments->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                        @else
                                                            {{number_format($payment_method_option->payments->sum('payment_amount_usd'), 2, ',', '.')}}
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <th>Total</th>
                                                <th>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->payments->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @endif
                                                </th>
                                                <th>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->payments->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @endif
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Bancos o procesadores de cambios</th>
                                                <th>Cambios en Bs.S</th>
                                                <th>Equivalente en $</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($payment_method->payment_method_options as $payment_method_option)
                                                <tr>
                                                    <td>{{$payment_method_option->name}}</td>
                                                    <td>
                                                        @if($initial_date!='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                        @elseif($initial_date!='none' && $final_date=='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                        @elseif($initial_date=='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                        @else
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->sum('change_amount_bss'), 2, ',', '.')}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($initial_date!='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                        @elseif($initial_date!='none' && $final_date=='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                        @elseif($initial_date=='none' && $final_date!='none')
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                        @else
                                                            {{number_format($payment_method_option->changes->where('pay_the_customer',0)->sum('change_amount_usd'), 2, ',', '.')}}
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <th>Total</th>
                                                <th>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @endif
                                                </th>
                                                <th>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @endif
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        @foreach($payment_method->payment_method_options as $payment_method_option)
                            <div>
                                <div>
                                    <p>
                                        <small>
                                            <b>Pagos en {{$payment_method_option->name}}</b>
                                            @if($initial_date!='none' && $final_date!='none')
                                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                            @elseif($initial_date!='none' && $final_date=='none')
                                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                            @elseif($initial_date=='none' && $final_date!='none')
                                                | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                            @else
                                                | Historial completo
                                            @endif
                                        </small>
                                    </p>
                                    <div>
                                        <table>
                                            <thead>                  
                                                <tr>
                                                    <th>Fecha</th>
                                                    @if($payment_method->id==1 || $payment_method->id==2 || $payment_method->id==11)
                                                        <th>Nro. de referencia</th>
                                                    @endif
                                                    <th>Pago en Bs.S</th>
                                                    <th>Equivalente en $</th>
                                                    <th>Cliente</th>
                                                    <th>Venta</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @if($initial_date!='none' && $final_date!='none')

                                                    @foreach($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $payment)
                                                        <tr>
                                                            <td>
                                                                {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            @if($payment_method->id==1 || $payment_method->id==2 || $payment_method->id==11)
                                                                <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                            @endif
                                                            <td>
                                                                {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$payment->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$payment->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @elseif($initial_date!='none' && $final_date=='none')

                                                    @foreach($payment_method_option->payments->where('created_at','>=',$initial_date) as $payment)
                                                        <tr>
                                                            <td>
                                                                {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            @if($payment_method->id==1 || $payment_method->id==2 || $payment_method->id==11)
                                                                <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                            @endif
                                                            <td>
                                                                {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$payment->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$payment->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @elseif($initial_date=='none' && $final_date!='none')

                                                    @foreach($payment_method_option->payments->where('created_at','<=',$final_date) as $payment)
                                                        <tr>
                                                            <td>
                                                                {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            @if($payment_method->id==1 || $payment_method->id==2 || $payment_method->id==11)
                                                                <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                            @endif
                                                            <td>
                                                                {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$payment->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$payment->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @else

                                                    @foreach($payment_method_option->payments as $payment)
                                                        <tr>
                                                            <td>
                                                                {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            @if($payment_method->id==1 || $payment_method->id==2 || $payment_method->id==11)
                                                                <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                            @endif
                                                            <td>
                                                                {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$payment->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$payment->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div>
                                    <p>
                                        <small>
                                            <b>Cambios en {{$payment_method_option->name}}</b>
                                            @if($initial_date!='none' && $final_date!='none')
                                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                            @elseif($initial_date!='none' && $final_date=='none')
                                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                            @elseif($initial_date=='none' && $final_date!='none')
                                                | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                            @else
                                                | Historial completo
                                            @endif
                                        </small>
                                    </p>
                                    <div>
                                        <table>
                                            <thead>                  
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>Cambio en Bs.S</th>
                                                    <th>Equivalente en $</th>
                                                    <th>Cliente</th>
                                                    <th>Venta</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @if($initial_date!='none' && $final_date!='none')

                                                    @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('change_amount_usd','<>',0) as $change)
                                                        <tr>
                                                            <td>
                                                                {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$change->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$change->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @elseif($initial_date!='none' && $final_date=='none')

                                                    @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('change_amount_usd','<>',0) as $change)
                                                        <tr>
                                                            <td>
                                                                {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$change->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$change->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @elseif($initial_date=='none' && $final_date!='none')

                                                    @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('change_amount_usd','<>',0) as $change)
                                                        <tr>
                                                            <td>
                                                                {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$change->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$change->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @else

                                                    @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('change_amount_usd','<>',0) as $change)
                                                        <tr>
                                                            <td>
                                                                {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                            </td>
                                                            <td>
                                                                {{$change->sale->client->name_business_phone}}
                                                            </td>
                                                            <td>
                                                                ID de venta: {{$change->sale_id}}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    @else

                        <div>
                            <div>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Pagos en Bs.S</th>
                                                <th>Equivalente en $</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->payments->sum('payment_amount_bss'), 2, ',', '.')}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->payments->sum('payment_amount_usd'), 2, ',', '.')}}
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Cambios en Bs.S</th>
                                                <th>Equivalente en $</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('pay_the_customer',0)->sum('change_amount_bss'), 2, ',', '.')}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($initial_date!='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date!='none' && $final_date=='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @elseif($initial_date=='none' && $final_date!='none')
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @else
                                                        {{number_format($payment_method->changes->where('pay_the_customer',0)->where('pay_the_customer',0)->sum('change_amount_usd'), 2, ',', '.')}}
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div>
                                <p>
                                    <small>
                                        <b>Pagos en Bs.S</b>
                                        @if($initial_date!='none' && $final_date!='none')
                                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                        @elseif($initial_date!='none' && $final_date=='none')
                                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                        @elseif($initial_date=='none' && $final_date!='none')
                                            | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                        @else
                                            | Historial completo
                                        @endif
                                    </small>
                                </p>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Fecha</th>
                                                @if($payment_method->id==11)
                                                    <th>Nro. de referencia</th>
                                                @endif
                                                <th>Pago en Bs.S</th>
                                                <th>Equivalente en $</th>
                                                <th>Cliente</th>
                                                <th>Venta</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @if($initial_date!='none' && $final_date!='none')

                                                @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $payment)
                                                    <tr>
                                                        <td>
                                                            {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        @if($payment_method->id==11)
                                                            <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                        @endif
                                                        <td>
                                                            {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$payment->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$payment->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @elseif($initial_date!='none' && $final_date=='none')

                                                @foreach($payment_method->payments->where('created_at','>=',$initial_date) as $payment)
                                                    <tr>
                                                        <td>
                                                            {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        @if($payment_method->id==11)
                                                            <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                        @endif
                                                        <td>
                                                            {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$payment->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$payment->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @elseif($initial_date=='none' && $final_date!='none')

                                                @foreach($payment_method->payments->where('created_at','<=',$final_date) as $payment)
                                                    <tr>
                                                        <td>
                                                            {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        @if($payment_method->id==11)
                                                            <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                        @endif
                                                        <td>
                                                            {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$payment->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$payment->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @else

                                                @foreach($payment_method->payments as $payment)
                                                    <tr>
                                                        <td>
                                                            {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        @if($payment_method->id==11)
                                                            <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                        @endif
                                                        <td>
                                                            {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$payment->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$payment->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div>
                                <p>
                                    <small>
                                        <b>Cambios en Bs.S</b>
                                        @if($initial_date!='none' && $final_date!='none')
                                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                        @elseif($initial_date!='none' && $final_date=='none')
                                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                        @elseif($initial_date=='none' && $final_date!='none')
                                            | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                        @else
                                            | Historial completo
                                        @endif
                                    </small>
                                </p>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Fecha</th>
                                                <th>Cambio en Bs.S</th>
                                                <th>Equivalente en $</th>
                                                <th>Cliente</th>
                                                <th>Venta</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @if($initial_date!='none' && $final_date!='none')

                                                @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                    <tr>
                                                        <td>
                                                            {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$change->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$change->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @elseif($initial_date!='none' && $final_date=='none')

                                                @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                    <tr>
                                                        <td>
                                                            {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$change->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$change->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @elseif($initial_date=='none' && $final_date!='none')

                                                @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                    <tr>
                                                        <td>
                                                            {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$change->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$change->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @else

                                                @foreach($payment_method->changes->where('pay_the_customer',0)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                    <tr>
                                                        <td>
                                                            {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{$change->sale->client->name_business_phone}}
                                                        </td>
                                                        <td>
                                                            ID de venta: {{$change->sale_id}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        @php 
                            $sales_bss_usd_total=0; 
                            $sales_bss_bss_total=0; 
                        @endphp

                        @if($initial_date!='none' && $final_date!='none')
                            
                            @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null) as $sale_payment)

                                @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                    @php 
                                        $sales_bss_usd_total+=$sale_payment->sale->total;
                                        $sales_bss_bss_total+=$sale_payment->sale->total_bss; 
                                    @endphp
                                @else

                                    @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                        @php
                                            $sales_bss_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                            $sales_bss_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                        @endphp
                                    @else
                                        @php
                                            $sales_bss_usd_total+=($sale_payment->payment_amount_usd);
                                            $sales_bss_bss_total+=($sale_payment->payment_amount_bss);
                                        @endphp
                                    @endif

                                @endif 

                            @endforeach

                        @elseif($initial_date!='none' && $final_date=='none')

                            @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('sale_id','<>',null) as $sale_payment)

                                @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                    @php 
                                        $sales_bss_usd_total+=$sale_payment->sale->total;
                                        $sales_bss_bss_total+=$sale_payment->sale->total_bss; 
                                    @endphp
                                @else

                                    @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                        @php
                                            $sales_bss_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                            $sales_bss_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                        @endphp
                                    @else
                                        @php
                                            $sales_bss_usd_total+=($sale_payment->payment_amount_usd);
                                            $sales_bss_bss_total+=($sale_payment->payment_amount_bss);
                                        @endphp
                                    @endif

                                @endif  

                            @endforeach

                        @elseif($initial_date=='none' && $final_date!='none')

                            @foreach($payment_method->payments->where('created_at','<=',$final_date)->where('sale_id','<>',null) as $sale_payment)

                                @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                    @php 
                                        $sales_bss_usd_total+=$sale_payment->sale->total;
                                        $sales_bss_bss_total+=$sale_payment->sale->total_bss; 
                                    @endphp
                                @else

                                    @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                        @php
                                            $sales_bss_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                            $sales_bss_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                        @endphp
                                    @else
                                        @php
                                            $sales_bss_usd_total+=($sale_payment->payment_amount_usd);
                                            $sales_bss_bss_total+=($sale_payment->payment_amount_bss);
                                        @endphp
                                    @endif

                                @endif  

                            @endforeach

                        @else

                            @foreach($payment_method->payments->where('sale_id','<>',null) as $sale_payment)

                                @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                    @php 
                                        $sales_bss_usd_total+=$sale_payment->sale->total;
                                        $sales_bss_bss_total+=$sale_payment->sale->total_bss; 
                                    @endphp
                                @else

                                    @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                        @php
                                            $sales_bss_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                            $sales_bss_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                        @endphp
                                    @else
                                        @php
                                            $sales_bss_usd_total+=($sale_payment->payment_amount_usd);
                                            $sales_bss_bss_total+=($sale_payment->payment_amount_bss);
                                        @endphp
                                    @endif

                                @endif 

                            @endforeach

                        @endif

                        <br>
                        <div>
                            <div>
                                <div>
                                    <table>
                                        <thead>                  
                                            <tr>
                                                <th>Ingresos netos de ventas en Bs.S</th>
                                                <th>Equivalente en $</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    {{number_format($sales_bss_bss_total, 2, ',', '.')}}
                                                </td>
                                                <td>
                                                    {{number_format($sales_bss_usd_total, 2, ',', '.')}}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    @endif
                @endif
            </div>
        @endforeach
	</div>
</body>
</html>
<style type="text/css">
    * {
        font-family: sans-serif !important;
        font-size: 14px !important;
    }
    hr {
        height: 1px !important;
        background: black !important;
    }
    table, th, td {
        border: 0.1px solid gray !important;
        width: 100% !important;
        text-align: center !important;
        padding: 0px !important;
        margin: 0px !important;
        border-collapse: collapse !important;
    }
</style>