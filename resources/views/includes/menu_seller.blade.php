<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <a href="{{route('dashboard')}}" class="nav-link {{(isset($menu_active) && $menu_active=='dashboard')?'active':''}}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_clients')}}" class="nav-link {{(isset($menu_active) && $menu_active=='clients')?'active':''}}">
                <i class="nav-icon fas fa-address-book"></i>
                <p>
                    Clientes
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('sales')}}" class="nav-link {{(isset($menu_active) && $menu_active=='sales')?'active':''}}">
                <i class="nav-icon fas fa-store"></i>
                <p>
                    Ventas
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('payment_collections')}}" class="nav-link {{(isset($menu_active) && $menu_active=='payment_collections')?'active':''}}">
                <i class="nav-icon fas fa-handshake"></i>
                <p>
                    Cobranzas
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('cash_register')}}" class="nav-link {{(isset($menu_active) && $menu_active=='cash_register')?'active':''}}">
                <i class="nav-icon fas fa-cash-register"></i>
                <p>
                    Caja
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_reports')}}" class="nav-link {{(isset($menu_active) && $menu_active=='reports')?'active':''}}">
                <i class="nav-icon fas fa-chart-line"></i>
                <p>
                    Reportes
                </p>
            </a>
        </li>
        <li class="nav-header">Otros</li>
        <li class="nav-item">
            <a href="{{route('profile')}}" class="nav-link {{(isset($menu_active) && $menu_active=='profile')?'active':''}}">
                <i class="nav-icon fas fa-user"></i>
                <p>
                    Mi perfil
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('logout')}}" class="nav-link">
                <i class="nav-icon fas fa-sign-in-alt"></i>
                <p>
                    Cerrar sesión
                </p>
            </a>
        </li>
    </ul>
</nav>