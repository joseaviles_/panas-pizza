<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <a href="{{route('dashboard')}}" class="nav-link {{(isset($menu_active) && $menu_active=='dashboard')?'active':''}}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_users')}}" class="nav-link {{(isset($menu_active) && $menu_active=='users')?'active':''}}">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Usuarios
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_workers')}}" class="nav-link {{(isset($menu_active) && $menu_active=='workers')?'active':''}}">
                <i class="nav-icon fas fa-briefcase"></i>
                <p>
                    Trabajadores
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_clients')}}" class="nav-link {{(isset($menu_active) && $menu_active=='clients')?'active':''}}">
                <i class="nav-icon fas fa-address-book"></i>
                <p>
                    Clientes
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_categories')}}" class="nav-link {{(isset($menu_active) && $menu_active=='categories')?'active':''}}">
                <i class="nav-icon fas fa-list-alt"></i>
                <p>
                    Categorías
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_raw_material')}}" class="nav-link {{(isset($menu_active) && $menu_active=='raw_material')?'active':''}}">
                <i class="nav-icon fas fa-shopping-basket"></i>
                <p>
                    Materias primas
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_products')}}" class="nav-link {{(isset($menu_active) && $menu_active=='products')?'active':''}}">
                <i class="nav-icon fas fa-pizza-slice"></i>
                <p>
                    Productos
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_promotions')}}" class="nav-link {{(isset($menu_active) && $menu_active=='promotions')?'active':''}}">
                <i class="nav-icon fas fa-gifts"></i>
                <p>
                    Promociones
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_inventory')}}" class="nav-link {{(isset($menu_active) && $menu_active=='inventory')?'active':''}}">
                <i class="nav-icon fas fa-dolly-flatbed"></i>
                <p>
                    Inventario
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_payment_methods')}}" class="nav-link {{(isset($menu_active) && $menu_active=='payment_methods')?'active':''}}">
                <i class="nav-icon fas fa-file-invoice-dollar"></i>
                <p>
                    Métodos de pago
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_delivery_companies')}}" class="nav-link {{(isset($menu_active) && $menu_active=='delivery_companies')?'active':''}}">
                <i class="nav-icon fas fa-shipping-fast"></i>
                <p>
                    Empresas de delivery
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('sales')}}" class="nav-link {{(isset($menu_active) && $menu_active=='sales')?'active':''}}">
                <i class="nav-icon fas fa-store"></i>
                <p>
                    Ventas
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('discount_sales')}}" class="nav-link {{(isset($menu_active) && $menu_active=='discount_sales')?'active':''}}">
                <i class="nav-icon fas fa-percentage"></i>
                <p>
                    Ventas con descuento
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('payment_collections')}}" class="nav-link {{(isset($menu_active) && $menu_active=='payment_collections')?'active':''}}">
                <i class="nav-icon fas fa-handshake"></i>
                <p>
                    Cobranzas
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('cash_register')}}" class="nav-link {{(isset($menu_active) && $menu_active=='cash_register')?'active':''}}">
                <i class="nav-icon fas fa-cash-register"></i>
                <p>
                    Caja
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_reports')}}" class="nav-link {{(isset($menu_active) && $menu_active=='reports')?'active':''}}">
                <i class="nav-icon fas fa-chart-line"></i>
                <p>
                    Reportes
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin_trackings')}}" class="nav-link {{(isset($menu_active) && $menu_active=='trackings')?'active':''}}">
                <i class="nav-icon fas fa-list-ol"></i>
                <p>
                    Seguimientos
                </p>
            </a>
        </li>
        <li class="nav-header">Otros</li>
        <li class="nav-item">
            <a href="{{route('profile')}}" class="nav-link {{(isset($menu_active) && $menu_active=='profile')?'active':''}}">
                <i class="nav-icon fas fa-user"></i>
                <p>
                    Mi perfil
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('configuration')}}" class="nav-link {{(isset($menu_active) && $menu_active=='configuration')?'active':''}}">
                <i class="nav-icon fas fa-cog"></i>
                <p>
                    Configuración
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('logout')}}" class="nav-link">
                <i class="nav-icon fas fa-sign-in-alt"></i>
                <p>
                    Cerrar sesión
                </p>
            </a>
        </li>
    </ul>
</nav>