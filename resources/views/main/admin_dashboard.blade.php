@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-tachometer-alt"></i> Dashboard</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-danger">
                <div class="card-header">
                    <h3 class="card-title">Productos disponibles que requieren materia prima</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body p-2">
                    <div class="table-responsive">
                        <table id="table-list" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Cantidad disponible</th>
                                    <th>Categoría</th>
                                    <th>Materia prima</th>
                                    <th>Precio en $</th>
                                    <th>Precio en Bs.S</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
          		<div class="col-md-6">
		            <div class="card card-primary">
		                <div class="card-header">
		                    <h3 class="card-title">Tasa del día</h3>
		                </div>
		                <form action="{{route('admin_exchange_rate')}}" method="post">
		                    @csrf
		                    <div class="card-body">
		                        <div class="form-group">
		                            <div class="row">
		                                <div class="col-12">              
		                                    <div class="input-group mb-3">
		                                        <input id="exchange_rate" name="exchange_rate" type="text" class="form-control" placeholder="Ingresa la tasa del día" value="{{$configuration==null?'':$configuration->exchange_rate}}" required>
		                                        <div class="input-group-append">
		                                            <div class="input-group-text">
		                                                <span><b>Bs.S</b></span>
		                                            </div>
		                                        </div>
		                                    </div>
											@if($configuration)
												<p class="text-muted"><small>$1,00 equivale a Bs.S {{number_format($configuration->exchange_rate, 2, ',', '.')}} hoy {{date('d-m-Y h:i:s a')}}</small></p>
											@endif
		                                </div>
		                            </div>
		                        </div>
		                        <button type="submit" class="btn btn-primary">Actualizar</button>
		                    </div>
		                </form>
		            </div>
          		</div>
          	</div>
        </div>
    </section>
@endsection

@section("scripts")
	<script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script>
        $(document).ready(function() {
        	$('#exchange_rate').autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            $("#table-list").DataTable( {
                "ajax": "{{route('get_admin_elaborable_products')}}",
                "columnDefs": [{ "orderable": false, "targets": -1 }, { "orderable": false, "targets": -4 }],
                "order": [[ 1, "desc" ]],
                "iDisplayLength": 10,
                "language": {
                    "sProcessing":     "Procesando productos...",
                    "sLengthMenu":     "Mostrar _MENU_ productos",
                    "sZeroRecords":    "No se encontraron productos",
                    "sEmptyTable":     "Ningún producto disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ productos de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ningún producto",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ productos)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando productos...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );

        });
    </script>
@endsection