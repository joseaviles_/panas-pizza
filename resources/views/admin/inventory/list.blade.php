@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-dolly-flatbed"></i> Inventario</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr>
            <h5><i class="nav-icon fas fa-shopping-basket"></i> Materias primas</h5>
            <div class="table-responsive">
                <table id="table-raw-material" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>Nombre</th>
                            <th>Unidad</th>
                            <th>Stock</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <hr>
            <h5><i class="nav-icon fas fa-pizza-slice"></i> Productos sin elaboración</h5>
            <div class="table-responsive">
                <table id="table-products" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>Nombre</th>
                            <th>Categoría</th>
                            <th>Stock</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            $("#table-raw-material").DataTable( {
                "ajax": "{{route('get_raw_material_admin_inventory')}}",
                "columnDefs": [{ "orderable": false, "targets": -1 }],
                "iDisplayLength": 25,
                "language": {
                    "sProcessing":     "Procesando materias primas...",
                    "sLengthMenu":     "Mostrar _MENU_ materias primas",
                    "sZeroRecords":    "No se encontraron materias primas",
                    "sEmptyTable":     "Ninguna materia prima disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ materias primas de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ninguna materia prima",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ materias primas)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando materias primas...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );

            $("#table-products").DataTable( {
                "ajax": "{{route('get_unprocessed_products_admin_inventory')}}",
                "columnDefs": [{ "orderable": false, "targets": -1 }],
                "iDisplayLength": 25,
                "language": {
                    "sProcessing":     "Procesando productos...",
                    "sLengthMenu":     "Mostrar _MENU_ productos",
                    "sZeroRecords":    "No se encontraron productos",
                    "sEmptyTable":     "Ningún producto disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ productos de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ningún producto",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ productos)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando productos...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );

        });
    </script>
@endsection