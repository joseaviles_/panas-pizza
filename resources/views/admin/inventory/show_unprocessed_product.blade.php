@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-dolly-flatbed"></i> Inventario</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_inventory')}}">Listado</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Información del producto sin elaboración</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <div class="row">
                            <!--<div class="col-12 col-sm-4 col-md-4">              
                                <small class="text-muted">Foto</small>
                                <div class="input-group mb-3">
                                    <img src="{{asset($product->photo)}}" class="rounded mx-auto" width="100%">
                                </div>
                            </div>-->
                            <div class="col-12 col-sm-3 col-md-3">              
                                <small class="text-muted">Nombre</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" value="{{$product->name}}" readonly>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">
                                <small class="text-muted">Categoría</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" value="{{$product->category->name}}" readonly>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-2 col-md-2">
                                <small class="text-muted">Precio en $</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" value="{{number_format($product->price, 2, ',', '.')}}" readonly>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-dollar-sign"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-2 col-md-2">
                                <small class="text-muted">Stock</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" value="{{number_format($product->amount, 0, ',', '.')}}" readonly>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-12 col-sm-2 col-md-2">              
                                <small class="text-muted">Tipo</small>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" value="{{$product->type==1?'Sin elaboración':'Requiere materia prima'}}" readonly>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Movimientos del stock</h3>
                </div>
                <div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="table-list" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>ID</th>
                                        <th>Fecha de registro</th>
                                        <th>Tipo</th>
                                        <th>Stock</th>
                                        <th>Cantidad</th>
                                        <th>Precio en $</th>
                                        <th>Precio en Bs.S</th>
                                        <th>Tasa en Bs.S</th>
                                        <th>Concepto</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title">Registrar entrada</h3>
                        </div>
                        <div>
                            <form class="card-body" action="{{route('admin_unprocessed_products_movements_post_create')}}" method="post">
                                @csrf
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">              
                                            <small class="text-muted">* Cantidad</small>
                                            <div class="input-group mb-3">
                                                <input name="amount" id="amount" type="text" class="form-control" placeholder="Cantidad" value="{{old('amount')}}" autocomplete="off" required>
                                                <div class="input-group-append">
                                                    <div class="input-group-text">
                                                        <span class="fas fa-info"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">              
                                            <small class="text-muted">* Precio en $</small>
                                            <div class="input-group mb-3">
                                                <input name="price" id="price" type="text" class="form-control" placeholder="Precio en $" value="{{old('price')}}" autocomplete="off" required>
                                                <div class="input-group-append">
                                                    <div class="input-group-text">
                                                        <span class="fas fa-dollar-sign"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">              
                                            <small class="text-muted">* Precio en Bs.S</small>
                                            <div class="input-group mb-3">
                                                <input name="price_bss" id="price_bss" type="text" class="form-control" placeholder="Precio en Bs.S" value="{{old('price_bss')}}" autocomplete="off" required>
                                                <div class="input-group-append">
                                                    <div class="input-group-text">
                                                        <span><b>Bs.S</b></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">              
                                            <small class="text-muted">* Tasa del día</small>
                                            <div class="input-group mb-3">
                                                <input name="exchange_rate" id="exchange_rate" type="text" class="form-control" placeholder="Precio en Bs.S" value="{{$configuration->exchange_rate}}" autocomplete="off" required>
                                                <div class="input-group-append">
                                                    <div class="input-group-text">
                                                        <span><b>Bs.S</b></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success">Registrar</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6">            
                    <div class="card card-danger">
                        <div class="card-header">
                            <h3 class="card-title">Eliminar cantidad del inventario</h3>
                        </div>
                        <div>
                            <form class="card-body" action="{{route('admin_unprocessed_products_movements_adjust_inventory')}}" method="post">
                                @csrf
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">              
                                            <small class="text-muted">* Cantidad</small>
                                            <div class="input-group mb-3">
                                                <input name="amount" id="amount_adjust_inventory" type="text" class="form-control" placeholder="Cantidad" value="{{old('amount')}}" autocomplete="off" required>
                                                <div class="input-group-append">
                                                    <div class="input-group-text">
                                                        <span class="fas fa-info"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="text-muted"><small><b>Disponible en stock: </b>{{number_format($product->amount, 0, ',', '.')}}</small></p>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script>
        $(document).ready(function() {

            $("#amount").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "0"});
            $("#amount_adjust_inventory").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "0"});
            $("#price").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});
            $("#price_bss").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});
            $("#exchange_rate").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            $('#price').keyup(function () {
                var re=new RegExp(escapeRegExp('.'),'g');
                var exchange_rate=$('#exchange_rate').val().replace(re,'');
                var exchange_rate=exchange_rate.replace(/,/g, ".");
                exchange_rate=parseFloat(exchange_rate);

                var price=$(this).val().replace(re,'');
                var price=price.replace(/,/g, ".");
                price=parseFloat(price);
                price*=exchange_rate;
                price=formatNumber(price.toFixed(2));
                $("#price_bss").val(price);
            });

            $('#price_bss').keyup(function () {
                var re=new RegExp(escapeRegExp('.'),'g');
                var exchange_rate=$('#exchange_rate').val().replace(re,'');
                var exchange_rate=exchange_rate.replace(/,/g, ".");
                exchange_rate=parseFloat(exchange_rate);
                
                var price_bss=$(this).val().replace(re,'');
                var price_bss=price_bss.replace(/,/g, ".");
                price_bss=parseFloat(price_bss);
                price_bss/=exchange_rate;
                price_bss=formatNumber(price_bss.toFixed(2));
                $("#price").val(price_bss);
            });

            $('#exchange_rate').keyup(function () {
                var re=new RegExp(escapeRegExp('.'),'g');
                var exchange_rate=$(this).val().replace(re,'');
                var exchange_rate=exchange_rate.replace(/,/g, ".");
                exchange_rate=parseFloat(exchange_rate);

                var price=$('#price').val().replace(re,'');
                var price=price.replace(/,/g, ".");
                price=parseFloat(price);
                price*=exchange_rate;
                price=formatNumber(price.toFixed(2));
                $("#price_bss").val(price);

                var price_bss=$('#price_bss').val().replace(re,'');
                var price_bss=price_bss.replace(/,/g, ".");
                price_bss=parseFloat(price_bss);
                price_bss/=exchange_rate;
                price_bss=formatNumber(price_bss.toFixed(2));
                $("#price").val(price_bss);

            });

            function escapeRegExp(string) {
                return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); 
            }

            function formatNumber(num) {
                return num.toString().replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1,$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
            }

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            $("#table-list").DataTable( {
                "ajax": "{{route('get_admin_unprocessed_products_movements',['product_id'=>$product->id])}}",
                "columnDefs": [{ "orderable": false, "targets": -1 }],
                "order": [[ 1, "desc" ]],
                "iDisplayLength": 10,
                "language": {
                    "sProcessing":     "Procesando movimientos...",
                    "sLengthMenu":     "Mostrar _MENU_ movimientos",
                    "sZeroRecords":    "No se encontraron movimientos",
                    "sEmptyTable":     "Ningún movimiento disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ movimientos de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ningún movimiento",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ movimientos)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando movimientos...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );

        });
    </script>
@endsection