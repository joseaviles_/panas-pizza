@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-dolly-flatbed"></i> Inventario</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_inventory_show_raw_material',['raw_material_id'=>$raw_material_movement->raw_material_id])}}">Volver a inventario de <b>{{$raw_material_movement->raw_material->name}}</b></a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-primary btn-sm" href="{{route('admin_raw_material_movements_edit',['raw_material_movement_id'=>$raw_material_movement->id])}}"><i class="fa fa-edit" aria-hidden="true"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Eliminar movimiento</h3>
                </div>
                <form action="{{route('admin_raw_material_movements_delete')}}" method="post">
                    @csrf
                    <input type="hidden" name="raw_material_movement_id" value="{{$raw_material_movement->id}}"/>
                    <div class="card-body text-center">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-12">
                                    <p class="text-muted">¿Está seguro que desea eliminar el movimiento <b>{!!$raw_material_movement->concept!!}</b>?</p>
                                    <p class="text-muted"><small>Cantidad en {{$raw_material_movement->raw_material->unit}}: {{number_format($raw_material_movement->amount, 2, ',', '.')}} | Precio en $: {{number_format($raw_material_movement->price, 2, ',', '.')}} | Precio en Bs.S: {{number_format($raw_material_movement->price_bss, 2, ',', '.')}} | Tasa en Bs.S: {{number_format($raw_material_movement->exchange_rate, 2, ',', '.')}} | Tipo: {{$raw_material_movement->type==1?'Entrada':'Salida'}}</small></p>
                                    <p class="text-muted">Escriba <b>eliminar</b> en el campo para confirmar</p>
                                    <div class="input-group mb-3">
                                        <input id="confirm_delete" type="text" class="form-control offset-sm-3 col-sm-6" placeholder="eliminar" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-trash"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button id="delete_button" type="submit" class="btn btn-warning" disabled>Eliminar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {
            $("#confirm_delete").keyup(function () {
                if($(this).val().toLowerCase()=="eliminar"){
                    $("#delete_button").prop("disabled",false);
                } else {
                    $("#delete_button").prop("disabled",true);
                }
            })
        });
    </script>
@endsection