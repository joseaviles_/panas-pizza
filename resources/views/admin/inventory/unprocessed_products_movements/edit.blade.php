@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-dolly-flatbed"></i> Inventario</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_inventory_show_unprocessed_products',['product_id'=>$unprocessed_product_movement->product_id])}}">Volver a inventario de <b>{{$unprocessed_product_movement->product->name}}</b></a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-warning btn-sm" href="{{route('admin_unprocessed_products_movements_trash',['unprocessed_product_movement_id'=>$unprocessed_product_movement->id])}}"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Editar movimiento</h3>
                </div>
                <form class="card-body" action="{{route('admin_unprocessed_products_movements_update')}}" method="post">
                    @csrf
                    <input type="hidden" name="unprocessed_product_movement_id" value="{{$unprocessed_product_movement->id}}"/>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-12 col-sm-3 col-md-3">              
                                <small class="text-muted">* Cantidad </small>
                                <div class="input-group mb-3">
                                    <input name="amount" id="amount" type="text" class="form-control" placeholder="Cantidad" value="{{$unprocessed_product_movement->amount}}" autocomplete="off" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">              
                                <small class="text-muted">* Precio en $</small>
                                <div class="input-group mb-3">
                                    <input name="price" id="price" type="text" class="form-control" placeholder="Precio en $" value="{{$unprocessed_product_movement->price}}" autocomplete="off" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-dollar-sign"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">              
                                <small class="text-muted">* Precio en Bs.S</small>
                                <div class="input-group mb-3">
                                    <input name="price_bss" id="price_bss" type="text" class="form-control" placeholder="Precio en Bs.S" value="{{$unprocessed_product_movement->price_bss}}" autocomplete="off" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span><b>Bs.S</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3">              
                                <small class="text-muted">* Tasa del día</small>
                                <div class="input-group mb-3">
                                    <input name="exchange_rate" id="exchange_rate" type="text" class="form-control" placeholder="Precio en Bs.S" value="{{$unprocessed_product_movement->exchange_rate}}" autocomplete="off" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span><b>Bs.S</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success">Actualizar</button>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script>
        $(document).ready(function() {

            $("#amount").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "0"});
            $("#price").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});
            $("#price_bss").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});
            $("#exchange_rate").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            $('#price').keyup(function () {
                var re=new RegExp(escapeRegExp('.'),'g');
                var exchange_rate=$('#exchange_rate').val().replace(re,'');
                var exchange_rate=exchange_rate.replace(/,/g, ".");
                exchange_rate=parseFloat(exchange_rate);

                var price=$(this).val().replace(re,'');
                var price=price.replace(/,/g, ".");
                price=parseFloat(price);
                price*=exchange_rate;
                price=formatNumber(price.toFixed(2));
                $("#price_bss").val(price);
            });

            $('#price_bss').keyup(function () {
                var re=new RegExp(escapeRegExp('.'),'g');
                var exchange_rate=$('#exchange_rate').val().replace(re,'');
                var exchange_rate=exchange_rate.replace(/,/g, ".");
                exchange_rate=parseFloat(exchange_rate);
                
                var price_bss=$(this).val().replace(re,'');
                var price_bss=price_bss.replace(/,/g, ".");
                price_bss=parseFloat(price_bss);
                price_bss/=exchange_rate;
                price_bss=formatNumber(price_bss.toFixed(2));
                $("#price").val(price_bss);
            });

            $('#exchange_rate').keyup(function () {
                var re=new RegExp(escapeRegExp('.'),'g');
                var exchange_rate=$(this).val().replace(re,'');
                var exchange_rate=exchange_rate.replace(/,/g, ".");
                exchange_rate=parseFloat(exchange_rate);

                var price=$('#price').val().replace(re,'');
                var price=price.replace(/,/g, ".");
                price=parseFloat(price);
                price*=exchange_rate;
                price=formatNumber(price.toFixed(2));
                $("#price_bss").val(price);

                var price_bss=$('#price_bss').val().replace(re,'');
                var price_bss=price_bss.replace(/,/g, ".");
                price_bss=parseFloat(price_bss);
                price_bss/=exchange_rate;
                price_bss=formatNumber(price_bss.toFixed(2));
                $("#price").val(price_bss);

            });

            function escapeRegExp(string) {
                return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); 
            }

            function formatNumber(num) {
                return num.toString().replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1,$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
            }

        });
    </script>
@endsection