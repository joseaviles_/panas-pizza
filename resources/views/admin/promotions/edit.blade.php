@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-gifts"></i> Promociones</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_promotions')}}">Listado</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-warning btn-sm" href="{{route('admin_promotions_trash',['promotion_id'=>$promotion->id])}}"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Editar</h3>
                </div>
                <form action="{{route('admin_promotions_update')}}" method="post">
                    @csrf
                    <input type="hidden" name="promotion_id" value="{{$promotion->id}}"/>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Nombre</small>
                                    <div class="input-group mb-3">
                                        <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{$promotion->name}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-info"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Precio en $</small>
                                    <div class="input-group mb-3">
                                        <input name="price" id="price" type="text" class="form-control" placeholder="Precio en $" value="{{$promotion->price}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-dollar-sign"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Estado</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" name="status" required>
                                            <option @if($promotion->status==1) selected @endif value="1">Activo</option>
                                            <option @if($promotion->status==0) selected @endif value="0">Inactivo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="raw-products">
                            <div class="card">
                                <div class="card-header border-0">
                                    <h3 class="card-title float-none text-center p-0">Productos</h3>
                                </div>
                                <div class="card-body p-0">
                                    <div class="table-responsive mx-auto div-table-dynamic">
                                        <table class="table table-bordered table-sm">
                                            <thead>                  
                                                <tr>
                                                    <td class="td-350">Nombre</td>
                                                    <td class="td-125">Cantidad</td>
                                                    <td class="td-125"></td>
                                                </tr>
                                            </thead>
                                            <tbody id="cols">
                                                @foreach($promotion->products->sortByDesc('pivot.amount') as $prod)
                                                    <tr id="tr-edit-{{$prod->id}}">
                                                        <td> 
                                                            <select style="width: 100%" class="select-cols-table" name="product_id[]" required>
                                                                @foreach($products as $product)
                                                                    <option {{$prod->pivot->product_id==$product->id?"selected":""}}  value="{{$product->id}}">{{$product->name}} - ${{number_format($product->price, 2, ',', '.')}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input name="amounts[]" type="text" class="form-control form-control-sm amounts" placeholder="Cantidad" value="{{$prod->pivot->amount}}" required>
                                                        </td>
                                                        <td class="px-1" style="text-align: center !important;">
                                                            <button type="button" id="remove-tr-edit-{{$prod->id}}" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="text-center mb-3">
                                    <button type="button" id="add-products" class="btn btn-dark btn-sm"><i class="fas fa-plus"></i> Agregar producto</button>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script>
        $(document).ready(function() {

            $('#price').autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});
            
            $(".select-cols-table").select2({
                matcher: matchCustom,
                theme: "classic"
            });

            $(".amounts").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "0"});

            @foreach($promotion->products->sortByDesc('pivot.amount') as $prod)
                $("#remove-tr-edit-{{$prod->id}}").click(function(event) {
                    $("#tr-edit-{{$prod->id}}").remove();
                });
            @endforeach

            var cols = $("#cols");
            var i=0;
            
            $("#add-products").click(function() {
                i++;
                addNew(i);
            });

            function addNew(j) {

                cols.append(`
                    <tr id="tr-`+j+`">
                        <td> 
                            <select id="select-`+j+`" style="width: 100%" name="product_id[]" required>
                                @foreach($products as $product)
                                    <option value="{{$product->id}}">{{$product->name}} - ${{number_format($product->price, 2, ',', '.')}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input name="amounts[]" id="amount-`+j+`" type="text" class="form-control form-control-sm" placeholder="Cantidad" required>
                        </td>
                        <td class="px-1" style="text-align: center !important;">
                            <button type="button" id="remove-div-`+j+`" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                `);

                $("#remove-div-"+j).click(function(event) {
                    $("#tr-"+j).remove();
                });

                $("#select-"+j).select2({
                    matcher: matchCustom,
                    theme: "classic"
                });

                $("#amount-"+j).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "0"});

            }

            function matchCustom(params, data) {
                
                if($.trim(params.term) === '') {
                    return data;
                }

                if(typeof data.text === 'undefined') {
                    return null;
                }
                
                if(data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }

                return null;
            }

        });
    </script>
@endsection