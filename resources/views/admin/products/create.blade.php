@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-pizza-slice"></i> Productos</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_products')}}">Listado</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Registrar</h3>
                </div>
                <form action="{{route('admin_products_post_create')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Nombre</small>
                                    <div class="input-group mb-3">
                                        <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{old('name')}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-info"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Categoría</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" id="category_id" name="category_id" required>
                                            @foreach($categories as $category)
                                                <option @if(old('category_id')==$category->id) selected @endif value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                                
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Precio en $</small>
                                    <div class="input-group mb-3">
                                        <input name="price" id="price" type="text" class="form-control" placeholder="Precio en $" value="{{old('price')}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-dollar-sign"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Tipo</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" name="type" id="type" required>
                                            <option @if(old('type')==1) selected @endif value="1">Sin elaboración</option>
                                            <option @if(old('type')==2) selected @endif value="2">Requiere materia prima</option>
                                        </select>
                                    </div>
                                </div>
                                <!--<div class="col-12 col-sm-6 col-md-6">
                                    <small class="text-muted">* Foto</small>
                                    <div class="custom-file mb-3">
                                        <input name="photo" type="file" accept=".jpeg,.jpg,.png" class="custom-file-input" id="customFile" required>
                                        <label class="custom-file-label" for="customFile"><i class="fas fa-upload"></i> Seleccionar</label>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                        <div class="raw-material" style="display: none;">
                            <div class="card">
                                <div class="card-header border-0">
                                    <h3 class="card-title float-none text-center p-0">Materias primas</h3>
                                </div>
                                <div class="card-body p-0">
                                    <div class="table-responsive mx-auto div-table-dynamic">
                                        <table class="table table-bordered table-sm">
                                            <thead>                  
                                                <tr>
                                                    <td class="td-350">Nombre</td>
                                                    <td class="td-125">Cantidad</td>
                                                    <td class="td-125"></td>
                                                </tr>
                                            </thead>
                                            <tbody id="cols">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="text-center mb-3">
                                    <button type="button" id="add-raw-material" class="btn btn-dark btn-sm"><i class="fas fa-plus"></i> Agregar materia prima</button>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script>
        $(document).ready(function() {

            $('#price').autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            $(".custom-file-input").on("change", function() { 
                let fileName = $(this).val().split('\\').pop(); 
                $(this).next(".custom-file-label").addClass("selected").html(fileName); 
            });

            $("#type").change(function () {
                var type=$(this).val();

                if(type==1) {
                    $(".raw-material").hide();
                }else {
                    $(".raw-material").show();
                }

            });

            var cols = $("#cols");
            var i=0;
            
            $("#add-raw-material").click(function() {
                i++;
                var raw_material_id="";
                var amount="";
                addNew(i, raw_material_id, amount);
            });

            function addNew(j, raw_material_id, amount) {

                var option=``;
                @foreach($raw_material as $rm)
                    if(raw_material_id=="{{$rm->id}}") {
                        option+=`<option selected value="{{$rm->id}}">{{$rm->name}} - {{$rm->unit}}</option>`;
                    }else {
                        option+=`<option value="{{$rm->id}}">{{$rm->name}} - {{$rm->unit}}</option>`;
                    }
                @endforeach

                cols.append(`
                    <tr id="tr-`+j+`">
                        <td> 
                            <select id="select-`+j+`" style="width: 100%" name="raw_material_id[]" required>
                                `+option+`
                            </select>
                        </td>
                        <td>
                            <input name="amounts[]" id="amount-`+j+`" type="text" class="form-control form-control-sm" placeholder="Cantidad" value="`+amount+`" required>
                        </td>
                        <td class="px-1" style="text-align: center !important;">
                            <button type="button" id="remove-div-`+j+`" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                `);

                $("#remove-div-"+j).click(function(event) {
                    $("#tr-"+j).remove();
                });

                $("#select-"+j).select2({
                    matcher: matchCustom,
                    theme: "classic"
                });

                $("#amount-"+j).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            }

            $("#category_id").change(function () {
                getRawMaterial($(this));
            });

            function getRawMaterial(selector) {

                $("#cols").empty();

                var route="/admin/products/get_raw_material/"+selector.val();
                $.get(route,{"_token":"{{csrf_token()}}"}, 
                function(data) {

                    var raw_material=data.raw_material;

                    if(raw_material.length > 0) {
                        
                        $('#type option[value="1"]').attr("selected", false);
                        $('#type option[value="2"]').attr("selected", true);
                        $(".raw-material").show();

                        $.each(raw_material,function(key, value) {
                            i++;
                            addNew(i, value.id, value.pivot.amount);
                        });

                    }else {

                        $('#type option[value="2"]').attr("selected", false);
                        $('#type option[value="1"]').attr("selected", true);
                        $(".raw-material").hide();

                    }

                });

                
            }

            function matchCustom(params, data) {
                
                if($.trim(params.term) === '') {
                    return data;
                }

                if(typeof data.text === 'undefined') {
                    return null;
                }
                
                if(data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }

                return null;
            }

            @if(old('raw_material_id')!=null)
                @foreach(old('raw_material_id') as $key=> $raw_material_id)
                    i++;
                    addNew(i, '{{$raw_material_id}}', '{{old("amounts")[$key]}}');
                @endforeach
                $(".raw-material").show();
            @endif

        });
    </script>
@endsection