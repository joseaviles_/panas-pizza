@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-pizza-slice"></i> Productos</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_products')}}">Listado</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-warning btn-sm" href="{{route('admin_products_trash',['product_id'=>$product->id])}}"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Editar</h3>
                </div>
                <form action="{{route('admin_products_update')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="product_id" value="{{$product->id}}"/>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <!--<div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">Foto</small>
                                    <div class="input-group mb-3">
                                        <img src="{{asset($product->photo)}}" class="rounded mx-auto" width="100%">
                                    </div>
                                </div>-->
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Nombre</small>
                                    <div class="input-group mb-3">
                                        <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{$product->name}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-info"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Categoría</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" name="category_id" id="category_id" required>
                                            @foreach($categories as $category)
                                                <option {{$product->category_id==$category->id?"selected":""}} value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">
                                    <small class="text-muted">* Precio en $</small>
                                    <div class="input-group mb-3">
                                        <input name="price" id="price" type="text" class="form-control" placeholder="Precio en $" value="{{$product->price}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-dollar-sign"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">
                                    <small class="text-muted">* Tipo</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" name="type" id="type" required>
                                            <option @if($product->type==1) selected @endif value="1">Sin elaboración</option>
                                            <option @if($product->type==2) selected @endif value="2">Requiere materia prima</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="raw-material" @if($product->type==1) style="display: none;" @endif>
                            <div class="card">
                                <div class="card-header border-0">
                                    <h3 class="card-title float-none text-center p-0">Materias primas</h3>
                                </div>
                                <div class="card-body p-0">
                                    <div class="table-responsive mx-auto div-table-dynamic">
                                        <table class="table table-bordered table-sm">
                                            <thead>                  
                                                <tr>
                                                    <td class="td-350">Nombre</td>
                                                    <td class="td-125">Cantidad</td>
                                                    <td class="td-125"></td>
                                                </tr>
                                            </thead>
                                            <tbody id="cols">
                                                @foreach($product->raw_material->sortByDesc('pivot.amount') as $rm)
                                                    <tr id="tr-edit-{{$rm->id}}">
                                                        <td> 
                                                            <select style="width: 100%" class="select-cols-table" name="raw_material_id[]" required>
                                                                @foreach($raw_material as $rm_prod)
                                                                    <option {{$rm->pivot->raw_material_id==$rm_prod->id?"selected":""}} value="{{$rm_prod->id}}" data-subtext="{{$rm_prod->unit}}">{{$rm_prod->name}} - {{$rm_prod->unit}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input name="amounts[]" type="text" class="form-control form-control-sm amounts" placeholder="Cantidad" value="{{$rm->pivot->amount}}" required>
                                                        </td>
                                                        <td class="px-1" style="text-align: center !important;">
                                                            <button type="button" id="remove-tr-edit-{{$rm->id}}" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="text-center mb-3">
                                    <button type="button" id="add-raw-material" class="btn btn-dark btn-sm"><i class="fas fa-plus"></i> Agregar materia prima</button>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script>
        $(document).ready(function() {

            $('#price').autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});
            
            $(".select-cols-table").select2({
                matcher: matchCustom,
                theme: "classic"
            });

            $(".amounts").autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            @foreach($product->raw_material->sortByDesc('pivot.amount') as $rm)
                $("#remove-tr-edit-{{$rm->id}}").click(function(event) {
                    $("#tr-edit-{{$rm->id}}").remove();
                });
            @endforeach

            $(".custom-file-input").on("change", function() { 
                let fileName = $(this).val().split('\\').pop(); 
                $(this).next(".custom-file-label").addClass("selected").html(fileName); 
            });

            $("#type").change(function () {
                var type=$(this).val();

                if(type==1) {
                    $(".raw-material").hide();
                }else {
                    $(".raw-material").show();
                }

            });

            var cols = $("#cols");
            var i=0;
            
            $("#add-raw-material").click(function() {
                i++;
                var raw_material_id="";
                var amount="";
                addNew(i, raw_material_id, amount);
            });

            function addNew(j, raw_material_id, amount) {

                var option=``;
                @foreach($raw_material as $rm)
                    if(raw_material_id=="{{$rm->id}}") {
                        option+=`<option selected value="{{$rm->id}}">{{$rm->name}} - {{$rm->unit}}</option>`;
                    }else {
                        option+=`<option value="{{$rm->id}}">{{$rm->name}} - {{$rm->unit}}</option>`;
                    }
                @endforeach

                cols.append(`
                    <tr id="tr-`+j+`">
                        <td> 
                            <select id="select-`+j+`" style="width: 100%" name="raw_material_id[]" required>
                                `+option+`
                            </select>
                        </td>
                        <td>
                            <input name="amounts[]" id="amount-`+j+`" type="text" class="form-control form-control-sm" placeholder="Cantidad" value="`+amount+`" required>
                        </td>
                        <td class="px-1" style="text-align: center !important;">
                            <button type="button" id="remove-div-`+j+`" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                `);

                $("#remove-div-"+j).click(function(event) {
                    $("#tr-"+j).remove();
                });

                $("#select-"+j).select2({
                    matcher: matchCustom,
                    theme: "classic"
                });

                $("#amount-"+j).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            }

            $("#category_id").change(function () {
                getRawMaterial($(this));
            });

            function getRawMaterial(selector) {

                $("#cols").empty();
                $("#cols-edit").empty();

                var route="/admin/products/get_raw_material/"+selector.val();
                $.get(route,{"_token":"{{csrf_token()}}"}, 
                function(data) {

                    var raw_material=data.raw_material;

                    if(raw_material.length > 0) {
                        
                        $('#type option[value="1"]').attr("selected", false);
                        $('#type option[value="2"]').attr("selected", true);
                        $(".raw-material").show();

                        $.each(raw_material,function(key, value) {
                            i++;
                            addNew(i, value.id, value.pivot.amount);
                        });

                    }else {

                        $('#type option[value="2"]').attr("selected", false);
                        $('#type option[value="1"]').attr("selected", true);
                        $(".raw-material").hide();

                    }

                });

                
            }

            function matchCustom(params, data) {
                
                if($.trim(params.term) === '') {
                    return data;
                }

                if(typeof data.text === 'undefined') {
                    return null;
                }
                
                if(data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }

                return null;
            }

        });
    </script>
@endsection