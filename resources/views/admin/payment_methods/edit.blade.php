@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-file-invoice-dollar"></i> Métodos de pago</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_payment_methods')}}">Listado</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-warning btn-sm" href="{{route('admin_payment_methods_trash',['payment_method_id'=>$payment_method->id])}}"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Editar</h3>
                </div>
                <form action="{{route('admin_payment_methods_update')}}" method="post">
                    @csrf
                    <input type="hidden" name="payment_method_id" value="{{$payment_method->id}}"/>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Nombre</small>
                                    <div class="input-group mb-3">
                                        <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{$payment_method->name}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-info"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Moneda</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" name="currency" required>
                                            <option @if($payment_method->currency=="$") selected @endif value="$">$ - Dólares</option>
                                            <option @if($payment_method->currency=="Bs.S") selected @endif value="Bs.S">Bs.S - Bolívares Soberanos</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Tipo</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" name="type" required>
                                            <option @if($payment_method->type=="Electrónico") selected @endif value="Electrónico">Electrónico</option>
                                            <option @if($payment_method->type=="Efectivo") selected @endif value="Efectivo">Efectivo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="raw-options">
                            <div class="card">
                                <div class="card-header border-0">
                                    <h3 class="card-title float-none text-center p-0">Bancos o procesadores de pago</h3>
                                </div>
                                <div class="card-body p-0">
                                    <div class="table-responsive mx-auto div-table-dynamic">
                                        <table class="table table-bordered table-sm">
                                            <thead>                  
                                                <tr>
                                                    <td class="td-350">Nombre</td>
                                                    <td class="td-125">Porcentaje de comisión</td>
                                                    <td class="td-125">Monto extra</td>
                                                    <td class="td-125"></td>
                                                </tr>
                                            </thead>
                                            <tbody id="cols">
                                                @foreach($payment_method->payment_method_options->sortBy('name') as $payment_method_option)
                                                    <tr id="tr-edit-{{$payment_method_option->id}}">
                                                        <td> 
                                                            <input name="edit_payment_method_option_ids[]" type="hidden" value="{{$payment_method_option->id}}">
                                                            <input name="edit_names[]" type="text" class="form-control form-control-sm" placeholder="Nombre" value="{{$payment_method_option->name}}" required>
                                                        </td>
                                                        <td>
                                                            <input name="edit_commissions[]" type="text" class="form-control form-control-sm commission" placeholder="Comisión" value="{{$payment_method_option->commission}}" required>
                                                        </td>
                                                        <td>
                                                            <input name="edit_extra_amounts[]" type="text" class="form-control form-control-sm extra-amount" placeholder="Monto extra" value="{{$payment_method_option->extra_amount}}" required>
                                                        </td>
                                                        <td class="px-1" style="text-align: center !important;">
                                                            @if(count($payment_method_option->payments)==0 && count($payment_method_option->changes)==0)
                                                                <button type="button" id="remove-tr-edit-{{$payment_method_option->id}}" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></button>
                                                            @else
                                                                @if(count($payment_method_option->payments)>0)
                                                                    <small>
                                                                        {{count($payment_method_option->payments)}} pago(s) asociado(s).
                                                                    </small>
                                                                @elseif(count($payment_method_option->changes)>0)
                                                                    <small>
                                                                        {{count($payment_method_option->changes)}} cambio(s) asociado(s).
                                                                    </small>
                                                                @endif
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <small class="text-muted text-center">
                                            Si el banco o procesador de pago no tiene un porcentaje de comisión o un monto extra, deja los campos en 0,00. Por ejemplo: PayPal tiene un porcentaje de comisión de 5,4% y un monto extra de $0,30.
                                        </small>
                                    </div>
                                </div>
                                <div class="text-center mt-3 mb-3">
                                    <button type="button" id="add-options" class="btn btn-dark btn-sm"><i class="fas fa-plus"></i> Agregar banco o procesador de pago</button>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script>
        $(document).ready(function() {

            $('.commission').autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});
            $('.extra-amount').autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            @foreach($payment_method->payment_method_options->sortBy('name')  as $payment_method_option)
                $("#remove-tr-edit-{{$payment_method_option->id}}").click(function(event) {
                    $("#tr-edit-{{$payment_method_option->id}}").remove();
                });
            @endforeach

            var cols = $("#cols");
            var i=0;
            
            $("#add-options").click(function() {
                i++;
                addNew(i);
            });

            function addNew(j) {

                cols.append(`
                    <tr id="tr-`+j+`">
                        <td> 
                            <input name="names[]" type="text" class="form-control form-control-sm" placeholder="Nombre" required>
                        </td>
                        <td>
                            <input name="commissions[]" id="commission-`+j+`" type="text" class="form-control form-control-sm" placeholder="Comisión" value="0" required>
                        </td>
                        <td>
                            <input name="extra_amounts[]" id="extra-amount-`+j+`" type="text" class="form-control form-control-sm" placeholder="Monto extra" value="0" required>
                        </td>
                        <td class="px-1" style="text-align: center !important;">
                            <button type="button" id="remove-div-`+j+`" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                `);

                $("#remove-div-"+j).click(function(event) {
                    $("#tr-"+j).remove();
                });

                $("#commission-"+j).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                $("#extra-amount-"+j).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            }

            function matchCustom(params, data) {
                
                if($.trim(params.term) === '') {
                    return data;
                }

                if(typeof data.text === 'undefined') {
                    return null;
                }
                
                if(data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }

                return null;
            }

        });
    </script>
@endsection