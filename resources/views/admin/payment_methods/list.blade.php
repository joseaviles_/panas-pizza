@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-file-invoice-dollar"></i> Métodos de pago</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_payment_methods_create')}}">Registrar</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="table-responsive">
                <table id="table-list" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>Nombre</th>
                            <th>Moneda</th>
                            <th>Tipo</th>
                            <th>Bancos o procesadores de pago</th>
                            <th>Fecha de registro</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            $("#table-list").DataTable( {
                "ajax": "{{route('get_admin_payment_methods')}}",
                "columnDefs": [{ "orderable": false, "targets": -1 }, { "orderable": false, "targets": -3 }],
                "iDisplayLength": 25,
                "language": {
                    "sProcessing":     "Procesando métodos de pago",
                    "sLengthMenu":     "Mostrar _MENU_ métodos de pago",
                    "sZeroRecords":    "No se encontraron métodos de pago",
                    "sEmptyTable":     "Ningún método de pago disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ métodos de pago de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ningún métodos de pago",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ métodos de pago)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando métodos de pago...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
        });
    </script>
@endsection