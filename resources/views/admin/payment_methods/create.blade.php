@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-file-invoice-dollar"></i> Métodos de pago</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_payment_methods')}}">Listado</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Registrar</h3>
                </div>
                <form action="{{route('admin_payment_methods_post_create')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Nombre</small>
                                    <div class="input-group mb-3">
                                        <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{old('name')}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-info"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Moneda</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" name="currency" required>
                                            <option @if(old('currency')=="$") selected @endif value="$">$ - Dólares</option>
                                            <option @if(old('currency')=="Bs.S") selected @endif value="Bs.S">Bs.S - Bolívares Soberanos</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Tipo</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" name="type" required>
                                            <option @if(old('type')=="Electrónico") selected @endif value="Electrónico">Electrónico</option>
                                            <option @if(old('type')=="Efectivo") selected @endif value="Efectivo">Efectivo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="raw-options">
                            <div class="card">
                                <div class="card-header border-0">
                                    <h3 class="card-title float-none text-center p-0">Bancos o procesadores de pago</h3>
                                </div>
                                <div class="card-body p-0">
                                    <div class="table-responsive mx-auto div-table-dynamic">
                                        <table class="table table-bordered table-sm">
                                            <thead>                  
                                                <tr>
                                                    <td class="td-350">Nombre</td>
                                                    <td class="td-125">Porcentaje de comisión</td>
                                                    <td class="td-125">Monto extra</td>
                                                    <td class="td-125"></td>
                                                </tr>
                                            </thead>
                                            <tbody id="cols">
                                            </tbody>
                                        </table>
                                        <small class="text-muted text-center">
                                            Si el banco o procesador de pago no tiene un porcentaje de comisión o un monto extra, deja los campos en 0,00. Por ejemplo: PayPal tiene un porcentaje de comisión de 5,4% y un monto extra de $0,30.
                                        </small>
                                    </div>
                                </div>
                                <div class="text-center mt-3 mb-3">
                                    <button type="button" id="add-options" class="btn btn-dark btn-sm"><i class="fas fa-plus"></i> Agregar banco o procesador de pago</button>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script>
        $(document).ready(function() {

            var cols = $("#cols");
            var i=0;
            
            $("#add-options").click(function() {
                i++;
                addNew(i);
            });

            function addNew(j) {

                cols.append(`
                    <tr id="tr-`+j+`">
                        <td> 
                            <input name="names[]" type="text" class="form-control form-control-sm" placeholder="Nombre" required>
                        </td>
                        <td>
                            <input name="commissions[]" id="commission-`+j+`" type="text" class="form-control form-control-sm" placeholder="Comisión" value="0" required>
                        </td>
                        <td>
                            <input name="extra_amounts[]" id="extra-amount-`+j+`" type="text" class="form-control form-control-sm" placeholder="Monto extra" value="0" required>
                        </td>
                        <td class="px-1" style="text-align: center !important;">
                            <button type="button" id="remove-div-`+j+`" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                `);

                $("#remove-div-"+j).click(function(event) {
                    $("#tr-"+j).remove();
                });

                $("#commission-"+j).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

                $("#extra-amount-"+j).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            }

            function matchCustom(params, data) {
                
                if($.trim(params.term) === '') {
                    return data;
                }

                if(typeof data.text === 'undefined') {
                    return null;
                }
                
                if(data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }

                return null;
            }
            
        });
    </script>
@endsection