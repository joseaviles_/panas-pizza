@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-chart-line"></i> Reportes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_reports')}}">Volver</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        @if(isset($initial_date) && isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'delivery_companies', 'initial_date'=>$initial_date, 'final_date'=>$final_date])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @elseif(isset($initial_date) && !isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'delivery_companies', 'initial_date'=>$initial_date, 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @elseif(!isset($initial_date) && isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'delivery_companies', 'initial_date'=>'none', 'final_date'=>$final_date])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @else
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'delivery_companies', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			<div class="invoice p-3 mb-3">
			    <div class="row">
			        <div class="col-12">
			            <h4>
			                <img class="rounded mx-auto" width="50px" src="{{asset($configuration->logo)}}" alt="{{$configuration->name}}">
			                {{$configuration->name}}
			            </h4>
			        </div>
			    </div>
			    <div class="row invoice-info mb-2">
			        <div class="col-sm-12 invoice-col">
			            <strong>Entregas de las empresas de delivery</strong>
                        <small>
                            @if(isset($initial_date) && isset($final_date))
                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                            @elseif(isset($initial_date) && !isset($final_date))
                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                            @elseif(!isset($initial_date) && isset($final_date))
                                | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                            @else
                                | Historial completo
                            @endif
                        </small>
			        </div>
			    </div>

                @php
                    $final_delivery_sales=0;
                    $final_delivery_amount_usd=0;
                    $final_delivery_amount_bss=0;
                @endphp

                @foreach($delivery_companies as $delivery_company)
                    <div class="callout callout-info shadow">
                        <p>
                            <b>{{$delivery_company->name}}</b>                       
                            <small>
                                @if(isset($initial_date) && isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @elseif(isset($initial_date) && !isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                                @elseif(!isset($initial_date) && isset($final_date))
                                    | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @else
                                    | Historial completo
                                @endif
                            </small>
                        </p>

                        <div class="table-responsive">
                            <table class="table table-bordered table-sm">
                                <thead>                  
                                    <tr>
                                        <th>Total de entregas</th>
                                        <th>Total de generado en $</th>
                                        <th>Total de generado en Bs.S</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if(isset($initial_date) && isset($final_date))
                                                
                                                {{number_format($delivery_company->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}
                                                
                                                @php
                                                    $final_delivery_sales+=$delivery_company->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('amount');
                                                @endphp

                                            @elseif(isset($initial_date) && !isset($final_date))

                                                {{number_format($delivery_company->delivery_sales->where('created_at','>=',$initial_date)->sum('amount'), 0, ',', '.')}}

                                                @php
                                                    $final_delivery_sales+=$delivery_company->delivery_sales->where('created_at','>=',$initial_date)->sum('amount');
                                                @endphp

                                            @elseif(!isset($initial_date) && isset($final_date))

                                                {{number_format($delivery_company->delivery_sales->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}

                                                @php
                                                    $final_delivery_sales+=$delivery_company->delivery_sales->where('created_at','<=',$final_date)->sum('amount');
                                                @endphp

                                            @else

                                                {{number_format($delivery_company->delivery_sales->sum('amount'), 0, ',', '.')}}

                                                @php
                                                    $final_delivery_sales+=$delivery_company->delivery_sales->sum('amount');
                                                @endphp

                                            @endif
                                        </td>
                                        <td>
                                            
                                            @php 
                                                $total_delivery_amount_usd=0; 
                                                $total_delivery_amount_bss=0; 
                                            @endphp

                                            @if(isset($initial_date) && isset($final_date))
                                                
                                                @foreach($delivery_company->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $delivery_sale)
                                                    @php 
                                                        $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                        $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                    @endphp
                                                @endforeach

                                                {{number_format($total_delivery_amount_usd, 2, ',', '.')}}

                                                @php
                                                    $final_delivery_amount_usd+=$total_delivery_amount_usd;
                                                    $final_delivery_amount_bss+=$total_delivery_amount_bss;
                                                @endphp

                                            @elseif(isset($initial_date) && !isset($final_date))

                                                @foreach($delivery_company->delivery_sales->where('created_at','>=',$initial_date) as $delivery_sale)
                                                    @php 
                                                        $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                        $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                    @endphp
                                                @endforeach

                                                {{number_format($total_delivery_amount_usd, 2, ',', '.')}}

                                                @php
                                                    $final_delivery_amount_usd+=$total_delivery_amount_usd;
                                                    $final_delivery_amount_bss+=$total_delivery_amount_bss;
                                                @endphp

                                            @elseif(!isset($initial_date) && isset($final_date))

                                                @foreach($delivery_company->delivery_sales->where('created_at','<=',$final_date) as $delivery_sale)
                                                    @php 
                                                        $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                        $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                    @endphp
                                                @endforeach

                                                {{number_format($total_delivery_amount_usd, 2, ',', '.')}}

                                                @php
                                                    $final_delivery_amount_usd+=$total_delivery_amount_usd;
                                                    $final_delivery_amount_bss+=$total_delivery_amount_bss;
                                                @endphp

                                            @else

                                                @foreach($delivery_company->delivery_sales as $delivery_sale)
                                                    @php 
                                                        $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                        $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                    @endphp
                                                @endforeach

                                                {{number_format($total_delivery_amount_usd, 2, ',', '.')}}

                                                @php
                                                    $final_delivery_amount_usd+=$total_delivery_amount_usd;
                                                    $final_delivery_amount_bss+=$total_delivery_amount_bss;
                                                @endphp

                                            @endif

                                        </td>
                                        <td>
                                            {{number_format($total_delivery_amount_bss, 2, ',', '.')}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6">
                                <p class="m-0 text-primary">
                                    <small>
                                        <b>Tipos de delivery</b> 
                                    </small>
                                </p>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-sm font-12">
                                        <thead>                  
                                            <tr>
                                                <th>Tipo de delivery</th>
                                                <th>Total de entregas</th>
                                                <th>Total de generado en $</th>
                                                <th>Total de generado en Bs.S</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($delivery_company->delivery_types->sortBy('price') as $delivery_type)
                                                <tr>
                                                    <td>{{$delivery_type->name}}: {{number_format($delivery_type->price, 2, ',', '.')}}$</td>
                                                    <td>
                                                        @if(isset($initial_date) && isset($final_date))
                                                            {{number_format($delivery_type->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}
                                                        @elseif(isset($initial_date) && !isset($final_date))
                                                            {{number_format($delivery_type->delivery_sales->where('created_at','>=',$initial_date)->sum('amount'), 0, ',', '.')}}
                                                        @elseif(!isset($initial_date) && isset($final_date))
                                                            {{number_format($delivery_type->delivery_sales->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}
                                                        @else
                                                            {{number_format($delivery_type->delivery_sales->sum('amount'), 0, ',', '.')}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        
                                                        @php 
                                                            $total_delivery_amount_usd=0; 
                                                            $total_delivery_amount_bss=0; 
                                                        @endphp

                                                        @if(isset($initial_date) && isset($final_date))
                                                            @foreach($delivery_type->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $delivery_sale)
                                                                @php 
                                                                    $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                                    $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                                @endphp
                                                            @endforeach
                                                            {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                        @elseif(isset($initial_date) && !isset($final_date))
                                                            @foreach($delivery_type->delivery_sales->where('created_at','>=',$initial_date) as $delivery_sale)
                                                                @php 
                                                                    $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                                    $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                                @endphp
                                                            @endforeach
                                                            {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                        @elseif(!isset($initial_date) && isset($final_date))
                                                            @foreach($delivery_type->delivery_sales->where('created_at','<=',$final_date) as $delivery_sale)
                                                                @php 
                                                                    $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                                    $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                                @endphp
                                                            @endforeach
                                                            {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                        @else
                                                            @foreach($delivery_type->delivery_sales as $delivery_sale)
                                                                @php 
                                                                    $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                                    $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                                @endphp
                                                            @endforeach
                                                            {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                        @endif

                                                    </td>
                                                    <td>
                                                        {{number_format($total_delivery_amount_bss, 2, ',', '.')}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <p class="m-0 text-primary">
                                    <small>
                                        <b>Conductores</b> 
                                    </small>
                                </p>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-sm font-12">
                                        <thead>                  
                                            <tr>
                                                <th>Conductor</th>
                                                <th>Total de entregas</th>
                                                <th>Total de generado en $</th>
                                                <th>Total de generado en Bs.S</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($delivery_company->delivery_company_drivers->sortBy('name') as $delivery_company_driver)
                                                <tr>
                                                    <td>{{$delivery_company_driver->name}}</td>
                                                    <td>
                                                        @if(isset($initial_date) && isset($final_date))
                                                            {{number_format($delivery_company_driver->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}
                                                        @elseif(isset($initial_date) && !isset($final_date))
                                                            {{number_format($delivery_company_driver->delivery_sales->where('created_at','>=',$initial_date)->sum('amount'), 0, ',', '.')}}
                                                        @elseif(!isset($initial_date) && isset($final_date))
                                                            {{number_format($delivery_company_driver->delivery_sales->where('created_at','<=',$final_date)->sum('amount'), 0, ',', '.')}}
                                                        @else
                                                            {{number_format($delivery_company_driver->delivery_sales->sum('amount'), 0, ',', '.')}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        
                                                        @php 
                                                            $total_delivery_amount_usd=0; 
                                                            $total_delivery_amount_bss=0; 
                                                        @endphp

                                                        @if(isset($initial_date) && isset($final_date))
                                                            @foreach($delivery_company_driver->delivery_sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $delivery_sale)
                                                                @php 
                                                                    $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                                    $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                                @endphp
                                                            @endforeach
                                                            {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                        @elseif(isset($initial_date) && !isset($final_date))
                                                            @foreach($delivery_company_driver->delivery_sales->where('created_at','>=',$initial_date) as $delivery_sale)
                                                                @php 
                                                                    $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                                    $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                                @endphp
                                                            @endforeach
                                                            {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                        @elseif(!isset($initial_date) && isset($final_date))
                                                            @foreach($delivery_company_driver->delivery_sales->where('created_at','<=',$final_date) as $delivery_sale)
                                                                @php 
                                                                    $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                                    $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                                @endphp
                                                            @endforeach
                                                            {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                        @else
                                                            @foreach($delivery_company_driver->delivery_sales as $delivery_sale)
                                                                @php 
                                                                    $total_delivery_amount_usd+=($delivery_sale->amount*$delivery_sale->price); 
                                                                    $total_delivery_amount_bss+=($delivery_sale->amount*$delivery_sale->price*$delivery_sale->sale->exchange_rate); 
                                                                @endphp
                                                            @endforeach
                                                            {{number_format($total_delivery_amount_usd, 2, ',', '.')}}
                                                        @endif

                                                    </td>
                                                    <td>
                                                        {{number_format($total_delivery_amount_bss, 2, ',', '.')}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                @endforeach
                <hr>
                <p>
                    <b>Sumatoria global</b>                       
                    <small>
                        @if(isset($initial_date) && isset($final_date))
                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                        @elseif(isset($initial_date) && !isset($final_date))
                            | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                        @elseif(!isset($initial_date) && isset($final_date))
                            | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                        @else
                            | Historial completo
                        @endif
                    </small>
                </p>
                <div class="table-responsive">
                    <table class="table table-bordered table-sm">
                        <thead>                  
                            <tr>
                                <th>Total de entregas</th>
                                <th>Total de generado en $</th>
                                <th>Total de generado en Bs.S</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    {{number_format($final_delivery_sales, 0, ',', '.')}}
                                </td>
                                <td>
                                    {{number_format($final_delivery_amount_usd, 2, ',', '.')}}
                                </td>
                                <td>
                                    {{number_format($final_delivery_amount_bss, 2, ',', '.')}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

			</div>
	        <div class="row">
                <div class="offset-md-2 col-md-8">
                    <form id="delivery_companies" class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="delivery_companies" required>
                        <div class="card-header">
                            <h3 class="card-title">Volver a consultar las entregas de las empresas de delivery</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar cuanto generaron y cuantas entregas realizaron cada empresa de delivery mediante un rango de fechas!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" id="delivery_companies_initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" id="delivery_companies_final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                <button id="delivery_companies_today" type="button" class="btn btn-danger btn-sm mt-1">¡Entregas de hoy!</button>
                                <small class="text-left text-muted">
                                    <p class="m-0"><b>Tips</b></p>
                                    <p class="m-0">1. Si no ingresas ninguna fecha podrás visualizar el reporte del historial completo.</p>
                                    <p class="m-0">2. Si ingresas ambas fechas podrás visualizar el reporte en el rango determinado.</p>
                                    <p class="m-0">3. Si solo ingresas la fecha inicial podrás visualizar el reporte desde la fecha seleccionada en adelante.</p>
                                    <p class="m-0">4. Si solo ingresas la fecha final podrás visualizar el reporte desde el inicio del sistema hasta la fecha seleccionada.</p>
                                </small>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {
            
            $("#delivery_companies_today").click(function(event) {
                $("#delivery_companies_initial_date").val('{{date("Y-m-d\T00:00")}}');
                $("#delivery_companies_final_date").val('{{date("Y-m-d\T23:59")}}');
                $("#delivery_companies").submit();
            });

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

        });
    </script>
@endsection