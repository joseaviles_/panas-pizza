@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-chart-line"></i> Reportes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_reports')}}">Volver</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'customers_purchases', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			<div class="invoice p-3 mb-3">
			    <div class="row">
			        <div class="col-12">
			            <h4>
			                <img class="rounded mx-auto" width="50px" src="{{asset($configuration->logo)}}" alt="{{$configuration->name}}">
			                {{$configuration->name}}
			            </h4>
			        </div>
			    </div>
			    <div class="row invoice-info">
			        <div class="col-sm-12 invoice-col">
			            <strong>Clientes con mas compras</strong>
			        </div>
			    </div>
		        <div class="row mt-3">
		            <div class="col-12 table-responsive font-12">
                        <table id="table" class="table table-bordered table-sm display table-hover table-striped table-sm" cellspacing="0" width="100%">
                            <thead class="thead-dark">
		                        <tr>
                                    <th>Número de cliente</th>
                                    <th>Nombre registrado en el teléfono del negocio</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Total gastado en $</th>
		                            <th>Compras</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                        @foreach($clients as $client)
		                            <tr>
                                        <td>{{$client->client_number}}</td>
		                                <td>{{$client->name_business_phone}}</td>
                                        <td>{{$client->first_name}}</td>
                                        <td>{{$client->last_name}}</td>
                                        <td>{{number_format($client->total, 2, ',', '.')}}</td>
		                                <td>{{number_format($client->purchases, 0, ',', '.')}}</td>
		                            </tr>
		                        @endforeach
		                    </tbody>
		                </table>
		            </div>
		        </div>
			</div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $("#table").DataTable( {
                "iDisplayLength": 25,
                "order": [[ 5, "desc" ]],
                "language": {
                    "sProcessing":     "Procesando compras...",
                    "sLengthMenu":     "Mostrar _MENU_ compras",
                    "sZeroRecords":    "No se encontraron compras",
                    "sEmptyTable":     "Ninguna compra disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ compras de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ninguna compra",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ compras)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando compras...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );

        });
    </script>
@endsection