@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-chart-line"></i> Reportes</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <small class="text-left text-muted">
                <p class="m-0"><b>Tips</b></p>
                <p class="m-0">1. Si no ingresas ninguna fecha podrás visualizar el reporte del historial completo.</p>
                <p class="m-0">2. Si ingresas ambas fechas podrás visualizar el reporte en el rango determinado.</p>
                <p class="m-0">3. Si solo ingresas la fecha inicial podrás visualizar el reporte desde la fecha seleccionada en adelante.</p>
                <p class="m-0">4. Si solo ingresas la fecha final podrás visualizar el reporte desde el inicio del sistema hasta la fecha seleccionada.</p>
            </small>
            <div class="row mt-4">
                <div class="col-md-6">
                    <form id="sales" class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="sales" required>
                        <div class="card-header">
                            <h3 class="card-title">Ventas</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar las ventas mediante un rango de fechas!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" id="sales_initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" id="sales_final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                <button id="sales_today" type="button" class="btn btn-danger btn-sm mt-1">¡Ventas de hoy!</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form id="payment_methods" class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="payment_methods" required>
                        <div class="card-header">
                            <h3 class="card-title">Pagos/cambios de los métodos de pago</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar los pagos/cambios de todos los métodos de pago mediante un rango de fechas!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" id="pm_initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" id="pm_final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                <button id="pm_today" type="button" class="btn btn-danger btn-sm mt-1">¡Pagos/cambios de hoy!</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form id="admin_new_report" class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="most_selled_products" required>
                        <div class="card-header">
                            <h3 class="card-title">Productos más vendidos</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar las ventas de todos los productos mediante un rango de fechas!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" id="initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" id="final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                <button id="selling_products_today" type="button" class="btn btn-danger btn-sm mt-1">¡Productos más vendidos hoy!</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="raw_material_incomes" required>
                        <div class="card-header">
                            <h3 class="card-title">Productos generados de las entradas de materias primas</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar cuantos productos se generaron a partir de las entradas de materias primas mediante un rango de fechas!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form id="delivery_companies" class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="delivery_companies" required>
                        <div class="card-header">
                            <h3 class="card-title">Entregas de las empresas de delivery</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar cuanto generaron y cuantas entregas realizaron cada empresa de delivery mediante un rango de fechas!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" id="delivery_companies_initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" id="delivery_companies_final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                <button id="delivery_companies_today" type="button" class="btn btn-danger btn-sm mt-1">¡Entregas de hoy!</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form id="expenses" class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="expenses" required>
                        <div class="card-header">
                            <h3 class="card-title">Gastos</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar los gastos que se generaron en {{$configuration->name}}!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" id="expenses_initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" id="expenses_final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                <button id="expenses_today" type="button" class="btn btn-danger btn-sm mt-1">¡Gastos de hoy!</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form id="inputs_outputs_rmp" class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="inputs_outputs_rmp" required>
                        <div class="card-header">
                            <h3 class="card-title">Entradas/salidas de materias primas y de productos sin elaboración</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar las entradas/salidas mediante un rango de fechas!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" id="inputs_outputs_rmp_initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" id="inputs_outputs_rmp_final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12">              
                                        <small class="text-muted">Selecciona la materia prima o el producto sin elaboración</small>
                                        <select class="selectpicker show-tick form-control form-control-sm" name="rm_prod"data-live-search="true" data-size="5" data-show-subtext="true" required>
                                            @foreach($raw_material as $rm)
                                                <option value="r{{$rm->id}}" data-subtext="Materia prima">{{$rm->name}}</option>
                                            @endforeach
                                            <option data-divider="true"></option>
                                            @foreach($products as $product)
                                                <option value="p{{$product->id}}" data-subtext="Producto sin elaboración">{{$product->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                <button id="inputs_outputs_rmp_today" type="button" class="btn btn-danger btn-sm mt-1">¡Entradas/salidas de hoy!</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="customers_purchases" required>
                        <div class="card-header">
                            <h3 class="card-title">Clientes con mas compras</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar los clientes con mas compras!</small>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Listado de categorías</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes generar un PDF con todas las categorías registradas en {{$configuration->name}}!</small>
                            </div>
                            <div class="text-center">
                                <a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'categories', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Listado de productos</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes generar un PDF con todos los productos registrados en {{$configuration->name}}!</small>
                            </div>
                            <div class="text-center">
                                <a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'products', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Listado de clientes</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes generar un PDF con todos los clientes registrados en {{$configuration->name}}!</small>
                            </div>
                            <div class="text-center">
                                <a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'clients', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Clientes con deudas</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes generar un PDF con todos los clientes deudores en {{$configuration->name}}!</small>
                            </div>
                            <div class="text-center">
                                <a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'clients_with_debts', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Historial de caja</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes generar un PDF con todo el historial de caja de {{$configuration->name}}!</small>
                            </div>
                            <div class="text-center">
                                <a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'cash_history', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Inventario general</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes generar un PDF con todo el inventario general de {{$configuration->name}}!</small>
                            </div>
                            <div class="text-center">
                                <a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'general_inventory', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $("#sales_today").click(function(event) {
                $("#sales_initial_date").val('{{date("Y-m-d\T00:00")}}');
                $("#sales_final_date").val('{{date("Y-m-d\T23:59")}}');
                $("#sales").submit();
            });

            $("#pm_today").click(function(event) {
                $("#pm_initial_date").val('{{date("Y-m-d\T00:00")}}');
                $("#pm_final_date").val('{{date("Y-m-d\T23:59")}}');
                $("#payment_methods").submit();
            });

            $("#selling_products_today").click(function(event) {
                $("#initial_date").val('{{date("Y-m-d\T00:00")}}');
                $("#final_date").val('{{date("Y-m-d\T23:59")}}');
                $("#admin_new_report").submit();
            });

            $("#delivery_companies_today").click(function(event) {
                $("#delivery_companies_initial_date").val('{{date("Y-m-d\T00:00")}}');
                $("#delivery_companies_final_date").val('{{date("Y-m-d\T23:59")}}');
                $("#delivery_companies").submit();
            });

            $("#expenses_today").click(function(event) {
                $("#expenses_initial_date").val('{{date("Y-m-d\T00:00")}}');
                $("#expenses_final_date").val('{{date("Y-m-d\T23:59")}}');
                $("#expenses").submit();
            });

            $("#inputs_outputs_rmp_today").click(function(event) {
                $("#inputs_outputs_rmp_initial_date").val('{{date("Y-m-d\T00:00")}}');
                $("#inputs_outputs_rmp_final_date").val('{{date("Y-m-d\T23:59")}}');
                $("#inputs_outputs_rmp").submit();
            });

        });
    </script>
@endsection