@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-chart-line"></i> Reportes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_reports')}}">Volver</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        @if(isset($initial_date) && isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'raw_material_incomes', 'initial_date'=>$initial_date, 'final_date'=>$final_date])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @elseif(isset($initial_date) && !isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'raw_material_incomes', 'initial_date'=>$initial_date, 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @elseif(!isset($initial_date) && isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'raw_material_incomes', 'initial_date'=>'none', 'final_date'=>$final_date])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @else
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'raw_material_incomes', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			<div class="invoice p-3 mb-3">
			    <div class="row">
			        <div class="col-12">
			            <h4>
			                <img class="rounded mx-auto" width="50px" src="{{asset($configuration->logo)}}" alt="{{$configuration->name}}">
			                {{$configuration->name}}
			            </h4>
			        </div>
			    </div>
			    <div class="row invoice-info mb-2">
			        <div class="col-sm-12 invoice-col">
			            <strong>Productos generados de las entradas de materias primas</strong>
                        <small>
                            @if(isset($initial_date) && isset($final_date))
                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                            @elseif(isset($initial_date) && !isset($final_date))
                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                            @elseif(!isset($initial_date) && isset($final_date))
                                | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                            @else
                                | Historial completo
                            @endif
                        </small>
			        </div>
			    </div>
                @foreach($raw_material_incomes as $raw_material_income)
                    <div class="callout callout-info shadow">
                        <p>
                            <b>Entrada de {{number_format($raw_material_income->raw_material_movement->amount, 2, ',', '.')}} {{$raw_material_income->raw_material->unit}} de {{$raw_material_income->raw_material->name}} el {{$raw_material_income->created_at->format('d-m-Y h:i:sa')}} <small class="text-muted">| Actualmente quedan {{number_format($raw_material_income->amount, 2, ',', '.')}} {{$raw_material_income->raw_material->unit}}</small></b>
                        </p>
                        <div class="row mt-3">
                            @php
                                $total_array_product=0; 
                                $total_array_raw_material_used=0;
                            @endphp
                            <div class="col-12 table-responsive">
                                <table id="table-{{$raw_material_income->id}}" class="table table-bordered table-sm display table-hover table-striped table-sm" cellspacing="0" width="100%">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Producto</th>
                                            <th>{{$raw_material_income->raw_material->unit}} usados(as)</th>
                                            <th>Cantidad generada</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($raw_material_income->array_products as $key=> $array_product)
                                            <tr>
                                                <td>{{$key}}</td>
                                                <td>{{number_format($raw_material_income->array_raw_material_used[$key], 2, ',', '.')}}</td>
                                                <td>{{number_format($array_product, 0, ',', '.')}}</td>
                                            </tr>
                                            @php
                                                $total_array_product+=$array_product; 
                                                $total_array_raw_material_used+=$raw_material_income->array_raw_material_used[$key];
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr>
                        <small>
                            <p class="text-muted m-0 p-0"><b>Total de {{$raw_material_income->raw_material->unit}} de {{$raw_material_income->raw_material->name}} usados(as):</b> {{number_format($total_array_raw_material_used, 2, ',', '.')}}</p>
                            <p class="text-muted m-0 p-0"><b>Total de {{$raw_material_income->raw_material->unit}} de {{$raw_material_income->raw_material->name}} restantes:</b> {{number_format($raw_material_income->amount, 2, ',', '.')}} <b>=</b> ({{number_format($raw_material_income->raw_material_movement->amount, 2, ',', '.')}}-{{number_format($total_array_raw_material_used, 2, ',', '.')}})</p>
                            <p class="text-muted m-0 p-0"><b>Total de productos de generados:</b> {{number_format($total_array_product, 0, ',', '.')}}</p>
                        </small>
                    </div>
                @endforeach
			</div>
            <div class="row">
                <div class="offset-md-2 col-md-8">
                    <form class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="raw_material_incomes" required>
                        <div class="card-header">
                            <h3 class="card-title">Volver a consultar los productos generados de las entradas de materias primas</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar cuantos productos se generaron a partir de las entradas de materias primas mediante un rango de fechas!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 offset-sm-2 offset-md-2 col-sm-4 col-md-4">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4 col-md-4">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                <small class="text-left text-muted">
                                    <p class="m-0"><b>Tips</b></p>
                                    <p class="m-0">1. Si no ingresas ninguna fecha podrás visualizar el reporte del historial completo.</p>
                                    <p class="m-0">2. Si ingresas ambas fechas podrás visualizar el reporte en el rango determinado.</p>
                                    <p class="m-0">3. Si solo ingresas la fecha inicial podrás visualizar el reporte desde la fecha seleccionada en adelante.</p>
                                    <p class="m-0">4. Si solo ingresas la fecha final podrás visualizar el reporte desde el inicio del sistema hasta la fecha seleccionada.</p>
                                </small>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            @foreach($raw_material_incomes as $raw_material_income)
                $("#table-{{$raw_material_income->id}}").DataTable( {
                    "iDisplayLength": 10,
                    "order": [[ 2, "desc" ]],
                    "language": {
                        "sProcessing":     "Procesando productos...",
                        "sLengthMenu":     "Mostrar _MENU_ productos",
                        "sZeroRecords":    "No se encontraron productos",
                        "sEmptyTable":     "Ningún producto disponible en esta tabla",
                        "sInfo":           "Mostrando de _START_ a _END_ productos de un total de _TOTAL_",
                        "sInfoEmpty":      "No se ha generado ningún producto",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ productos)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando productos...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "decimal": ",",
                        "thousands": "."
                    }
                } );
            @endforeach

        });
    </script>
@endsection