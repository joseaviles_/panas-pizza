@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-chart-line"></i> Reportes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_reports')}}">Volver</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">

                        @if($raw_material)

                            @if(isset($initial_date) && isset($final_date))
                                <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report_rmp', ['type'=>'inputs_outputs_rmp', 'initial_date'=>$initial_date, 'final_date'=>$final_date, 'rmp'=>'r'.$raw_material->id])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                            @elseif(isset($initial_date) && !isset($final_date))
                                <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report_rmp', ['type'=>'inputs_outputs_rmp', 'initial_date'=>$initial_date, 'final_date'=>'none', 'rmp'=>'r'.$raw_material->id])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                            @elseif(!isset($initial_date) && isset($final_date))
                                <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report_rmp', ['type'=>'inputs_outputs_rmp', 'initial_date'=>'none', 'final_date'=>$final_date, 'rmp'=>'r'.$raw_material->id])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                            @else
                                <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report_rmp', ['type'=>'inputs_outputs_rmp', 'initial_date'=>'none', 'final_date'=>'none', 'rmp'=>'r'.$raw_material->id])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                            @endif

                        @else
                            
                            @if(isset($initial_date) && isset($final_date))
                                <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report_rmp', ['type'=>'inputs_outputs_rmp', 'initial_date'=>$initial_date, 'final_date'=>$final_date, 'rmp'=>'p'.$product->id])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                            @elseif(isset($initial_date) && !isset($final_date))
                                <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report_rmp', ['type'=>'inputs_outputs_rmp', 'initial_date'=>$initial_date, 'final_date'=>'none', 'rmp'=>'p'.$product->id])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                            @elseif(!isset($initial_date) && isset($final_date))
                                <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report_rmp', ['type'=>'inputs_outputs_rmp', 'initial_date'=>'none', 'final_date'=>$final_date, 'rmp'=>'p'.$product->id])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                            @else
                                <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report_rmp', ['type'=>'inputs_outputs_rmp', 'initial_date'=>'none', 'final_date'=>'none', 'rmp'=>'p'.$product->id])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                            @endif


                        @endif

                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			<div class="invoice p-3 mb-3">
			    <div class="row">
			        <div class="col-12">
			            <h4>
			                <img class="rounded mx-auto" width="50px" src="{{asset($configuration->logo)}}" alt="{{$configuration->name}}">
			                {{$configuration->name}}
			            </h4>
			        </div>
			    </div>
                @if($raw_material)
                    <div class="callout callout-info shadow">
                        <p>
                            <b>Entradas de {{$raw_material->name}}</b>                       
                            <small>
                                @if(isset($initial_date) && isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @elseif(isset($initial_date) && !isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                                @elseif(!isset($initial_date) && isset($final_date))
                                    | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @else
                                    | Historial completo
                                @endif
                            </small>
                        </p>
                        <div class="table-responsive">
                            <table id="inputs" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Fecha de registro</th>
                                        <th>Tipo</th>
                                        <th>Stock en {{$raw_material->unit}}</th>
                                        <th>Cantidad en {{$raw_material->unit}}</th>
                                        <th>Precio en $</th>
                                        <th>Precio en Bs.S</th>
                                        <th>Tasa en Bs.S</th>
                                        <th>Concepto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($raw_material_movements as $raw_material_movement)
                                        @if($raw_material_movement->type==1)
                                            <tr>
                                                <td>{{$raw_material_movement->created_at->format('d-m-Y h:i:s a')}}</td>
                                                <td>{{$raw_material_movement->type==1?'Entrada':'Salida'}}</td>
                                                <td>{{number_format($raw_material_movement->new_amount, 2, ',', '.')}}</td>
                                                <td>{{number_format($raw_material_movement->amount, 2, ',', '.')}}</td>
                                                <td>{{$raw_material_movement->price==0?'N/A':number_format($raw_material_movement->price, 2, ',', '.')}}</td>
                                                <td>{{$raw_material_movement->price_bss==0?'N/A':number_format($raw_material_movement->price_bss, 2, ',', '.')}}</td>
                                                <td>{{$raw_material_movement->exchange_rate==0?'N/A':number_format($raw_material_movement->exchange_rate, 2, ',', '.')}}</td>
                                                <td>{!!$raw_material_movement->concept!!}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="callout callout-info shadow">
                        <p>
                            <b>Salidas de {{$raw_material->name}}</b>                       
                            <small>
                                @if(isset($initial_date) && isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @elseif(isset($initial_date) && !isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                                @elseif(!isset($initial_date) && isset($final_date))
                                    | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @else
                                    | Historial completo
                                @endif
                            </small>
                        </p>
                        <div class="table-responsive">
                            <table id="outputs" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Fecha de registro</th>
                                        <th>Tipo</th>
                                        <th>Stock en {{$raw_material->unit}}</th>
                                        <th>Cantidad en {{$raw_material->unit}}</th>
                                        <th>Precio en $</th>
                                        <th>Precio en Bs.S</th>
                                        <th>Tasa en Bs.S</th>
                                        <th>Concepto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($raw_material_movements as $raw_material_movement)
                                        @if($raw_material_movement->type==2)
                                            <tr>
                                                <td>{{$raw_material_movement->created_at->format('d-m-Y h:i:s a')}}</td>
                                                <td>{{$raw_material_movement->type==1?'Entrada':'Salida'}}</td>
                                                <td>{{number_format($raw_material_movement->new_amount, 2, ',', '.')}}</td>
                                                <td>{{number_format($raw_material_movement->amount, 2, ',', '.')}}</td>
                                                <td>{{$raw_material_movement->price==0?'N/A':number_format($raw_material_movement->price, 2, ',', '.')}}</td>
                                                <td>{{$raw_material_movement->price_bss==0?'N/A':number_format($raw_material_movement->price_bss, 2, ',', '.')}}</td>
                                                <td>{{$raw_material_movement->exchange_rate==0?'N/A':number_format($raw_material_movement->exchange_rate, 2, ',', '.')}}</td>
                                                <td>{!!$raw_material_movement->concept!!}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <div class="callout callout-info shadow">
                        <p>
                            <b>Entradas de {{$product->name}}</b>                       
                            <small>
                                @if(isset($initial_date) && isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @elseif(isset($initial_date) && !isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                                @elseif(!isset($initial_date) && isset($final_date))
                                    | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @else
                                    | Historial completo
                                @endif
                            </small>
                        </p>
                        <div class="table-responsive">
                            <table id="inputs" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Fecha de registro</th>
                                        <th>Tipo</th>
                                        <th>Stock</th>
                                        <th>Cantidad</th>
                                        <th>Precio en $</th>
                                        <th>Precio en Bs.S</th>
                                        <th>Tasa en Bs.S</th>
                                        <th>Concepto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($unprocessed_products_movements as $unprocessed_product_movement)
                                        @if($unprocessed_product_movement->type==1)
                                            <tr>
                                                <td>{{$unprocessed_product_movement->created_at->format('d-m-Y h:i:s a')}}</td>
                                                <td>{{$unprocessed_product_movement->type==1?'Entrada':'Salida'}}</td>
                                                <td>{{number_format($unprocessed_product_movement->new_amount, 0, ',', '.')}}</td>
                                                <td>{{number_format($unprocessed_product_movement->amount, 0, ',', '.')}}</td>
                                                <td>{{$unprocessed_product_movement->price==0?'N/A':number_format($unprocessed_product_movement->price, 2, ',', '.')}}</td>
                                                <td>{{$unprocessed_product_movement->price_bss==0?'N/A':number_format($unprocessed_product_movement->price_bss, 2, ',', '.')}}</td>
                                                <td>{{$unprocessed_product_movement->price==0?'N/A':number_format($unprocessed_product_movement->exchange_rate, 2, ',', '.')}}</td>
                                                <td>{!!$unprocessed_product_movement->concept!!}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="callout callout-info shadow">
                        <p>
                            <b>Salidas de {{$product->name}}</b>                       
                            <small>
                                @if(isset($initial_date) && isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @elseif(isset($initial_date) && !isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                                @elseif(!isset($initial_date) && isset($final_date))
                                    | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @else
                                    | Historial completo
                                @endif
                            </small>
                        </p>
                        <div class="table-responsive">
                            <table id="outputs" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Fecha de registro</th>
                                        <th>Tipo</th>
                                        <th>Stock</th>
                                        <th>Cantidad</th>
                                        <th>Precio en $</th>
                                        <th>Precio en Bs.S</th>
                                        <th>Tasa en Bs.S</th>
                                        <th>Concepto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($unprocessed_products_movements as $unprocessed_product_movement)
                                        @if($unprocessed_product_movement->type==2)
                                            <tr>
                                                <td>{{$unprocessed_product_movement->created_at->format('d-m-Y h:i:s a')}}</td>
                                                <td>{{$unprocessed_product_movement->type==1?'Entrada':'Salida'}}</td>
                                                <td>{{number_format($unprocessed_product_movement->new_amount, 0, ',', '.')}}</td>
                                                <td>{{number_format($unprocessed_product_movement->amount, 0, ',', '.')}}</td>
                                                <td>{{$unprocessed_product_movement->price==0?'N/A':number_format($unprocessed_product_movement->price, 2, ',', '.')}}</td>
                                                <td>{{$unprocessed_product_movement->price_bss==0?'N/A':number_format($unprocessed_product_movement->price_bss, 2, ',', '.')}}</td>
                                                <td>{{$unprocessed_product_movement->price==0?'N/A':number_format($unprocessed_product_movement->exchange_rate, 2, ',', '.')}}</td>
                                                <td>{!!$unprocessed_product_movement->concept!!}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="offset-md-2 col-md-8">
                        <form id="inputs_outputs_rmp" class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                            <input type="hidden" name="type" value="inputs_outputs_rmp" required>
                            <div class="card-header">
                                <h3 class="card-title">Volver a consultar las entradas/salidas de materias primas y de productos sin elaboración</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body p-2">
                                <div class="text-center mb-1">
                                    <small class="text-muted">¡Aquí puedes consultar las entradas/salidas mediante un rango de fechas!</small>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-6">              
                                            <small class="text-muted">Fecha inicial</small>
                                            <div class="input-group mb-3">
                                                <input name="initial_date" id="inputs_outputs_rmp_initial_date" type="datetime-local" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-6">              
                                            <small class="text-muted">Fecha final</small>
                                            <div class="input-group mb-3">
                                                <input name="final_date" id="inputs_outputs_rmp_final_date" type="datetime-local" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-12">              
                                            <small class="text-muted">Selecciona la materia prima o el producto sin elaboración</small>
                                            <select class="selectpicker show-tick form-control form-control-sm" name="rm_prod"data-live-search="true" data-size="5" data-show-subtext="true" required>
                                                @foreach($raw_material_list as $rm)
                                                    @if($raw_material)
                                                        <option {{$raw_material->id==$rm->id?'selected':''}} value="r{{$rm->id}}" data-subtext="Materia prima">{{$rm->name}}</option>
                                                    @else
                                                        <option value="r{{$rm->id}}" data-subtext="Materia prima">{{$rm->name}}</option>
                                                    @endif
                                                @endforeach
                                                <option data-divider="true"></option>
                                                @foreach($products_list as $prod)
                                                    @if($product)
                                                        <option {{$product->id==$prod->id?'selected':''}} value="p{{$prod->id}}" data-subtext="Producto sin elaboración">{{$prod->name}}</option>
                                                    @else
                                                        <option value="p{{$prod->id}}" data-subtext="Producto sin elaboración">{{$prod->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                    <button id="inputs_outputs_rmp_today" type="button" class="btn btn-danger btn-sm mt-1">¡Gastos de hoy!</button>
                                    <small class="text-left text-muted">
                                        <p class="m-0"><b>Tips</b></p>
                                        <p class="m-0">1. Si no ingresas ninguna fecha podrás visualizar el reporte del historial completo.</p>
                                        <p class="m-0">2. Si ingresas ambas fechas podrás visualizar el reporte en el rango determinado.</p>
                                        <p class="m-0">3. Si solo ingresas la fecha inicial podrás visualizar el reporte desde la fecha seleccionada en adelante.</p>
                                        <p class="m-0">4. Si solo ingresas la fecha final podrás visualizar el reporte desde el inicio del sistema hasta la fecha seleccionada.</p>
                                    </small>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $("#inputs_outputs_rmp_today").click(function(event) {
                $("#inputs_outputs_rmp_initial_date").val('{{date("Y-m-d\T00:00")}}');
                $("#inputs_outputs_rmp_final_date").val('{{date("Y-m-d\T23:59")}}');
                $("#inputs_outputs_rmp").submit();
            });

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            $("#inputs").DataTable( {
                "columnDefs": [{ "orderable": false, "targets": -1 }],
                "order": [[ 0, "desc" ]],
                "iDisplayLength": 10,
                "language": {
                    "sProcessing":     "Procesando entradas...",
                    "sLengthMenu":     "Mostrar _MENU_ entradas",
                    "sZeroRecords":    "No se encontraron entradas",
                    "sEmptyTable":     "Ninguna entrada disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ entradas de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ninguna entrada",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ entradas)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando entradas...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );

            $("#outputs").DataTable( {
                "columnDefs": [{ "orderable": false, "targets": -1 }],
                "order": [[ 0, "desc" ]],
                "iDisplayLength": 10,
                "language": {
                    "sProcessing":     "Procesando salidas...",
                    "sLengthMenu":     "Mostrar _MENU_ salidas",
                    "sZeroRecords":    "No se encontraron salidas",
                    "sEmptyTable":     "Ninguna salida disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ salidas de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ninguna salida",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ salidas)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando salidas...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );

        });
    </script>
@endsection