@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-chart-line"></i> Reportes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_reports')}}">Volver</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        @if(isset($initial_date) && isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'most_selled_products', 'initial_date'=>$initial_date, 'final_date'=>$final_date])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @elseif(isset($initial_date) && !isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'most_selled_products', 'initial_date'=>$initial_date, 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @elseif(!isset($initial_date) && isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'most_selled_products', 'initial_date'=>'none', 'final_date'=>$final_date])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @else
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'most_selled_products', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			<div class="invoice p-3 mb-3">
			    <div class="row">
			        <div class="col-12">
			            <h4>
			                <img class="rounded mx-auto" width="50px" src="{{asset($configuration->logo)}}" alt="{{$configuration->name}}">
			                {{$configuration->name}}
			            </h4>
			        </div>
			    </div>
			    <div class="row invoice-info">
			        <div class="col-sm-12 invoice-col">
			            <strong>Productos más vendidos</strong>
			            <small>
                            @if(isset($initial_date) && isset($final_date))
                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                            @elseif(isset($initial_date) && !isset($final_date))
                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                            @elseif(!isset($initial_date) && isset($final_date))
                                | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                            @else
                                | Historial completo
                            @endif
		                </small>
			        </div>
			    </div>
		        <div class="row mt-3">
		            <div class="col-12 table-responsive">
                        <table id="table" class="table table-bordered table-sm display table-hover table-striped table-sm table-pp" cellspacing="0" width="100%">
                            <thead class="thead-dark">
		                        <tr>
		                            <th>Producto</th>
		                            <th>Ventas</th>
                                    <th>Generado en $</th>
                                    <th></th>
		                        </tr>
		                    </thead>
		                    <tbody>
                                @php 
                                    $categories=[];
                                    $categories_prices=[];
                                @endphp
		                        @foreach($products as $product)
		                            <tr>
		                                <td>{{$product->name}}</td>
		                                <td>{{number_format($product->product_sale_amount, 0, ',', '.')}}</td>
                                        <td>{{number_format($product->product_sale_price, 2, ',', '.')}}</td>
                                        <td><a class="btn btn-sm btn-primary mt-1" href="{{route('product_sales',['product_id'=>$product->id])}}"><i class="fa fa-store" aria-hidden="true"></i></a></td>
		                            </tr>

                                    @if(!isset($categories[App\Models\Product::where('id',$product->id)->first()->category->name]))
                                        @php
                                            $categories[App\Models\Product::where('id',$product->id)->first()->category->name]=0;
                                        @endphp
                                    @endif
                                    @php
                                        $categories[App\Models\Product::where('id',$product->id)->first()->category->name]+=$product->product_sale_amount;
                                    @endphp

                                    @if(!isset($categories_prices[App\Models\Product::where('id',$product->id)->first()->category->name]))
                                        @php
                                            $categories_prices[App\Models\Product::where('id',$product->id)->first()->category->name]=0;
                                        @endphp
                                    @endif
                                    @php
                                        $categories_prices[App\Models\Product::where('id',$product->id)->first()->category->name]+=$product->product_sale_price;
                                    @endphp

		                        @endforeach
		                    </tbody>
		                </table>
		            </div>
		        </div>
                <hr>
                <div class="row invoice-info">
                    <div class="col-sm-12 invoice-col">
                        <strong>Promociones más vendidas</strong>
                        <small>
                            @if(isset($initial_date) && isset($final_date))
                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                            @elseif(isset($initial_date) && !isset($final_date))
                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                            @elseif(!isset($initial_date) && isset($final_date))
                                | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                            @else
                                | Historial completo
                            @endif
                        </small>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12 table-responsive">
                        <table id="table-promotions" class="table table-bordered table-sm display table-hover table-striped table-sm table-pp" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Promoción</th>
                                    <th>Ventas</th>
                                    <th>Generado en $</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($promotions as $promotion)
                                    <tr>
                                        <td>{{$promotion->name}}</td>
                                        <td>{{number_format($promotion->promotion_sale_amount, 0, ',', '.')}}</td>
                                        <td>{{number_format($promotion->promotion_sale_price, 2, ',', '.')}}</td>
                                        <td><a class="btn btn-sm btn-primary mt-1" href="{{route('promotion_sales',['promotion_id'=>$promotion->id])}}"><i class="fa fa-store" aria-hidden="true"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row invoice-info">
                    <div class="col-sm-12 invoice-col">
                        <strong>Categorías más vendidas</strong>
                        <small>
                            @if(isset($initial_date) && isset($final_date))
                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                            @elseif(isset($initial_date) && !isset($final_date))
                                | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                            @elseif(!isset($initial_date) && isset($final_date))
                                | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                            @else
                                | Historial completo
                            @endif
                        </small>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12 table-responsive">
                        <table id="table-categories" class="table table-bordered table-sm display table-hover table-striped table-sm" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Categoría</th>
                                    <th>Ventas</th>
                                    <th>Generado en $</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $key=> $sales)
                                    <tr>
                                        <td>{{$key}}</td>
                                        <td>{{number_format($sales, 0, ',', '.')}}</td>
                                        <td>{{number_format($categories_prices[$key], 2, ',', '.')}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>
	        <div class="row">
                <div class="offset-md-2 col-md-8">
                    <form id="admin_new_report" class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="most_selled_products" required>
                        <div class="card-header">
                            <h3 class="card-title">Volver a consultar los productos más vendidos</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar las ventas de todos los productos mediante un rango de fechas!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 offset-sm-2 offset-md-2 col-sm-4 col-md-4">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" id="initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4 col-md-4">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" id="final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                <button id="selling_products_today" type="button" class="btn btn-danger btn-sm mt-1">¡Productos más vendidos hoy!</button>
                                <small class="text-left text-muted">
                                    <p class="m-0"><b>Tips</b></p>
                                    <p class="m-0">1. Si no ingresas ninguna fecha podrás visualizar el reporte del historial completo.</p>
                                    <p class="m-0">2. Si ingresas ambas fechas podrás visualizar el reporte en el rango determinado.</p>
                                    <p class="m-0">3. Si solo ingresas la fecha inicial podrás visualizar el reporte desde la fecha seleccionada en adelante.</p>
                                    <p class="m-0">4. Si solo ingresas la fecha final podrás visualizar el reporte desde el inicio del sistema hasta la fecha seleccionada.</p>
                                </small>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $("#table").DataTable( {
                "iDisplayLength": 100,
                "order": [[ 1, "desc" ]],
                "columnDefs": [{ "orderable": false, "targets": -1 },],
                "language": {
                    "sProcessing":     "Procesando productos...",
                    "sLengthMenu":     "Mostrar _MENU_ productos",
                    "sZeroRecords":    "No se encontraron productos",
                    "sEmptyTable":     "Ningún producto disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ productos de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ningún producto",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ productos)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando productos...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );

            $("#table-promotions").DataTable( {
                "iDisplayLength": 100,
                "order": [[ 1, "desc" ]],
                "columnDefs": [{ "orderable": false, "targets": -1 },],
                "language": {
                    "sProcessing":     "Procesando promociones...",
                    "sLengthMenu":     "Mostrar _MENU_ promociones",
                    "sZeroRecords":    "No se encontraron promociones",
                    "sEmptyTable":     "Ninguna promoción disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ promociones de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ninguna promoción",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ promociones)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando promociones...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );
            
            $("#selling_products_today").click(function(event) {
                $("#initial_date").val('{{date("Y-m-d\T00:00")}}');
                $("#final_date").val('{{date("Y-m-d\T23:59")}}');
                $("#admin_new_report").submit();
            });

            $("#table-categories").DataTable( {
                "iDisplayLength": 100,
                "order": [[ 1, "desc" ]],
                "language": {
                    "sProcessing":     "Procesando categorías...",
                    "sLengthMenu":     "Mostrar _MENU_ categorías",
                    "sZeroRecords":    "No se encontraron categorías",
                    "sEmptyTable":     "Ninguna categoría disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ categorías de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ninguna categoría",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ categorías)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando categorías...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );

        });
    </script>
@endsection