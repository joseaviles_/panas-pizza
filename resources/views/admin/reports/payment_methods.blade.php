@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-chart-line"></i> Reportes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_reports')}}">Volver</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        @if(isset($initial_date) && isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'payment_methods', 'initial_date'=>$initial_date, 'final_date'=>$final_date])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @elseif(isset($initial_date) && !isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'payment_methods', 'initial_date'=>$initial_date, 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @elseif(!isset($initial_date) && isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'payment_methods', 'initial_date'=>'none', 'final_date'=>$final_date])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @else
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'payment_methods', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			<div class="invoice p-3 mb-3">
			    <div class="row">
			        <div class="col-12">
			            <h4>
			                <img class="rounded mx-auto" width="50px" src="{{asset($configuration->logo)}}" alt="{{$configuration->name}}">
			                {{$configuration->name}}
			            </h4>
			        </div>
			    </div>
			    <div class="row invoice-info mb-2">
			        <div class="col-sm-12 invoice-col">
			            <strong>Pagos/cambios de los métodos de pago</strong>
			        </div>
			    </div>
                @foreach($payment_methods as $payment_method)
                    <div class="callout callout-info shadow">
                        <p>
                            <b>{{$payment_method->name}}</b>                       
                            <small>
                                @if(isset($initial_date) && isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @elseif(isset($initial_date) && !isset($final_date))
                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
                                @elseif(!isset($initial_date) && isset($final_date))
                                    | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
                                @else
                                    | Historial completo
                                @endif
                            </small>
                        </p>
                        @if($payment_method->currency=='$')
                            @if(count($payment_method->payment_method_options)>0)
                                
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-sm font-12">
                                                <thead>                  
                                                    <tr>
                                                        <th>Banco o procesador de pago</th>
                                                        <th>Pago en $</th>
                                                        <th>Equivalente en Bs.S</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($payment_method->payment_method_options as $payment_method_option)
                                                        <tr>
                                                            <td>{{$payment_method_option->name}}</td>
                                                            <td>
                                                                @if(isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                                @elseif(isset($initial_date) && !isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                                @elseif(!isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                                @else
                                                                    {{number_format($payment_method_option->payments->sum('payment_amount_usd'), 2, ',', '.')}}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                                @elseif(isset($initial_date) && !isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                                @elseif(!isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                                @else
                                                                    {{number_format($payment_method_option->payments->sum('payment_amount_bss'), 2, ',', '.')}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <th>Total</th>
                                                        <th>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->payments->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @endif
                                                        </th>
                                                        <th>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->payments->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @endif
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-sm font-12">
                                                <thead>                  
                                                    <tr>
                                                        <th>Banco o procesador de cambio</th>
                                                        <th>Cambio en $</th>
                                                        <th>Equivalente en Bs.S</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($payment_method->payment_method_options as $payment_method_option)
                                                        <tr>
                                                            <td>{{$payment_method_option->name}}</td>
                                                            <td>
                                                                @if(isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                                @elseif(isset($initial_date) && !isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                                @elseif(!isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                                @else
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->sum('change_amount_usd'), 2, ',', '.')}}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                                @elseif(isset($initial_date) && !isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                                @elseif(!isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                                @else
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->sum('change_amount_bss'), 2, ',', '.')}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <th>Total</th>
                                                        <th>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @endif
                                                        </th>
                                                        <th>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @endif
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                @foreach($payment_method->payment_method_options as $payment_method_option)
                                    <div class="row mb-3">
                                        <div class="col-12 col-sm-6 col-md-6 border">
                                            <p class="m-0 p-0 text-primary">
                                                <small>
                                                    <b>Pagos en {{$payment_method_option->name}}</b>
                                                    @if(isset($initial_date) && isset($final_date))
                                                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                    @elseif(isset($initial_date) && !isset($final_date))
                                                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                                    @elseif(!isset($initial_date) && isset($final_date))
                                                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                    @else
                                                        | Historial completo
                                                    @endif
                                                </small>
                                            </p>
                                            <div class="table-responsive font-12">
                                                <table id="table-payments-{{$payment_method_option->id}}" class="table table-bordered table-sm">
                                                    <thead>                  
                                                        <tr>
                                                            <th>Fecha</th>
                                                            <th>Pago en $</th>
                                                            <th>Equivalente en Bs.S</th>
                                                            <th>Cliente</th>
                                                            <th>Venta</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        @if(isset($initial_date) && isset($final_date))

                                                            @foreach($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $payment)
                                                                <tr>
                                                                    <td>
                                                                        {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$payment->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @elseif(isset($initial_date) && !isset($final_date))

                                                            @foreach($payment_method_option->payments->where('created_at','>=',$initial_date) as $payment)
                                                                <tr>
                                                                    <td>
                                                                        {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$payment->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @elseif(!isset($initial_date) && isset($final_date))

                                                            @foreach($payment_method_option->payments->where('created_at','<=',$final_date) as $payment)
                                                                <tr>
                                                                    <td>
                                                                        {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$payment->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @else

                                                            @foreach($payment_method_option->payments as $payment)
                                                                <tr>
                                                                    <td>
                                                                        {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$payment->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-6 border">
                                            <p class="m-0 p-0 text-danger">
                                                <small>
                                                    <b>Cambios en {{$payment_method_option->name}}</b>
                                                    @if(isset($initial_date) && isset($final_date))
                                                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                    @elseif(isset($initial_date) && !isset($final_date))
                                                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                                    @elseif(!isset($initial_date) && isset($final_date))
                                                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                    @else
                                                        | Historial completo
                                                    @endif
                                                </small>
                                            </p>
                                            <div class="table-responsive font-12">
                                                <table id="table-changes-{{$payment_method_option->id}}" class="table table-bordered table-sm">
                                                    <thead>                  
                                                        <tr>
                                                            <th>Fecha</th>
                                                            <th>Cambio en $</th>
                                                            <th>Equivalente en Bs.S</th>
                                                            <th>Cliente</th>
                                                            <th>Venta</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        @if(isset($initial_date) && isset($final_date))

                                                            @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('change_amount_usd','<>',0) as $change)
                                                                <tr>
                                                                    <td>
                                                                        {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$change->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @elseif(isset($initial_date) && !isset($final_date))

                                                            @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('change_amount_usd','<>',0) as $change)
                                                                <tr>
                                                                    <td>
                                                                        {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$change->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @elseif(!isset($initial_date) && isset($final_date))

                                                            @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('change_amount_usd','<>',0) as $change)
                                                                <tr>
                                                                    <td>
                                                                        {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$change->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @else

                                                            @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('change_amount_usd','<>',0) as $change)
                                                                <tr>
                                                                    <td>
                                                                        {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$change->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            @else

                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-sm font-12">
                                                <thead>                  
                                                    <tr>
                                                        <th>Pagos en $</th>
                                                        <th>Equivalente en Bs.S</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->payments->where('sale_id','<>',null)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->payments->where('sale_id','<>',null)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-sm font-12">
                                                <thead>                  
                                                    <tr>
                                                        <th>Cambios en $</th>
                                                        <th>Equivalente en Bs.S</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('pay_the_customer',0)->where('sale_id','<>',null)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('pay_the_customer',0)->where('sale_id','<>',null)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-12 col-sm-6 col-md-6 border">
                                        <p class="m-0 p-0 text-primary">
                                            <small>
                                                <b>Pagos en $</b>
                                                @if(isset($initial_date) && isset($final_date))
                                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                @elseif(isset($initial_date) && !isset($final_date))
                                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                                @elseif(!isset($initial_date) && isset($final_date))
                                                    | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                @else
                                                    | Historial completo
                                                @endif
                                            </small>
                                        </p>
                                        <div class="table-responsive font-12">
                                            <table id="table-payment-method-{{$payment_method->id}}" class="table table-bordered table-sm">
                                                <thead>                  
                                                    <tr>
                                                        <th>Fecha</th>
                                                        <th>Pago en $</th>
                                                        <th>Equivalente en Bs.S</th>
                                                        <th>Cliente</th>
                                                        <th>Venta</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @if(isset($initial_date) && isset($final_date))

                                                        @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $payment)
                                                            <tr>
                                                                <td>
                                                                    {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$payment->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @elseif(isset($initial_date) && !isset($final_date))

                                                        @foreach($payment_method->payments->where('created_at','>=',$initial_date) as $payment)
                                                            <tr>
                                                                <td>
                                                                    {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$payment->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @elseif(!isset($initial_date) && isset($final_date))

                                                        @foreach($payment_method->payments->where('created_at','<=',$final_date) as $payment)
                                                            <tr>
                                                                <td>
                                                                    {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$payment->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @else

                                                        @foreach($payment_method->payments as $payment)
                                                            <tr>
                                                                <td>
                                                                    {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$payment->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @endif

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6 border">
                                        <p class="m-0 p-0 text-danger">
                                            <small>
                                                <b>Cambios en $</b>
                                                @if(isset($initial_date) && isset($final_date))
                                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                @elseif(isset($initial_date) && !isset($final_date))
                                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                                @elseif(!isset($initial_date) && isset($final_date))
                                                    | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                @else
                                                    | Historial completo
                                                @endif
                                            </small>
                                        </p>
                                        <div class="table-responsive font-12">
                                            <table id="table-change-method-{{$payment_method->id}}" class="table table-bordered table-sm">
                                                <thead>                  
                                                    <tr>
                                                        <th>Fecha</th>
                                                        <th>Cambio en $</th>
                                                        <th>Equivalente en Bs.S</th>
                                                        <th>Cliente</th>
                                                        <th>Venta</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @if(isset($initial_date) && isset($final_date))

                                                        @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                            <tr>
                                                                <td>
                                                                    {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$change->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @elseif(isset($initial_date) && !isset($final_date))

                                                        @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                            <tr>
                                                                <td>
                                                                    {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$change->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @elseif(!isset($initial_date) && isset($final_date))

                                                        @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                            <tr>
                                                                <td>
                                                                    {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$change->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @else

                                                        @foreach($payment_method->changes->where('pay_the_customer',0)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                            <tr>
                                                                <td>
                                                                    {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$change->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @endif

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                @php 
                                    $sales_usd_total=0; 
                                    $sales_bss_total=0; 
                                @endphp

                                @if(isset($initial_date) && isset($final_date))
                                    
                                    @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null) as $sale_payment)

                                        @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                            @php 
                                                $sales_usd_total+=$sale_payment->sale->total;
                                                $sales_bss_total+=$sale_payment->sale->total_bss; 
                                            @endphp
                                        @else

                                            @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                                @php
                                                    $sales_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                                    $sales_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                                @endphp
                                            @else
                                                @php
                                                    $sales_usd_total+=($sale_payment->payment_amount_usd);
                                                    $sales_bss_total+=($sale_payment->payment_amount_bss);
                                                @endphp
                                            @endif

                                        @endif   

                                    @endforeach

                                @elseif(isset($initial_date) && !isset($final_date))

                                    @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('sale_id','<>',null) as $sale_payment)

                                        @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                            @php 
                                                $sales_usd_total+=$sale_payment->sale->total;
                                                $sales_bss_total+=$sale_payment->sale->total_bss; 
                                            @endphp
                                        @else

                                            @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                                @php
                                                    $sales_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                                    $sales_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                                @endphp
                                            @else
                                                @php
                                                    $sales_usd_total+=($sale_payment->payment_amount_usd);
                                                    $sales_bss_total+=($sale_payment->payment_amount_bss);
                                                @endphp
                                            @endif

                                        @endif 

                                    @endforeach

                                @elseif(!isset($initial_date) && isset($final_date))

                                    @foreach($payment_method->payments->where('created_at','<=',$final_date)->where('sale_id','<>',null) as $sale_payment)

                                        @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                            @php 
                                                $sales_usd_total+=$sale_payment->sale->total;
                                                $sales_bss_total+=$sale_payment->sale->total_bss; 
                                            @endphp
                                        @else

                                            @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                                @php
                                                    $sales_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                                    $sales_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                                @endphp
                                            @else
                                                @php
                                                    $sales_usd_total+=($sale_payment->payment_amount_usd);
                                                    $sales_bss_total+=($sale_payment->payment_amount_bss);
                                                @endphp
                                            @endif

                                        @endif  

                                    @endforeach

                                @else

                                    @foreach($payment_method->payments->where('sale_id','<>',null) as $sale_payment)

                                        @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                            @php 
                                                $sales_usd_total+=$sale_payment->sale->total;
                                                $sales_bss_total+=$sale_payment->sale->total_bss; 
                                            @endphp
                                        @else

                                            @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                                @php
                                                    $sales_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                                    $sales_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                                @endphp
                                            @else
                                                @php
                                                    $sales_usd_total+=($sale_payment->payment_amount_usd);
                                                    $sales_bss_total+=($sale_payment->payment_amount_bss);
                                                @endphp
                                            @endif

                                        @endif 

                                    @endforeach

                                @endif

                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-sm font-12">
                                                <thead>                  
                                                    <tr>
                                                        <th>Ingresos netos de ventas en $</th>
                                                        <th>Equivalente en Bs.S</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            {{number_format($sales_usd_total, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($sales_bss_total, 2, ',', '.')}}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            @endif
                        @elseif($payment_method->currency=='Bs.S')
                            @if(count($payment_method->payment_method_options)>0)
                                
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-sm font-12">
                                                <thead>                  
                                                    <tr>
                                                        <th>Banco o procesador de pago</th>
                                                        <th>Pago en Bs.S</th>
                                                        <th>Equivalente en $</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($payment_method->payment_method_options as $payment_method_option)
                                                        <tr>
                                                            <td>{{$payment_method_option->name}}</td>
                                                            <td>
                                                                @if(isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                                @elseif(isset($initial_date) && !isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                                @elseif(!isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                                @else
                                                                    {{number_format($payment_method_option->payments->sum('payment_amount_bss'), 2, ',', '.')}}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                                @elseif(isset($initial_date) && !isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','>=',$initial_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                                @elseif(!isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->payments->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                                @else
                                                                    {{number_format($payment_method_option->payments->sum('payment_amount_usd'), 2, ',', '.')}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <th>Total</th>
                                                        <th>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->payments->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @endif
                                                        </th>
                                                        <th>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','<=',$final_date)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->payments->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @endif
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-sm font-12">
                                                <thead>                  
                                                    <tr>
                                                        <th>Banco o procesador de cambio</th>
                                                        <th>Cambio en Bs.S</th>
                                                        <th>Equivalente en $</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($payment_method->payment_method_options as $payment_method_option)
                                                        <tr>
                                                            <td>{{$payment_method_option->name}}</td>
                                                            <td>
                                                                @if(isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                                @elseif(isset($initial_date) && !isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                                @elseif(!isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                                @else
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->sum('change_amount_bss'), 2, ',', '.')}}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                                @elseif(isset($initial_date) && !isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                                @elseif(!isset($initial_date) && isset($final_date))
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                                @else
                                                                    {{number_format($payment_method_option->changes->where('pay_the_customer',0)->sum('change_amount_usd'), 2, ',', '.')}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <th>Total</th>
                                                        <th>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @endif
                                                        </th>
                                                        <th>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @endif
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                @foreach($payment_method->payment_method_options as $payment_method_option)
                                    <div class="row mb-3">
                                        <div class="col-12 col-sm-6 col-md-6 border">
                                            <p class="m-0 p-0 text-primary">
                                                <small>
                                                    <b>Pagos en {{$payment_method_option->name}}</b>
                                                    @if(isset($initial_date) && isset($final_date))
                                                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                    @elseif(isset($initial_date) && !isset($final_date))
                                                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                                    @elseif(!isset($initial_date) && isset($final_date))
                                                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                    @else
                                                        | Historial completo
                                                    @endif
                                                </small>
                                            </p>
                                            <div class="table-responsive font-12">
                                                <table id="table-payments-{{$payment_method_option->id}}" class="table table-bordered table-sm">
                                                    <thead>                  
                                                        <tr>
                                                            <th>Fecha</th>
                                                            @if($payment_method->id==1 || $payment_method->id==2 || $payment_method->id==11)
                                                                <th>Nro. de referencia</th>
                                                            @endif
                                                            <th>Pago en Bs.S</th>
                                                            <th>Equivalente en $</th>
                                                            <th>Cliente</th>
                                                            <th>Venta</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        @if(isset($initial_date) && isset($final_date))

                                                            @foreach($payment_method_option->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $payment)
                                                                <tr>
                                                                    <td>
                                                                        {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    @if($payment_method->id==1 || $payment_method->id==2 || $payment_method->id==11)
                                                                        <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                                    @endif
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$payment->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @elseif(isset($initial_date) && !isset($final_date))

                                                            @foreach($payment_method_option->payments->where('created_at','>=',$initial_date) as $payment)
                                                                <tr>
                                                                    <td>
                                                                        {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    @if($payment_method->id==1 || $payment_method->id==2 || $payment_method->id==11)
                                                                        <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                                    @endif
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$payment->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @elseif(!isset($initial_date) && isset($final_date))

                                                            @foreach($payment_method_option->payments->where('created_at','<=',$final_date) as $payment)
                                                                <tr>
                                                                    <td>
                                                                        {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    @if($payment_method->id==1 || $payment_method->id==2 || $payment_method->id==11)
                                                                        <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                                    @endif
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$payment->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @else

                                                            @foreach($payment_method_option->payments as $payment)
                                                                <tr>
                                                                    <td>
                                                                        {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    @if($payment_method->id==1 || $payment_method->id==2 || $payment_method->id==11)
                                                                        <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                                    @endif
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$payment->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-6 border">
                                            <p class="m-0 p-0 text-danger">
                                                <small>
                                                    <b>Cambios en {{$payment_method_option->name}}</b>
                                                    @if(isset($initial_date) && isset($final_date))
                                                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                    @elseif(isset($initial_date) && !isset($final_date))
                                                        | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                                    @elseif(!isset($initial_date) && isset($final_date))
                                                        | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                    @else
                                                        | Historial completo
                                                    @endif
                                                </small>
                                            </p>
                                            <div class="table-responsive font-12">
                                                <table id="table-changes-{{$payment_method_option->id}}" class="table table-bordered table-sm">
                                                    <thead>                  
                                                        <tr>
                                                            <th>Fecha</th>
                                                            <th>Cambio en Bs.S</th>
                                                            <th>Equivalente en $</th>
                                                            <th>Cliente</th>
                                                            <th>Venta</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        @if(isset($initial_date) && isset($final_date))

                                                            @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('change_amount_usd','<>',0) as $change)
                                                                <tr>
                                                                    <td>
                                                                        {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$change->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @elseif(isset($initial_date) && !isset($final_date))

                                                            @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('change_amount_usd','<>',0) as $change)
                                                                <tr>
                                                                    <td>
                                                                        {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$change->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @elseif(!isset($initial_date) && isset($final_date))

                                                            @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('change_amount_usd','<>',0) as $change)
                                                                <tr>
                                                                    <td>
                                                                        {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$change->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @else

                                                            @foreach($payment_method_option->changes->where('pay_the_customer',0)->where('change_amount_usd','<>',0) as $change)
                                                                <tr>
                                                                    <td>
                                                                        {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                    </td>
                                                                    <td>
                                                                        {{$change->sale->client->name_business_phone}}
                                                                    </td>
                                                                    <td>
                                                                        <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                            <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            @else

                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-sm font-12">
                                                <thead>                  
                                                    <tr>
                                                        <th>Pagos en Bs.S</th>
                                                        <th>Equivalente en $</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->payments->where('sale_id','<>',null)->sum('payment_amount_bss'), 2, ',', '.')}}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->payments->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->payments->where('sale_id','<>',null)->sum('payment_amount_usd'), 2, ',', '.')}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-sm font-12">
                                                <thead>                  
                                                    <tr>
                                                        <th>Cambios en Bs.S</th>
                                                        <th>Equivalente en $</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('pay_the_customer',0)->where('sale_id','<>',null)->sum('change_amount_bss'), 2, ',', '.')}}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @elseif(isset($initial_date) && !isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @elseif(!isset($initial_date) && isset($final_date))
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @else
                                                                {{number_format($payment_method->changes->where('pay_the_customer',0)->where('pay_the_customer',0)->where('sale_id','<>',null)->sum('change_amount_usd'), 2, ',', '.')}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-12 col-sm-6 col-md-6 border">
                                        <p class="m-0 p-0 text-primary">
                                            <small>
                                                <b>Pagos en Bs.S</b>
                                                @if(isset($initial_date) && isset($final_date))
                                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                @elseif(isset($initial_date) && !isset($final_date))
                                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                                @elseif(!isset($initial_date) && isset($final_date))
                                                    | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                @else
                                                    | Historial completo
                                                @endif
                                            </small>
                                        </p>
                                        <div class="table-responsive font-12">
                                            <table id="table-payment-method-{{$payment_method->id}}" class="table table-bordered table-sm">
                                                <thead>                  
                                                    <tr>
                                                        <th>Fecha</th>
                                                        @if($payment_method->id==11)
                                                            <th>Nro. de referencia</th>
                                                        @endif
                                                        <th>Pago en Bs.S</th>
                                                        <th>Equivalente en $</th>
                                                        <th>Cliente</th>
                                                        <th>Venta</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @if(isset($initial_date) && isset($final_date))

                                                        @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date) as $payment)
                                                            <tr>
                                                                <td>
                                                                    {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                @if($payment_method->id==11)
                                                                    <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                                @endif
                                                                <td>
                                                                    {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$payment->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @elseif(isset($initial_date) && !isset($final_date))

                                                        @foreach($payment_method->payments->where('created_at','>=',$initial_date) as $payment)
                                                            <tr>
                                                                <td>
                                                                    {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                @if($payment_method->id==11)
                                                                    <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                                @endif
                                                                <td>
                                                                    {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$payment->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @elseif(!isset($initial_date) && isset($final_date))

                                                        @foreach($payment_method->payments->where('created_at','<=',$final_date) as $payment)
                                                            <tr>
                                                                <td>
                                                                    {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                @if($payment_method->id==11)
                                                                    <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                                @endif
                                                                <td>
                                                                    {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$payment->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @else

                                                        @foreach($payment_method->payments as $payment)
                                                            <tr>
                                                                <td>
                                                                    {{$payment->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                @if($payment_method->id==11)
                                                                    <td>{{$payment->payment_reference_number==null?'N/A':$payment->payment_reference_number}}</td>
                                                                @endif
                                                                <td>
                                                                    {{number_format($payment->payment_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($payment->payment_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$payment->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$payment->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$payment->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @endif

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6 border">
                                        <p class="m-0 p-0 text-danger">
                                            <small>
                                                <b>Cambios en Bs.S</b>
                                                @if(isset($initial_date) && isset($final_date))
                                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                @elseif(isset($initial_date) && !isset($final_date))
                                                    | Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y')}}</b> en adelante
                                                @elseif(!isset($initial_date) && isset($final_date))
                                                    | Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y')}}</b>
                                                @else
                                                    | Historial completo
                                                @endif
                                            </small>
                                        </p>
                                        <div class="table-responsive font-12">
                                            <table id="table-change-method-{{$payment_method->id}}" class="table table-bordered table-sm">
                                                <thead>                  
                                                    <tr>
                                                        <th>Fecha</th>
                                                        <th>Cambio en Bs.S</th>
                                                        <th>Equivalente en $</th>
                                                        <th>Cliente</th>
                                                        <th>Venta</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @if(isset($initial_date) && isset($final_date))

                                                        @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                            <tr>
                                                                <td>
                                                                    {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$change->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @elseif(isset($initial_date) && !isset($final_date))

                                                        @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','>=',$initial_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                            <tr>
                                                                <td>
                                                                    {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$change->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @elseif(!isset($initial_date) && isset($final_date))

                                                        @foreach($payment_method->changes->where('pay_the_customer',0)->where('created_at','<=',$final_date)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                            <tr>
                                                                <td>
                                                                    {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$change->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @else

                                                        @foreach($payment_method->changes->where('pay_the_customer',0)->where('sale_id','<>',null)->where('change_amount_usd','<>',0) as $change)
                                                            <tr>
                                                                <td>
                                                                    {{$change->created_at->format('d-m-Y h:i:s a')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_bss, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{number_format($change->change_amount_usd, 2, ',', '.')}}
                                                                </td>
                                                                <td>
                                                                    {{$change->sale->client->name_business_phone}}
                                                                </td>
                                                                <td>
                                                                    <a target="_blank" class="text-primary text-decoration-none" href="{{route('sales_show',['sale_id'=>$change->sale_id])}}">
                                                                        <i class="fa fa-search" aria-hidden="true"></i> ID de venta: {{$change->sale_id}}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @endif

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                @php 
                                    $sales_bss_usd_total=0; 
                                    $sales_bss_bss_total=0; 
                                @endphp

                                @if(isset($initial_date) && isset($final_date))
                                    
                                    @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->where('sale_id','<>',null) as $sale_payment)

                                        @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                            @php 
                                                $sales_bss_usd_total+=$sale_payment->sale->total;
                                                $sales_bss_bss_total+=$sale_payment->sale->total_bss; 
                                            @endphp
                                        @else

                                            @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                                @php
                                                    $sales_bss_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                                    $sales_bss_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                                @endphp
                                            @else
                                                @php
                                                    $sales_bss_usd_total+=($sale_payment->payment_amount_usd);
                                                    $sales_bss_bss_total+=($sale_payment->payment_amount_bss);
                                                @endphp
                                            @endif

                                        @endif  

                                    @endforeach

                                @elseif(isset($initial_date) && !isset($final_date))

                                    @foreach($payment_method->payments->where('created_at','>=',$initial_date)->where('sale_id','<>',null) as $sale_payment)

                                        @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                            @php 
                                                $sales_bss_usd_total+=$sale_payment->sale->total;
                                                $sales_bss_bss_total+=$sale_payment->sale->total_bss; 
                                            @endphp
                                        @else

                                            @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                                @php
                                                    $sales_bss_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                                    $sales_bss_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                                @endphp
                                            @else
                                                @php
                                                    $sales_bss_usd_total+=($sale_payment->payment_amount_usd);
                                                    $sales_bss_bss_total+=($sale_payment->payment_amount_bss);
                                                @endphp
                                            @endif

                                        @endif   

                                    @endforeach

                                @elseif(!isset($initial_date) && isset($final_date))

                                    @foreach($payment_method->payments->where('created_at','<=',$final_date)->where('sale_id','<>',null) as $sale_payment)

                                        @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                            @php 
                                                $sales_bss_usd_total+=$sale_payment->sale->total;
                                                $sales_bss_bss_total+=$sale_payment->sale->total_bss; 
                                            @endphp
                                        @else

                                            @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                                @php
                                                    $sales_bss_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                                    $sales_bss_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                                @endphp
                                            @else
                                                @php
                                                    $sales_bss_usd_total+=($sale_payment->payment_amount_usd);
                                                    $sales_bss_bss_total+=($sale_payment->payment_amount_bss);
                                                @endphp
                                            @endif

                                        @endif  

                                    @endforeach

                                @else

                                    @foreach($payment_method->payments->where('sale_id','<>',null) as $sale_payment)

                                        @if($sale_payment->sale->total < $sale_payment->payment_amount_usd)
                                            @php 
                                                $sales_bss_usd_total+=$sale_payment->sale->total;
                                                $sales_bss_bss_total+=$sale_payment->sale->total_bss; 
                                            @endphp
                                        @else

                                            @if($sale_payment->payment_amount_usd >= $sale_payment->change_amount_usd)
                                                @php
                                                    $sales_bss_usd_total+=($sale_payment->payment_amount_usd-$sale_payment->change_amount_usd);
                                                    $sales_bss_bss_total+=($sale_payment->payment_amount_bss-$sale_payment->change_amount_bss);
                                                @endphp
                                            @else
                                                @php
                                                    $sales_bss_usd_total+=($sale_payment->payment_amount_usd);
                                                    $sales_bss_bss_total+=($sale_payment->payment_amount_bss);
                                                @endphp
                                            @endif

                                        @endif 

                                    @endforeach

                                @endif

                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-sm font-12">
                                                <thead>                  
                                                    <tr>
                                                        <th>Ingresos netos de ventas en Bs.S</th>
                                                        <th>Equivalente en $</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            {{number_format($sales_bss_bss_total, 2, ',', '.')}}
                                                        </td>
                                                        <td>
                                                            {{number_format($sales_bss_usd_total, 2, ',', '.')}}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            @endif
                        @endif
                    </div>
                @endforeach
			</div>
	        <div class="row">
                <div class="offset-md-2 col-md-8">
                    <form id="payment_methods" class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="payment_methods" required>
                        <div class="card-header">
                            <h3 class="card-title">Volver a consultar los pagos/cambios de los métodos de pagos</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar los pagos/cambios de todos los métodos de pagos mediante un rango de fechas!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" id="pm_initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" id="pm_final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                <button id="pm_today" type="button" class="btn btn-danger btn-sm mt-1">¡Pagos/cambios de hoy!</button>
                                <small class="text-left text-muted">
                                    <p class="m-0"><b>Tips</b></p>
                                    <p class="m-0">1. Si no ingresas ninguna fecha podrás visualizar el reporte del historial completo.</p>
                                    <p class="m-0">2. Si ingresas ambas fechas podrás visualizar el reporte en el rango determinado.</p>
                                    <p class="m-0">3. Si solo ingresas la fecha inicial podrás visualizar el reporte desde la fecha seleccionada en adelante.</p>
                                    <p class="m-0">4. Si solo ingresas la fecha final podrás visualizar el reporte desde el inicio del sistema hasta la fecha seleccionada.</p>
                                </small>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {
            
            $("#pm_today").click(function(event) {
                $("#pm_initial_date").val('{{date("Y-m-d\T00:00")}}');
                $("#pm_final_date").val('{{date("Y-m-d\T23:59")}}');
                $("#payment_methods").submit();
            });

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            @foreach($payment_methods as $payment_method)

                $("#table-payment-method-{{$payment_method->id}}").DataTable( {
                    "iDisplayLength": 10,
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "sProcessing":     "Procesando pagos...",
                        "sLengthMenu":     "Mostrar _MENU_ pagos",
                        "sZeroRecords":    "No se encontraron pagos",
                        "sEmptyTable":     "Ningún pago disponible en esta tabla",
                        "sInfo":           "",
                        "sInfoEmpty":      "",
                        "sInfoFiltered":   "",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando pagos...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "decimal": ",",
                        "thousands": "."
                    }
                } );

                $("#table-change-method-{{$payment_method->id}}").DataTable( {
                    "iDisplayLength": 10,
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "sProcessing":     "Procesando cambios...",
                        "sLengthMenu":     "Mostrar _MENU_ cambios",
                        "sZeroRecords":    "No se encontraron cambios",
                        "sEmptyTable":     "Ningún cambio disponible en esta tabla",
                        "sInfo":           "",
                        "sInfoEmpty":      "",
                        "sInfoFiltered":   "",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando cambios...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "decimal": ",",
                        "thousands": "."
                    }
                } );

                @foreach($payment_method->payment_method_options as $payment_method_option)
                    
                    $("#table-payments-{{$payment_method_option->id}}").DataTable( {
                        "iDisplayLength": 10,
                        "order": [[ 0, "desc" ]],
                        "language": {
                            "sProcessing":     "Procesando pagos...",
                            "sLengthMenu":     "Mostrar _MENU_ pagos",
                            "sZeroRecords":    "No se encontraron pagos",
                            "sEmptyTable":     "Ningún pago disponible en esta tabla",
                            "sInfo":           "",
                            "sInfoEmpty":      "",
                            "sInfoFiltered":   "",
                            "sInfoPostFix":    "",
                            "sSearch":         "Buscar:",
                            "sUrl":            "",
                            "sInfoThousands":  ",",
                            "sLoadingRecords": "Cargando pagos...",
                            "oPaginate": {
                                "sFirst":    "Primero",
                                "sLast":     "Último",
                                "sNext":     "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            },
                            "decimal": ",",
                            "thousands": "."
                        }
                    } );

                    $("#table-changes-{{$payment_method_option->id}}").DataTable( {
                        "iDisplayLength": 10,
                        "order": [[ 0, "desc" ]],
                        "language": {
                            "sProcessing":     "Procesando cambios...",
                            "sLengthMenu":     "Mostrar _MENU_ cambios",
                            "sZeroRecords":    "No se encontraron cambios",
                            "sEmptyTable":     "Ningún cambio disponible en esta tabla",
                            "sInfo":           "",
                            "sInfoEmpty":      "",
                            "sInfoFiltered":   "",
                            "sInfoPostFix":    "",
                            "sSearch":         "Buscar:",
                            "sUrl":            "",
                            "sInfoThousands":  ",",
                            "sLoadingRecords": "Cargando cambios...",
                            "oPaginate": {
                                "sFirst":    "Primero",
                                "sLast":     "Último",
                                "sNext":     "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            },
                            "decimal": ",",
                            "thousands": "."
                        }
                    } );

                @endforeach
            @endforeach

        });
    </script>
@endsection