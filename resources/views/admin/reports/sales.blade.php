@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-chart-line"></i> Reportes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_reports')}}">Volver</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        @if(isset($initial_date) && isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'sales', 'initial_date'=>$initial_date, 'final_date'=>$final_date])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @elseif(isset($initial_date) && !isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'sales', 'initial_date'=>$initial_date, 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @elseif(!isset($initial_date) && isset($final_date))
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'sales', 'initial_date'=>'none', 'final_date'=>$final_date])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @else
                            <li class="breadcrumb-item"><a target="_blank" class="btn btn-warning btn-sm" href="{{route('admin_pdf_report', ['type'=>'sales', 'initial_date'=>'none', 'final_date'=>'none'])}}"><b><i class="nav-icon fas fa-download"></i> PDF</b></a></li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			<div class="invoice p-3 mb-3">
			    <div class="row">
			        <div class="col-12">
			            <h4>
			                <img class="rounded mx-auto" width="50px" src="{{asset($configuration->logo)}}" alt="{{$configuration->name}}">
			                {{$configuration->name}}
			            </h4>
			        </div>
			    </div>
			    <div class="row invoice-info">
			        <div class="col-sm-12 invoice-col">
			            <strong>Ventas</strong>
			            <small>
			            	@if(isset($initial_date) && isset($final_date))
				            	| Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
			            	@elseif(isset($initial_date) && !isset($final_date))
				            	| Desde el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$initial_date)->format('d-m-Y h:i:s a')}}</b> en adelante
			            	@elseif(!isset($initial_date) && isset($final_date))
			            		| Desde el inicio de las ventas hasta el <b>{{DateTime::createFromFormat('Y-m-d H:i:s',$final_date)->format('d-m-Y h:i:s a')}}</b>
		            		@else
		            			| Historial completo
			            	@endif
		                </small>
			        </div>
			    </div>
	            <div class="row mt-3">
	          		<div class="col-lg-3 col-6">
	            		<div class="small-box bg-info">
	              			<div class="inner">
	                			<p><b>{{count($sales)}}</b></p>
	                			<p>Ventas</p>
	              			</div>
	              			<div class="icon">
	                			<i class="fas fa-store"></i>
	              			</div>
	            		</div>
	          		</div>
	          		<div class="col-lg-3 col-6">
	            		<div class="small-box bg-success">
	              			<div class="inner">
	                			<p><b>{{number_format($sales->sum('total'), 2, ',', '.')}}</b></p>
	                			<p>Total en $</p>
	              			</div>
	              			<div class="icon">
	                			<i class="fas fa-dollar-sign"></i>
	              			</div>
	            		</div>
	          		</div>
	          		<div class="col-lg-3 col-6">
	            		<div class="small-box bg-danger">
	              			<div class="inner">
	              				@php
	              					$pending=0;
								@endphp
	              				@foreach($sales as $sale)
									@php 
										($sale->total-$sale->total_paid)<0?$pending+=0:$pending+=($sale->total-$sale->total_paid);
									@endphp
								@endforeach
	                			<p><b>{{number_format($pending, 2, ',', '.')}}</b></p>
	                			<p>Pendiente en $</p>
	              			</div>
	              			<div class="icon">
	                			<i class="fas fa-dollar-sign"></i>
	              			</div>
	            		</div>
	          		</div>                 
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <p><b>{{number_format($sales->sum('total')-$pending, 2, ',', '.')}}</b></p>
                                <p>Ingreso total en $</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-dollar-sign"></i>
                            </div>
                        </div>
                    </div>
	        	</div>
                <p>Total en $ sin delivery: {{number_format($sales->sum('total'), 2, ',', '.')}}$ <b>(Total)</b> - {{number_format($sales->sum('delivery_amount'), 2, ',', '.')}}$ <b>(Delivery)</b> = <b class="text-success">{{number_format($sales->sum('total')-$sales->sum('delivery_amount'), 2, ',', '.')}}$</b></p>
		        <div class="row">
		            <div class="col-12 table-responsive">
                        <table id="table" class="table table-bordered table-sm display table-hover table-striped table-sm font-12" cellspacing="0" width="100%">
                            <thead class="thead-dark">
		                        <tr>
		                            <th>ID</th>
		                            <th>Fecha de registro</th>
		                            <th>Vendedor</th>
		                            <th>Cliente</th>
		                            <th>Total en $</th>
		                            <th>Pendiente en $</th>
		                            <th>Tipo</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                        @foreach($sales as $sale)
		                            <tr>
		                                <td>{{$sale->id}}</td>
		                                <td>{{$sale->created_at->format('d-m-Y h:i:s a')}}</td>
		                                <td>{{$sale->user->first_name.' '.$sale->user->last_name}}</td>
		                                <td>{{$sale->client->name_business_phone}}</td>
		                                <td>{{number_format($sale->total, 2, ',', '.')}}</td>
		                                <td>{{($sale->total-$sale->total_paid)<0?'0.00':number_format($sale->total-$sale->total_paid, 2, ',', '.')}}</td>
		                                <td>{{$sale->sale_type==1?'Al contado':'A crédito'}}</td>
		                            </tr>
		                        @endforeach
		                    </tbody>
		                </table>
		            </div>
		        </div>
			</div>
	        <div class="row">
                <div class="offset-md-2 col-md-8">
                    <form id="sales" class="card card-primary" action="{{route('admin_new_report')}}" method="get">
                        <input type="hidden" name="type" value="sales" required>
                        <div class="card-header">
                            <h3 class="card-title">Volver a consultar las ventas</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="text-center mb-1">
                                <small class="text-muted">¡Aquí puedes consultar las ventas mediante un rango de fechas!</small>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 offset-sm-2 offset-md-2 col-sm-4 col-md-4">              
                                        <small class="text-muted">Fecha inicial</small>
                                        <div class="input-group mb-3">
                                            <input name="initial_date" id="sales_initial_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4 col-md-4">              
                                        <small class="text-muted">Fecha final</small>
                                        <div class="input-group mb-3">
                                            <input name="final_date" id="sales_final_date" type="datetime-local" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                <button id="sales_today" type="button" class="btn btn-danger btn-sm mt-1">¡Ventas de hoy!</button>
                                <small class="text-left text-muted">
                                    <p class="m-0"><b>Tips</b></p>
                                    <p class="m-0">1. Si no ingresas ninguna fecha podrás visualizar el reporte del historial completo.</p>
                                    <p class="m-0">2. Si ingresas ambas fechas podrás visualizar el reporte en el rango determinado.</p>
                                    <p class="m-0">3. Si solo ingresas la fecha inicial podrás visualizar el reporte desde la fecha seleccionada en adelante.</p>
                                    <p class="m-0">4. Si solo ingresas la fecha final podrás visualizar el reporte desde el inicio del sistema hasta la fecha seleccionada.</p>
                                </small>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

        	$.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            $("#table").DataTable( {
                "iDisplayLength": 100,
                "order": [[ 0, "desc" ]],
                "language": {
                    "sProcessing":     "Procesando ventas...",
                    "sLengthMenu":     "Mostrar _MENU_ ventas",
                    "sZeroRecords":    "No se encontraron ventas",
                    "sEmptyTable":     "Ninguna venta disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ ventas de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ninguna venta",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ ventas)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando ventas...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "decimal": ",",
                    "thousands": "."
                }
            } );
            
            $("#sales_today").click(function(event) {
                $("#sales_initial_date").val('{{date("Y-m-d\T00:00")}}');
                $("#sales_final_date").val('{{date("Y-m-d\T23:59")}}');
                $("#sales").submit();
            });

        });
    </script>
@endsection