@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-users"></i> Usuarios</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_users_create')}}">Registrar</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="table-responsive">
                <table id="table-list" class="table table-bordered table-sm display table-hover table-pp font-12" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Cédula de identidad</th>
                            <th>Correo electrónico</th>
                            <th>Rol</th>
                            <th>Fecha de registro</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            $("#table-list").DataTable( {
                "ajax": "{{route('get_admin_users')}}",
                "columnDefs": [{ "orderable": false, "targets": -1 }],
                "iDisplayLength": 25,
                "language": {
                    "sProcessing":     "Procesando usuarios...",
                    "sLengthMenu":     "Mostrar _MENU_ usuarios",
                    "sZeroRecords":    "No se encontraron usuarios",
                    "sEmptyTable":     "Ningún usuario disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ usuarios de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ningún usuario",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando usuarios...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
        });
    </script>
@endsection