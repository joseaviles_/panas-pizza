@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-users"></i> Usuarios</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_users')}}">Listado</a></li>
                    </ol>
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-warning btn-sm" href="{{route('admin_users_trash',['user_id'=>$edit_user->id])}}"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Editar</h3>
                </div>
                <form action="{{route('admin_users_update')}}" method="post">
                    @csrf
                    <input type="hidden" name="user_id" value="{{$edit_user->id}}"/>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-3">              
                                    <small class="text-muted">* Nombre</small>
                                    <div class="input-group mb-3">
                                        <input name="first_name" type="text" class="form-control" placeholder="Nombre" value="{{$edit_user->first_name}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-user"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3">
                                    <small class="text-muted">* Apellido</small>
                                    <div class="input-group mb-3">
                                        <input name="last_name" type="text" class="form-control" placeholder="Apellido" value="{{$edit_user->last_name}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-user"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3">
                                    <small class="text-muted">* Cédula de identidad</small>
                                    <div class="input-group mb-3">
                                        <input name="identity_card" type="text" class="form-control" placeholder="Cédula de identidad" value="{{$edit_user->identity_card}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-id-card"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3">              
                                    <small class="text-muted">* Correo electrónico</small>
                                    <div class="input-group mb-3">
                                        <input name="email" type="email" class="form-control" placeholder="Correo electrónico" value="{{$edit_user->email}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-envelope"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-4">
                                    <small class="text-muted">* Rol</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" name="role" id="role">
                                            @foreach($roles as $role)
                                                <option @if($edit_user->hasRole($role->name)) selected @endif value="{{$role->name}}">{{$role->name=='admin'?'Administrador':'Vendedor'}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4">
                                    <small class="text-muted">Contraseña</small>
                                    <div class="input-group mb-3">
                                        <input name="password" type="password" class="form-control" placeholder="Opcional">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-lock"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4">
                                    <small class="text-muted">Confirmar contraseña</small>
                                    <div class="input-group mb-3">
                                        <input name="password_confirmation" type="password" class="form-control" placeholder="Opcional">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-lock"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection