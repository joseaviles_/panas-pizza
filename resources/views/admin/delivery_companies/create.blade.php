@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-shipping-fast"></i> Empresas de delivery</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_delivery_companies')}}">Listado</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Registrar</h3>
                </div>
                <form action="{{route('admin_delivery_companies_post_create')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6">              
                                    <small class="text-muted">* Nombre</small>
                                    <div class="input-group mb-3">
                                        <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{old('name')}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-info"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="div-type-delivery">
                            <div class="card">
                                <div class="card-header border-0">
                                    <h3 class="card-title float-none text-center p-0">Tipos de delivery</h3>
                                </div>
                                <div class="card-body p-0">
                                    <div class="table-responsive mx-auto div-table-dynamic">
                                        <table class="table table-bordered table-sm">
                                            <thead>                  
                                                <tr>
                                                    <td class="td-350">Nombre del tipo de delivery</td>
                                                    <td class="td-125">Precio del tipo de delivery</td>
                                                    <td class="td-125"></td>
                                                </tr>
                                            </thead>
                                            <tbody id="type-delivery">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="text-center mt-3 mb-3">
                                    <button type="button" id="add-type-delivery" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> Agregar tipo de delivery</button>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="raw-options">
                            <div class="card">
                                <div class="card-header border-0">
                                    <h3 class="card-title float-none text-center p-0">Conductores</h3>
                                </div>
                                <div class="card-body p-0">
                                    <div class="table-responsive mx-auto div-table-dynamic">
                                        <table class="table table-bordered table-sm">
                                            <thead>                  
                                                <tr>
                                                    <td class="td-350">Nombre</td>
                                                    <td class="td-125"></td>
                                                </tr>
                                            </thead>
                                            <tbody id="cols">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="text-center mt-3 mb-3">
                                    <button type="button" id="add-drivers" class="btn btn-dark btn-sm"><i class="fas fa-plus"></i> Agregar conductor</button>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/autoNumeric.js')}}"></script>
    <script>
        $(document).ready(function() {

            var type_delivery = $("#type-delivery");
            var j=0;
            
            $("#add-type-delivery").click(function() {
                j++;
                addNewTypeDelivery(j);
            });

            function addNewTypeDelivery(k) {

                type_delivery.append(`
                    <tr id="tr-`+k+`">
                        <td>
                            <input name="types_delivery[]" type="text" class="form-control form-control-sm" placeholder="Ej: Delivery corto" required>
                        </td>
                        <td>
                            <input name="prices_delivery[]" id="price-`+k+`" type="text" class="form-control form-control-sm" placeholder="Ej: 2,00" required>
                        </td>
                        <td class="px-1" style="text-align: center !important;">
                            <button type="button" id="remove-div-`+k+`" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                `);

                $("#remove-div-"+k).click(function(event) {
                    $("#tr-"+k).remove();
                });

                $("#price-"+k).autoNumeric("init", {aSep: ".", aDec: ",", mDec: "2"});

            }

            var cols = $("#cols");
            var i=0;
            
            $("#add-drivers").click(function() {
                i++;
                addNew(i);
            });

            function addNew(j) {

                cols.append(`
                    <tr id="tr-col-`+j+`">
                        <td> 
                            <input name="names[]" type="text" class="form-control form-control-sm" placeholder="Nombre y apellido del conductor" required>
                        </td>
                        <td class="px-1" style="text-align: center !important;">
                            <button type="button" id="remove-div-col-`+j+`" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                `);

                $("#remove-div-col-"+j).click(function(event) {
                    $("#tr-col-"+j).remove();
                });

            }

            function matchCustom(params, data) {
                
                if($.trim(params.term) === '') {
                    return data;
                }

                if(typeof data.text === 'undefined') {
                    return null;
                }
                
                if(data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }

                return null;
            }

        });
    </script>
@endsection