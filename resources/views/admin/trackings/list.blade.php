@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-list-ol"></i> Seguimientos</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="table-responsive">
                <table id="table-list" class="table table-bordered table-sm display table-hover font-12" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>Fecha de registro</th>
                            <th>Usuario</th>
                            <th>Operación</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $.fn.dataTable.moment('DD-MM-YYYY hh:mm:ss a');

            $("#table-list").DataTable( {
                "ajax": "{{route('get_admin_trackings')}}",
                "iDisplayLength": 25,
                "order": [[ 0, "desc" ]],
                "language": {
                    "sProcessing":     "Procesando seguimientos...",
                    "sLengthMenu":     "Mostrar _MENU_ seguimientos",
                    "sZeroRecords":    "No se encontraron seguimientos",
                    "sEmptyTable":     "Ningún seguimiento disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ seguimientos de un total de _TOTAL_",
                    "sInfoEmpty":      "No se ha registrado ningún seguimiento",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ seguimientos)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando seguimientos...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
        });
    </script>
@endsection