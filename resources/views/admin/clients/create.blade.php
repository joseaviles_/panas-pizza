@extends("layouts.main")

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-address-book"></i> Clientes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-danger btn-sm" href="{{route('admin_clients')}}">Listado</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Registrar</h3>
                </div>
                <form action="{{route('admin_clients_post_create')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Número de cliente</small>
                                    <div class="input-group mb-3">
                                        <input name="client_number" type="text" class="form-control" placeholder="Número de cliente" value="{{old('client_number')}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-info"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Nombre</small>
                                    <div class="input-group mb-3">
                                        <input name="first_name" type="text" class="form-control" placeholder="Nombre" value="{{old('first_name')}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-user"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Apellido</small>
                                    <div class="input-group mb-3">
                                        <input name="last_name" type="text" class="form-control" placeholder="Apellido" value="{{old('last_name')}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-user"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">Cédula de identidad</small>
                                    <div class="input-group mb-3">
                                        <input name="identity_card" type="text" class="form-control" placeholder="Opcional" value="{{old('identity_card')}}">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-id-card"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Nombre registrado en el teléfono del negocio</small>
                                    <div class="input-group mb-3">
                                        <input name="name_business_phone" type="text" class="form-control" placeholder="Nombre registrado en el teléfono del negocio" value="{{old('name_business_phone')}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-info"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">* Teléfono #1</small>
                                    <div class="input-group mb-3">
                                        <input name="first_phone" type="text" class="form-control" placeholder="Teléfono #1" value="{{old('first_phone')}}" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-phone"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">              
                                    <small class="text-muted">Teléfono #2</small>
                                    <div class="input-group mb-3">
                                        <input name="second_phone" type="text" class="form-control" placeholder="Opcional" value="{{old('second_phone')}}">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-phone"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection