@extends("layouts.home")

@section("content")
  	<div class="login-logo">
		<a href="{{route('home')}}"><img src="{{asset($configuration->logo)}}" class="rounded" alt="{{$configuration->name}}" width="150"></a>
  	</div>
  	<div class="card">
	    <div class="card-body login-card-body">
	      	<p class="login-box-msg">{{$user->first_name}}, ingresa tu nueva contraseña</p>
	      	@if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                  	<h5><i class="icon fas fa-check"></i> Info</h5>
              		{{session('message_info')}}
                </div>
  			@endif
	      	@if($errors->any())
                <div class="alert alert-danger alert-dismissible">
              		<h5><i class="icon fas fa-ban"></i> Error</h5>
              		<ul>
			          	@foreach ($errors->all() as $error)
		                	<li>{{$error}}</li>
			          	@endforeach
		          	</ul>
	          	</div>
 	 		@endif
			<form action="{{route('post_password_reset_token')}}" method="post">
				@csrf
				<input name="token" type="hidden" value="{{$token}}"/>
		        <small class="text-muted">Contraseña</small>
		        <div class="input-group mb-3">
		          	<input name="password" type="password" class="form-control" placeholder="Contraseña" required>
		          	<div class="input-group-append">
			            <div class="input-group-text">
			              	<span class="fas fa-lock"></span>
			            </div>
		          	</div>
		        </div>
		        <div class="row">
		          	<div class="col-12">
			            <button type="submit" class="btn btn-primary btn-block">Cambiar contraseña</button>
		          	</div>
		        </div>
	      	</form>
	    </div>
  	</div>
@endsection