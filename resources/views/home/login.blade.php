@extends("layouts.home")

@section("content")
  	<div class="login-logo">
		<a href="{{route('home')}}"><img src="{{asset($configuration->logo)}}" class="rounded" alt="{{$configuration->name}}" width="150"></a>
  	</div>
  	<div class="card">
	    <div class="card-body login-card-body">
	      	<p class="login-box-msg">Ingresa tus credenciales</p>
	      	@if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                  	<h5><i class="icon fas fa-check"></i> Info</h5>
              		{{session('message_info')}}
                </div>
  			@endif
	      	@if($errors->any())
                <div class="alert alert-danger alert-dismissible">
              		<h5><i class="icon fas fa-ban"></i> Error</h5>
              		<ul>
			          	@foreach ($errors->all() as $error)
		                	<li>{{$error}}</li>
			          	@endforeach
		          	</ul>
	          	</div>
 	 		@endif
			<form action="{{route('post_login')}}" method="post">
				@csrf
				<small class="text-muted">Correo electrónico</small>
		        <div class="input-group mb-3">
		          	<input name="email" type="email" class="form-control" placeholder="Correo electrónico" value="{{old('email')}}" required>
		          	<div class="input-group-append">
		            	<div class="input-group-text">
		              		<span class="fas fa-envelope"></span>
		            	</div>
		          	</div>
		        </div>
		        <small class="text-muted">Contraseña</small>
		        <div class="input-group mb-3">
		          	<input name="password" type="password" class="form-control" placeholder="Contraseña" required>
		          	<div class="input-group-append">
			            <div class="input-group-text">
			              	<span class="fas fa-lock"></span>
			            </div>
		          	</div>
		        </div>
		        <div class="row">
		          	<div class="col-12">
			            <button type="submit" class="btn btn-primary btn-block">Iniciar sesión</button>
		          	</div>
		        </div>
	      	</form>
	      	<p class="text-center mt-2 mb-0">
	        	<a href="{{route('password_reset')}}" class="text-center">¿Olvidaste tu contraseña?</a>
	      	</p>
	    </div>
  	</div>
@endsection