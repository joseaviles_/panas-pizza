<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{$configuration->name}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css?v=5')}}">
        @yield("links")
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            @yield("content")
        </div>
        <script type="text/javascript" src="{{asset('js/app.js?v=5')}}"></script>
        @yield("scripts")
    </body>
</html>