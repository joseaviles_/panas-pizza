<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{$configuration->name}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css?v=5')}}">
        @yield("links")
    </head>
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
        <div id="ajaxLoader">
            <i class="fas fa-sync fa-spin"></i>
        </div>
        <div class="wrapper">
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>
            </nav>
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <a href="{{route('dashboard')}}" class="brand-link">
                    <img src="{{asset($configuration->logo)}}" alt="{{$configuration->name}}" class="brand-image">
                    <span class="brand-text font-weight-light">{{$configuration->name}}</span>
                </a>
                <div class="sidebar">
                    @if($user->hasRole('admin'))
                        @include("includes.menu_admin")
                    @elseif($user->hasRole('seller'))
                        @include("includes.menu_seller")
                    @endif
                </div>
            </aside>
            <div class="content-wrapper mb-5">
                @yield("content")
            </div>
            <footer class="main-footer">
                <strong>Copyright &copy; 2020 <a href="{{route('dashboard')}}">{{$configuration->name}}</a>.</strong>
                Todos los derechos reservados.
                <div class="float-right d-none d-sm-inline-block">
                    <b>Versión</b> 1.0.0
                </div>
            </footer>
        </div>
        <script type="text/javascript" src="{{asset('js/app.js?v=5')}}"></script>
        @yield("scripts")
    </body>
</html>