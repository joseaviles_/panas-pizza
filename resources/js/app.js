// Imports
global.$=window.jQuery=require("jquery/dist/jquery");
require("popper.js/dist/popper");
require("admin-lte/plugins/bootstrap/js/bootstrap.bundle.js");
require("admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.js");
require("admin-lte/dist/js/adminlte.js");
require("admin-lte/dist/js/demo.js");
require("admin-lte/plugins/datatables/jquery.dataTables.js");
require("admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js");
require("moment/moment.js");
require("datetime-moment/datetime-moment.js");
require("admin-lte/plugins/chart.js/Chart.js");
require('bootstrap-select/dist/js/bootstrap-select');
require('select2/dist/js/select2.js');

// ajaxStart - ajaxStop
$(document).ready(function() {
	
	$(document).ajaxStart(function() {
        $("#ajaxLoader").addClass("active");
    });

    $(document).ajaxStop(function() {
        $("#ajaxLoader").removeClass("active");
    });

});