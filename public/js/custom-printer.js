const RUTA_API = "http://localhost:8000";
const $estado = document.querySelector("#estado"),
    $listaDeImpresoras = document.querySelector("#listaDeImpresoras"),
    $btnLimpiarLog = document.querySelector("#btnLimpiarLog"),
    $btnRefrescarLista = document.querySelector("#btnRefrescarLista"),
    $btnEstablecerImpresora = document.querySelector("#btnEstablecerImpresora"),
    $texto = document.querySelector("#texto"),
    $impresoraSeleccionada = document.querySelector("#impresoraSeleccionada"),
    $btnImprimir = document.querySelector("#btnImprimir");



const loguear = texto => $estado.textContent += (new Date()).toLocaleString() + " " + texto + "\n";
const limpiarLog = () => $estado.textContent = "";

$btnLimpiarLog.addEventListener("click", limpiarLog);

const limpiarLista = () => {
    for (let i = $listaDeImpresoras.options.length; i >= 0; i--) {
        $listaDeImpresoras.remove(i);
    }
};

const obtenerListaDeImpresoras = () => {
    loguear("Cargando lista...");
    Impresora.getImpresoras()
        .then(listaDeImpresoras => {
            refrescarNombreDeImpresoraSeleccionada();
            loguear("Lista cargada");
            limpiarLista();
            listaDeImpresoras.forEach(nombreImpresora => {
                const option = document.createElement('option');
                option.value = option.text = nombreImpresora;
                $listaDeImpresoras.appendChild(option);
            })
        });
}

const establecerImpresoraComoPredeterminada = nombreImpresora => {
    loguear("Estableciendo impresora...");
    Impresora.setImpresora(nombreImpresora)
        .then(respuesta => {
            refrescarNombreDeImpresoraSeleccionada();
            if (respuesta) {
                loguear(`Impresora ${nombreImpresora} establecida correctamente`);
            } else {
                loguear(`No se pudo establecer la impresora con el nombre ${nombreImpresora}`);
            }
        });
};

const refrescarNombreDeImpresoraSeleccionada = () => {
    Impresora.getImpresora()
        .then(nombreImpresora => {
            $impresoraSeleccionada.textContent = nombreImpresora;
        });
}


$btnRefrescarLista.addEventListener("click", obtenerListaDeImpresoras);
$btnEstablecerImpresora.addEventListener("click", () => {
    const indice = $listaDeImpresoras.selectedIndex;
    if (indice === -1) return loguear("No hay ninguna impresora seleccionada")
    const opcionSeleccionada = $listaDeImpresoras.options[indice];
    establecerImpresoraComoPredeterminada(opcionSeleccionada.value);
});

$btnImprimir.addEventListener("click", () => {
    let impresora = new Impresora(RUTA_API);
    impresora.setFontSize(1, 1);

    impresora.write("\nPANAS PIZZA\n");

    // Today sales

    if($("#today-sales").val().length>0) {
    	impresora.write("VENTA DEL DIA: #"+$("#today-sales").val()+"\n");
	}

    // Client
    var count_clients="{{count($clients)}}";
    var client_type=$("#client-type").val();

    if(count_clients==0 || client_type==2) {

        if($("#name_business_phone").val().length>0) {
            impresora.write("CLIENTE: "+$("#name_business_phone").val()+"\n");
        }

    }else if(client_type==1) {

        if($("#edit_name_business_phone").val().length>0) {
            impresora.write("CLIENTE: "+$("#edit_name_business_phone").val()+"\n");
        }
    }

    impresora.write("\n");

    // Product
    impresora.setFontSize(2, 2);
    impresora.write("PEDIDO: "+"\n");

    var product_id=document.getElementsByName('product_id[]');
    var products_amounts=document.getElementsByName('products_amounts[]');
    var products_descriptions=document.getElementsByName('products_descriptions[]');

    if(product_id.length>0) {
        for(p=0; p<product_id.length; p++) {
            impresora.setFontSize(2, 2);
            impresora.write("- "+products_amounts[p].value+" "+$("#"+product_id[p].id+" option:selected").text()+"\n");

            if(products_descriptions[p].value.length>0) {
                impresora.setFontSize(1, 1);
                impresora.write(" - Descripcion del producto: "+products_descriptions[p].value+"\n");
            }

            impresora.write("\n");

        }
    }

    // Promotions
    var promotion_id=document.getElementsByName('promotion_id[]');
    var promotions_amounts=document.getElementsByName('promotions_amounts[]');
    var promotions_descriptions=document.getElementsByName('promotions_descriptions[]');

    if(promotion_id.length>0) {
        for(p=0; p<promotion_id.length; p++) {
            impresora.setFontSize(2, 2);
            impresora.write("- "+promotions_amounts[p].value+" "+$("#"+promotion_id[p].id+" option:selected").text()+"\n");

            if(promotions_descriptions[p].value.length>0) {
                impresora.setFontSize(1, 1);
                impresora.write(" - Descripcion de promoción: "+promotions_descriptions[p].value+"\n");
            }

            impresora.write("\n");

        }
    }

    //Description
    if($("#sale_description").val().length>0) {
        impresora.setFontSize(2, 2);
        impresora.write("DIRECCION:\n"+$("#sale_description").val()+"\n");
        impresora.write("\n");
    }

    // Phone
    var count_clients="{{count($clients)}}";
    var client_type=$("#client-type").val();

    if(count_clients==0 || client_type==2) {

        if($("#first_phone").val().length>0) {
            impresora.setFontSize(2, 2);
            impresora.write("TLF #1:\n"+$("#first_phone").val()+"\n");
        }

        if($("#second_phone").val().length>0) {
            impresora.setFontSize(2, 2);
            impresora.write("TLF #2:\n"+$("#second_phone").val()+"\n");
        }

    }else if(client_type==1) {

        if($("#edit_first_phone").val().length>0) {
            impresora.setFontSize(2, 2);
            impresora.write("TLF #1:\n"+$("#edit_first_phone").val()+"\n");
        }

        if($("#edit_second_phone").val().length>0) {
            impresora.setFontSize(2, 2);
            impresora.write("TLF #2:\n"+$("#edit_second_phone").val()+"\n");
        }

    }

    impresora.setFontSize(1, 1);
    impresora.write("\nNota opcional:\n\n\n\n");

    impresora.cut();
    impresora.cutPartial(); // Pongo este y también cut porque en ocasiones no funciona con cut, solo con cutPartial
    impresora.end()
        .then(valor => {
            loguear("Al imprimir: " + valor);
        });

    $(".form-sale").submit();
});

// En el init, obtenemos la lista
//obtenerListaDeImpresoras();
// Y también la impresora seleccionada
refrescarNombreDeImpresoraSeleccionada();