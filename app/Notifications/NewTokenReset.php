<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\Configuration;

class NewTokenReset extends Notification
{
    use Queueable;

    protected $user;
    protected $reset_link;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $reset_link)
    {
        $this->user=$user;
        $this->reset_link=$reset_link;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $configuration=Configuration::first();
        return (new MailMessage)->view('emails.password_recover', ['user'=>$this->user, 'reset_link'=>$this->reset_link, 'configuration'=>$configuration])->subject('Recuperar contraseña de '.$configuration->name)->from('panas.pizza.notificaciones@gmail.com', $configuration->name);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
