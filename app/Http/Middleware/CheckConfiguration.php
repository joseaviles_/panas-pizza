<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\CashRegister;
use App\Models\Configuration;
use Illuminate\Support\Facades\View;
        
class CheckConfiguration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        View::share('cash_register', CashRegister::first());
        View::share('configuration', Configuration::first());
        return $next($request);
    }
}
