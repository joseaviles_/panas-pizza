<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_number'       => 'required',
            'first_name'          => 'required',
            'last_name'           => 'required',
            'name_business_phone' => 'required',
            'first_phone'         => 'required'
        ];
    }
}
