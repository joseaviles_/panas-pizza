<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Tracking;
use App\Models\TokenReset;
use App\Notifications\NewTokenReset;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class ProfileController extends Controller
{
    public function getLogin() 
    {
        $user=User::getCurrent();

        if($user) {
            return redirect()->route('dashboard');
        }

        return view('home.login');
    }

    public function postLogin(Request $request) 
    {
        $email=$request->email;
        $password=$request->password;

        $user=User::where('email',$email)->first();

        if(Auth::attempt(['email'=>$email, 'password'=>$password], true)) {
            Tracking::create(['user_id'=>$user->id, 'category'=>'login', 'operation'=>'Inicio de sesión']);
            return redirect()->route('dashboard');
        }

        if(!$user) {
            return redirect()->back()->withInput()->withErrors('El correo electrónico que has introducido no se encuentra registrado.');
        }else {
            return redirect()->back()->withInput()->withErrors('La contraseña que has introducido es incorrecta.');
        }
    }

    public function logout() 
    {
        $user=User::getCurrent();
        Tracking::create(['user_id'=>$user->id, 'category'=>'logout', 'operation'=>'Cierre de sesión']);
        Auth::logout();
        return redirect()->route('home');
    }

    public function getResetPassword() 
    {
        $user=User::getCurrent();

        if($user) {
            return redirect()->route('dashboard');
        }

        return view('home.password_recover');
    }

    public function sendResetLinkEmail(Request $request) 
    {
        $email=$request->email;
        $user=User::where('email',$email)->first();

        if($user) {

            TokenReset::where('email',$user->email)->delete();
            $token_reset=new TokenReset();
            $token_reset->email=$user->email;
            $token_reset->save();

            $reset_link=route('password_reset_token',['token'=>$token_reset->token]);

            if(App::environment('production')) {
                $user->notify(new NewTokenReset($user, $reset_link));
            }

        }else {
            return redirect()->back()->withErrors('El correo electrónico ingresado no se encuentra registrado.');
        }

        return redirect()->route('login')->with(['message_info'=>'Te hemos enviado un correo electrónico para que recuperes tu contraseña.']);
    }

    public function getResetPasswordToken($token) 
    {
        $user=User::getCurrent();

        if($user) {
            return redirect()->route('dashboard');
        }

        $token_reset=TokenReset::where('token',$token)->first();
        
        if(!$token_reset) {
            return redirect()->route('login')->withErrors(['El token ha expirado.']);
        }

        $user=User::where('email',$token_reset->email)->first();

        return view('home.password_change',['token'=>$token_reset->token, 'user'=>$user]);
    }

    public function resetPassword(Request $request) 
    {
        $token=$request->token;
        $password=$request->password;

        if(!$password || strlen($password)<8) {
            return redirect()->back()->withErrors(['La contraseña debe tener al menos 8 caracteres.']);
        }

        $token_reset=TokenReset::where('token',$token)->first();
        if(!$token_reset) {
            return redirect()->route('login')->withErrors(['El token ha expirado.']);
        }

        $user=User::where('email',$token_reset->email)->first();
        if(!$user) {
            return redirect()->route('login')->withErrors(['El token ha expirado.']);
        }

        User::where('id',$user->id)->update(['password'=>Hash::make($password)]);
        TokenReset::where('token',$token_reset->token)->delete();
        Tracking::create(['user_id'=>$user->id, 'category'=>'reset_password', 'operation'=>'Restauración de contraseña']);
        return redirect()->route('login')->with(['message_info'=>'La contraseña se ha cambiado exitosamente.']);
    }

    public function getDashboard()
    {
        $user=User::getCurrent();

        if($user->hasRole('admin')) {
            return redirect()->route('admin_dashboard');
        }elseif($user->hasRole('seller')) {
            return redirect()->route('seller_dashboard');
        }
    }

    public function getProfile() {
        return view('user.profile',['menu_active'=>'profile']);
    }

    public function saveProfile(UpdateProfileRequest $request) {
        $user_id=$request->user_id;
        $data=$request->only(['first_name','last_name','identity_card','email']);

        $password=$request->password;
        $password_confirmation=$request->password_confirmation;

        if(isset($password)) {

            if(strlen($password)<8) {
                return redirect()->back()->withErrors(['El campo contraseña debe contener al menos 8 caracteres.']);
            }elseif(!isset($password_confirmation)) {
                return redirect()->back()->withErrors(['El campo confirmación de contraseña es obligatorio si deseas cambiar la contraseña.']);
            }elseif(isset($password_confirmation) && $password_confirmation!=$password) {
                return redirect()->back()->withErrors(['El campo confirmación de contraseña no coincide.']);
            }

            $data['password']=Hash::make($password);
        }

        User::where('id',$user_id)->update($data);
        Tracking::create(['user_id'=>$user_id, 'category'=>'save_profile', 'operation'=>'Actualización de perfil']);

        return redirect()->route('profile')->with(['message_info'=>'Perfil actualizado exitosamente.']);
    }
}
