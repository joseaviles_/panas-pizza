<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\CashRegister;
use App\Models\CashRegisterMovement;
use App\Models\Client;
use App\Models\DeliveryCompany;
use App\Models\DeliveryCompanyDriver;
use App\Models\DeliverySale;
use App\Models\DeliveryType;
use App\Models\Payment;
use App\Models\PaymentMethod;
use App\Models\PaymentMethodOption;
use App\Models\Product;
use App\Models\Promotion;
use App\Models\RawMaterial;
use App\Models\RawMaterialIncome;
use App\Models\RawMaterialIncomeMovement;
use App\Models\RawMaterialMovement;
use App\Models\Sale;
use App\Models\Tracking;
use App\Models\UnprocessedProductsMovement;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

class SalesController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'sales');
    }

    public function getIndex()
    {
        return view('seller.sales.list',['route'=>Route::currentRouteName()]);
    }

    public function getList()
    {
        $user=User::getCurrent();
        $sales_list=[];
        $count=0;

        if(Route::currentRouteName()=='get_sales_history') {
            $sales=Sale::orderBy('id','asc')->get();
        }else {
            $sales=Sale::where('created_at','>=',date('Y-m-d 00:00:00'))->where('created_at','<=',date('Y-m-d 23:59:59'))->orderBy('id','asc')->get();            
        }

        foreach($sales as $sale) {

            $count++;

            $actions='';

            if($sale->dispatched==0) {
                $actions.=' <a style="color: black !important;" href="'.route("sales_update_dispatched",["sale_id"=>$sale->id]).'"><i class="fas fa-shipping-fast" aria-hidden="true"></i></a>';
            }

            if($sale->total_paid<$sale->total) {
                $actions.=' <a style="color: #28a745 !important;" href="'.route("payment_collections_pay",["sale_id"=>$sale->id]).'"><i class="fas fa-handshake" aria-hidden="true"></i></a>';
            }

            if($user->hasRole('admin')) {
                $actions.=' <a style="color: #17a2b8 !important;" href="'.route("sales_show",["sale_id"=>$sale->id]).'"><i class="fa fa-search" aria-hidden="true"></i></a> <a style="color: #007bff !important;" href="'.route("sales_edit",["sale_id"=>$sale->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a style="color: orange !important;" href="'.route("sales_trash",["sale_id"=>$sale->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            }elseif($user->hasRole('seller')) {
                $actions.=' <a style="color: #17a2b8 !important;" href="'.route("sales_show",["sale_id"=>$sale->id]).'"><i class="fa fa-search" aria-hidden="true"></i></a> <a style="color: #007bff !important;" href="'.route("sales_edit",["sale_id"=>$sale->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a>';
            }

            $sales_list[]=[$count, $sale->id, $sale->created_at->format('d-m-Y h:i:s a'), $sale->user->first_name.' '.$sale->user->last_name, $sale->client->name_business_phone, number_format($sale->total, 2, ',', '.'), number_format($sale->total_paid, 2, ',', '.'), ($sale->total-$sale->total_paid)<0?'0.00':number_format($sale->total-$sale->total_paid, 2, ',', '.'), $sale->sale_type==1?'Al contado':'A crédito', ($sale->total-$sale->total_paid)<0?'Si':'No', $sale->dispatched==1?'Si':'No',$actions];

        }

        return response()->json(['data' => $sales_list]);
    }

    public function getCreate() 
    {
    	$clients=Client::orderBy('name_business_phone')->get();
    	$products=Product::orderBy('name','asc')->get();

    	if(count($products)==0) {
			return redirect()->route('admin_products_create')->with(['message_info'=>'Debes registrar como mínimo un producto para proceder a registrar una venta.']);
    	}

        foreach($products as $product) {
        	$product->available=$product->amount;
            $raw_material=$product->raw_material->sortByDesc('pivot.amount');

            if(count($raw_material)>0) {
	            $min=0;
	            $count=0;

	            foreach($raw_material as $rm) {

	                $available_products=$rm->amount/$rm->pivot->amount;

	                if($count==0 || $available_products < $min) {
	                    $min=$available_products;
	                    $count++;
	                }

	            }

	            $product->available=floor($min);
            }
        }

        $products=$products->sortByDesc('available');

    	$promotions=Promotion::where('status',1)->orderBy('name','asc')->get();

        foreach($promotions as $promotion) {
        	$promotion->available=0;
            $promotion_products=$promotion->products->sortByDesc('pivot.amount');

            $min=0;
            $count=0;

            foreach($promotion_products as $promotion_product) {

                $available_promotions=$products->where('id',$promotion_product->id)->first()->available/$promotion_product->pivot->amount;

                if($count==0 || $available_promotions < $min) {
                    $min=$available_promotions;
                    $count++;
                }

            }

            $promotion->available=floor($min);

        }

    	$delivery_companies=DeliveryCompany::orderBy('name')->get();

    	$payment_methods=PaymentMethod::orderBy('name')->get();

    	if(count($payment_methods)==0) {
			return redirect()->route('admin_payment_methods_create')->with(['message_info'=>'Debes registrar como mínimo un método de pago para proceder a registrar una venta.']);
    	}

        $payment_method_options=PaymentMethodOption::orderBy('name')->get();
        $delivery_types=DeliveryType::orderBy('price')->get();

        $today_sales=Sale::where('created_at','>=',date('Y-m-d 00:00:00'))->where('created_at','<=',date('Y-m-d 23:59:59'))->orderBy('id','asc')->count()+1; 

        return view('seller.sales.create', ['clients'=>$clients, 'products'=>$products, 'promotions'=>$promotions, 'delivery_companies'=>$delivery_companies, 'payment_methods'=>$payment_methods, 'payment_method_options'=>$payment_method_options, 'delivery_types'=>$delivery_types, 'today_sales'=>$today_sales]);
    }
    
    public function create(Request $request) 
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);

        // Validate that the raw material reaches for the products and promotions
        $product_ids=$request->product_id;
        $products_amounts=$request->products_amounts;
        $raw_material_used=[];

        if(isset($product_ids)) {
            
            foreach($product_ids as $key => $product_id) {
            
                $amount=str_replace('.', '', $products_amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                $product=Product::find($product_id);
                $raw_material=$product->raw_material;

                foreach($raw_material as $key => $rm) {

                    if(!isset($raw_material_used[$rm->id])) {
                        $raw_material_used[$rm->id]=$rm->amount-($rm->pivot->amount*$amount);
                    }else {
                        $raw_material_used[$rm->id]=$raw_material_used[$rm->id]-($rm->pivot->amount*$amount);

                        if($raw_material_used[$rm->id]<0) {
                            return back()->withErrors($rm->name.' insuficiente para cubrir los productos seleccionados.');
                        }

                    }

                }

            }

        }

        $promotion_ids=$request->promotion_id;
        $promotions_amounts=$request->promotions_amounts;

        if(isset($promotion_ids)) {
            
            foreach($promotion_ids as $key => $promotion_id) {
            
                $amount=str_replace('.', '', $promotions_amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                $promotion=Promotion::find($promotion_id);
                $products=$promotion->products;

                foreach($products as $product) {
                    $raw_material=$product->raw_material;

                    foreach($raw_material as $key => $rm) {

                        if(!isset($raw_material_used[$rm->id])) {
                            $raw_material_used[$rm->id]=$rm->amount-($rm->pivot->amount*$product->pivot->amount*$amount);
                        }else {
                            $raw_material_used[$rm->id]=$raw_material_used[$rm->id]-($rm->pivot->amount*$product->pivot->amount*$amount);

                            if($raw_material_used[$rm->id]<0) {
                                return back()->withErrors($rm->name.' insuficiente para cubrir las promociones seleccionadas.');
                            }

                        }

                    }
                }

            }

        }

        // Validate that it reaches the raw material for products and promotions
        $products=Product::orderBy('name','asc')->get();
        
        foreach($products as $product) {
            $product->available=$product->amount;
            $raw_material=$product->raw_material->sortByDesc('pivot.amount');

            if(count($raw_material)>0) {
                $min=0;
                $count=0;

                foreach($raw_material as $rm) {

                    $available_products=$rm->amount/$rm->pivot->amount;

                    if($count==0 || $available_products < $min) {
                        $min=$available_products;
                        $count++;
                    }

                }

                $product->available=floor($min);
            }
        }

        $products=$products->sortByDesc('available');

        $product_ids=$request->product_id;
        $products_amounts=$request->products_amounts;
        if(isset($product_ids)) {
            foreach($product_ids as $key => $product_id) {
                $amount=str_replace('.', '', $products_amounts[$key]);
                $amount=str_replace(',', '.', $amount);
                $products->where('id',$product_id)->first()->available=$products->where('id',$product_id)->first()->available-$amount;
            }
        }

        $promotions=Promotion::where('status',1)->orderBy('name','asc')->get();

        foreach($promotions as $promotion) {
            $promotion->available=0;
            $promotion_products=$promotion->products->sortByDesc('pivot.amount');

            $min=0;
            $count=0;

            foreach($promotion_products as $promotion_product) {

                $available_promotions=$products->where('id',$promotion_product->id)->first()->available/$promotion_product->pivot->amount;

                if($count==0 || $available_promotions < $min) {
                    $min=$available_promotions;
                    $count++;
                }

            }

            $promotion->available=floor($min);

        }

        $promotions=$promotions->sortByDesc('available');

        $promotion_ids=$request->promotion_id;
        $promotions_amounts=$request->promotions_amounts;
        if(isset($promotion_ids)) {
            foreach($promotion_ids as $key => $promotion_id) {
                $amount=str_replace('.', '', $promotions_amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                if($promotions->where('id',$promotion_id)->first()->available < $amount) {
                    return back()->withErrors('Inventario insuficiente para vender '.$amount.' promociones '.$promotions->where('id',$promotion_id)->first()->name.'. Esto ocurre debido a que seleccionaste la cantidad de productos necesarios que componen la promoción para venderlos de forma individual.');
                }

            }
        }

        // User
    	$user=User::getCurrent();
    	$client_type=$request->client_type;

        // Client
    	if(!isset($client_type) || (isset($client_type) && $client_type==2)) {
			
			$client_data=$request->only(['client_number', 'first_name', 'last_name', 'identity_card', 'name_business_phone', 'first_phone', 'second_phone']);

			$client=Client::create($client_data);

        	Tracking::create(['user_id'=>$user->id, 'category'=>'create_client', 'operation'=>'Registro del cliente: '.$client->first_name.' '.$client->last_name]);

    	}elseif(isset($client_type) && $client_type==1) {
			
            $client=Client::where('id',$request->client_id)->first();

            $client_data=$request->only(['edit_client_number', 'edit_first_name', 'edit_last_name', 'edit_identity_card', 'edit_name_business_phone', 'edit_first_phone', 'edit_second_phone']);
            $new_client_data['client_number']=$client_data['edit_client_number'];
            $new_client_data['first_name']=$client_data['edit_first_name'];
            $new_client_data['last_name']=$client_data['edit_last_name'];
            $new_client_data['identity_card']=$client_data['edit_identity_card'];
            $new_client_data['name_business_phone']=$client_data['edit_name_business_phone'];
            $new_client_data['first_phone']=$client_data['edit_first_phone'];
            $new_client_data['second_phone']=$client_data['edit_second_phone'];

            $client->update($new_client_data);

    	}

        // Sale
    	$sale_data=$request->only(['description', 'sale_type', 'total', 'exchange_rate', 'total_bss', 'discount']);

        $sale_data['total']=str_replace('.', '', $sale_data['total']);
        $sale_data['total']=str_replace(',', '.', $sale_data['total']);

        $sale_data['exchange_rate']=str_replace('.', '', $sale_data['exchange_rate']);
        $sale_data['exchange_rate']=str_replace(',', '.', $sale_data['exchange_rate']);

        $sale_data['total_bss']=str_replace('.', '', $sale_data['total_bss']);
        $sale_data['total_bss']=str_replace(',', '.', $sale_data['total_bss']);

    	$sale_data['user_id']=$user->id;
    	$sale_data['client_id']=$client->id;

        $sale_data['discount']=str_replace('.', '', $sale_data['discount']);
        $sale_data['discount']=str_replace(',', '.', $sale_data['discount']);

        $sale=Sale::create($sale_data);

        // Products
    	$product_ids=$request->product_id;
    	$products_amounts=$request->products_amounts;
    	$products_prices=$request->products_prices;
    	$products_descriptions=$request->products_descriptions;

        if(isset($product_ids)) {
            foreach($product_ids as $key => $product_id) {
                
                $amount=str_replace('.', '', $products_amounts[$key]);
                $amount=str_replace(',', '.', $amount);
                $price=str_replace('.', '', $products_prices[$key]);
                $price=str_replace(',', '.', $price);
                $sale->products()->attach($product_id,['amount'=>$amount, 'price'=>$price, 'description'=>$products_descriptions[$key]]);

                $product=Product::find($product_id);

                if($product->type==1) {

                    $new_amount=($product->amount-$amount);
                    //$product->update(['amount'=>$new_amount]);
                    Product::where('id',$product_id)->update(['amount'=>$new_amount]);

                    $concept='Venta: '.$amount.' '.$product->name.'<br><b>ID de venta: '.$sale->id.'</b>';

                    UnprocessedProductsMovement::create(['product_id'=>$product->id, 'new_amount'=>$new_amount, 'amount'=>$amount, 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>2, 'sale_id'=>$sale->id]);

                }else {
                    $raws_material=$product->raw_material;
                    foreach($raws_material as $raw_material) {

                        $sale_amount=$raw_material->pivot->amount*$amount;
                        $new_amount=$raw_material->amount-$sale_amount;
                        //$raw_material->update(['amount'=>$new_amount]);
                        RawMaterial::where('id',$raw_material->id)->update(['amount'=>$new_amount]);

                        $concept='Venta: '.$amount.' '.$product->name.'<br><b>ID de venta: '.$sale->id.'</b>';
                        
                        // Raw material incomes
                        $check_amount=$raw_material->pivot->amount*$amount;
                        while($check_amount>0) {

                            $raw_material_income=RawMaterialIncome::orderBy('created_at','asc')->where('raw_material_id',$raw_material->id)->where('amount','>',0)->first();

                            if($raw_material_income) {
                                if($check_amount>=$raw_material_income->amount) {
                                    RawMaterialIncomeMovement::create(['raw_material_income_id'=>$raw_material_income->id, 'product_id'=>$product->id, 'sale_id'=>$sale->id, 'amount'=>$raw_material_income->amount, 'products_quantity'=>round(($raw_material_income->amount/$raw_material->pivot->amount), 2)]);
                                    $check_amount-=$raw_material_income->amount;
                                    RawMaterialIncome::where('id',$raw_material_income->id)->update(['amount'=>($raw_material_income->amount-$raw_material_income->amount)]);

                                }else {
                                    RawMaterialIncomeMovement::create(['raw_material_income_id'=>$raw_material_income->id, 'product_id'=>$product->id, 'sale_id'=>$sale->id, 'amount'=>$check_amount, 'products_quantity'=>round(($check_amount/$raw_material->pivot->amount), 2)]);
                                    RawMaterialIncome::where('id',$raw_material_income->id)->update(['amount'=>($raw_material_income->amount-$check_amount)]);
                                    $check_amount-=$check_amount;
                                }
                            }

                        }

                        RawMaterialMovement::create(['raw_material_id'=>$raw_material->id, 'new_amount'=>$new_amount, 'amount'=>$sale_amount, 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>2, 'sale_id'=>$sale->id]);

                    }
                }

            }
        }

        // Promotions
    	$promotion_ids=$request->promotion_id;
    	$promotions_amounts=$request->promotions_amounts;
    	$promotions_prices=$request->promotions_prices;
    	$promotions_descriptions=$request->promotions_descriptions;

        if(isset($promotion_ids)) {
            foreach($promotion_ids as $key => $promotion_id) {
                $amount=str_replace('.', '', $promotions_amounts[$key]);
                $amount=str_replace(',', '.', $amount);
                $price=str_replace('.', '', $promotions_prices[$key]);
                $price=str_replace(',', '.', $price);
                $sale->promotions()->attach($promotion_id,['amount'=>$amount, 'price'=>$price, 'description'=>$promotions_descriptions[$key]]);

                $promotion=Promotion::find($promotion_id);
                $products=$promotion->products;

                foreach($products as $product) {

                    if($product->type==1) {

                        $new_amount=($product->amount-($product->pivot->amount*$amount));
                        //$product->update(['amount'=>$new_amount]);
                        Product::where('id',$product->id)->update(['amount'=>$new_amount]);

                        $concept='Venta: '.($product->pivot->amount*$amount).' '.$product->name.'<br>Promoción: '.$amount.' '.$promotion->name.'<br><b>ID de venta: '.$sale->id.'</b>';

                        UnprocessedProductsMovement::create(['product_id'=>$product->id, 'new_amount'=>$new_amount, 'amount'=>($product->pivot->amount*$amount), 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>2, 'sale_id'=>$sale->id]);

                    }else {
                        $raws_material=$product->raw_material;
                        foreach($raws_material as $raw_material) {

                            $sale_amount=($raw_material->pivot->amount*$product->pivot->amount*$amount);
                            $new_amount=$raw_material->amount-$sale_amount;
                            //$raw_material->update(['amount'=>$new_amount]);
                            RawMaterial::where('id',$raw_material->id)->update(['amount'=>$new_amount]);

                            $concept='Venta: '.($product->pivot->amount*$amount).' '.$product->name.'<br>Promoción: '.$amount.' '.$promotion->name.'<br><b>ID de venta: '.$sale->id.'</b>';
                            
                            // Raw material incomes
                            $check_amount=$raw_material->pivot->amount*$product->pivot->amount*$amount;
                            while($check_amount>0) {

                                $raw_material_income=RawMaterialIncome::orderBy('created_at','asc')->where('raw_material_id',$raw_material->id)->where('amount','>',0)->first();

                                if($raw_material_income) {
                                    if($check_amount>=$raw_material_income->amount) {
                                        RawMaterialIncomeMovement::create(['raw_material_income_id'=>$raw_material_income->id, 'product_id'=>$product->id, 'sale_id'=>$sale->id, 'amount'=>$raw_material_income->amount, 'products_quantity'=>round(($raw_material_income->amount/$raw_material->pivot->amount), 2)]);
                                        $check_amount-=$raw_material_income->amount;
                                        RawMaterialIncome::where('id',$raw_material_income->id)->update(['amount'=>($raw_material_income->amount-$raw_material_income->amount)]);

                                    }else {
                                        RawMaterialIncomeMovement::create(['raw_material_income_id'=>$raw_material_income->id, 'product_id'=>$product->id, 'sale_id'=>$sale->id, 'amount'=>$check_amount, 'products_quantity'=>round(($check_amount/$raw_material->pivot->amount), 2)]);
                                        RawMaterialIncome::where('id',$raw_material_income->id)->update(['amount'=>($raw_material_income->amount-$check_amount)]);
                                        $check_amount-=$check_amount;
                                    }
                                }

                            }

                            RawMaterialMovement::create(['raw_material_id'=>$raw_material->id, 'new_amount'=>$new_amount, 'amount'=>$sale_amount, 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>2, 'sale_id'=>$sale->id]);

                        }
                    }

                }

            }
        }

        // Delivery
        $delivery_company_ids=$request->delivery_company_id;
        $delivery_type_ids=$request->delivery_type_id;
        $delivery_company_driver_ids=$request->delivery_company_driver_id;
        $services_quantity=$request->services_quantity;

        if(isset($delivery_company_ids)) {
            foreach($delivery_company_ids as $key => $delivery_company_id) {
                $delivery_sale['delivery_company_id']=$delivery_company_id;

                if($delivery_type_ids[$key]=='NULL') {
                    $delivery_sale['delivery_type_id']=NULL;
                    $delivery_sale['price']=0;
                }else {
                    $delivery_sale['delivery_type_id']=$delivery_type_ids[$key];
                    $delivery_type=DeliveryType::where('id',$delivery_type_ids[$key])->first();
                    $delivery_sale['price']=$delivery_type->price;
                }

                if($delivery_company_driver_ids[$key]=='NULL') {
                    $delivery_sale['delivery_company_driver_id']=NULL;
                }else {
                    $delivery_sale['delivery_company_driver_id']=$delivery_company_driver_ids[$key];
                }

                $amount=str_replace('.', '', $services_quantity[$key]);
                $delivery_sale['amount']=str_replace(',', '.', $amount);

                $delivery_sale['sale_id']=$sale->id;

                $sale->update(['delivery_amount'=>$sale->delivery_amount+($delivery_sale['amount']*$delivery_sale['price'])]);
                DeliverySale::create($delivery_sale);
            }
        }

        // Payments and changes
        $payment_method_id=$request->payment_method_id;
        $payment_method_option_id=$request->payment_method_option_id;
        $payment_amount_usd=$request->payment_amount_usd;
        $payment_amount_bss=$request->payment_amount_bss;
        $payment_exchange_rate=$request->payment_exchange_rate;
        $payment_reference_number=$request->payment_reference_number;
        $payment_description=$request->payment_descriptions;

        $change_method_id=$request->change_method_id;
        $change_method_option_id=$request->change_method_option_id;
        $change_amount_usd=$request->change_amount_usd;
        $change_amount_bss=$request->change_amount_bss;
        $change_exchange_rate=$request->change_exchange_rate;
        $change_reference_number=$request->change_reference_number;
        $change_description=$request->change_descriptions;
        $pay_the_customer=$request->pay_the_customers;
        
        if(isset($payment_method_id)) {
            foreach($payment_method_id as $key => $payment_method) {

                $data['sale_id']=$sale->id;

                // Payment
                $data['payment_method_id']=$payment_method;

                if(isset($payment_method_option_id) && isset($payment_method_option_id[$key])) {
                    $data['payment_method_option_id']=$payment_method_option_id[$key];
                    if($data['payment_method_option_id']=='NULL')
                        $data['payment_method_option_id']=NULL;
                }else {
                    $data['payment_method_option_id']=NULL;
                }

                $data['payment_amount_usd']=$payment_amount_usd[$key];
                $data['payment_amount_usd']=str_replace('.', '', $data['payment_amount_usd']);
                $data['payment_amount_usd']=str_replace(',', '.', $data['payment_amount_usd']);
                $data['payment_amount_bss']=$payment_amount_bss[$key];
                $data['payment_amount_bss']=str_replace('.', '', $data['payment_amount_bss']);
                $data['payment_amount_bss']=str_replace(',', '.', $data['payment_amount_bss']);
                $data['payment_exchange_rate']=$payment_exchange_rate[$key];
                $data['payment_exchange_rate']=str_replace('.', '', $data['payment_exchange_rate']);
                $data['payment_exchange_rate']=str_replace(',', '.', $data['payment_exchange_rate']);
                $data['payment_reference_number']=$payment_reference_number[$key];
                $data['payment_description']=$payment_description[$key];

                // Change
                $data['change_method_id']=$change_method_id[$key];
                $data['change_method_option_id']=$change_method_option_id[$key];
                if($data['change_method_option_id']=='NULL')
                    $data['change_method_option_id']=NULL;

                $data['change_amount_usd']=$change_amount_usd[$key];
                if(!isset($data['change_amount_usd']))
                    $data['change_amount_usd']=0;

                $data['change_amount_usd']=str_replace('.', '', $data['change_amount_usd']);
                $data['change_amount_usd']=str_replace(',', '.', $data['change_amount_usd']);

                $data['change_amount_bss']=$change_amount_bss[$key];
                if(!isset($data['change_amount_bss']))
                    $data['change_amount_bss']=0;

                $data['change_amount_bss']=str_replace('.', '', $data['change_amount_bss']);
                $data['change_amount_bss']=str_replace(',', '.', $data['change_amount_bss']);

                $data['change_exchange_rate']=$change_exchange_rate[$key];
                $data['change_exchange_rate']=str_replace('.', '', $data['change_exchange_rate']);
                $data['change_exchange_rate']=str_replace(',', '.', $data['change_exchange_rate']);
                $data['change_reference_number']=$change_reference_number[$key];
                $data['change_description']=$change_description[$key];
                $data['pay_the_customer']=$pay_the_customer[$key];

                $payment=Payment::create($data);

                $sale=Sale::find($sale->id);
                $sale->update(['total_paid'=>$sale->total_paid+$payment->payment_amount_usd]);

                if($data['pay_the_customer']==0) {
                    $sale=Sale::find($sale->id);
                    $sale->update(['total_paid'=>$sale->total_paid-$payment->change_amount_usd]); 
                }

                // Payment
                if($payment->payment_method->type=='Efectivo') {
                    $cash_register=CashRegister::first();
                    $cash_register_movement=null;
                    $cash_register_movement['cash_register_id']=$cash_register->id;
                    $cash_register_movement['type']=1;
                    $cash_register_movement['currency']=$payment->payment_method->currency;
                    $cash_register_movement['movement']='Pago';

                    if($payment->payment_method->currency=='$') {
                        $cash_register_movement['amount']=$data['payment_amount_usd'];
                        $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount+$data['payment_amount_usd']]);
                    }else {
                        $cash_register_movement['amount']=$data['payment_amount_bss'];
                        $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount+$data['payment_amount_bss']]);
                    }

                    $cash_register_movement['sale_id']=$sale->id;

                    CashRegisterMovement::create($cash_register_movement);
                }

                // Change
                if($payment->change_method->type=='Efectivo' && ($data['change_amount_usd']!=0 || $data['change_amount_bss']!=0) && $data['pay_the_customer']==0) {
                    $cash_register=CashRegister::first();
                    $cash_register_movement=null;
                    $cash_register_movement['cash_register_id']=$cash_register->id;
                    $cash_register_movement['type']=2;
                    $cash_register_movement['currency']=$payment->change_method->currency;
                    $cash_register_movement['movement']='Cambio';

                    if($payment->change_method->currency=='$') {
                        $cash_register_movement['amount']=$data['change_amount_usd'];
                        $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount-$data['change_amount_usd']]);
                    }else {
                        $cash_register_movement['amount']=$data['change_amount_bss'];
                        $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount-$data['change_amount_bss']]);
                    }

                    $cash_register_movement['sale_id']=$sale->id;

                    CashRegisterMovement::create($cash_register_movement);
                }
            }
        }

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_sale', 'operation'=>'Registro de la venta con ID: '.$sale->id.' - Monto total: '.number_format($sale->total, 2, ',', '.').'$.']);

        return redirect()->route('sales_create')->with(['message_info'=>'Venta con ID: '.$sale->id.' registrada exitosamente. A continuación puedes registrar una nueva venta.']);
    }

    public function getShow($sale_id) 
    {
        $sale=Sale::where('id',$sale_id)->first();

        if(!$sale) {
            return redirect()->route('sales')->withErrors(['La venta con ID '.$sale_id.' no se encuentra registrada.']);
        }

        return view('seller.sales.show',['sale'=>$sale]);
    }

    public function getEdit($sale_id) 
    {
        $sale=Sale::where('id',$sale_id)->first();

        if(!$sale) {
            return redirect()->route('sales')->withErrors(['La venta con ID '.$sale_id.' no se encuentra registrada.']);
        }

        $clients=Client::orderBy('name_business_phone')->get();
        $products=Product::orderBy('name','asc')->get();

        if(count($products)==0) {
            return redirect()->route('admin_products_create')->with(['message_info'=>'Debes registrar como mínimo un producto para proceder a registrar una venta.']);
        }

        foreach($products as $product) {
            $product->available=$product->amount;
            $raw_material=$product->raw_material->sortByDesc('pivot.amount');

            if(count($raw_material)>0) {
                $min=0;
                $count=0;

                foreach($raw_material as $rm) {

                    $available_products=$rm->amount/$rm->pivot->amount;

                    if($count==0 || $available_products < $min) {
                        $min=$available_products;
                        $count++;
                    }

                }

                $product->available=floor($min);
            }

            $product_sale=$sale->products()->wherePivot('product_id',$product->id)->first();
            if($product_sale) {
                $product->available=$product->available+$product_sale->pivot->amount;
            }
        }

        $promotions_sale=$sale->promotions;
        foreach($promotions_sale as $promotion_sale) {
            $promotion_products=$promotion_sale->products;
            foreach($promotion_products as $promotion_product) {
                $products->where('id',$promotion_product->id)->first()->available=$products->where('id',$promotion_product->id)->first()->available+($promotion_sale->pivot->amount*$promotion_product->pivot->amount);
            }
        }

        $products=$products->sortByDesc('available');

        $promotions=Promotion::where('status',1)->orderBy('name','asc')->get();

        foreach($promotions as $promotion) {
            $promotion->available=0;
            $promotion_products=$promotion->products->sortByDesc('pivot.amount');

            $min=0;
            $count=0;

            foreach($promotion_products as $promotion_product) {

                $available_promotions=$products->where('id',$promotion_product->id)->first()->available/$promotion_product->pivot->amount;

                if($count==0 || $available_promotions < $min) {
                    $min=$available_promotions;
                    $count++;
                }

            }

            $promotion->available=floor($min);

        }

        $delivery_companies=DeliveryCompany::orderBy('name')->get();

        $payment_methods=PaymentMethod::orderBy('name')->get();

        if(count($payment_methods)==0) {
            return redirect()->route('admin_payment_methods_create')->with(['message_info'=>'Debes registrar como mínimo un método de pago para proceder a registrar una venta.']);
        }

        $payment_method_options=PaymentMethodOption::orderBy('name')->get();
        $delivery_types=DeliveryType::orderBy('price')->get();

        return view('seller.sales.edit',['sale'=>$sale, 'clients'=>$clients, 'products'=>$products, 'promotions'=>$promotions, 'delivery_companies'=>$delivery_companies, 'payment_methods'=>$payment_methods, 'payment_method_options'=>$payment_method_options, 'delivery_types'=>$delivery_types]);
    }

    public function update(Request $request) 
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        
        $sale_id=$request->sale_id;
        $sale=Sale::where('id',$sale_id)->first();

        // Validate that the raw material reaches for the products and promotions
        $product_ids=$request->product_id;
        $products_amounts=$request->products_amounts;
        $raw_material_used=[];

        $products=$sale->products;
        foreach($products as $product) {
            $raw_material=$product->raw_material;

            foreach($raw_material as $key => $rm) {

                if(!isset($raw_material_used[$rm->id])) {
                    $raw_material_used[$rm->id]=$rm->amount+($rm->pivot->amount*$product->pivot->amount);
                }else {
                    $raw_material_used[$rm->id]=$raw_material_used[$rm->id]+($rm->pivot->amount*$product->pivot->amount);
                }

            }
        }

        $promotions=$sale->promotions;
        foreach($promotions as $promotion) { 
            $products=$promotion->products;
            foreach($products as $product) {
                $raw_material=$product->raw_material;

                foreach($raw_material as $key => $rm) {

                    if(!isset($raw_material_used[$rm->id])) {
                        $raw_material_used[$rm->id]=$rm->amount+($rm->pivot->amount*$product->pivot->amount*$promotion->pivot->amount);
                    }else {
                        $raw_material_used[$rm->id]=$raw_material_used[$rm->id]+($rm->pivot->amount*$product->pivot->amount*$promotion->pivot->amount);
                    }

                }
            }
        }

        if(isset($product_ids)) {
            
            foreach($product_ids as $key => $product_id) {
            
                $amount=str_replace('.', '', $products_amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                $product=Product::find($product_id);
                $raw_material=$product->raw_material;

                foreach($raw_material as $key => $rm) {

                    if(!isset($raw_material_used[$rm->id])) {
                        $raw_material_used[$rm->id]=$rm->amount-($rm->pivot->amount*$amount);
                    }else {
                        $raw_material_used[$rm->id]=$raw_material_used[$rm->id]-($rm->pivot->amount*$amount);

                        if($raw_material_used[$rm->id]<0) {
                            return back()->withErrors($rm->name.' insuficiente para cubrir los productos seleccionados.');
                        }

                    }

                }

            }

        }

        $promotion_ids=$request->promotion_id;
        $promotions_amounts=$request->promotions_amounts;

        if(isset($promotion_ids)) {
            
            foreach($promotion_ids as $key => $promotion_id) {
            
                $amount=str_replace('.', '', $promotions_amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                $promotion=Promotion::find($promotion_id);
                $products=$promotion->products;

                foreach($products as $product) {
                    $raw_material=$product->raw_material;

                    foreach($raw_material as $key => $rm) {

                        if(!isset($raw_material_used[$rm->id])) {
                            $raw_material_used[$rm->id]=$rm->amount-($rm->pivot->amount*$product->pivot->amount*$amount);
                        }else {
                            $raw_material_used[$rm->id]=$raw_material_used[$rm->id]-($rm->pivot->amount*$product->pivot->amount*$amount);

                            if($raw_material_used[$rm->id]<0) {
                                return back()->withErrors($rm->name.' insuficiente para cubrir las promociones seleccionadas.');
                            }

                        }

                    }
                }

            }

        }

        // Validate that it reaches the raw material for products and promotions
        $products=Product::orderBy('name','asc')->get();
        
        foreach($products as $product) {
            $product->available=$product->amount;
            $raw_material=$product->raw_material->sortByDesc('pivot.amount');

            if(count($raw_material)>0) {
                $min=0;
                $count=0;

                foreach($raw_material as $rm) {

                    $available_products=$rm->amount/$rm->pivot->amount;

                    if($count==0 || $available_products < $min) {
                        $min=$available_products;
                        $count++;
                    }

                }

                $product->available=floor($min);
            }

            $product_sale=$sale->products()->wherePivot('product_id',$product->id)->first();
            if($product_sale) {
                $product->available=$product->available+$product_sale->pivot->amount;
            }
        }

        $promotions_sale=$sale->promotions;
        foreach($promotions_sale as $promotion_sale) {
            $promotion_products=$promotion_sale->products;
            foreach($promotion_products as $promotion_product) {
                $products->where('id',$promotion_product->id)->first()->available=$products->where('id',$promotion_product->id)->first()->available+($promotion_sale->pivot->amount*$promotion_product->pivot->amount);
            }
        }

        $products=$products->sortByDesc('available');

        $product_ids=$request->product_id;
        $products_amounts=$request->products_amounts;
        if(isset($product_ids)) {
            foreach($product_ids as $key => $product_id) {
                $amount=str_replace('.', '', $products_amounts[$key]);
                $amount=str_replace(',', '.', $amount);
                $products->where('id',$product_id)->first()->available=$products->where('id',$product_id)->first()->available-$amount;
            }
        }

        $promotions=Promotion::where('status',1)->orderBy('name','asc')->get();

        foreach($promotions as $promotion) {
            $promotion->available=0;
            $promotion_products=$promotion->products->sortByDesc('pivot.amount');

            $min=0;
            $count=0;

            foreach($promotion_products as $promotion_product) {

                $available_promotions=$products->where('id',$promotion_product->id)->first()->available/$promotion_product->pivot->amount;

                if($count==0 || $available_promotions < $min) {
                    $min=$available_promotions;
                    $count++;
                }

            }

            $promotion->available=floor($min);

        }

        $promotions=$promotions->sortByDesc('available');

        $promotion_ids=$request->promotion_id;
        $promotions_amounts=$request->promotions_amounts;
        if(isset($promotion_ids)) {
            foreach($promotion_ids as $key => $promotion_id) {
                $amount=str_replace('.', '', $promotions_amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                if($promotions->where('id',$promotion_id)->first()->available < $amount) {
                    return back()->withErrors('Inventario insuficiente para vender '.$amount.' promociones '.$promotions->where('id',$promotion_id)->first()->name.'. Esto ocurre debido a que seleccionaste la cantidad de productos necesarios que componen la promoción para venderlos de forma individual.');
                }

            }
        }

        // Delete cash register movement
        $cash_register=CashRegister::first();
        $cash_register_movements=$sale->cash_register_movements->sortBy('id');
        foreach($cash_register_movements as $movement) {

            if($movement->type==1) {

                if($movement->currency=='$') {
                    $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount-$movement->amount]);
                }else {
                    $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount-$movement->amount]);
                }

            }else {

                if($movement->currency=='$') {
                    $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount+$movement->amount]);
                }else {
                    $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount+$movement->amount]);
                }

            }

            $movement->delete();

        }

        // Delete payments
        Payment::where('sale_id',$sale_id)->delete();

        // Delete promotions
        $promotions=$sale->promotions;
        foreach($promotions as $promotion) {
            
            $amount=$promotion->pivot->amount;
            $products=$promotion->products;

            foreach($products as $product) {

                if($product->type==1) {

                    $pro=Product::find($product->id);
                    //$product->update(['amount'=>($pro->amount+($product->pivot->amount*$amount))]);
                    Product::where('id',$product->id)->update(['amount'=>($pro->amount+($product->pivot->amount*$amount))]);

                }else {
                    $raws_material=$product->raw_material;
                    foreach($raws_material as $raw_material) {

                        $rm=RawMaterial::find($raw_material->id);
                        //$raw_material->update(['amount'=>$rm->amount+($raw_material->pivot->amount*$product->pivot->amount*$amount)]);
                        RawMaterial::where('id',$raw_material->id)->update(['amount'=>$rm->amount+($raw_material->pivot->amount*$product->pivot->amount*$amount)]);

                    }
                }

            }

        }

        // Delete products
        $products=$sale->products;
        foreach($products as $product) {

            $amount=$product->pivot->amount;
            $price=$product->pivot->price;

            if($product->type==1) {

                $pro=Product::find($product->id);
                //$product->update(['amount'=>($pro->amount+$amount)]);
                Product::where('id',$product->id)->update(['amount'=>($pro->amount+$amount)]);

            }else {
                $raws_material=$product->raw_material;
                foreach($raws_material as $raw_material) {

                    $rm=RawMaterial::find($raw_material->id);
                    //$raw_material->update(['amount'=>$rm->amount+($raw_material->pivot->amount*$amount)]);
                    RawMaterial::where('id',$raw_material->id)->update(['amount'=>$rm->amount+($raw_material->pivot->amount*$amount)]);

                }
            }

        }

        // Delete raw material income movements
        /*$raw_material_income_movements=$sale->raw_material_income_movements;
        foreach($raw_material_income_movements as $raw_material_income_movement) {
            $raw_material_income_movement->raw_material_income->update(['amount'=>$raw_material_income_movement->raw_material_income->amount+$raw_material_income_movement->amount]);
            $raw_material_income_movement->delete();
        }*/

        // Delete raw material income movements
        $raw_material_income_movements=$sale->raw_material_income_movements;
        foreach($raw_material_income_movements as $raw_material_income_movement) {
            //$raw_material_income_movement->raw_material_income->update(['amount'=>$raw_material_income_movement->raw_material_income->amount+$raw_material_income_movement->amount]);

            $raw_material_income=RawMaterialIncome::find($raw_material_income_movement->raw_material_income_id);
            $new_amount=$raw_material_income->amount+$raw_material_income_movement->amount;
            RawMaterialIncome::where('id',$raw_material_income->id)->update(['amount'=>$new_amount]);
            
            $raw_material_income_movement->delete();
        }


        // Delete deliveries
        DeliverySale::where('sale_id',$sale_id)->delete();

        // User
        $user=User::getCurrent();
        $client_type=$request->client_type;

        // Client
        if(!isset($client_type) || (isset($client_type) && $client_type==2)) {
            
            $client_data=$request->only(['client_number', 'first_name', 'last_name', 'identity_card', 'name_business_phone', 'first_phone', 'second_phone']);

            $client=Client::create($client_data);

            Tracking::create(['user_id'=>$user->id, 'category'=>'create_client', 'operation'=>'Registro del cliente: '.$client->first_name.' '.$client->last_name]);

        }elseif(isset($client_type) && $client_type==1) {

            $client=Client::where('id',$request->client_id)->first();

            $client_data=$request->only(['edit_client_number', 'edit_first_name', 'edit_last_name', 'edit_identity_card', 'edit_name_business_phone', 'edit_first_phone', 'edit_second_phone']);
            $new_client_data['client_number']=$client_data['edit_client_number'];
            $new_client_data['first_name']=$client_data['edit_first_name'];
            $new_client_data['last_name']=$client_data['edit_last_name'];
            $new_client_data['identity_card']=$client_data['edit_identity_card'];
            $new_client_data['name_business_phone']=$client_data['edit_name_business_phone'];
            $new_client_data['first_phone']=$client_data['edit_first_phone'];
            $new_client_data['second_phone']=$client_data['edit_second_phone'];

            $client->update($new_client_data);
            
        }

        // Sale
        $sale_data=$request->only(['description', 'sale_type', 'total', 'exchange_rate', 'total_bss', 'discount']);

        $sale_data['total']=str_replace('.', '', $sale_data['total']);
        $sale_data['total']=str_replace(',', '.', $sale_data['total']);

        $sale_data['exchange_rate']=str_replace('.', '', $sale_data['exchange_rate']);
        $sale_data['exchange_rate']=str_replace(',', '.', $sale_data['exchange_rate']);

        $sale_data['total_bss']=str_replace('.', '', $sale_data['total_bss']);
        $sale_data['total_bss']=str_replace(',', '.', $sale_data['total_bss']);

        $sale_data['user_id']=$user->id;
        $sale_data['client_id']=$client->id;

        $sale_data['discount']=str_replace('.', '', $sale_data['discount']);
        $sale_data['discount']=str_replace(',', '.', $sale_data['discount']);

        $sale_data['total_paid']=0;
        $sale_data['delivery_amount']=0;
        
        Sale::where('id', $sale_id)->update($sale_data);
        $sale=Sale::where('id',$sale_id)->first();
        $created_at=$sale->created_at;

        UnprocessedProductsMovement::where('sale_id',$sale->id)->delete();
        RawMaterialMovement::where('sale_id',$sale->id)->delete();

        // Products
        $product_ids=$request->product_id;
        $products_amounts=$request->products_amounts;
        $products_prices=$request->products_prices;
        $products_descriptions=$request->products_descriptions;

        $sale->products()->detach();

        if(isset($product_ids)) {
            foreach($product_ids as $key => $product_id) {
                
                $amount=str_replace('.', '', $products_amounts[$key]);
                $amount=str_replace(',', '.', $amount);
                $price=str_replace('.', '', $products_prices[$key]);
                $price=str_replace(',', '.', $price);
                $sale->products()->attach($product_id,['amount'=>$amount, 'price'=>$price, 'description'=>$products_descriptions[$key]]);

                $product=Product::find($product_id);

                if($product->type==1) {

                    $new_amount=($product->amount-$amount);
                    //$product->update(['amount'=>$new_amount]);
                    Product::where('id',$product_id)->update(['amount'=>$new_amount]);

                    $concept='Actualización de venta: '.$amount.' '.$product->name.'<br><b>ID de venta: '.$sale->id.'</b>';

                    UnprocessedProductsMovement::create(['product_id'=>$product->id, 'new_amount'=>$new_amount, 'amount'=>$amount, 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>2, 'sale_id'=>$sale->id]);

                }else {
                    $raws_material=$product->raw_material;
                    foreach($raws_material as $raw_material) {

                        $sale_amount=$raw_material->pivot->amount*$amount;
                        $new_amount=$raw_material->amount-$sale_amount;
                        //$raw_material->update(['amount'=>$new_amount]);
                        RawMaterial::where('id',$raw_material->id)->update(['amount'=>$new_amount]);

                        $concept='Actualización de venta: '.$amount.' '.$product->name.'<br><b>ID de venta: '.$sale->id.'</b>';
                        
                        // Raw material incomes
                        $check_amount=$raw_material->pivot->amount*$amount;
                        while($check_amount>0) {

                            $raw_material_income=RawMaterialIncome::orderBy('created_at','asc')->where('raw_material_id',$raw_material->id)->where('amount','>',0)->first();

                            if($raw_material_income) {
                                if($check_amount>=$raw_material_income->amount) {
                                    RawMaterialIncomeMovement::create(['raw_material_income_id'=>$raw_material_income->id, 'product_id'=>$product->id, 'sale_id'=>$sale->id, 'amount'=>$raw_material_income->amount, 'products_quantity'=>round(($raw_material_income->amount/$raw_material->pivot->amount), 2)]);
                                    $check_amount-=$raw_material_income->amount;
                                    RawMaterialIncome::where('id',$raw_material_income->id)->update(['amount'=>($raw_material_income->amount-$raw_material_income->amount)]);

                                }else {
                                    RawMaterialIncomeMovement::create(['raw_material_income_id'=>$raw_material_income->id, 'product_id'=>$product->id, 'sale_id'=>$sale->id, 'amount'=>$check_amount, 'products_quantity'=>round(($check_amount/$raw_material->pivot->amount), 2)]);
                                    RawMaterialIncome::where('id',$raw_material_income->id)->update(['amount'=>($raw_material_income->amount-$check_amount)]);
                                    $check_amount-=$check_amount;
                                }
                            }

                        }

                        RawMaterialMovement::create(['raw_material_id'=>$raw_material->id, 'new_amount'=>$new_amount, 'amount'=>$sale_amount, 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>2, 'sale_id'=>$sale->id]);

                    }
                }

            }
        }

        // Promotions
        $promotion_ids=$request->promotion_id;
        $promotions_amounts=$request->promotions_amounts;
        $promotions_prices=$request->promotions_prices;
        $promotions_descriptions=$request->promotions_descriptions;

        $sale->promotions()->detach();

        if(isset($promotion_ids)) {
            foreach($promotion_ids as $key => $promotion_id) {
                $amount=str_replace('.', '', $promotions_amounts[$key]);
                $amount=str_replace(',', '.', $amount);
                $price=str_replace('.', '', $promotions_prices[$key]);
                $price=str_replace(',', '.', $price);
                $sale->promotions()->attach($promotion_id,['amount'=>$amount, 'price'=>$price, 'description'=>$promotions_descriptions[$key]]);

                $promotion=Promotion::find($promotion_id);
                $products=$promotion->products;

                foreach($products as $product) {

                    if($product->type==1) {

                        $new_amount=($product->amount-($product->pivot->amount*$amount));
                        //$product->update(['amount'=>$new_amount]);
                        Product::where('id',$product->id)->update(['amount'=>$new_amount]);

                        $concept='Actualización de venta: '.($product->pivot->amount*$amount).' '.$product->name.'<br>Promoción: '.$amount.' '.$promotion->name.'<br><b>ID de venta: '.$sale->id.'</b>';

                        UnprocessedProductsMovement::create(['product_id'=>$product->id, 'new_amount'=>$new_amount, 'amount'=>($product->pivot->amount*$amount), 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>2, 'sale_id'=>$sale->id]);

                    }else {
                        $raws_material=$product->raw_material;
                        foreach($raws_material as $raw_material) {

                            $sale_amount=($raw_material->pivot->amount*$product->pivot->amount*$amount);
                            $new_amount=$raw_material->amount-$sale_amount;
                            //$raw_material->update(['amount'=>$new_amount]);
                            RawMaterial::where('id',$raw_material->id)->update(['amount'=>$new_amount]);

                            $concept='Actualización de venta: '.($product->pivot->amount*$amount).' '.$product->name.'<br>Promoción: '.$amount.' '.$promotion->name.'<br><b>ID de venta: '.$sale->id.'</b>';
                            
                            // Raw material incomes
                            $check_amount=$raw_material->pivot->amount*$product->pivot->amount*$amount;
                            while($check_amount>0) {

                                $raw_material_income=RawMaterialIncome::orderBy('created_at','asc')->where('raw_material_id',$raw_material->id)->where('amount','>',0)->first();

                                if($raw_material_income) {
                                    if($check_amount>=$raw_material_income->amount) {
                                        RawMaterialIncomeMovement::create(['raw_material_income_id'=>$raw_material_income->id, 'product_id'=>$product->id, 'sale_id'=>$sale->id, 'amount'=>$raw_material_income->amount, 'products_quantity'=>round(($raw_material_income->amount/$raw_material->pivot->amount), 2)]);
                                        $check_amount-=$raw_material_income->amount;
                                        RawMaterialIncome::where('id',$raw_material_income->id)->update(['amount'=>($raw_material_income->amount-$raw_material_income->amount)]);

                                    }else {
                                        RawMaterialIncomeMovement::create(['raw_material_income_id'=>$raw_material_income->id, 'product_id'=>$product->id, 'sale_id'=>$sale->id, 'amount'=>$check_amount, 'products_quantity'=>round(($check_amount/$raw_material->pivot->amount), 2)]);
                                        RawMaterialIncome::where('id',$raw_material_income->id)->update(['amount'=>($raw_material_income->amount-$check_amount)]);
                                        $check_amount-=$check_amount;
                                    }
                                }

                            }

                            RawMaterialMovement::create(['raw_material_id'=>$raw_material->id, 'new_amount'=>$new_amount, 'amount'=>$sale_amount, 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>2, 'sale_id'=>$sale->id]);

                        }
                    }

                }

            }
        }

        $payment_method_id=$request->payment_method_id;
        $payment_method_option_id=$request->payment_method_option_id;
        $payment_amount_usd=$request->payment_amount_usd;
        $payment_amount_bss=$request->payment_amount_bss;
        $payment_exchange_rate=$request->payment_exchange_rate;
        $payment_reference_number=$request->payment_reference_number;
        $payment_description=$request->payment_descriptions;

        $change_method_id=$request->change_method_id;
        $change_method_option_id=$request->change_method_option_id;
        $change_amount_usd=$request->change_amount_usd;
        $change_amount_bss=$request->change_amount_bss;
        $change_exchange_rate=$request->change_exchange_rate;
        $change_reference_number=$request->change_reference_number;
        $change_description=$request->change_descriptions;
        $pay_the_customer=$request->pay_the_customers;

        // Delivery
        $delivery_company_ids=$request->delivery_company_id;
        $delivery_type_ids=$request->delivery_type_id;
        $delivery_company_driver_ids=$request->delivery_company_driver_id;
        $services_quantity=$request->services_quantity;
        
        if(isset($delivery_company_ids)) {
            foreach($delivery_company_ids as $key => $delivery_company_id) {
                $delivery_sale['delivery_company_id']=$delivery_company_id;

                if($delivery_type_ids[$key]=='NULL') {
                    $delivery_sale['delivery_type_id']=NULL;
                    $delivery_sale['price']=0;
                }else {
                    $delivery_sale['delivery_type_id']=$delivery_type_ids[$key];
                    $delivery_type=DeliveryType::where('id',$delivery_type_ids[$key])->first();
                    $delivery_sale['price']=$delivery_type->price;
                }

                if($delivery_company_driver_ids[$key]=='NULL') {
                    $delivery_sale['delivery_company_driver_id']=NULL;
                }else {
                    $delivery_sale['delivery_company_driver_id']=$delivery_company_driver_ids[$key];
                }

                $amount=str_replace('.', '', $services_quantity[$key]);
                $delivery_sale['amount']=str_replace(',', '.', $amount);

                $delivery_sale['sale_id']=$sale->id;
                $delivery_sale['created_at']=$created_at;

                $sale->update(['delivery_amount'=>$sale->delivery_amount+($delivery_sale['amount']*$delivery_sale['price'])]);
                DeliverySale::create($delivery_sale);
            }
        }

        // Payments and changes
        if(isset($payment_method_id)) {
            foreach($payment_method_id as $key => $payment_method) {

                $data['sale_id']=$sale->id;

                // Payment
                $data['payment_method_id']=$payment_method;

                if(isset($payment_method_option_id) && isset($payment_method_option_id[$key])) {
                    $data['payment_method_option_id']=$payment_method_option_id[$key];
                    if($data['payment_method_option_id']=='NULL')
                        $data['payment_method_option_id']=NULL;
                }else {
                    $data['payment_method_option_id']=NULL;
                }

                $data['payment_amount_usd']=$payment_amount_usd[$key];
                $data['payment_amount_usd']=str_replace('.', '', $data['payment_amount_usd']);
                $data['payment_amount_usd']=str_replace(',', '.', $data['payment_amount_usd']);
                $data['payment_amount_bss']=$payment_amount_bss[$key];
                $data['payment_amount_bss']=str_replace('.', '', $data['payment_amount_bss']);
                $data['payment_amount_bss']=str_replace(',', '.', $data['payment_amount_bss']);
                $data['payment_exchange_rate']=$payment_exchange_rate[$key];
                $data['payment_exchange_rate']=str_replace('.', '', $data['payment_exchange_rate']);
                $data['payment_exchange_rate']=str_replace(',', '.', $data['payment_exchange_rate']);
                $data['payment_reference_number']=$payment_reference_number[$key];
                $data['payment_description']=$payment_description[$key];

                // Change
                $data['change_method_id']=$change_method_id[$key];
                $data['change_method_option_id']=$change_method_option_id[$key];
                if($data['change_method_option_id']=='NULL')
                    $data['change_method_option_id']=NULL;

                $data['change_amount_usd']=$change_amount_usd[$key];
                if(!isset($data['change_amount_usd']))
                    $data['change_amount_usd']=0;

                $data['change_amount_usd']=str_replace('.', '', $data['change_amount_usd']);
                $data['change_amount_usd']=str_replace(',', '.', $data['change_amount_usd']);

                $data['change_amount_bss']=$change_amount_bss[$key];
                if(!isset($data['change_amount_bss']))
                    $data['change_amount_bss']=0;

                $data['change_amount_bss']=str_replace('.', '', $data['change_amount_bss']);
                $data['change_amount_bss']=str_replace(',', '.', $data['change_amount_bss']);

                $data['change_exchange_rate']=$change_exchange_rate[$key];
                $data['change_exchange_rate']=str_replace('.', '', $data['change_exchange_rate']);
                $data['change_exchange_rate']=str_replace(',', '.', $data['change_exchange_rate']);
                $data['change_reference_number']=$change_reference_number[$key];
                $data['change_description']=$change_description[$key];
                $data['pay_the_customer']=$pay_the_customer[$key];
                $data['created_at']=$created_at;

                $payment=Payment::create($data);

                $sale=Sale::find($sale->id);
                $sale->update(['total_paid'=>$sale->total_paid+$payment->payment_amount_usd]);

                if($data['pay_the_customer']==0) {
                    $sale=Sale::find($sale->id);
                    $sale->update(['total_paid'=>$sale->total_paid-$payment->change_amount_usd]); 
                }

                // Payment
                if($payment->payment_method->type=='Efectivo') {
                    $cash_register=CashRegister::first();
                    $cash_register_movement=null;
                    $cash_register_movement['cash_register_id']=$cash_register->id;
                    $cash_register_movement['type']=1;
                    $cash_register_movement['currency']=$payment->payment_method->currency;
                    $cash_register_movement['movement']='Pago';

                    if($payment->payment_method->currency=='$') {
                        $cash_register_movement['amount']=$data['payment_amount_usd'];
                        $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount+$data['payment_amount_usd']]);
                    }else {
                        $cash_register_movement['amount']=$data['payment_amount_bss'];
                        $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount+$data['payment_amount_bss']]);
                    }

                    $cash_register_movement['sale_id']=$sale->id;
                    $cash_register_movement['created_at']=$created_at;

                    CashRegisterMovement::create($cash_register_movement);
                }

                // Change
                if($payment->change_method->type=='Efectivo' && ($data['change_amount_usd']!=0 || $data['change_amount_bss']!=0) && $data['pay_the_customer']==0) {
                    $cash_register=CashRegister::first();
                    $cash_register_movement=null;
                    $cash_register_movement['cash_register_id']=$cash_register->id;
                    $cash_register_movement['type']=2;
                    $cash_register_movement['currency']=$payment->change_method->currency;
                    $cash_register_movement['movement']='Cambio';

                    if($payment->change_method->currency=='$') {
                        $cash_register_movement['amount']=$data['change_amount_usd'];
                        $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount-$data['change_amount_usd']]);
                    }else {
                        $cash_register_movement['amount']=$data['change_amount_bss'];
                        $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount-$data['change_amount_bss']]);
                    }

                    $cash_register_movement['sale_id']=$sale->id;
                    $cash_register_movement['created_at']=$created_at;
                    
                    CashRegisterMovement::create($cash_register_movement);
                }
            }
        }

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_sale', 'operation'=>'Actualización de la venta con ID: '.$sale->id.' - Monto total: '.number_format($sale->total, 2, ',', '.').'$.']);

        return redirect()->route('sales_show',['sale_id'=>$sale->id])->with(['message_info'=>'Venta con ID: '.$sale->id.' actualizada exitosamente.']);
    }

    public function getTrash($sale_id) 
    {
        $sale=Sale::where('id',$sale_id)->first();

        if(!$sale) {
            return redirect()->route('sales')->withErrors(['La venta con ID '.$sale_id.' no se encuentra registrada.']);
        }

        return view('seller.sales.trash',['sale'=>$sale]);
    }

    public function delete(Request $request) 
    {
        $sale_id=$request->sale_id;
        $sale=Sale::where('id',$sale_id)->first();

        $sale_id=$sale->id;

        // Delete cash register movement
        $cash_register_movements=$sale->cash_register_movements->sortBy('id');
        foreach($cash_register_movements as $movement) {

            if($movement->type==1) {

                $cash_register=CashRegister::first();
                $cash_register_movement=null;
                $cash_register_movement['cash_register_id']=$cash_register->id;
                $cash_register_movement['type']=2;
                $cash_register_movement['currency']=$movement->currency;
                $cash_register_movement['movement']='Eliminación del pago - ID de venta: '.$sale_id;

                if($movement->currency=='$') {
                    $cash_register_movement['amount']=$movement->amount;
                    $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount-$movement->amount]);
                }else {
                    $cash_register_movement['amount']=$movement->amount;
                    $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount-$movement->amount]);
                }

                $cash_register_movement['sale_id']=null;

                $movement->update(['movement'=>'Pago - ID de venta: '.$sale_id, 'sale_id'=>null]);
                CashRegisterMovement::create($cash_register_movement);

            }else {

                $cash_register=CashRegister::first();
                $cash_register_movement=null;
                $cash_register_movement['cash_register_id']=$cash_register->id;
                $cash_register_movement['type']=1;
                $cash_register_movement['currency']=$movement->currency;
                $cash_register_movement['movement']='Eliminación del cambio - ID de venta: '.$sale_id;

                if($movement->currency=='$') {
                    $cash_register_movement['amount']=$movement->amount;
                    $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount+$movement->amount]);
                }else {
                    $cash_register_movement['amount']=$movement->amount;
                    $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount+$movement->amount]);
                }

                $cash_register_movement['sale_id']=null;

                $movement->update(['movement'=>'Cambio - ID de venta: '.$sale_id, 'sale_id'=>null]);
                CashRegisterMovement::create($cash_register_movement);

            }

        }

        // Delete payments
        Payment::where('sale_id',$sale_id)->delete();

        // Delete promotions
        $promotions=$sale->promotions;
        foreach($promotions as $promotion) {
            
            $amount=$promotion->pivot->amount;
            $products=$promotion->products;

            foreach($products as $product) {

                if($product->type==1) {

                    $new_amount=($product->amount+($product->pivot->amount*$amount));
                    //$product->update(['amount'=>$new_amount]);
                    Product::where('id',$product->id)->update(['amount'=>$new_amount]);

                    $concept='Eliminación de venta: '.($product->pivot->amount*$amount).' '.$product->name.'<br>Promoción: '.$amount.' '.$promotion->name.'<br><b>ID de venta: '.$sale->id.'</b>';

                    UnprocessedProductsMovement::create(['product_id'=>$product->id, 'new_amount'=>$new_amount, 'amount'=>($product->pivot->amount*$amount), 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>1, 'is_deleted'=>1]);

                }else {
                    $raws_material=$product->raw_material;
                    foreach($raws_material as $raw_material) {

                        $new_amount=$raw_material->amount+($raw_material->pivot->amount*$product->pivot->amount*$amount);
                        //$raw_material->update(['amount'=>$new_amount]);
                        RawMaterial::where('id',$raw_material->id)->update(['amount'=>$new_amount]);
                        
                        $concept='Eliminación de venta: '.($product->pivot->amount*$amount).' '.$product->name.'<br>Promoción: '.$amount.' '.$promotion->name.'<br><b>ID de venta: '.$sale->id.'</b>';
                        
                        RawMaterialMovement::create(['raw_material_id'=>$raw_material->id, 'new_amount'=>$new_amount, 'amount'=>($raw_material->pivot->amount*$product->pivot->amount*$amount), 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>1, 'is_deleted'=>1]);

                    }
                }

            }

        }

        // Delete products
        $products=$sale->products;
        foreach($products as $product) {

            $amount=$product->pivot->amount;
            $price=$product->pivot->price;

            if($product->type==1) {

                $new_amount=($product->amount+$amount);
                //$product->update(['amount'=>$new_amount]);
                Product::where('id',$product->id)->update(['amount'=>$new_amount]);

                $concept='Eliminación de venta: '.$amount.' '.$product->name.'<br><b>ID de venta: '.$sale->id.'</b>';

                UnprocessedProductsMovement::create(['product_id'=>$product->id, 'new_amount'=>$new_amount, 'amount'=>$amount, 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>1, 'is_deleted'=>1]);

            }else {
                $raws_material=$product->raw_material;
                foreach($raws_material as $raw_material) {

                    $new_amount=$raw_material->amount+$raw_material->pivot->amount*$amount;
                    //$raw_material->update(['amount'=>$new_amount]);
                    RawMaterial::where('id',$raw_material->id)->update(['amount'=>$new_amount]);
                    
                    $concept='Eliminación de venta: '.$amount.' '.$product->name.'<br><b>ID de venta: '.$sale->id.'</b>';
                    
                    RawMaterialMovement::create(['raw_material_id'=>$raw_material->id, 'new_amount'=>$new_amount, 'amount'=>$raw_material->pivot->amount*$amount, 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>1, 'is_deleted'=>1]);

                }
            }

        }

        // Delete raw material income movements
        $raw_material_income_movements=$sale->raw_material_income_movements;
        foreach($raw_material_income_movements as $raw_material_income_movement) {
            //$raw_material_income_movement->raw_material_income->update(['amount'=>$raw_material_income_movement->raw_material_income->amount+$raw_material_income_movement->amount]);

            $raw_material_income=RawMaterialIncome::find($raw_material_income_movement->raw_material_income_id);
            $new_amount=$raw_material_income->amount+$raw_material_income_movement->amount;
            RawMaterialIncome::where('id',$raw_material_income->id)->update(['amount'=>$new_amount]);

        }

        // Delete sale
        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_sale', 'operation'=>'Eliminación de la venta con ID: '.$sale->id.' - Monto total: '.number_format($sale->total, 2, ',', '.').'$.']);

        $sale->delete();


        return redirect()->route('sales_history')->with(['message_info'=>'Venta con ID: '.$sale_id.' eliminada exitosamente.']);
    }

    public function getDeliveryCompanyDrivers($delivery_company_id) 
    {
        $delivery_company_drivers=DeliveryCompanyDriver::where('delivery_company_id',$delivery_company_id)->get();
        return response()->json(['delivery_company_drivers'=>$delivery_company_drivers]);
    }

    public function getPaymentMethodOptions($payment_method_id) 
    {
        $payment_method_options=PaymentMethodOption::where('payment_method_id',$payment_method_id)->get();
        return response()->json(['payment_method_options'=>$payment_method_options]);
    }

    public function getDeliveryTypes($delivery_company_id) 
    {
        $delivery_types=DeliveryType::where('delivery_company_id',$delivery_company_id)->orderBy('price','asc')->get();
        return response()->json(['delivery_types'=>$delivery_types]);
    }

    public function updateDispatched($sale_id) {
        $sale=Sale::where('id',$sale_id)->first();

        if(!$sale) {
            return redirect()->route('sales')->withErrors(['La venta con ID '.$sale_id.' no se encuentra registrada.']);
        }

        $sale->update(['dispatched'=>1]);

        return redirect()->back()->with(['message_info'=>'Venta con ID: '.$sale->id.' despachada exitosamente.']);
    }

    public function getDiscountIndex(Request $request)
    {
        if($request->final_date<$request->initial_date) {
            return redirect()->back()->withInput()->withErrors(['La fecha final no puede ser menor a la fecha del inicio.']);
        }

        return view('seller.sales.discount_list',['menu_active'=>'discount_sales', 'initial_date'=>$request->initial_date, 'final_date'=>$request->final_date]);
    }

    public function getDiscountList(Request $request)
    {
        $user=User::getCurrent();
        $sales_list=[];
        $count=0;
        
        if($request->initial_date && $request->final_date) {
            $sales=Sale::where('created_at','>=',$request->initial_date.' 00:00:00')->where('created_at','<=',$request->final_date.' 23:59:59')->orderBy('id','asc')->get();
        }else {
            $sales=Sale::orderBy('id','asc')->get();
        }

        foreach($sales as $sale) {

            if($sale->discount>0) {

                $count++;

                $actions='';

                if($sale->dispatched==0) {
                    $actions.=' <a style="color: black !important;" href="'.route("sales_update_dispatched",["sale_id"=>$sale->id]).'"><i class="fas fa-shipping-fast" aria-hidden="true"></i></a>';
                }

                if($sale->total_paid<$sale->total) {
                    $actions.=' <a style="color: #28a745 !important;" href="'.route("payment_collections_pay",["sale_id"=>$sale->id]).'"><i class="fas fa-handshake" aria-hidden="true"></i></a>';
                }

                if($user->hasRole('admin')) {
                    $actions.=' <a style="color: #17a2b8 !important;" href="'.route("sales_show",["sale_id"=>$sale->id]).'"><i class="fa fa-search" aria-hidden="true"></i></a> <a style="color: #007bff !important;" href="'.route("sales_edit",["sale_id"=>$sale->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a style="color: orange !important;" href="'.route("sales_trash",["sale_id"=>$sale->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                }elseif($user->hasRole('seller')) {
                    $actions.=' <a style="color: #17a2b8 !important;" href="'.route("sales_show",["sale_id"=>$sale->id]).'"><i class="fa fa-search" aria-hidden="true"></i></a> <a style="color: #007bff !important;" href="'.route("sales_edit",["sale_id"=>$sale->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a>';
                }

                $sales_list[]=[$count, $sale->id, $sale->created_at->format('d-m-Y h:i:s a'), $sale->user->first_name.' '.$sale->user->last_name, $sale->client->name_business_phone, number_format($sale->discount, 2, ',', '.'), number_format($sale->total, 2, ',', '.'), number_format($sale->total_paid, 2, ',', '.'), ($sale->total-$sale->total_paid)<0?'0.00':number_format($sale->total-$sale->total_paid, 2, ',', '.'), $sale->sale_type==1?'Al contado':'A crédito', ($sale->total-$sale->total_paid)<0?'Si':'No', $sale->dispatched==1?'Si':'No',$actions];

            }

        }

        return response()->json(['data' => $sales_list]);
    }

    public function getProductSales($product_id)
    {
        $product=Product::where('id',$product_id)->first();

        if(!$product) {
            return redirect()->back()->withErrors(['El producto con ID #'.$product_id.' no se encuentra registrado.']);
        }

        return view('seller.sales.product_sales',['product'=>$product, 'sales'=>$product->sales, 'menu_active'=>'reports']);
    }

    public function getPromotionSales($promotion_id)
    {
        $promotion=Promotion::where('id',$promotion_id)->first();

        if(!$promotion) {
            return redirect()->route('admin_promotions')->withErrors(['La promoción con ID #'.$promotion_id.' no se encuentra registrada.']);
        }

        return view('seller.sales.promotion_sales',['promotion'=>$promotion, 'sales'=>$promotion->sales, 'menu_active'=>'reports']);
    }
}
