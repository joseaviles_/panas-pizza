<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\CashRegister;
use App\Models\CashRegisterMovement;
use App\Models\Tracking;
use App\Http\Requests\CreateCashRegisterMovementRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class CashRegisterController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'cash_register');
    }

    public function getIndex()
    {
        return view('seller.cash_register.list');
    }

    public function getList()
    {
        $movements=CashRegisterMovement::orderBy('created_at','desc')->get();
        $movements_list=[];

        foreach($movements as $movement) {

        	$sale='N/A';
        	if($movement->sale_id) {
        		$sale='<b>ID de venta:</b> '.$movement->sale->id;
        		$sale.='<br><a href="'.route("sales_show",["sale_id"=>$movement->sale->id]).'"><b><i class="fa fa-search" aria-hidden="true"></i> Mostrar venta</b></a>';
        	}

            $movements_list[]=[$movement->id, $movement->created_at->format('d-m-Y h:i:s a'), $movement->type==1?'Entrada':'Salida', $movement->movement, number_format($movement->amount, 2, ',', '.'), $movement->currency, $sale];

        }

        return response()->json(['data' => $movements_list]);
    }

    public function getCreate() 
    {
        return view('seller.cash_register.create');
    }

    public function create(CreateCashRegisterMovementRequest $request) 
    {
    	$cash_register=CashRegister::first();
        $data=$request->only(['currency','amount']);
        $data['amount']=str_replace('.', '', $data['amount']);
        $data['amount']=str_replace(',', '.', $data['amount']);
        $data['cash_register_id']=$cash_register->id;
    	$data['type']=1;
    	$data['movement']='Ingreso';

    	if($data['amount']<=0) {
    		return redirect()->back()->withErrors('El monto del ingreso no puede ser cero (0).');
    	}

        if($data['currency']=='$') {
            $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount+$data['amount']]);
        }else {
            $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount+$data['amount']]);
        }

        $movement=CashRegisterMovement::create($data);

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_cash_register_movement', 'operation'=>'Ingreso de efectivo: '.number_format($movement->amount, 2, ',', '.').' '.$movement->currency.'.']);

        return redirect()->route('cash_register')->with(['message_info'=>'El ingreso de la cantidad de: '.number_format($movement->amount, 2, ',', '.').' '.$movement->currency.' ha sido registrado exitosamente.']);
    }

    public function getWithdrawCash() 
    {
        return view('seller.cash_register.withdraw_cash');
    }

    public function postWithdrawCash(CreateCashRegisterMovementRequest $request) 
    {
    	$cash_register=CashRegister::first();
        $data=$request->only(['currency','amount']);
        $data['amount']=str_replace('.', '', $data['amount']);
        $data['amount']=str_replace(',', '.', $data['amount']);
        $data['cash_register_id']=$cash_register->id;
    	$data['type']=2;
    	$data['movement']='Retiro';

    	if($data['amount']<=0) {
    		return redirect()->back()->withErrors('El monto del retiro no puede ser cero (0).');
    	}

        if($data['currency']=='$' && $cash_register->dollar_amount<$data['amount']) {
        	return redirect()->back()->withErrors('El monto del retiro no puede ser mayor al monto disponible en la caja.');
        }elseif($data['currency']=='Bs.S' && $cash_register->bolivars_amount<$data['amount']) {
        	return redirect()->back()->withErrors('El monto del retiro no puede ser mayor al monto disponible en la caja.');
        }

        if($data['currency']=='$') {
            $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount-$data['amount']]);
        }else {
            $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount-$data['amount']]);
        }

        $movement=CashRegisterMovement::create($data);

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_cash_register_movement', 'operation'=>'Retiro de efectivo: '.number_format($movement->amount, 2, ',', '.').' '.$movement->currency.'.']);

        return redirect()->route('cash_register')->with(['message_info'=>'El retiro de la cantidad de: '.number_format($movement->amount, 2, ',', '.').' '.$movement->currency.' ha sido registrado exitosamente.']);
    }
}
