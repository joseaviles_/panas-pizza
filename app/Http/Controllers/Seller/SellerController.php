<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class SellerController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'dashboard');
    }

    public function getSellerDashboard()
    {
    	return view('main.seller_dashboard');
    }
}
