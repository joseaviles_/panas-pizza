<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CashRegister;
use App\Models\CashRegisterMovement;
use App\Models\Sale;
use App\Models\Payment;
use App\Models\PaymentMethod;
use App\Models\PaymentMethodOption;
use App\Models\Tracking;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class PaymentCollectionsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'payment_collections');
    }

    public function getIndex()
    {
        return view('seller.payment_collections.list');
    }

    public function getList()
    {
        $user=User::getCurrent();
        $sales=Sale::where('sale_type',2)->get();
        $sales_list=[];

        foreach($sales as $sale) {

        	if($sale->total_paid<$sale->total) {

                if($user->hasRole('admin')) {

                    $sales_list[]=[$sale->id, $sale->created_at->format('d-m-Y h:i:s a'), $sale->user->first_name.' '.$sale->user->last_name, $sale->client->name_business_phone, number_format($sale->total, 2, ',', '.'), number_format($sale->total_paid, 2, ',', '.'), ($sale->total-$sale->total_paid)<0?'0.00':number_format($sale->total-$sale->total_paid, 2, ',', '.'), $sale->sale_type==1?'Al contado':'A crédito', ($sale->total-$sale->total_paid)<0?'Si':'No', '<a style="color: #28a745 !important;" href="'.route("payment_collections_pay",["sale_id"=>$sale->id]).'"><i class="fas fa-handshake" aria-hidden="true"></i></a> <a style="color: #17a2b8 !important;" href="'.route("sales_show",["sale_id"=>$sale->id]).'"><i class="fa fa-search" aria-hidden="true"></i></a> <a style="color: #007bff !important;" href="'.route("sales_edit",["sale_id"=>$sale->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a style="color: orange !important;" href="'.route("sales_trash",["sale_id"=>$sale->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>'];

                }elseif($user->hasRole('seller')) {

                    $sales_list[]=[$sale->id, $sale->created_at->format('d-m-Y h:i:s a'), $sale->user->first_name.' '.$sale->user->last_name, $sale->client->name_business_phone, number_format($sale->total, 2, ',', '.'), number_format($sale->total_paid, 2, ',', '.'), ($sale->total-$sale->total_paid)<0?'0.00':number_format($sale->total-$sale->total_paid, 2, ',', '.'), $sale->sale_type==1?'Al contado':'A crédito', ($sale->total-$sale->total_paid)<0?'Si':'No', '<a style="color: #28a745 !important;" href="'.route("payment_collections_pay",["sale_id"=>$sale->id]).'"><i class="fas fa-handshake" aria-hidden="true"></i></a> <a style="color: #17a2b8 !important;" href="'.route("sales_show",["sale_id"=>$sale->id]).'"><i class="fa fa-search" aria-hidden="true"></i></a> <a style="color: #007bff !important;" href="'.route("sales_edit",["sale_id"=>$sale->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a>'];
                    
                }

	            

        	}

        }

        return response()->json(['data' => $sales_list]);
    }

    public function getPay($sale_id) 
    {
        $sale=Sale::where('id',$sale_id)->first();

        if(!$sale) {
            return redirect()->route('payment_collections')->withErrors(['La venta con ID '.$sale_id.' no se encuentra registrada.']);
        }

        if($sale->total_paid>=$sale->total) {
        	return redirect()->route('payment_collections')->withErrors(['La venta con ID '.$sale_id.' ya se encuentra pagada.']);
        }

    	if($sale->sale_type==1) {
        	return redirect()->route('payment_collections')->withErrors(['La venta con ID '.$sale_id.' es un tipo de venta al contado.']);
        }

    	$payment_methods=PaymentMethod::orderBy('name')->get();

    	if(count($payment_methods)==0) {
			return redirect()->route('admin_payment_methods_create')->with(['message_info'=>'Debes registrar como mínimo un método de pago para proceder a registrar una venta.']);
    	}

        $payment_method_options=PaymentMethodOption::orderBy('name')->get();

        return view('seller.payment_collections.pay',['sale'=>$sale, 'payment_methods'=>$payment_methods, 'payment_method_options'=>$payment_method_options]);
    }

    public function pay(Request $request)
    {
    	$sale_id=$request->sale_id;
		$sale=Sale::where('id',$sale_id)->first();

        $payment_method_id=$request->payment_method_id;
        $payment_method_option_id=$request->payment_method_option_id;
        $payment_amount_usd=$request->payment_amount_usd;
        $payment_amount_bss=$request->payment_amount_bss;
        $payment_exchange_rate=$request->payment_exchange_rate;
        $payment_reference_number=$request->payment_reference_number;
        $payment_description=$request->payment_descriptions;

        $change_method_id=$request->change_method_id;
        $change_method_option_id=$request->change_method_option_id;
        $change_amount_usd=$request->change_amount_usd;
        $change_amount_bss=$request->change_amount_bss;
        $change_exchange_rate=$request->change_exchange_rate;
        $change_reference_number=$request->change_reference_number;
        $change_description=$request->change_descriptions;
        $pay_the_customer=$request->pay_the_customers;

        // Payments and changes
        if(isset($payment_method_id)) {
            foreach($payment_method_id as $key => $payment_method) {

                $data['sale_id']=$sale->id;

                // Payment
                $data['payment_method_id']=$payment_method;
                $data['payment_method_option_id']=$payment_method_option_id[$key];
                if($data['payment_method_option_id']=='NULL')
                    $data['payment_method_option_id']=NULL;

                $data['payment_amount_usd']=$payment_amount_usd[$key];
                $data['payment_amount_usd']=str_replace('.', '', $data['payment_amount_usd']);
                $data['payment_amount_usd']=str_replace(',', '.', $data['payment_amount_usd']);
                $data['payment_amount_bss']=$payment_amount_bss[$key];
                $data['payment_amount_bss']=str_replace('.', '', $data['payment_amount_bss']);
                $data['payment_amount_bss']=str_replace(',', '.', $data['payment_amount_bss']);
                $data['payment_exchange_rate']=$payment_exchange_rate[$key];
                $data['payment_exchange_rate']=str_replace('.', '', $data['payment_exchange_rate']);
                $data['payment_exchange_rate']=str_replace(',', '.', $data['payment_exchange_rate']);
                $data['payment_reference_number']=$payment_reference_number[$key];
                $data['payment_description']=$payment_description[$key];

                // Change
                $data['change_method_id']=$change_method_id[$key];
                $data['change_method_option_id']=$change_method_option_id[$key];
                if($data['change_method_option_id']=='NULL')
                    $data['change_method_option_id']=NULL;

                $data['change_amount_usd']=$change_amount_usd[$key];
                if(!isset($data['change_amount_usd']))
                    $data['change_amount_usd']=0;

                $data['change_amount_usd']=str_replace('.', '', $data['change_amount_usd']);
                $data['change_amount_usd']=str_replace(',', '.', $data['change_amount_usd']);

                $data['change_amount_bss']=$change_amount_bss[$key];
                if(!isset($data['change_amount_bss']))
                    $data['change_amount_bss']=0;

                $data['change_amount_bss']=str_replace('.', '', $data['change_amount_bss']);
                $data['change_amount_bss']=str_replace(',', '.', $data['change_amount_bss']);

                $data['change_exchange_rate']=$change_exchange_rate[$key];
                $data['change_exchange_rate']=str_replace('.', '', $data['change_exchange_rate']);
                $data['change_exchange_rate']=str_replace(',', '.', $data['change_exchange_rate']);
                $data['change_reference_number']=$change_reference_number[$key];
                $data['change_description']=$change_description[$key];
                $data['pay_the_customer']=$pay_the_customer[$key];

                $payment=Payment::create($data);

                $sale=Sale::find($sale->id);
                $sale->update(['total_paid'=>$sale->total_paid+$payment->payment_amount_usd]);

                if($data['pay_the_customer']==0) {
                    $sale=Sale::find($sale->id);
                    $sale->update(['total_paid'=>$sale->total_paid-$payment->change_amount_usd]); 
                }

                // Payment
                if($payment->payment_method->type=='Efectivo') {
                    $cash_register=CashRegister::first();
                    $cash_register_movement=null;
                    $cash_register_movement['cash_register_id']=$cash_register->id;
                    $cash_register_movement['type']=1;
                    $cash_register_movement['currency']=$payment->payment_method->currency;
                    $cash_register_movement['movement']='Pago';

                    if($payment->payment_method->currency=='$') {
                        $cash_register_movement['amount']=$data['payment_amount_usd'];
                        $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount+$data['payment_amount_usd']]);
                    }else {
                        $cash_register_movement['amount']=$data['payment_amount_bss'];
                        $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount+$data['payment_amount_bss']]);
                    }

                    $cash_register_movement['sale_id']=$sale->id;

                    CashRegisterMovement::create($cash_register_movement);
                }

                // Change
                if($payment->change_method->type=='Efectivo' && ($data['change_amount_usd']!=0 || $data['change_amount_bss']!=0) && $data['pay_the_customer']==0) {
                    $cash_register=CashRegister::first();
                    $cash_register_movement=null;
                    $cash_register_movement['cash_register_id']=$cash_register->id;
                    $cash_register_movement['type']=2;
                    $cash_register_movement['currency']=$payment->change_method->currency;
                    $cash_register_movement['movement']='Cambio';

                    if($payment->change_method->currency=='$') {
                        $cash_register_movement['amount']=$data['change_amount_usd'];
                        $cash_register->update(['dollar_amount'=>$cash_register->dollar_amount-$data['change_amount_usd']]);
                    }else {
                        $cash_register_movement['amount']=$data['change_amount_bss'];
                        $cash_register->update(['bolivars_amount'=>$cash_register->bolivars_amount-$data['change_amount_bss']]);
                    }

                    $cash_register_movement['sale_id']=$sale->id;

                    CashRegisterMovement::create($cash_register_movement);
                }
            }
        }

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_payment', 'operation'=>'Registro de pago de la venta con ID: '.$sale->id.' - Cobranza.']);

        $sale=Sale::where('id',$sale_id)->first();

        if($sale->total_paid>=$sale->total) {
        	return redirect()->route('sales_show',['sale_id'=>$sale->id])->with(['message_info'=>'La venta ha sido pagada en su totalidad y el pago faltante fue registrado exitosamente.']);
        }else {
        	return redirect()->back()->with(['message_info'=>'Pago registrado exitosamente.']);
        }


    }
}
