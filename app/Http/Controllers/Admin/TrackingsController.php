<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tracking;
use Illuminate\Support\Facades\View;

class TrackingsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'trackings');
    }

    public function getIndex()
    {
        return view('admin.trackings.list');
    }

    public function getList()
    {
        $trackings=Tracking::all();
        $trackings_list=[];

        foreach($trackings as $tracking) {
            $trackings_list[]=[$tracking->created_at->format('d-m-Y h:i:s a'), $tracking->user->first_name.' '.$tracking->user->last_name.' - '.$tracking->user->email, $tracking->operation];
        }

        return response()->json(['data' => $trackings_list]);
    }
}
