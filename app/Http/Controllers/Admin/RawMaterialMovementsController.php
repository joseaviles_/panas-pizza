<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\RawMaterial;
use App\Models\RawMaterialIncome;
use App\Models\RawMaterialMovement;
use App\Models\Tracking;
use App\Http\Requests\CreateRawMaterialMovementRequest;
use App\Http\Requests\UpdateRawMaterialMovementRequest;
use Illuminate\Support\Facades\View;

class RawMaterialMovementsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'inventory');
    }

    public function getList($raw_material_id)
    {
        $raw_material_movements=RawMaterialMovement::where('raw_material_id',$raw_material_id)->get();
        $raw_material_movements_list=[];

        foreach($raw_material_movements as $raw_material_movement) {

            $actions=null;

            if($raw_material_movement->type==1 && $raw_material_movement->is_deleted==0) {

                if($raw_material_movement->raw_material_income && count($raw_material_movement->raw_material_income->raw_material_income_movements)==0) {
                    $actions='<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_raw_material_movements_edit",["raw_material_movement_id"=>$raw_material_movement->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_raw_material_movements_trash",["raw_material_movement_id"=>$raw_material_movement->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                }else {
                    $actions='N/A';
                }

            }else {
                $actions='N/A';
            }

            $raw_material_movements_list[]=[$raw_material_movement->id, $raw_material_movement->created_at->format('d-m-Y h:i:s a'), $raw_material_movement->type==1?'Entrada':'Salida', number_format($raw_material_movement->new_amount, 2, ',', '.'), number_format($raw_material_movement->amount, 2, ',', '.'), $raw_material_movement->price==0?'N/A':number_format($raw_material_movement->price, 2, ',', '.'), $raw_material_movement->price_bss==0?'N/A':number_format($raw_material_movement->price_bss, 2, ',', '.'), $raw_material_movement->price==0?'N/A':number_format($raw_material_movement->exchange_rate, 2, ',', '.'), $raw_material_movement->concept, $actions];

        }

        return response()->json(['data' => $raw_material_movements_list]);
    }

    public function create(CreateRawMaterialMovementRequest $request) 
    {
        $data=$request->only(['raw_material_id','amount','price','price_bss','exchange_rate']);

        $data['amount']=str_replace('.', '', $data['amount']);
        $data['amount']=str_replace(',', '.', $data['amount']);

        if($data['amount']<=0)
            return redirect()->back()->withInput()->withErrors('La cantidad a ingresar debe ser mayor a cero (0).');

        $data['price']=str_replace('.', '', $data['price']);
        $data['price']=str_replace(',', '.', $data['price']);

        $data['price_bss']=str_replace('.', '', $data['price_bss']);
        $data['price_bss']=str_replace(',', '.', $data['price_bss']);

        $data['exchange_rate']=str_replace('.', '', $data['exchange_rate']);
        $data['exchange_rate']=str_replace(',', '.', $data['exchange_rate']);

        $raw_material=RawMaterial::where('id',$data['raw_material_id'])->first();
        $new_amount=$raw_material->amount+$data['amount'];

        $data['concept']='Compra de la materia prima: '.$raw_material->name;
        $data['type']=1;
        $data['new_amount']=$new_amount;

        $raw_material_movement=RawMaterialMovement::create($data);
     	
     	$raw_material->update(['amount'=>$new_amount]);

        RawMaterialIncome::create(['raw_material_movement_id'=>$raw_material_movement->id, 'raw_material_id'=>$raw_material->id, 'amount'=>$data['amount']]);

        $current_user=User::getCurrent();
        $operation='Entrada al stock de la materia prima: '.$raw_material->name.' - '.number_format($data['amount'], 2, ',', '.').' '.$raw_material->unit;

        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_raw_material_movement', 'operation'=>$operation]);

        return redirect()->back()->with(['message_info'=>$operation]);
    }

    public function getEdit($raw_material_movement_id) 
    {
        $raw_material_movement=RawMaterialMovement::where('id',$raw_material_movement_id)->first();

        if(!$raw_material_movement) {
            return redirect()->back()->withErrors(['El movimiento con ID #'.$raw_material_movement_id.' no se encuentra registrado.']);
        }

        return view('admin.inventory.raw_material_movements.edit',['raw_material_movement'=>$raw_material_movement]);
    }

    public function update(UpdateRawMaterialMovementRequest $request)
    {
    	$raw_material_movement_id=$request->raw_material_movement_id;
    	$raw_material_movement=RawMaterialMovement::where('id',$raw_material_movement_id)->first();

        $data=$request->only(['amount','price','price_bss','exchange_rate']);

        $data['amount']=str_replace('.', '', $data['amount']);
        $data['amount']=str_replace(',', '.', $data['amount']);

        $data['price']=str_replace('.', '', $data['price']);
        $data['price']=str_replace(',', '.', $data['price']);

        $data['price_bss']=str_replace('.', '', $data['price_bss']);
        $data['price_bss']=str_replace(',', '.', $data['price_bss']);

        $data['exchange_rate']=str_replace('.', '', $data['exchange_rate']);
        $data['exchange_rate']=str_replace(',', '.', $data['exchange_rate']);

        $raw_material=RawMaterial::where('id',$raw_material_movement->raw_material_id)->first();

        if(($raw_material->amount-$raw_material_movement->amount)+$data['amount'] < 0) {
            return redirect()->back()->withErrors('El stock no puede quedar en negativo.');
        }

        $raw_material_movement->update(['new_amount'=>$raw_material->amount-$raw_material_movement->amount]);
        $raw_material->update(['amount'=>$raw_material->amount-$raw_material_movement->amount]);

        $raw_material_movement->update(['amount'=>$data['amount'], 'price'=>$data['price'], 'price_bss'=>$data['price_bss'], 'exchange_rate'=>$data['exchange_rate']]);
        
        $raw_material=RawMaterial::where('id',$raw_material_movement->raw_material_id)->first();
        $raw_material->update(['amount'=>$raw_material->amount+$data['amount']]);

        $raw_material_movement=RawMaterialMovement::where('id',$raw_material_movement_id)->first();
        if($raw_material_movement->raw_material_income && count($raw_material_movement->raw_material_income->raw_material_income_movements)==0) {
            $raw_material_movement->raw_material_income->update(['amount'=>$data['amount']]);
        }

        $raw_material=RawMaterial::where('id',$raw_material_movement->raw_material_id)->first();
        $raw_material_movement->update(['new_amount'=>$raw_material->amount]);

        $current_user=User::getCurrent();
        $operation='Actualización de entrada al stock de la materia prima: '.$raw_material->name.' - '.number_format($data['amount'], 2, ',', '.').' '.$raw_material->unit;

        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_raw_material_movement', 'operation'=>$operation]);

        return redirect()->route('admin_inventory_show_raw_material',['raw_material_id'=>$raw_material->id])->with(['message_info'=>$operation]);
    }

    public function getTrash($raw_material_movement_id) 
    {
        $raw_material_movement=RawMaterialMovement::where('id',$raw_material_movement_id)->first();

        if(!$raw_material_movement) {
            return redirect()->back()->withErrors(['El movimiento con ID #'.$raw_material_movement_id.' no se encuentra registrado.']);
        }

        return view('admin.inventory.raw_material_movements.trash',['raw_material_movement'=>$raw_material_movement]);
    }

    public function delete(Request $request) 
    {
        $raw_material_movement_id=$request->raw_material_movement_id;
        $raw_material_movement=RawMaterialMovement::where('id',$raw_material_movement_id)->first();
        $message='Se ha eliminado exitosamente el movimiento de stock seleccionado.';
        
        $operation='Eliminación de la entrada al stock de la materia prima: '.$raw_material_movement->raw_material->name.' - '.number_format($raw_material_movement->amount, 2, ',', '.').' '.$raw_material_movement->raw_material->unit;

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_raw_material_movement', 'operation'=>$operation]);
        
        $raw_material_id=$raw_material_movement->raw_material->id;
        
        $sum_outputs=RawMaterialMovement::where('raw_material_id',$raw_material_id)->where('type',2)->sum('amount');

        /*if(($raw_material_movement->raw_material->amount-$raw_material_movement->amount) < $sum_outputs) {
            return redirect()->back()->withErrors('El stock no puede quedar en negativo.');
        }*/

        /*if($raw_material_movement->raw_material->amount < $raw_material_movement->amount) {
            return redirect()->back()->withErrors('El stock no puede quedar en negativo.');
        }*/

        if($raw_material_movement->raw_material->amount-$raw_material_movement->amount < 0) {
            return redirect()->back()->withErrors('El stock no puede quedar en negativo.');
        }

        if($raw_material_movement->type==1)
            $raw_material_movement->raw_material->update(['amount'=>$raw_material_movement->raw_material->amount-$raw_material_movement->amount]);
        else
            $raw_material_movement->raw_material->update(['amount'=>$raw_material_movement->raw_material->amount+$raw_material_movement->amount]);

        RawMaterialMovement::where('id',$raw_material_movement_id)->delete();

        return redirect()->route('admin_inventory_show_raw_material',['raw_material_id'=>$raw_material_id])->with(['message_info'=>$message]);
    }

    public function adjustInventory(Request $request)
    {
        $raw_material_id=$request->raw_material_id;
        $amount=str_replace('.', '', $request->amount);
        $amount=str_replace(',', '.', $amount);
        $raw_material=RawMaterial::find($raw_material_id);

        if($raw_material->amount < $amount) {
            return redirect()->back()->withErrors('El stock no puede quedar en negativo.');
        }

        $new_amount=$raw_material->amount-$amount;
        $raw_material->update(['amount'=>$new_amount]);
        
        $concept='Ajuste de inventario';
        
        // Raw material incomes
        $check_amount=$amount;
        while($check_amount>0) {

            $raw_material_income=RawMaterialIncome::orderBy('created_at','asc')->where('raw_material_id',$raw_material->id)->where('amount','>',0)->first();

            if($raw_material_income) {
                if($check_amount>=$raw_material_income->amount) {
                    $check_amount-=$raw_material_income->amount;
                    $raw_material_income->update(['amount'=>($raw_material_income->amount-$raw_material_income->amount)]);
                }else {
                    $raw_material_income->update(['amount'=>($raw_material_income->amount-$check_amount)]);
                    $check_amount-=$check_amount;
                }
            }

        }

        RawMaterialMovement::create(['raw_material_id'=>$raw_material->id, 'new_amount'=>$new_amount, 'amount'=>$amount, 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>2]);

        $current_user=User::getCurrent();
        $operation='Ajuste del stock de la materia prima: '.$raw_material->name.' - '.number_format($amount, 2, ',', '.').' '.$raw_material->unit;

        Tracking::create(['user_id'=>$current_user->id, 'category'=>'adjust_inventory_raw_material_movement', 'operation'=>$operation]);

        return redirect()->back()->with(['message_info'=>$operation]);
    }
}
