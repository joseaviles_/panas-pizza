<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Client;
use App\Models\Tracking;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\UpdateClientRequest;
use Illuminate\Support\Facades\View;

class ClientsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'clients');
    }

    public function getIndex()
    {
        return view('admin.clients.list');
    }

    public function getList()
    {
        $clients=Client::all();
        $clients_list=[];

        foreach($clients as $client) {

        	$phones=$client->first_phone;
        	if($client->second_phone)
        		$phones=$phones.'<br>'.$client->second_phone;

            $clients_list[]=[$client->client_number, $client->first_name, $client->last_name, $client->identity_card==null?'N/A':$client->identity_card, $client->name_business_phone, $phones, $client->created_at->format('d-m-Y h:i:s a'), '<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_clients_edit",["client_id"=>$client->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_clients_trash",["client_id"=>$client->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>'];

        }

        return response()->json(['data' => $clients_list]);
    }

    public function getCreate() 
    {
        return view('admin.clients.create');
    }

    public function create(CreateClientRequest $request) 
    {
        $data=$request->only(['client_number','first_name','last_name','identity_card','name_business_phone','first_phone','second_phone']);
        $client=Client::create($data);

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_client', 'operation'=>'Registro del cliente: '.$client->first_name.' '.$client->last_name]);

        return redirect()->route('admin_clients')->with(['message_info'=>'Cliente: '.$client->first_name.' '.$client->last_name.' registrado exitosamente.']);
    }

    public function getEdit($client_id) 
    {
        $client=Client::where('id',$client_id)->first();

        if(!$client) {
            return redirect()->route('admin_clients')->withErrors(['El cliente con ID #'.$client_id.' no se encuentra registrado.']);
        }

        return view('admin.clients.edit',['client'=>$client]);
    }

    public function update(UpdateClientRequest $request)
    {
        $client_id=$request->client_id;
        $data=$request->only(['client_number','first_name','last_name','identity_card','name_business_phone','first_phone','second_phone']);

        Client::where('id',$client_id)->update($data);
        $client=Client::where('id',$client_id)->first();

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_client', 'operation'=>'Actualización del cliente: '.$client->first_name.' '.$client->last_name]);

        return redirect()->route('admin_clients')->with(['message_info'=>'Cliente: '.$client->first_name.' '.$client->last_name.' actualizado exitosamente.']);
    }

    public function getTrash($client_id) 
    {
        $client=Client::where('id',$client_id)->first();

        if(!$client) {
            return redirect()->route('admin_clients')->withErrors(['El cliente con ID #'.$client_id.' no se encuentra registrado.']);
        }

        if(count($client->sales)>0) {
            return redirect()->route('admin_clients')->withErrors(['El cliente '.$client->first_name.' '.$client->last_name.' no puede ser eliminado, ya que tiene ventas asociadas.']);
        }

        return view('admin.clients.trash',['client'=>$client]);
    }

    public function delete(Request $request) 
    {
        $client_id=$request->client_id;
        $client=Client::where('id',$client_id)->first();
        $message='Se ha eliminado exitosamente el cliente: '.$client->first_name.' '.$client->last_name.'.';
        
        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_client', 'operation'=>'Eliminación del cliente: '.$client->first_name.' '.$client->last_name]);
        
        Client::where('id',$client_id)->delete();
        return redirect()->route('admin_clients')->with(['message_info'=>$message]);
    }
}
