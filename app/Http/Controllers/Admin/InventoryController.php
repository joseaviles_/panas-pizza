<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\RawMaterial;
use Illuminate\Support\Facades\View;

class InventoryController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'inventory');
    }

    public function getIndex()
    {
        return view('admin.inventory.list');
    }


    public function getListRawMaterial()
    {
        $raw_material=RawMaterial::all();
        $raw_material_list=[];

        foreach($raw_material as $rm) {
            $raw_material_list[]=[$rm->name, $rm->unit, number_format($rm->amount, 2, ',', '.'), '<a class="btn btn-sm btn-success mt-1" href="'.route("admin_inventory_show_raw_material",["raw_material_id"=>$rm->id]).'"><i class="fas fa-search"></i></a>'];
        }

        return response()->json(['data' => $raw_material_list]);
    }

    public function getListUnprocessedProducts()
    {
        $products=Product::where('type',1)->get();
        $products_list=[];

        foreach($products as $product) {

            $products_list[]=[$product->name, $product->category->name, number_format($product->amount, 0, ',', '.'), '<a class="btn btn-sm btn-success mt-1" href="'.route("admin_inventory_show_unprocessed_products",["product_id"=>$product->id]).'"><i class="fas fa-search"></i></a>'];

        }

        return response()->json(['data' => $products_list]);
    }

    public function showRawMaterial($raw_material_id) 
    {
        $raw_material=RawMaterial::where('id',$raw_material_id)->first();

        if(!$raw_material) {
            return redirect()->route('admin_inventory')->withErrors(['La materia prima con ID #'.$raw_material_id.' no se encuentra registrada.']);
        }

        return view('admin.inventory.show_raw_material',['raw_material'=>$raw_material]);
    }

    public function showUnprocessedProduct($product_id) 
    {
        $product=Product::where('id',$product_id)->first();

        if(!$product) {
            return redirect()->route('admin_inventory')->withErrors(['El producto con ID #'.$product_id.' no se encuentra registrado.']);
        }

        return view('admin.inventory.show_unprocessed_product',['product'=>$product]);
    }
}
