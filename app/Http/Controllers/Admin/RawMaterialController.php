<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\RawMaterial;
use App\Models\Tracking;
use App\Http\Requests\CreateRawMaterialRequest;
use App\Http\Requests\UpdateRawMaterialRequest;
use Illuminate\Support\Facades\View;

class RawMaterialController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'raw_material');
    }

    public function getIndex()
    {
        return view('admin.raw_material.list');
    }

    public function getList()
    {
        $raw_material=RawMaterial::all();
        $raw_material_list=[];

        foreach($raw_material as $rm) {

            $raw_material_list[]=[$rm->name, $rm->unit, $rm->description==null?'N/A':$rm->description, $rm->created_at->format('d-m-Y h:i:s a'), '<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_raw_material_edit",["raw_material_id"=>$rm->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_raw_material_trash",["raw_material_id"=>$rm->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>'];

        }

        return response()->json(['data' => $raw_material_list]);
    }

    public function getCreate() 
    {
        return view('admin.raw_material.create');
    }

    public function create(CreateRawMaterialRequest $request) 
    {
        $data=$request->only(['name','unit','description']);
        $raw_material=RawMaterial::create($data);

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_raw_material', 'operation'=>'Registro de la materia prima: '.$raw_material->name]);

        return redirect()->route('admin_raw_material')->with(['message_info'=>'Materia prima: '.$raw_material->name.' registrada exitosamente.']);
    }

    public function getEdit($raw_material_id) 
    {
        $raw_material=RawMaterial::where('id',$raw_material_id)->first();

        if(!$raw_material) {
            return redirect()->route('admin_raw_material')->withErrors(['La materia prima con ID #'.$raw_material_id.' no se encuentra registrada.']);
        }

        return view('admin.raw_material.edit',['raw_material'=>$raw_material]);
    }

    public function update(UpdateRawMaterialRequest $request)
    {
        $raw_material_id=$request->raw_material_id;
        $data=$request->only(['name','unit','description']);

        RawMaterial::where('id',$raw_material_id)->update($data);
        $raw_material=RawMaterial::where('id',$raw_material_id)->first();

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_raw_material', 'operation'=>'Actualización de la materia prima: '.$raw_material->name]);

        return redirect()->route('admin_raw_material')->with(['message_info'=>'Materia prima: '.$raw_material->name.' actualizada exitosamente.']);
    }

    public function getTrash($raw_material_id) 
    {
        $raw_material=RawMaterial::where('id',$raw_material_id)->first();

        if(!$raw_material) {
            return redirect()->route('admin_raw_material')->withErrors(['La materia prima con ID #'.$raw_material_id.' no se encuentra registrada.']);
        }

        if(count($raw_material->products)>0) {
            return redirect()->route('admin_raw_material')->withErrors(['La materia prima '.$raw_material->name.' no puede ser eliminada, ya que tiene productos asociados.']);
        }

        if(count($raw_material->raw_material_movements)>0) {
            return redirect()->route('admin_raw_material')->withErrors(['La materia prima '.$raw_material->name.' no puede ser eliminada, ya que tiene movimientos de stock asociados.']);
        }

        if(count($raw_material->categories)>0) {
            return redirect()->route('admin_raw_material')->withErrors(['La materia prima '.$raw_material->name.' no puede ser eliminada, ya que está asociada a bases para los futuros productos de categorías.']);
        }

        return view('admin.raw_material.trash',['raw_material'=>$raw_material]);
    }

    public function delete(Request $request) 
    {
        $raw_material_id=$request->raw_material_id;
        $raw_material=RawMaterial::where('id',$raw_material_id)->first();
        $message='Se ha eliminado exitosamente la materia prima: '.$raw_material->name.'.';
        
        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_raw_material', 'operation'=>'Eliminación de la materia prima: '.$raw_material->name]);
        
        RawMaterial::where('id',$raw_material_id)->delete();
        return redirect()->route('admin_raw_material')->with(['message_info'=>$message]);
    }
}
