<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Category;
use App\Models\RawMaterial;
use App\Models\Tracking;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Support\Facades\View;

class CategoriesController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'categories');
    }

    public function getIndex()
    {
        return view('admin.categories.list');
    }

    public function getList()
    {
        $categories=Category::all();
        $categories_list=[];

        foreach($categories as $category) {

            $categories_list[]=[$category->name, $category->created_at->format('d-m-Y h:i:s a'), '<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_categories_edit",["category_id"=>$category->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_categories_trash",["category_id"=>$category->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>'];

        }

        return response()->json(['data' => $categories_list]);
    }

    public function getCreate() 
    {
        $raw_material=RawMaterial::orderBy('name','asc')->get();

        return view('admin.categories.create', ['raw_material'=>$raw_material]);
    }

    public function create(CreateCategoryRequest $request) 
    {
        $data=$request->only(['name']);

        $raw_material_id=$request->raw_material_id;
        $amounts=$request->amounts;
        
        if(isset($raw_material_id) && isset($amounts)) {
            $rm_array=[];
            foreach($raw_material_id as $key => $rm_id) {
                $raw_material=RawMaterial::where('id',$rm_id)->first();
                $amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                if($amount==0){
                    return redirect()->back()->withInput()->withErrors(['Ingresaste 0,00 '.$raw_material->unit.' de cantidad en la materia prima: '.$raw_material->name.'.']);
                }

                if(!isset($rm_array[$rm_id])){
                    $rm_array[$rm_id]=$amounts[$key];
                }else {
                    return redirect()->back()->withInput()->withErrors(['Seleccionaste dos o más veces la materia prima: '.$raw_material->name.'.']);
                }
            }
        }

        $category=Category::create($data);

        if(isset($raw_material_id) && isset($amounts)) {
            foreach($raw_material_id as $key => $rm_id) {
                $amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);
                $category->raw_material()->attach($rm_id,['amount'=>$amount]);
            }
        }

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_category', 'operation'=>'Registro de la categoría: '.$category->name]);

        return redirect()->route('admin_categories')->with(['message_info'=>'Categoría: '.$category->name.' registrada exitosamente.']);
    }

    public function getEdit($category_id) 
    {
        $category=Category::where('id',$category_id)->first();

        if(!$category) {
            return redirect()->route('admin_categories')->withErrors(['La categoría con ID #'.$category_id.' no se encuentra registrada.']);
        }

        $raw_material=RawMaterial::orderBy('name','asc')->get();

        return view('admin.categories.edit',['category'=>$category, 'raw_material'=>$raw_material]);
    }

    public function update(UpdateCategoryRequest $request)
    {
        $category_id=$request->category_id;
        $data=$request->only(['name']);

        $raw_material_id=$request->raw_material_id;
        $amounts=$request->amounts;
        
        if(isset($raw_material_id) && isset($amounts)) {
            $rm_array=[];
            foreach($raw_material_id as $key => $rm_id) {
                $raw_material=RawMaterial::where('id',$rm_id)->first();
                $amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                if($amount==0){
                    return redirect()->back()->withInput()->withErrors(['Ingresaste 0,00 '.$raw_material->unit.' de cantidad en la materia prima: '.$raw_material->name.'.']);
                }

                if(!isset($rm_array[$rm_id])){
                    $rm_array[$rm_id]=$amounts[$key];
                }else {
                    return redirect()->back()->withInput()->withErrors(['Seleccionaste dos o más veces la materia prima: '.$raw_material->name.'.']);
                }
            }
        }

        Category::where('id',$category_id)->update($data);
        $category=Category::where('id',$category_id)->first();

        // Delete of products
        $raw_material=$category->raw_material->sortByDesc('pivot.amount');
        $products=$category->products;
        foreach($raw_material as $rm) {
            foreach($products as $product) {
                if($product->raw_material()->wherePivot('raw_material_id',$rm->id)->first()) {
                    $product->raw_material()->detach($rm->id);
                }
            }
            $category->raw_material()->detach($rm->id);
        }

        if(isset($raw_material_id) && isset($amounts)) {
            foreach($raw_material_id as $key => $rm_id) {
                $amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);
                $category->raw_material()->attach($rm_id,['amount'=>$amount]);
            }
        }

        // Update products
        $category=Category::where('id',$category_id)->first();
        $raw_material=$category->raw_material->sortByDesc('pivot.amount');
        $products=$category->products;
        foreach($raw_material as $rm) {
            foreach($products as $product) {
                if($product->raw_material()->wherePivot('raw_material_id',$rm->id)->first()) {
                    $product->raw_material()->updateExistingPivot($rm->id, ['amount'=>$rm->pivot->amount]);
                }else {
                    $product->raw_material()->attach($rm->id, ['amount'=>$rm->pivot->amount]);
                }
            }
        }

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_category', 'operation'=>'Actualización de la categoría: '.$category->name]);

        return redirect()->route('admin_categories')->with(['message_info'=>'Categoría: '.$category->name.' actualizada exitosamente.']);
    }

    public function getTrash($category_id) 
    {
        $category=Category::where('id',$category_id)->first();

        if(!$category) {
            return redirect()->route('admin_categories')->withErrors(['La categoría con ID #'.$category_id.' no se encuentra registrada.']);
        }

        if(count($category->products)>0) {
            return redirect()->route('admin_categories')->withErrors(['La categoría '.$category->name.' no puede ser eliminada, ya que tiene productos asociados.']);
        }

        if(count($category->raw_material)>0) {
            return redirect()->route('admin_categories')->withErrors(['La categoría '.$category->name.' no puede ser eliminada, ya que posee una base para los futuros productos.']);
        }

        return view('admin.categories.trash',['category'=>$category]);
    }

    public function delete(Request $request) 
    {
        $category_id=$request->category_id;
        $category=Category::where('id',$category_id)->first();
        $message='Se ha eliminado exitosamente la categoría: '.$category->name.'.';
        
        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_category', 'operation'=>'Eliminación de la categoría: '.$category->name]);
        
        Category::where('id',$category_id)->delete();
        return redirect()->route('admin_categories')->with(['message_info'=>$message]);
    }

    public function getRawMaterial($category_id)
    {
        $category=Category::where('id',$category_id)->first();
        $raw_material=$category->raw_material->sortByDesc('pivot.amount');
        $array=[];
        foreach($raw_material as $rm) {
            $array[]=$rm;
        }
        return response()->json(['raw_material'=>$array]);
    }
}
