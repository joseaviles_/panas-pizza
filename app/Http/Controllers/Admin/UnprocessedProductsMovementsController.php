<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Product;
use App\Models\UnprocessedProductsMovement;
use App\Models\Tracking;
use App\Http\Requests\CreateUnprocessedProductMovementRequest;
use App\Http\Requests\UpdateUnprocessedProductMovementRequest;
use Illuminate\Support\Facades\View;

class UnprocessedProductsMovementsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'inventory');
    }

    public function getList($product_id)
    {
        $unprocessed_products_movements=UnprocessedProductsMovement::where('product_id',$product_id)->get();
        $unprocessed_products_movements_list=[];

        foreach($unprocessed_products_movements as $unprocessed_product_movement) {

            $actions=null;

            if($unprocessed_product_movement->type==1 && $unprocessed_product_movement->is_deleted==0) {
                $actions='<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_unprocessed_products_movements_edit",["unprocessed_product_movement_id"=>$unprocessed_product_movement->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_unprocessed_products_movements_trash",["unprocessed_product_movement_id"=>$unprocessed_product_movement->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            }else {
                $actions='N/A';
            }

            $unprocessed_products_movements_list[]=[$unprocessed_product_movement->id, $unprocessed_product_movement->created_at->format('d-m-Y h:i:s a'), $unprocessed_product_movement->type==1?'Entrada':'Salida', number_format($unprocessed_product_movement->new_amount, 0, ',', '.'), number_format($unprocessed_product_movement->amount, 0, ',', '.'), $unprocessed_product_movement->price==0?'N/A':number_format($unprocessed_product_movement->price, 2, ',', '.'), $unprocessed_product_movement->price_bss==0?'N/A':number_format($unprocessed_product_movement->price_bss, 2, ',', '.'), $unprocessed_product_movement->price==0?'N/A':number_format($unprocessed_product_movement->exchange_rate, 2, ',', '.'), $unprocessed_product_movement->concept, $actions];

        }

        return response()->json(['data' => $unprocessed_products_movements_list]);
    }

    public function create(CreateUnprocessedProductMovementRequest $request) 
    {
        $data=$request->only(['product_id','amount','price','price_bss','exchange_rate']);

        $data['amount']=str_replace('.', '', $data['amount']);
        $data['amount']=str_replace(',', '.', $data['amount']);

        if($data['amount']<=0)
            return redirect()->back()->withInput()->withErrors('La cantidad a ingresar debe ser mayor a cero (0).');

        $data['price']=str_replace('.', '', $data['price']);
        $data['price']=str_replace(',', '.', $data['price']);

        $data['price_bss']=str_replace('.', '', $data['price_bss']);
        $data['price_bss']=str_replace(',', '.', $data['price_bss']);

        $data['exchange_rate']=str_replace('.', '', $data['exchange_rate']);
        $data['exchange_rate']=str_replace(',', '.', $data['exchange_rate']);

        $product=Product::where('id',$data['product_id'])->first();
        $new_amount=$product->amount+$data['amount'];

        $data['concept']='Compra del producto sin elaboración: '.$product->name;
        $data['type']=1;
        $data['new_amount']=$new_amount;

        $unprocessed_product_movement=UnprocessedProductsMovement::create($data);
     	
     	$product->update(['amount'=>$new_amount]);

        $current_user=User::getCurrent();
        $operation='Entrada al stock del producto sin elaboración: '.$product->name.' - Cantidad: '.$data['amount'];

        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_unprocessed_product_movement', 'operation'=>$operation]);

        return redirect()->back()->with(['message_info'=>$operation]);
    }

    public function getEdit($unprocessed_product_movement_id) 
    {
        $unprocessed_product_movement=UnprocessedProductsMovement::where('id',$unprocessed_product_movement_id)->first();

        if(!$unprocessed_product_movement) {
            return redirect()->back()->withErrors(['El movimiento con ID #'.$unprocessed_product_movement_id.' no se encuentra registrado.']);
        }

        return view('admin.inventory.unprocessed_products_movements.edit',['unprocessed_product_movement'=>$unprocessed_product_movement]);
    }

    public function update(UpdateUnprocessedProductMovementRequest $request)
    {
    	$unprocessed_product_movement_id=$request->unprocessed_product_movement_id;
    	$unprocessed_product_movement=UnprocessedProductsMovement::where('id',$unprocessed_product_movement_id)->first();

        $data=$request->only(['amount','price','price_bss','exchange_rate']);

        $data['amount']=str_replace('.', '', $data['amount']);
        $data['amount']=str_replace(',', '.', $data['amount']);

        $data['price']=str_replace('.', '', $data['price']);
        $data['price']=str_replace(',', '.', $data['price']);

        $data['price_bss']=str_replace('.', '', $data['price_bss']);
        $data['price_bss']=str_replace(',', '.', $data['price_bss']);

        $data['exchange_rate']=str_replace('.', '', $data['exchange_rate']);
        $data['exchange_rate']=str_replace(',', '.', $data['exchange_rate']);

        $product=Product::where('id',$unprocessed_product_movement->product_id)->first();

        if(($product->amount-$unprocessed_product_movement->amount)+$data['amount'] < 0) {
            return redirect()->back()->withErrors('El stock no puede quedar en negativo.');
        }

        $unprocessed_product_movement->update(['new_amount'=>$product->amount-$unprocessed_product_movement->amount]);
        $product->update(['amount'=>$product->amount-$unprocessed_product_movement->amount]);

        $unprocessed_product_movement->update(['amount'=>$data['amount'], 'price'=>$data['price'], 'price_bss'=>$data['price_bss'], 'exchange_rate'=>$data['exchange_rate']]);
        
        $product=Product::where('id',$unprocessed_product_movement->product_id)->first();
        $product->update(['amount'=>$product->amount+$data['amount']]);

        $product=Product::where('id',$unprocessed_product_movement->product_id)->first();
        $unprocessed_product_movement->update(['new_amount'=>$product->amount]);

        $current_user=User::getCurrent();
        $operation='Actualización de entrada al stock del producto sin elaboración: '.$product->name.' - Cantidad: '.$data['amount'];

        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_unprocessed_product_movement', 'operation'=>$operation]);

        return redirect()->route('admin_inventory_show_unprocessed_products',['product_id'=>$product->id])->with(['message_info'=>$operation]);
    }

    public function getTrash($unprocessed_product_movement_id) 
    {
        $unprocessed_product_movement=UnprocessedProductsMovement::where('id',$unprocessed_product_movement_id)->first();

        if(!$unprocessed_product_movement) {
            return redirect()->back()->withErrors(['El movimiento con ID #'.$unprocessed_product_movement_id.' no se encuentra registrado.']);
        }

        return view('admin.inventory.unprocessed_products_movements.trash',['unprocessed_product_movement'=>$unprocessed_product_movement]);
    }

    public function delete(Request $request) 
    {
        $unprocessed_product_movement_id=$request->unprocessed_product_movement_id;
        $unprocessed_product_movement=UnprocessedProductsMovement::where('id',$unprocessed_product_movement_id)->first();
        $message='Se ha eliminado exitosamente el movimiento de stock seleccionado.';
        
        $operation='Eliminación de la entrada al stock del producto sin elaboración: '.$unprocessed_product_movement->product->name.' - Cantidad: '.$unprocessed_product_movement->amount;

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_unprocessed_product_movement', 'operation'=>$operation]);
        
        $product_id=$unprocessed_product_movement->product->id;

        $sum_outputs=UnprocessedProductsMovement::where('product_id',$product_id)->where('type',2)->sum('amount');

        /*if(($unprocessed_product_movement->product->amount-$unprocessed_product_movement->amount) < $sum_outputs) {
            return redirect()->back()->withErrors('El stock no puede quedar en negativo.');
        }*/

        if($unprocessed_product_movement->product->amount-$unprocessed_product_movement->amount < 0) {
            return redirect()->back()->withErrors('El stock no puede quedar en negativo.');
        }

        if($unprocessed_product_movement->type==1)
            $unprocessed_product_movement->product->update(['amount'=>$unprocessed_product_movement->product->amount-$unprocessed_product_movement->amount]);
        else
            $unprocessed_product_movement->product->update(['amount'=>$unprocessed_product_movement->product->amount+$unprocessed_product_movement->amount]);

        UnprocessedProductsMovement::where('id',$unprocessed_product_movement_id)->delete();

        return redirect()->route('admin_inventory_show_unprocessed_products',['product_id'=>$product_id])->with(['message_info'=>$message]);
    }

    public function adjustInventory(Request $request)
    {
        $product_id=$request->product_id;
        $amount=str_replace('.', '', $request->amount);
        $amount=str_replace(',', '.', $amount);
        $product=Product::find($product_id);

        if($product->amount < $amount) {
            return redirect()->back()->withErrors('El stock no puede quedar en negativo.');
        }

        $new_amount=$product->amount-$amount;
        $product->update(['amount'=>$new_amount]);

        $concept='Ajuste de inventario';

        UnprocessedProductsMovement::create(['product_id'=>$product->id, 'new_amount'=>$new_amount, 'amount'=>$amount, 'price'=>0, 'price_bss'=>0, 'exchange_rate'=>0, 'concept'=>$concept, 'type'=>2]);

        $current_user=User::getCurrent();
        $operation='Ajuste del stock del producto sin elaboración: '.$product->name.' - Cantidad: '.number_format($amount, 0, ',', '.');

        Tracking::create(['user_id'=>$current_user->id, 'category'=>'adjust_inventory_unprocessed_product_movement', 'operation'=>$operation]);

        return redirect()->back()->with(['message_info'=>$operation]);
    }
}
