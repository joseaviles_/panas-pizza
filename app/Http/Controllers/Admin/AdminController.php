<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Configuration;
use App\Models\Product;
use App\Models\Tracking;
use App\Http\Requests\UpdateConfigurationRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'dashboard');
    }

    public function getAdminDashboard()
    {
		return view('main.admin_dashboard');
    }

    public function getElaborableProducts()
    {
        $user=User::getCurrent();
        $products=Product::where('type',2)->get();
        $products_list=[];

        foreach($products as $product) {

            $raw_material=$product->raw_material->sortByDesc('pivot.amount');
            $min=0;
            $count=0;

            $all_raw_material='';

            foreach($raw_material as $rm) {

                $available_products=$rm->amount/$rm->pivot->amount;

                if($count==0 || $available_products < $min) {
                    $min=$available_products;
                    $count++;
                }

                $color='';
                if($rm->amount < $rm->pivot->amount) {
                    $color='color: red !important;';
                }

                if($user->hasRole('admin')) {
                    $all_raw_material.='<a style="'.$color.'" href="'.route("admin_inventory_show_raw_material",["raw_material_id"=>$rm->id]).'"><b><i class="fa fa-search" aria-hidden="true"></i> '.$rm->name.'</b></a>: '.number_format($rm->pivot->amount, 2, ',', '.').' '.$rm->unit.'<br>';
                }elseif($user->hasRole('seller')) {
                    $all_raw_material.='<a style="'.$color.'" href="#"><b>'.$rm->name.'</b></a>: '.number_format($rm->pivot->amount, 2, ',', '.').' '.$rm->unit.'<br>';
                }
                
            }

            $product->available=floor($min);

            $configuration=Configuration::first();
            $price_bss=number_format($product->price, 2, ',', '.');
            if($configuration)
                $price_bss=number_format($product->price*$configuration->exchange_rate, 2, ',', '.');

            if($user->hasRole('admin')) {
                $products_list[]=[$product->name, $product->available, $product->category->name, $all_raw_material, number_format($product->price, 2, ',', '.'), $price_bss, '<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_products_edit",["product_id"=>$product->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_products_trash",["product_id"=>$product->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>'];
            }elseif($user->hasRole('seller')) {
                $products_list[]=[$product->name, $product->available, $product->category->name, $all_raw_material, number_format($product->price, 2, ',', '.'), $price_bss];
            }
            
        }

        return response()->json(['data' => $products_list]);
    }

    public function exchangeRate(Request $request)
    {
    	$exchange_rate=$request->exchange_rate;
        $exchange_rate=str_replace('.', '', $exchange_rate);
        $exchange_rate=str_replace(',', '.', $exchange_rate);

		$configuration=Configuration::first();

        if(!$configuration) {
       		$configuration=Configuration::create(['exchange_rate'=>$exchange_rate]);
        }else {
        	Configuration::where('id',$configuration->id)->update(['exchange_rate'=>$exchange_rate]);
        	$configuration=Configuration::first();
        }

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_exchange_rate', 'operation'=>'Actualización de la tasa del día a: '.number_format($configuration->exchange_rate, 2, ',', '.').' Bs.S']);

        return redirect()->back()->with(['message_info'=>'Tasa del día actualizada exitosamente.']);
    }

    public function getConfiguration() {
        return view('configuration.personalization',['menu_active'=>'configuration']);
    }

    public function saveConfiguration(UpdateConfigurationRequest $request) {
        $name=$request->name;
        $unlock_code=$request->unlock_code;
        $configuration=Configuration::first();
        $configuration->update(['name'=>$name, 'unlock_code'=>$unlock_code]);

        if(isset($request->logo)) {
            
            if($configuration->real_name_logo)
                Storage::disk('uploads')->delete($configuration->real_name_logo);

            $extension=new \SplFileInfo($_FILES['logo']['name']);
            $extension=$extension->getExtension();
            $rand=rand();
            $request->logo->storeAs('images/', 'logo_'.$configuration->id.'.'.$extension, 'uploads');
            $logo='images/logo_'.$configuration->id.'.'.$extension.'?v='.$rand;
            $real_name_logo='images/logo_'.$configuration->id.'.'.$extension;
            $configuration->update(['logo'=>$logo, 'real_name_logo'=>$real_name_logo]);
        }

        $user=User::getCurrent();
        Tracking::create(['user_id'=>$user->id, 'category'=>'save_configuration', 'operation'=>'Actualización de configuración']);

        return redirect()->route('configuration')->with(['message_info'=>'Configuración actualizada exitosamente.']);
    }
}
