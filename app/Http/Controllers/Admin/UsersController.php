<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Tracking;
use Spatie\Permission\Models\Role;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;

class UsersController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'users');
    }

    public function getIndex()
    {
        return view('admin.users.list');
    }

    public function getList()
    {
        $users=User::all();
        $users_list=[];
        $current_user=User::getCurrent();

        foreach($users as $user) {

            if($current_user->id!=$user->id) {
                $role='none';

                if($user->hasRole('admin'))
                    $role='Administrador';
                elseif($user->hasRole('seller'))
                	$role='Vendedor';

                $users_list[]=[$user->first_name, $user->last_name, $user->identity_card, $user->email, $role, $user->created_at->format('d-m-Y h:i:s a'), '<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_users_edit",["user_id"=>$user->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_users_trash",["user_id"=>$user->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>']; 
            }

        }

        return response()->json(['data' => $users_list]);
    }

    public function getCreate() 
    {
        $roles=Role::orderBy('name','asc')->get();
        return view('admin.users.create',['roles'=>$roles]);
    }

    public function create(CreateUserRequest $request) 
    {
        $data=$request->only(['first_name','last_name','identity_card','email','password']);
        $data['password']=Hash::make($data['password']);
        $role=$request->role;
        $user=User::create($data);
        $user->assignRole($role);

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_user', 'operation'=>'Registro del usuario: '.$user->email]);

        return redirect()->route('admin_users')->with(['message_info'=>'Usuario: '.$user->email.' registrado exitosamente.']);
    }

    public function getEdit($user_id) 
    {
        $edit_user=User::where('id',$user_id)->first();

        if(!$edit_user) {
            return redirect()->route('admin_users')->withErrors(['El usuario con ID #'.$user_id.' no se encuentra registrado.']);
        }

        $roles=Role::orderBy('name','asc')->get();

        return view('admin.users.edit',['edit_user'=>$edit_user, 'roles'=>$roles]);
    }

    public function update(UpdateUserRequest $request)
    {
        $user_id=$request->user_id;
        $data=$request->only(['first_name','last_name','identity_card','email']);
        $role=$request->role;

        $password=$request->password;
        $password_confirmation=$request->password_confirmation;

        if(isset($password)) {

            if(strlen($password)<8) {
                return redirect()->back()->withErrors(['El campo contraseña debe contener al menos 8 caracteres.']);
            }elseif(!isset($password_confirmation)) {
                return redirect()->back()->withErrors(['El campo confirmación de contraseña es obligatorio si deseas cambiar la contraseña.']);
            }elseif(isset($password_confirmation) && $password_confirmation!=$password) {
                return redirect()->back()->withErrors(['El campo confirmación de contraseña no coincide.']);
            }

            $data['password']=Hash::make($password);
        }

        User::where('id',$user_id)->update($data);
        $user=User::where('id',$user_id)->first();

        $roles=Role::orderBy('name','asc')->get();
        foreach($roles as $remove_role) {
            $user->removeRole($remove_role->name);
        }
        
        $user->assignRole($role);

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_user', 'operation'=>'Actualización del usuario: '.$user->email]);

        return redirect()->route('admin_users')->with(['message_info'=>'Usuario: '.$user->email.' actualizado exitosamente.']);
    }

    public function getTrash($user_id) 
    {
        $trash_user=User::where('id',$user_id)->first();

        if(!$trash_user) {
            return redirect()->route('admin_users')->withErrors(['El usuario con ID #'.$user_id.' no se encuentra registrado.']);
        }

        if(count($trash_user->sales)>0) {
            return redirect()->route('admin_users')->withErrors(['El usuario '.$trash_user->first_name.' '.$trash_user->last_name.' no puede ser eliminado, ya que tiene ventas asociadas.']);
        }

        return view('admin.users.trash',['trash_user'=>$trash_user]);
    }

    public function delete(Request $request) 
    {
        $user_id=$request->user_id;
        $user=User::where('id',$user_id)->first();
        $message='Se ha eliminado exitosamente el usuario: '.$user->email.'.';
        
        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_user', 'operation'=>'Eliminación del usuario: '.$user->email]);
        
        User::where('id',$user_id)->delete();
        return redirect()->route('admin_users')->with(['message_info'=>$message]);
    }
}
