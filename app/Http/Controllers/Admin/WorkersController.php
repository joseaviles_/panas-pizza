<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Tracking;
use App\Models\Worker;
use App\Http\Requests\CreateWorkerRequest;
use App\Http\Requests\UpdateWorkerRequest;
use Illuminate\Support\Facades\View;

class WorkersController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'workers');
    }

    public function getIndex()
    {
        return view('admin.workers.list');
    }

    public function getList()
    {
        $workers=Worker::all();
        $workers_list=[];

        foreach($workers as $worker) {

            $workers_list[]=[$worker->first_name, $worker->last_name, $worker->identity_card, \DateTime::createFromFormat('Y-m-d',$worker->birthdate)->format('d-m-Y'), $worker->job_title, $worker->created_at->format('d-m-Y h:i:s a'), '<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_workers_edit",["worker_id"=>$worker->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_workers_trash",["worker_id"=>$worker->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>'];

        }

        return response()->json(['data' => $workers_list]);
    }

    public function getCreate() 
    {
        return view('admin.workers.create');
    }

    public function create(CreateWorkerRequest $request) 
    {
        $data=$request->only(['first_name','last_name','identity_card','birthdate','job_title']);
        $worker=Worker::create($data);

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_worker', 'operation'=>'Registro del trabajador: '.$worker->first_name.' '.$worker->last_name]);

        return redirect()->route('admin_workers')->with(['message_info'=>'Trabajador: '.$worker->first_name.' '.$worker->last_name.' registrado exitosamente.']);
    }

    public function getEdit($worker_id) 
    {
        $worker=Worker::where('id',$worker_id)->first();

        if(!$worker) {
            return redirect()->route('admin_workers')->withErrors(['El trabajador con ID #'.$worker_id.' no se encuentra registrado.']);
        }

        return view('admin.workers.edit',['worker'=>$worker]);
    }

    public function update(UpdateWorkerRequest $request)
    {
        $worker_id=$request->worker_id;
        $data=$request->only(['first_name','last_name','identity_card','birthdate','job_title']);

        Worker::where('id',$worker_id)->update($data);
        $worker=Worker::where('id',$worker_id)->first();

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_worker', 'operation'=>'Actualización del trabajador: '.$worker->first_name.' '.$worker->last_name]);

        return redirect()->route('admin_workers')->with(['message_info'=>'Trabajador: '.$worker->first_name.' '.$worker->last_name.' actualizado exitosamente.']);
    }

    public function getTrash($worker_id) 
    {
        $worker=Worker::where('id',$worker_id)->first();

        if(!$worker) {
            return redirect()->route('admin_workers')->withErrors(['El trabajador con ID #'.$worker_id.' no se encuentra registrado.']);
        }

        return view('admin.workers.trash',['worker'=>$worker]);
    }

    public function delete(Request $request) 
    {
        $worker_id=$request->worker_id;
        $worker=Worker::where('id',$worker_id)->first();
        $message='Se ha eliminado exitosamente el trabajador: '.$worker->first_name.' '.$worker->last_name.'.';
        
        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_worker', 'operation'=>'Eliminación del trabajador: '.$worker->first_name.' '.$worker->last_name]);
        
        Worker::where('id',$worker_id)->delete();
        return redirect()->route('admin_workers')->with(['message_info'=>$message]);
    }
}
