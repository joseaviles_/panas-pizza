<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\DeliveryCompany;
use App\Models\DeliveryCompanyDriver;
use App\Models\DeliveryType;
use App\Models\Tracking;
use App\Http\Requests\CreateDeliveryCompanyRequest;
use App\Http\Requests\UpdateDeliveryCompanyRequest;
use Illuminate\Support\Facades\View;

class DeliveryCompaniesController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'delivery_companies');
    }

    public function getIndex()
    {
        return view('admin.delivery_companies.list');
    }

    public function getList()
    {
        $delivery_companies=DeliveryCompany::all();
        $delivery_companies_list=[];

        foreach($delivery_companies as $delivery_company) {

            $types=null;
            foreach($delivery_company->delivery_types->sortBy('price') as $delivery_type) {
                $types.=$delivery_type->name.': '.number_format($delivery_type->price, 2, ',', '.').'$<br>';
            }

            $drivers=null;
            foreach($delivery_company->delivery_company_drivers->sortBy('name') as $delivery_company_driver) {
                $drivers.=$delivery_company_driver->name.'<br>';
            }

            $delivery_companies_list[]=[$delivery_company->name, $types==null?'N/A':$types, $drivers==null?'N/A':$drivers, $delivery_company->created_at->format('d-m-Y h:i:s a'), '<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_delivery_companies_edit",["delivery_company_id"=>$delivery_company->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_delivery_companies_trash",["delivery_company_id"=>$delivery_company->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>'];

        }

        return response()->json(['data' => $delivery_companies_list]);
    }

    public function getCreate() 
    {
        return view('admin.delivery_companies.create');
    }

    public function create(CreateDeliveryCompanyRequest $request) 
    {
        $data=$request->only(['name']);

        $types_delivery=$request->types_delivery;
        $prices_delivery=$request->prices_delivery;
        if(!isset($types_delivery) || !isset($prices_delivery)) {
            return redirect()->back()->withInput()->withErrors(['Debes agregar como mínimo un tipo de delivery.']);
        }

        $delivery_company=DeliveryCompany::create($data);

        $names=$request->names;

        if(isset($names)) {
            foreach($names as $key => $name) {
                DeliveryCompanyDriver::create(['name'=>$name, 'delivery_company_id'=>$delivery_company->id]);
            }
        }

        if(isset($types_delivery) && isset($prices_delivery)) {
            foreach($types_delivery as $key => $type_delivery) {
                $price=str_replace('.', '', $prices_delivery[$key]);
                $price=str_replace(',', '.', $price);
                DeliveryType::create(['name'=>$type_delivery, 'price'=>$price, 'delivery_company_id'=>$delivery_company->id]);
            }
        }

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_delivery_company', 'operation'=>'Registro de la empresa de delivery: '.$delivery_company->name]);

        return redirect()->route('admin_delivery_companies')->with(['message_info'=>'Empresa de delivery: '.$delivery_company->name.' registrada exitosamente.']);
    }

    public function getEdit($delivery_company_id) 
    {
        $delivery_company=DeliveryCompany::where('id',$delivery_company_id)->first();

        if(!$delivery_company) {
            return redirect()->route('admin_delivery_companies')->withErrors(['La empresa de delivery con ID #'.$delivery_company_id.' no se encuentra registrada.']);
        }

        return view('admin.delivery_companies.edit',['delivery_company'=>$delivery_company]);
    }

    public function update(UpdateDeliveryCompanyRequest $request)
    {
        $delivery_company_id=$request->delivery_company_id;
        $data=$request->only(['name']);

        $types_delivery=$request->types_delivery;
        $prices_delivery=$request->prices_delivery;
        $delivery_types_id=$request->delivery_types_id;
        $edit_types_delivery=$request->edit_types_delivery;
        $edit_prices_delivery=$request->edit_prices_delivery;

        if(!isset($types_delivery) || !isset($prices_delivery)) {
            
            if(!isset($delivery_types_id) || !isset($edit_types_delivery) || !isset($edit_prices_delivery)) {
                return redirect()->back()->withInput()->withErrors(['Debes agregar como mínimo un tipo de delivery.']);
            }

        }

        DeliveryCompany::where('id',$delivery_company_id)->update($data);
        $delivery_company=DeliveryCompany::where('id',$delivery_company_id)->first();

        // Delivery company drivers
        $drivers=$delivery_company->delivery_company_drivers;
        foreach($drivers as $driver) {
            if(count($driver->delivery_sales)==0) {
                $driver->delete();
            }
        }

        $names=$request->names;

        if(isset($names)) {
            foreach($names as $key => $name) {
                DeliveryCompanyDriver::create(['name'=>$name, 'delivery_company_id'=>$delivery_company->id]);
            }
        }

        // Delivery types
        $delivery_types=$delivery_company->delivery_types;
        foreach($delivery_types as $delivery_type) {
            if(count($delivery_type->delivery_sales)==0) {
                $delivery_type->delete();
            }
        }

        if(isset($types_delivery) && isset($prices_delivery)) {
            foreach($types_delivery as $key => $type_delivery) {
                $price=str_replace('.', '', $prices_delivery[$key]);
                $price=str_replace(',', '.', $price);
                DeliveryType::create(['name'=>$type_delivery, 'price'=>$price, 'delivery_company_id'=>$delivery_company->id]);
            }
        }

        $delivery_types_id=$request->delivery_types_id;
        $edit_types_delivery=$request->edit_types_delivery;
        $edit_prices_delivery=$request->edit_prices_delivery;

        if(isset($delivery_types_id) && isset($edit_types_delivery) && isset($edit_prices_delivery)) {
            foreach($delivery_types_id as $key => $delivery_type_id) {
                
                $delivery_type=DeliveryType::find($delivery_type_id);
                $price=str_replace('.', '', $edit_prices_delivery[$key]);
                $price=str_replace(',', '.', $price);

                if($delivery_type) {
                    DeliveryType::where('id',$delivery_type_id)->update(['name'=>$edit_types_delivery[$key], 'price'=>$price]);
                }else {
                    DeliveryType::create(['name'=>$edit_types_delivery[$key], 'price'=>$price, 'delivery_company_id'=>$delivery_company->id]);
                }

            }
        }

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_delivery_company', 'operation'=>'Actualización de la empresa de delivery: '.$delivery_company->name]);

        return redirect()->route('admin_delivery_companies')->with(['message_info'=>'Empresa de delivery: '.$delivery_company->name.' actualizada exitosamente.']);
    }

    public function getTrash($delivery_company_id) 
    {
        $delivery_company=DeliveryCompany::where('id',$delivery_company_id)->first();

        if(!$delivery_company) {
            return redirect()->route('admin_delivery_companies')->withErrors(['La empresa de delivery con ID #'.$delivery_company_id.' no se encuentra registrada.']);
        }

        if(count($delivery_company->delivery_sales)>0) {
            return redirect()->route('admin_delivery_companies')->withErrors(['La empresa de delivery '.$delivery_company->name.' no puede ser eliminada, ya que tiene ventas asociadas.']);
        }

        return view('admin.delivery_companies.trash',['delivery_company'=>$delivery_company]);
    }

    public function delete(Request $request) 
    {
        $delivery_company_id=$request->delivery_company_id;
        $delivery_company=DeliveryCompany::where('id',$delivery_company_id)->first();
        $message='Se ha eliminado exitosamente la empresa de delivery: '.$delivery_company->name.'.';
        
        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_delivery_company', 'operation'=>'Eliminación de la empresa de delivery: '.$delivery_company->name]);
        
        DeliveryCompany::where('id',$delivery_company_id)->delete();
        return redirect()->route('admin_delivery_companies')->with(['message_info'=>$message]);
    }
}
