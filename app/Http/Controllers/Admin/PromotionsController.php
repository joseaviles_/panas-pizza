<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Configuration;
use App\Models\Product;
use App\Models\Promotion;
use App\Models\Tracking;
use App\Http\Requests\CreatePromotionRequest;
use App\Http\Requests\UpdatePromotionRequest;
use Illuminate\Support\Facades\View;

class PromotionsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'promotions');
    }

    public function getIndex()
    {
        return view('admin.promotions.list');
    }

    public function getList()
    {
        $promotions=Promotion::all();
        $promotions_list=[];

        foreach($promotions as $promotion) {

            $configuration=Configuration::first();
            $price_bss=number_format($promotion->price, 2, ',', '.');
            if($configuration)
                $price_bss=number_format($promotion->price*$configuration->exchange_rate, 2, ',', '.');

            $products='';
            $total=0;
            foreach($promotion->products->sortByDesc('pivot.amount') as $product) {
                $products.='<a href="'.route("admin_products_edit",["product_id"=>$product->id]).'"><b><i class="fa fa-search" aria-hidden="true"></i> '.$product->name.'</b></a>: '.$product->pivot->amount.' en $'.number_format($product->price*$product->pivot->amount, 2, ',', '.').'<br>';
                $total+=$product->price*$product->pivot->amount;
            }
            $products.='Total: $'.number_format($total, 2, ',', '.');

            $promotions_list[]=[$promotion->name, number_format($promotion->price, 2, ',', '.'), $price_bss, $products, $promotion->status==1?'Activo':'Inactivo', $promotion->created_at->format('d-m-Y h:i:s a'), '<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_promotions_edit",["promotion_id"=>$promotion->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_promotions_trash",["promotion_id"=>$promotion->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>'];

        }

        return response()->json(['data' => $promotions_list]);
    }

    public function getCreate() 
    {
    	$products=Product::orderBy('name','asc')->get();

    	if(count($products)==0) {
			return redirect()->route('admin_products_create')->with(['message_info'=>'Debes registrar como mínimo un producto para proceder a registrar una promoción.']);
    	}

        return view('admin.promotions.create',['products'=>$products]);
    }

    public function create(CreatePromotionRequest $request) 
    {
        $data=$request->only(['name','price','status']);

        $data['price']=str_replace('.', '', $data['price']);
        $data['price']=str_replace(',', '.', $data['price']);

        $product_id=$request->product_id;
        $amounts=$request->amounts;
    	
    	if(!isset($product_id) || !isset($amounts)) {
			return redirect()->back()->withInput()->withErrors(['Olvidaste agregar como mínimo un producto.']);
        }

        if(isset($product_id) && isset($amounts)) {
        	$product_array=[];
	        foreach($product_id as $key => $prod_id) {
                $product=Product::where('id',$prod_id)->first();
                $amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                if($amount==0){
                    return redirect()->back()->withInput()->withErrors(['Ingresaste 0 de cantidad en el producto: '.$product->name.'.']);
                }

	        	if(!isset($product_array[$prod_id])) {
	        		$product_array[$prod_id]=$amounts[$key];
	        	}else {
	        		return redirect()->back()->withInput()->withErrors(['Seleccionaste dos veces o más el producto: '.$product->name.'.']);
	        	}
	        }
        }

        $promotion=Promotion::create($data);

    	if(isset($product_id) && isset($amounts)) {
	        foreach($product_id as $key => $prod_id) {
	        	$amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);
	        	$promotion->products()->attach($prod_id,['amount'=>$amount]);
	        }
    	}

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_promotion', 'operation'=>'Registro de la promoción: '.$promotion->name]);

        return redirect()->route('admin_promotions')->with(['message_info'=>'Promoción: '.$promotion->name.' registrada exitosamente.']);
    }

    public function getEdit($promotion_id) 
    {
        $promotion=Promotion::where('id',$promotion_id)->first();

        if(!$promotion) {
            return redirect()->route('admin_promotions')->withErrors(['La promoción con ID #'.$promotion_id.' no se encuentra registrada.']);
        }

		$products=Product::orderBy('name','asc')->get();

        return view('admin.promotions.edit',['promotion'=>$promotion, 'products'=>$products]);
    }

    public function update(UpdatePromotionRequest $request)
    {
        $promotion_id=$request->promotion_id;
        $data=$request->only(['name','price','status']);

        $data['price']=str_replace('.', '', $data['price']);
        $data['price']=str_replace(',', '.', $data['price']);

        $product_id=$request->product_id;
        $amounts=$request->amounts;
    	
    	if(!isset($product_id) || !isset($amounts)) {
			return redirect()->back()->withInput()->withErrors(['Olvidaste agregar como mínimo un producto.']);
        }

        if(isset($product_id) && isset($amounts)) {
        	$product_array=[];
	        foreach($product_id as $key => $prod_id) {
                $product=Product::where('id',$prod_id)->first();
                $amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                if($amount==0){
                    return redirect()->back()->withInput()->withErrors(['Ingresaste 0 de cantidad en el producto: '.$product->name.'.']);
                }

	        	if(!isset($product_array[$prod_id])) {
	        		$product_array[$prod_id]=$amounts[$key];
	        	}else {
	        		return redirect()->back()->withInput()->withErrors(['Seleccionaste dos veces o más el producto: '.$product->name.'.']);
	        	}
	        }
        }

        Promotion::where('id',$promotion_id)->update($data);
        $promotion=Promotion::where('id',$promotion_id)->first();

    	$promotion->products()->detach();
    	if(isset($product_id) && isset($amounts)) {
	        foreach($product_id as $key => $prod_id) {
	        	$amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);
	        	$promotion->products()->attach($prod_id,['amount'=>$amount]);
	        }
    	}

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_promotion', 'operation'=>'Actualización de la promoción: '.$promotion->name]);

        return redirect()->route('admin_promotions')->with(['message_info'=>'Promoción: '.$promotion->name.' actualizada exitosamente.']);
    }

    public function getTrash($promotion_id) 
    {
        $promotion=Promotion::where('id',$promotion_id)->first();

        if(!$promotion) {
            return redirect()->route('admin_promotions')->withErrors(['La promoción con ID #'.$promotion_id.' no se encuentra registrada.']);
        }

        if(count($promotion->sales)>0) {
            return redirect()->route('admin_promotions')->withErrors(['La promoción '.$promotion->name.' no puede ser eliminada, ya que tiene ventas asociadas.']);
        }

        return view('admin.promotions.trash',['promotion'=>$promotion]);
    }

    public function delete(Request $request) 
    {
        $promotion_id=$request->promotion_id;
        $promotion=Promotion::where('id',$promotion_id)->first();
        $message='Se ha eliminado exitosamente la promoción: '.$promotion->name.'.';
        
        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_promotion', 'operation'=>'Eliminación de la promoción: '.$promotion->name]);

        Promotion::where('id',$promotion_id)->delete();
        return redirect()->route('admin_promotions')->with(['message_info'=>$message]);
    }
}
