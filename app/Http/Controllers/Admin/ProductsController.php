<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Category;
use App\Models\Configuration;
use App\Models\Product;
use App\Models\RawMaterial;
use App\Models\Tracking;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class ProductsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'products');
    }

    public function getIndex()
    {
        return view('admin.products.list');
    }

    public function getList()
    {
        $products=Product::all();
        $products_list=[];

        foreach($products as $product) {

            $configuration=Configuration::first();
            $price_bss=number_format($product->price, 2, ',', '.');
            if($configuration)
                $price_bss=number_format($product->price*$configuration->exchange_rate, 2, ',', '.');

            $products_list[]=[$product->name, $product->category->name, number_format($product->price, 2, ',', '.'), $price_bss, $product->type==1?'Sin elaboración':'Requiere materia prima', $product->created_at->format('d-m-Y h:i:s a'), '<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_products_edit",["product_id"=>$product->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_products_trash",["product_id"=>$product->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>'];

        }

        return response()->json(['data' => $products_list]);
    }

    public function getCreate() 
    {
    	$categories=Category::orderBy('name','asc')->get();

    	if(count($categories)==0) {
			return redirect()->route('admin_categories_create')->with(['message_info'=>'Debes registrar como mínimo una categoría para proceder a registrar un producto.']);
    	}

    	$raw_material=RawMaterial::orderBy('name','asc')->get();

    	if(count($raw_material)==0) {
			return redirect()->route('admin_raw_material_create')->with(['message_info'=>'Debes registrar como mínimo una materia prima para proceder a registrar un producto.']);
    	}

        return view('admin.products.create',['categories'=>$categories, 'raw_material'=>$raw_material]);
    }

    public function create(CreateProductRequest $request) 
    {
        $data=$request->only(['name','category_id','price','type']);

        $data['price']=str_replace('.', '', $data['price']);
        $data['price']=str_replace(',', '.', $data['price']);

        $raw_material_id=$request->raw_material_id;
        $amounts=$request->amounts;
        
        if($data['type']==2 && isset($raw_material_id) && isset($amounts)) {
        	$rm_array=[];
	        foreach($raw_material_id as $key => $rm_id) {
                $raw_material=RawMaterial::where('id',$rm_id)->first();
                $amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                if($amount==0){
                    return redirect()->back()->withInput()->withErrors(['Ingresaste 0,00 '.$raw_material->unit.' de cantidad en la materia prima: '.$raw_material->name.'.']);
                }

	        	if(!isset($rm_array[$rm_id])){
	        		$rm_array[$rm_id]=$amounts[$key];
	        	}else {
	        		return redirect()->back()->withInput()->withErrors(['Seleccionaste dos o más veces la materia prima: '.$raw_material->name.'.']);
	        	}
	        }
        }

        if($data['type']==2 && (!isset($raw_material_id) || !isset($amounts))) {
			return redirect()->back()->withInput()->withErrors(['Seleccionaste el tipo: "Requiere materia prima" pero olvidaste agregar como mínimo una materia prima.']);
        }

        $data['photo']='none';
        $data['real_name_photo']='none';
        $product=Product::create($data);

        // Photo
		/*$extension=new \SplFileInfo($_FILES['photo']['name']);
		$extension=$extension->getExtension();
    	$rand=rand();
    	$request->photo->storeAs('images/products/', 'product_'.$product->id.'.'.$extension, 'uploads');
    	$photo='images/products/product_'.$product->id.'.'.$extension.'?v='.$rand;
    	$real_name_photo='images/products/product_'.$product->id.'.'.$extension;
    	$product->update(['photo'=>$photo, 'real_name_photo'=>$real_name_photo]);*/

    	if($data['type']==2 && isset($raw_material_id) && isset($amounts)) {
	        foreach($raw_material_id as $key => $rm_id) {
	        	$amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);
	        	$product->raw_material()->attach($rm_id,['amount'=>$amount]);
	        }
    	}

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_product', 'operation'=>'Registro del producto: '.$product->name]);

        return redirect()->route('admin_products')->with(['message_info'=>'Producto: '.$product->name.' registrado exitosamente.']);
    }

    public function getEdit($product_id) 
    {
        $product=Product::where('id',$product_id)->first();

        if(!$product) {
            return redirect()->route('admin_products')->withErrors(['El producto con ID #'.$product_id.' no se encuentra registrado.']);
        }

        $categories=Category::orderBy('name','asc')->get();
		$raw_material=RawMaterial::orderBy('name','asc')->get();

        return view('admin.products.edit',['product'=>$product, 'categories'=>$categories, 'raw_material'=>$raw_material]);
    }

    public function update(UpdateProductRequest $request)
    {
        $product_id=$request->product_id;
        $data=$request->only(['name','category_id','price','type']);

        $data['price']=str_replace('.', '', $data['price']);
        $data['price']=str_replace(',', '.', $data['price']);

        $raw_material_id=$request->raw_material_id;
        $amounts=$request->amounts;
        
        if($data['type']==2 && isset($raw_material_id) && isset($amounts)) {
            $rm_array=[];
            foreach($raw_material_id as $key => $rm_id) {
                $raw_material=RawMaterial::where('id',$rm_id)->first();
                $amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);

                if($amount==0){
                    return redirect()->back()->withInput()->withErrors(['Ingresaste 0,00 '.$raw_material->unit.' de cantidad en la materia prima: '.$raw_material->name.'.']);
                }

                if(!isset($rm_array[$rm_id])){
                    $rm_array[$rm_id]=$amounts[$key];
                }else {
                    return redirect()->back()->withInput()->withErrors(['Seleccionaste dos o más veces la materia prima: '.$raw_material->name.'.']);
                }
            }
        }

        if($data['type']==2 && (!isset($raw_material_id) || !isset($amounts))) {
			return redirect()->back()->withInput()->withErrors(['Seleccionaste el tipo: "Requiere materia prima" pero olvidaste agregar como mínimo una materia prima.']);
        }

        Product::where('id',$product_id)->update($data);
        $product=Product::where('id',$product_id)->first();

        // Photo
        /*if(isset($request->photo)) {

            if($product->real_name_photo)
                Storage::disk('uploads')->delete($product->real_name_photo);

			$extension=new \SplFileInfo($_FILES['photo']['name']);
			$extension=$extension->getExtension();
	    	$rand=rand();
	    	$request->photo->storeAs('images/products/', 'product_'.$product->id.'.'.$extension, 'uploads');
	    	$photo='images/products/product_'.$product->id.'.'.$extension.'?v='.$rand;
	    	$real_name_photo='images/products/product_'.$product->id.'.'.$extension;
	    	$product->update(['photo'=>$photo, 'real_name_photo'=>$real_name_photo]);
    	}*/

        foreach($product->raw_material as $raw_material) {
            $product->raw_material()->detach($raw_material->id);
        }

    	if($data['type']==2 && isset($raw_material_id) && isset($amounts)) {
	        foreach($raw_material_id as $key => $rm_id) {
	        	$amount=str_replace('.', '', $amounts[$key]);
                $amount=str_replace(',', '.', $amount);
	        	$product->raw_material()->attach($rm_id,['amount'=>$amount]);
	        }
    	}

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_product', 'operation'=>'Actualización del producto: '.$product->name]);

        return redirect()->route('admin_products')->with(['message_info'=>'Producto: '.$product->name.' actualizado exitosamente.']);
    }

    public function getTrash($product_id) 
    {
        $product=Product::where('id',$product_id)->first();

        if(!$product) {
            return redirect()->route('admin_products')->withErrors(['El producto con ID #'.$product_id.' no se encuentra registrado.']);
        }

        if($product->type==1 && count($product->unprocessed_products_movements)>0) {
            return redirect()->route('admin_products')->withErrors(['El producto '.$product->name.' no puede ser eliminado, ya que tiene movimientos de stock asociados.']);
        }

        if(count($product->promotions)>0) {
            return redirect()->route('admin_products')->withErrors(['El producto '.$product->name.' no puede ser eliminado, ya que tiene promociones asociadas.']);
        }

        if(count($product->sales)>0) {
            return redirect()->route('admin_products')->withErrors(['El producto '.$product->name.' no puede ser eliminado, ya que tiene ventas asociadas.']);
        }

        return view('admin.products.trash',['product'=>$product]);
    }

    public function delete(Request $request) 
    {
        $product_id=$request->product_id;
        $product=Product::where('id',$product_id)->first();
        $message='Se ha eliminado exitosamente el producto: '.$product->name.'.';
        
        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_product', 'operation'=>'Eliminación del producto: '.$product->name]);
        
        if($product->real_name_photo)
        	Storage::disk('uploads')->delete($product->real_name_photo);

        Product::where('id',$product_id)->delete();
        return redirect()->route('admin_products')->with(['message_info'=>$message]);
    }
}
