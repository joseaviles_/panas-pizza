<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\PaymentMethod;
use App\Models\PaymentMethodOption;
use App\Models\Tracking;
use App\Http\Requests\CreatePaymentMethodRequest;
use App\Http\Requests\UpdatePaymentMethodRequest;
use Illuminate\Support\Facades\View;

class PaymentMethodsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'payment_methods');
    }

    public function getIndex()
    {
        return view('admin.payment_methods.list');
    }

    public function getList()
    {
        $payment_methods=PaymentMethod::all();
        $payment_methods_list=[];

        foreach($payment_methods as $payment_method) {

        	$banks_payment_processors=null;
        	foreach($payment_method->payment_method_options->sortBy('name') as $payment_method_option) {

    			if($payment_method_option->commission==0 && $payment_method_option->extra_amount==0)
    				$banks_payment_processors.=$payment_method_option->name.'<br>';
    			elseif($payment_method_option->commission!=0 && $payment_method_option->extra_amount==0)
    				$banks_payment_processors.=$payment_method_option->name.' - Comisión: '.number_format($payment_method_option->commission, 2, ',', '.').'%<br>';
    			elseif($payment_method_option->commission==0 && $payment_method_option->extra_amount!=0)
    				$banks_payment_processors.=$payment_method_option->name.' - Monto extra: '.number_format($payment_method_option->extra_amount, 2, ',', '.').'<br>';
    			elseif($payment_method_option->commission!=0 && $payment_method_option->extra_amount!=0)
    				$banks_payment_processors.=$payment_method_option->name.' - Comisión: '.number_format($payment_method_option->commission, 2, ',', '.').'% + Monto extra: '.number_format($payment_method_option->extra_amount, 2, ',', '.').'<br>';
        	}

            $payment_methods_list[]=[$payment_method->name, $payment_method->currency, $payment_method->type, $banks_payment_processors==null?'N/A':$banks_payment_processors, $payment_method->created_at->format('d-m-Y h:i:s a'), '<a class="btn btn-sm btn-primary mt-1" href="'.route("admin_payment_methods_edit",["payment_method_id"=>$payment_method->id]).'"><i class="fa fa-edit" aria-hidden="true"></i></a> <a class="btn btn-sm btn-warning mt-1" href="'.route("admin_payment_methods_trash",["payment_method_id"=>$payment_method->id]).'"><i class="fa fa-trash" aria-hidden="true"></i></a>'];

        }

        return response()->json(['data' => $payment_methods_list]);
    }

    public function getCreate() 
    {
        return view('admin.payment_methods.create');
    }

    public function create(CreatePaymentMethodRequest $request) 
    {
        $data=$request->only(['name','currency','type']);

        $names=$request->names;
        $commissions=$request->commissions;
        $extra_amounts=$request->extra_amounts;

        $payment_method=PaymentMethod::create($data);

    	if(isset($names) && isset($commissions)) {
	        foreach($names as $key => $name) {
	        	$commission=str_replace('.', '', $commissions[$key]);
                $commission=str_replace(',', '.', $commission);
                $extra_amount=str_replace('.', '', $extra_amounts[$key]);
                $extra_amount=str_replace(',', '.', $extra_amount);
	        	PaymentMethodOption::create(['name'=>$name, 'commission'=>$commission, 'extra_amount'=>$extra_amount, 'payment_method_id'=>$payment_method->id]);
	        }
    	}

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'create_payment_method', 'operation'=>'Registro del método de pago: '.$payment_method->name]);

        return redirect()->route('admin_payment_methods')->with(['message_info'=>'Método de pago: '.$payment_method->name.' registrado exitosamente.']);
    }

    public function getEdit($payment_method_id) 
    {
        $payment_method=PaymentMethod::where('id',$payment_method_id)->first();

        if(!$payment_method) {
            return redirect()->route('admin_payment_methods')->withErrors(['El método de pago con ID #'.$payment_method_id.' no se encuentra registrado.']);
        }

        return view('admin.payment_methods.edit',['payment_method'=>$payment_method]);
    }

    public function update(UpdatePaymentMethodRequest $request)
    {
        $payment_method_id=$request->payment_method_id;
        $data=$request->only(['name','currency','type']);

        PaymentMethod::where('id',$payment_method_id)->update($data);
        $payment_method=PaymentMethod::where('id',$payment_method_id)->first();

        $payment_method_options=$payment_method->payment_method_options;
        foreach($payment_method_options as $payment_method_option) {
            if(count($payment_method_option->payments)==0 && count($payment_method_option->changes)==0) {
                $payment_method_option->delete();
            }
        }

        $edit_payment_method_option_ids=$request->edit_payment_method_option_ids;
        $edit_names=$request->edit_names;
        $edit_commissions=$request->edit_commissions;
        $edit_extra_amounts=$request->edit_extra_amounts;

        if(isset($edit_payment_method_option_ids) && isset($edit_names) && isset($edit_commissions) && isset($edit_extra_amounts)) {
            foreach($edit_payment_method_option_ids as $key => $edit_payment_method_option_id) {

                $edit_payment_method_option=PaymentMethodOption::find($edit_payment_method_option_id);

                $commission=str_replace('.', '', $edit_commissions[$key]);
                $commission=str_replace(',', '.', $commission);
                $extra_amount=str_replace('.', '', $edit_extra_amounts[$key]);
                $extra_amount=str_replace(',', '.', $extra_amount);

                if($edit_payment_method_option) {
                    PaymentMethodOption::where('id',$edit_payment_method_option->id)->update(['name'=>$edit_names[$key], 'commission'=>$commission, 'extra_amount'=>$extra_amount, 'payment_method_id'=>$payment_method->id]);
                }else {
                    PaymentMethodOption::create(['name'=>$edit_names[$key], 'commission'=>$commission, 'extra_amount'=>$extra_amount, 'payment_method_id'=>$payment_method->id]);
                }

            }
        }

        $names=$request->names;
        $commissions=$request->commissions;
        $extra_amounts=$request->extra_amounts;

    	if(isset($names) && isset($commissions) && isset($extra_amounts)) {
	        foreach($names as $key => $name) {
	        	$commission=str_replace('.', '', $commissions[$key]);
                $commission=str_replace(',', '.', $commission);
                $extra_amount=str_replace('.', '', $extra_amounts[$key]);
                $extra_amount=str_replace(',', '.', $extra_amount);
	        	PaymentMethodOption::create(['name'=>$name, 'commission'=>$commission, 'extra_amount'=>$extra_amount, 'payment_method_id'=>$payment_method->id]);
	        }
    	}

        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'update_payment_method', 'operation'=>'Actualización del método de pago: '.$payment_method->name]);

        return redirect()->route('admin_payment_methods')->with(['message_info'=>'Método de pago: '.$payment_method->name.' actualizado exitosamente.']);
    }

    public function getTrash($payment_method_id) 
    {
        $payment_method=PaymentMethod::where('id',$payment_method_id)->first();

        if(!$payment_method) {
            return redirect()->route('admin_payment_methods')->withErrors(['El método de pago con ID #'.$payment_method_id.' no se encuentra registrado.']);
        }

        if(count($payment_method->payments)>0) {
            return redirect()->route('admin_payment_methods')->withErrors(['El método de pago '.$payment_method->name.' no puede ser eliminado, ya que tiene pagos asociados.']);
        }

        if(count($payment_method->changes)>0) {
            return redirect()->route('admin_payment_methods')->withErrors(['El método de pago '.$payment_method->name.' no puede ser eliminado, ya que tiene cambios asociados.']);
        }

        return view('admin.payment_methods.trash',['payment_method'=>$payment_method]);
    }

    public function delete(Request $request) 
    {
        $payment_method_id=$request->payment_method_id;
        $payment_method=PaymentMethod::where('id',$payment_method_id)->first();
        $message='Se ha eliminado exitosamente el método de pago: '.$payment_method->name.'.';
        
        $current_user=User::getCurrent();
        Tracking::create(['user_id'=>$current_user->id, 'category'=>'delete_payment_method', 'operation'=>'Eliminación del método de pago: '.$payment_method->name]);

        PaymentMethod::where('id',$payment_method_id)->delete();
        return redirect()->route('admin_payment_methods')->with(['message_info'=>$message]);
    }
}
