<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CashRegisterMovement;
use App\Models\Category;
use App\Models\Client;
use App\Models\DeliveryCompany;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\Promotion;
use App\Models\RawMaterial;
use App\Models\RawMaterialIncome;
use App\Models\RawMaterialMovement;
use App\Models\Sale;
use App\Models\UnprocessedProductsMovement;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Barryvdh\DomPDF\Facade as PDF;

class ReportsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', 'reports');
    }

    public function getIndex()
    {
        $raw_material=RawMaterial::orderBy('name','asc')->get();
        $products=Product::where('type',1)->orderBy('name','asc')->get();
        return view('admin.reports.list',['raw_material'=>$raw_material, 'products'=>$products]);
    }

    public function newReport(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        
    	$type=$request->type;
    	$initial_date=$request->initial_date;
    	$final_date=$request->final_date;

        if(isset($initial_date) && isset($final_date) && $final_date<$initial_date) {
            return redirect()->back()->withErrors(['La fecha final no puede ser menor a la fecha inicial.']);
        }

        if($type=='most_selled_products') {
        	
            // Products
        	$check_products=Product::orderBy('name','asc')->get();

        	$products=Product::leftJoin('product_sale', 'product_sale.product_id', '=', 'products.id')
            ->leftJoin('sales', 'sales.id', '=', 'product_sale.sale_id')
            ->selectRaw('products.id, products.name, SUM(product_sale.amount) as product_sale_amount, SUM(product_sale.price-(product_sale.price*(sales.discount/100))) as product_sale_price')
            ->groupBy('products.id')
            ->groupBy('products.name')
            ->orderBy('product_sale_amount','desc')
            ->orderBy('products.name','asc')
            ->where('sales.discount','<',100);

            // Promotions
            $check_promotions=Promotion::orderBy('name','asc')->get();
            
            $promotions=Promotion::leftJoin('promotion_sale', 'promotion_sale.promotion_id', '=', 'promotions.id')
            ->leftJoin('sales', 'sales.id', '=', 'promotion_sale.sale_id')
            ->selectRaw('promotions.id, promotions.name, SUM(promotion_sale.amount) as promotion_sale_amount, SUM(promotion_sale.price-(promotion_sale.price*(sales.discount/100))) as promotion_sale_price')
            ->groupBy('promotions.id')
            ->groupBy('promotions.name')
            ->orderBy('promotion_sale_amount','desc')
            ->orderBy('promotions.name','asc')
            ->where('sales.discount','<',100);

        	if(isset($initial_date) && isset($final_date)) {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');

        		$products=$products->where('sales.created_at','>=',$initial_date)
        		->where('sales.created_at','<=',$final_date)
        		->get();

                $promotions=$promotions->where('sales.created_at','>=',$initial_date)
                ->where('sales.created_at','<=',$final_date)
                ->get();

        	}elseif(isset($initial_date) && !isset($final_date)) {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
        		$products=$products->where('sales.created_at','>=',$initial_date)
        		->get();

                $promotions=$promotions->where('sales.created_at','>=',$initial_date)
                ->get();

        	}elseif(!isset($initial_date) && isset($final_date)) {

                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
        		$products=$products->where('sales.created_at','<=',$final_date)
        		->get();

                $promotions=$promotions->where('sales.created_at','<=',$final_date)
                ->get();

        	}else {

				$products=$products->get();

                $promotions=$promotions->get();

        	}

            // Products
            foreach($check_products as $check_product) {
                if(!$products->where('id',$check_product->id)->first()) {
                    $check_product->product_sale_amount=0;
                    $products->push($check_product);
                }
            }
            $products=$products->sortBy('name')->sortByDesc('product_sale_amount');

            // Promotions
            foreach($check_promotions as $check_promotion) {
                if(!$promotions->where('id',$check_promotion->id)->first()) {
                    $check_promotion->product_sale_amount=0;
                    $promotions->push($check_promotion);
                }
            }
            $promotions=$promotions->sortBy('name')->sortByDesc('promotion_sale_amount');

        	return view('admin.reports.most_selled_products',['products'=>$products, 'promotions'=>$promotions, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);

        }elseif($type=='sales') {

            $sales=Sale::orderBy('id','desc');

            if(isset($initial_date) && isset($final_date)) {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
                $sales=$sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();

            }elseif(isset($initial_date) && !isset($final_date)) {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $sales=$sales->where('created_at','>=',$initial_date)->get();

            }elseif(!isset($initial_date) && isset($final_date)) {

                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
                $sales=$sales->where('created_at','<=',$final_date)->get();

            }else {

                $sales=$sales->get();

            }

            return view('admin.reports.sales',['sales'=>$sales, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);

        }elseif($type=='payment_methods') {

            $payment_methods=PaymentMethod::orderBy('name','asc')->get();

            if($initial_date) {
                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
            }

            if($final_date) {
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
            }

            return view('admin.reports.payment_methods',['payment_methods'=>$payment_methods, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);

        }elseif($type=='raw_material_incomes') {

            $raw_material_incomes=RawMaterialIncome::orderBy('created_at','desc');

            if(isset($initial_date) && isset($final_date)) {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
                $raw_material_incomes=$raw_material_incomes->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();

            }elseif(isset($initial_date) && !isset($final_date)) {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $raw_material_incomes=$raw_material_incomes->where('created_at','>=',$initial_date)->get();

            }elseif(!isset($initial_date) && isset($final_date)) {

                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
                $raw_material_incomes=$raw_material_incomes->where('created_at','<=',$final_date)->get();

            }else {

                $raw_material_incomes=$raw_material_incomes->get();

            }

            foreach($raw_material_incomes as $raw_material_income) {

                $array_products=[];
                $array_raw_material_used=[];

                foreach($raw_material_income->raw_material_income_movements as $raw_material_income_movement) {

                    if(!isset($array_products[$raw_material_income_movement->product->name])) {
                        $array_products[$raw_material_income_movement->product->name]=0;
                    }

                    if(!isset($array_raw_material_used[$raw_material_income_movement->product->name])) {
                        $array_raw_material_used[$raw_material_income_movement->product->name]=0;
                    }

                    $array_products[$raw_material_income_movement->product->name]+=$raw_material_income_movement->products_quantity;
                    $array_raw_material_used[$raw_material_income_movement->product->name]+=$raw_material_income_movement->amount;

                }

                arsort($array_products);
                $raw_material_income->array_products=$array_products;

                arsort($array_raw_material_used);
                $raw_material_income->array_raw_material_used=$array_raw_material_used;

            }

            return view('admin.reports.raw_material_incomes',['raw_material_incomes'=>$raw_material_incomes, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);

        }elseif($type=='delivery_companies') {
            
            $delivery_companies=DeliveryCompany::orderBy('name','asc')->get();

            if($initial_date) {
                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
            }

            if($final_date) {
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
            }

            return view('admin.reports.delivery_companies',['delivery_companies'=>$delivery_companies, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);

        }elseif($type=='expenses') {

            $raw_material_movements=RawMaterialMovement::where('type',1)->where('sale_id',null)->where('price','>',0)->orderBy('created_at','desc');

            $unprocessed_products_movements=UnprocessedProductsMovement::where('type',1)->where('sale_id',null)->where('price','>',0)->orderBy('created_at','desc');

            if(isset($initial_date) && isset($final_date)) {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');

                $raw_material_movements=$raw_material_movements->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();
                $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();

            }elseif(isset($initial_date) && !isset($final_date)) {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $raw_material_movements=$raw_material_movements->where('created_at','>=',$initial_date)->get();
                $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','>=',$initial_date)->get();

            }elseif(!isset($initial_date) && isset($final_date)) {

                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
                $raw_material_movements=$raw_material_movements->where('created_at','<=',$final_date)->get();
                $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','<=',$final_date)->get();

            }else {

                $raw_material_movements=$raw_material_movements->get();
                $unprocessed_products_movements=$unprocessed_products_movements->get();

            }

            return view('admin.reports.expenses',['raw_material_movements'=>$raw_material_movements, 'unprocessed_products_movements'=>$unprocessed_products_movements, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);

        }elseif($type=='inputs_outputs_rmp') {

            $raw_material_id=null;
            $raw_material=null;
            $raw_material_movements=null;

            $product_id=null;
            $product=null;
            $unprocessed_products_movements=null;

            if(strpos($request->rm_prod, 'r') !== false) {
                $raw_material_id=substr($request->rm_prod, 1);
                $raw_material=RawMaterial::where('id',$raw_material_id)->first();
                $raw_material_movements=RawMaterialMovement::where('raw_material_id',$raw_material_id);
            }else {
                $product_id=substr($request->rm_prod, 1);
                $product=Product::where('id',$product_id)->first();
                $unprocessed_products_movements=UnprocessedProductsMovement::where('product_id',$product_id);
            }

            if(isset($initial_date) && isset($final_date)) {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');

                if($raw_material_movements) {
                    $raw_material_movements=$raw_material_movements->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();
                }else {
                    $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();
                }

            }elseif(isset($initial_date) && !isset($final_date)) {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');

                if($raw_material_movements) {
                    $raw_material_movements=$raw_material_movements->where('created_at','>=',$initial_date)->get();
                }else {
                    $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','>=',$initial_date)->get();
                }

            }elseif(!isset($initial_date) && isset($final_date)) {

                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');

                if($raw_material_movements) {
                    $raw_material_movements=$raw_material_movements->where('created_at','<=',$final_date)->get();
                }else {
                    $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','<=',$final_date)->get();
                }


            }else {

                if($raw_material_movements) {
                    $raw_material_movements=$raw_material_movements->get();
                }else {
                    $unprocessed_products_movements=$unprocessed_products_movements->get();
                }

            }

            $raw_material_list=RawMaterial::orderBy('name','asc')->get();
            $products_list=Product::where('type',1)->orderBy('name','asc')->get();

            return view('admin.reports.inputs_outputs_rmp',['raw_material'=>$raw_material, 'product'=>$product, 'raw_material_movements'=>$raw_material_movements, 'unprocessed_products_movements'=>$unprocessed_products_movements,'initial_date'=>$initial_date, 'final_date'=>$final_date, 'raw_material_list'=>$raw_material_list, 'products_list'=>$products_list]);

        }elseif($type=='customers_purchases') {

            $clients=Client::orderBy('name_business_phone','asc')->get();

            foreach($clients as $client) {
                $client->purchases=count($client->sales);
                $client->total=$client->sales->sum('total');
            }

            $clients=$clients->sortBy('name_business_phone')->sortByDesc('purchases');

            return view('admin.reports.customers_purchases',['clients'=>$clients]);

        }
    	
    }

    public function pdfReport($type, $initial_date, $final_date, $rmp=false)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);

        if($type=='most_selled_products') {
        	
            // Products 
        	$check_products=Product::orderBy('name','asc')->get();

        	$products=Product::leftJoin('product_sale', 'product_sale.product_id', '=', 'products.id')
        	->selectRaw('products.id, products.name, SUM(product_sale.amount) as product_sale_amount, SUM(product_sale.price) as product_sale_price')
        	->groupBy('products.id')
        	->groupBy('products.name')
        	->orderBy('product_sale_amount','desc')
        	->orderBy('products.name','asc');

            // Promotions
            $check_promotions=Promotion::orderBy('name','asc')->get();
            
            $promotions=Promotion::leftJoin('promotion_sale', 'promotion_sale.promotion_id', '=', 'promotions.id')
            ->selectRaw('promotions.id, promotions.name, SUM(promotion_sale.amount) as promotion_sale_amount, SUM(promotion_sale.price) as promotion_sale_price')
            ->groupBy('promotions.id')
            ->groupBy('promotions.name')
            ->orderBy('promotion_sale_amount','desc')
            ->orderBy('promotions.name','asc');

        	if($initial_date!='none' && $final_date!='none') {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');

        		$products=$products->leftJoin('sales', 'sales.id', '=', 'product_sale.sale_id')
        		->where('sales.created_at','>=',$initial_date)
        		->where('sales.created_at','<=',$final_date)
                ->where('sales.discount','<',100)
        		->get();

                $promotions=$promotions->leftJoin('sales', 'sales.id', '=', 'promotion_sale.sale_id')
                ->where('sales.created_at','>=',$initial_date)
                ->where('sales.created_at','<=',$final_date)
                ->where('sales.discount','<',100)
                ->get();

        	}elseif($initial_date!='none' && $final_date=='none') {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
        		$products=$products->leftJoin('sales', 'sales.id', '=', 'product_sale.sale_id')
        		->where('sales.created_at','>=',$initial_date)
                ->where('sales.discount','<',100)
        		->get();

                $promotions=$promotions->leftJoin('sales', 'sales.id', '=', 'promotion_sale.sale_id')
                ->where('sales.created_at','>=',$initial_date)
                ->where('sales.discount','<',100)
                ->get();

        	}elseif($initial_date=='none' && $final_date!='none') {

                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
        		$products=$products->leftJoin('sales', 'sales.id', '=', 'product_sale.sale_id')
        		->where('sales.created_at','<=',$final_date)
                ->where('sales.discount','<',100)
        		->get();

                $promotions=$promotions->leftJoin('sales', 'sales.id', '=', 'promotion_sale.sale_id')
                ->where('sales.created_at','<=',$final_date)
                ->where('sales.discount','<',100)
                ->get();

        	}else {

				$products=$products->leftJoin('sales', 'sales.id', '=', 'product_sale.sale_id')
                ->where('sales.discount','<',100)->get();

                $promotions=$promotions->leftJoin('sales', 'sales.id', '=', 'promotion_sale.sale_id')
                ->where('sales.discount','<',100)->get();

        	}

            // Products
            foreach($check_products as $check_product) {
                if(!$products->where('id',$check_product->id)->first()) {
                    $check_product->product_sale_amount=0;
                    $products->push($check_product);
                }
            }

            $products=$products->sortBy('name')->sortByDesc('product_sale_amount');

            // Promotions
            foreach($check_promotions as $check_promotion) {
                if(!$promotions->where('id',$check_promotion->id)->first()) {
                    $check_promotion->product_sale_amount=0;
                    $promotions->push($check_promotion);
                }
            }
            $promotions=$promotions->sortBy('name')->sortByDesc('promotion_sale_amount');

        	$pdf=PDF::loadView('pdf.most_selled_products',['products'=>$products, 'promotions'=>$promotions, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);
        	return $pdf->stream('Productos más vendidos.pdf');

        }elseif($type=='sales') {

            $sales=Sale::orderBy('id','desc');

            if($initial_date!='none' && $final_date!='none') {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
                $sales=$sales->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();

            }elseif($initial_date!='none' && $final_date=='none') {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $sales=$sales->where('created_at','>=',$initial_date)->get();

            }elseif($initial_date=='none' && $final_date!='none') {

                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
                $sales=$sales->where('created_at','<=',$final_date)->get();

            }else {

                $sales=$sales->get();

            }

            $pdf=PDF::loadView('pdf.sales',['sales'=>$sales, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);
            return $pdf->stream('Ventas.pdf');

        }elseif($type=='payment_methods') {

            $payment_methods=PaymentMethod::orderBy('name','asc')->get();

            if($initial_date!='none') {
                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
            }

            if($final_date!='none') {
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
            }

            $pdf=PDF::loadView('pdf.payment_methods',['payment_methods'=>$payment_methods, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);
            return $pdf->stream('Pagos/cambios de los métodos de pago.pdf');

        }elseif($type=='raw_material_incomes') {

            $raw_material_incomes=RawMaterialIncome::orderBy('created_at','desc');

            if($initial_date!='none' && $final_date!='none') {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
                $raw_material_incomes=$raw_material_incomes->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();

            }elseif($initial_date!='none' && $final_date=='none') {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $raw_material_incomes=$raw_material_incomes->where('created_at','>=',$initial_date)->get();

            }elseif($initial_date=='none' && $final_date!='none') {

                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
                $raw_material_incomes=$raw_material_incomes->where('created_at','<=',$final_date)->get();

            }else {

                $raw_material_incomes=$raw_material_incomes->get();

            }

            foreach($raw_material_incomes as $raw_material_income) {

                $array_products=[];
                $array_raw_material_used=[];

                foreach($raw_material_income->raw_material_income_movements as $raw_material_income_movement) {

                    if(!isset($array_products[$raw_material_income_movement->product->name])) {
                        $array_products[$raw_material_income_movement->product->name]=0;
                    }

                    if(!isset($array_raw_material_used[$raw_material_income_movement->product->name])) {
                        $array_raw_material_used[$raw_material_income_movement->product->name]=0;
                    }

                    $array_products[$raw_material_income_movement->product->name]+=$raw_material_income_movement->products_quantity;
                    $array_raw_material_used[$raw_material_income_movement->product->name]+=$raw_material_income_movement->amount;

                }

                arsort($array_products);
                $raw_material_income->array_products=$array_products;

                arsort($array_raw_material_used);
                $raw_material_income->array_raw_material_used=$array_raw_material_used;

            }

            $pdf=PDF::loadView('pdf.raw_material_incomes',['raw_material_incomes'=>$raw_material_incomes, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);
            return $pdf->stream('Productos generados de las entradas de materias primas.pdf');

        }elseif($type=='delivery_companies') {

            $delivery_companies=DeliveryCompany::orderBy('name','asc')->get();

            if($initial_date!='none') {
                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
            }

            if($final_date!='none') {
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
            }
            
            $pdf=PDF::loadView('pdf.delivery_companies',['delivery_companies'=>$delivery_companies, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);
            return $pdf->stream('Entregas de las empresas de delivery.pdf');
            

        }elseif($type=='clients_with_debts') {
         
            $sales=Sale::where('sale_type',2)->orderBy('id','desc')->get();
            $pdf=PDF::loadView('pdf.clients_with_debts',['sales'=>$sales]);
            return $pdf->stream('Clientes con deudas.pdf');   

        }elseif($type=='clients') {
         
            $clients=Client::orderBy('name_business_phone','asc')->get();
            $pdf=PDF::loadView('pdf.clients',['clients'=>$clients]);
            return $pdf->stream('Clientes.pdf');   

        }elseif($type=='products') {
         
            $products=Product::orderBy('name','asc')->get();
            $pdf=PDF::loadView('pdf.products',['products'=>$products]);
            return $pdf->stream('Productos.pdf');   

        }elseif($type=='categories') {
         
            $categories=Category::orderBy('name','asc')->get();
            $pdf=PDF::loadView('pdf.categories',['categories'=>$categories]);
            return $pdf->stream('Categorías.pdf');   

        }elseif($type=='cash_history') {
         
            $movements=CashRegisterMovement::orderBy('created_at','desc')->get();
            $pdf=PDF::loadView('pdf.cash_history',['movements'=>$movements]);
            return $pdf->stream('Historial de caja.pdf');   

        }elseif($type=='general_inventory') {
         
            $raw_material=RawMaterial::orderBy('name','asc')->get();
            $products=Product::where('type',1)->orderBy('name','asc')->get();
            
            $pdf=PDF::loadView('pdf.general_inventory',['raw_material'=>$raw_material, 'products'=>$products]);
            return $pdf->stream('Inventario general.pdf');   

        }elseif($type=='expenses') {

            $raw_material_movements=RawMaterialMovement::where('type',1)->where('sale_id',null)->where('price','>',0)->orderBy('created_at','desc');

            $unprocessed_products_movements=UnprocessedProductsMovement::where('type',1)->where('sale_id',null)->where('price','>',0)->orderBy('created_at','desc');

            if($initial_date!='none' && $final_date!='none') {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
                
                $raw_material_movements=$raw_material_movements->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();
                $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();

            }elseif($initial_date!='none' && $final_date=='none') {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $raw_material_movements=$raw_material_movements->where('created_at','>=',$initial_date)->get();
                $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','>=',$initial_date)->get();

            }elseif($initial_date=='none' && $final_date!='none') {

                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');
                $raw_material_movements=$raw_material_movements->where('created_at','<=',$final_date)->get();
                $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','<=',$final_date)->get();

            }else {

                $raw_material_movements=$raw_material_movements->get();
                $unprocessed_products_movements=$unprocessed_products_movements->get();

            }

            $pdf=PDF::loadView('pdf.expenses',['raw_material_movements'=>$raw_material_movements, 'unprocessed_products_movements'=>$unprocessed_products_movements, 'initial_date'=>$initial_date, 'final_date'=>$final_date]);
            return $pdf->stream('Gastos.pdf');

        }elseif($type=='inputs_outputs_rmp') {

            $raw_material_id=null;
            $raw_material=null;
            $raw_material_movements=null;

            $product_id=null;
            $product=null;
            $unprocessed_products_movements=null;

            if(strpos($rmp, 'r') !== false) {
                $raw_material_id=substr($rmp, 1);
                $raw_material=RawMaterial::where('id',$raw_material_id)->first();
                $raw_material_movements=RawMaterialMovement::where('raw_material_id',$raw_material_id)->orderBy('id','desc');
            }else {
                $product_id=substr($rmp, 1);
                $product=Product::where('id',$product_id)->first();
                $unprocessed_products_movements=UnprocessedProductsMovement::where('product_id',$product_id)->orderBy('id','desc');
            }

            if($initial_date!='none' && $final_date!='none') {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');
                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');

                if($raw_material_movements) {
                    $raw_material_movements=$raw_material_movements->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();
                }else {
                    $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','>=',$initial_date)->where('created_at','<=',$final_date)->get();
                }

            }elseif($initial_date!='none' && $final_date=='none') {

                $initial_date=new \DateTime($initial_date);
                $initial_date=$initial_date->format('Y-m-d H:i:s');

                if($raw_material_movements) {
                    $raw_material_movements=$raw_material_movements->where('created_at','>=',$initial_date)->get();
                }else {
                    $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','>=',$initial_date)->get();
                }

            }elseif($initial_date=='none' && $final_date!='none') {

                $final_date=new \DateTime($final_date);
                $final_date=$final_date->format('Y-m-d H:i:s');

                if($raw_material_movements) {
                    $raw_material_movements=$raw_material_movements->where('created_at','<=',$final_date)->get();
                }else {
                    $unprocessed_products_movements=$unprocessed_products_movements->where('created_at','<=',$final_date)->get();
                }


            }else {

                if($raw_material_movements) {
                    $raw_material_movements=$raw_material_movements->get();
                }else {
                    $unprocessed_products_movements=$unprocessed_products_movements->get();
                }

            }

            $raw_material_list=RawMaterial::orderBy('name','asc')->get();
            $products_list=Product::where('type',1)->orderBy('name','asc')->get();

            $pdf=PDF::loadView('pdf.inputs_outputs_rmp',['raw_material'=>$raw_material, 'product'=>$product, 'raw_material_movements'=>$raw_material_movements, 'unprocessed_products_movements'=>$unprocessed_products_movements, 'initial_date'=>$initial_date, 'final_date'=>$final_date, 'raw_material_list'=>$raw_material_list, 'products_list'=>$products_list]);
            return $pdf->stream('Entradas-salidas de materias primas y de productos sin elaboración.pdf');

        }elseif($type=='customers_purchases') {

            $clients=Client::orderBy('name_business_phone','asc')->get();

            foreach($clients as $client) {
                $client->purchases=count($client->sales);
                $client->total=$client->sales->sum('total');
            }

            $clients=$clients->sortBy('name_business_phone')->sortByDesc('purchases');

            $pdf=PDF::loadView('pdf.customers_purchases',['clients'=>$clients]);
            return $pdf->stream('Clientes con mas compras.pdf');
            
        }
	}
}
