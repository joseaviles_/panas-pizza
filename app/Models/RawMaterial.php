<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RawMaterial extends Model
{
    protected $table = 'raw_material';

    protected $fillable = [
		'name',
		'unit',
		'amount',
		'description'
  	];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product','product_raw_material')->withPivot('amount');
    }

    public function raw_material_movements()
    {
        return $this->hasMany('App\Models\RawMaterialMovement');
    }

    public function raw_material_incomes()
    {
        return $this->hasMany('App\Models\RawMaterialIncome');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category','category_raw_material','raw_material_id','category_id')->withPivot('amount');
    }
}
