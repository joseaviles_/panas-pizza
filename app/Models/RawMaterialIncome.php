<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RawMaterialIncome extends Model
{
    protected $table = 'raw_material_incomes';

    protected $fillable = [
		'raw_material_movement_id',
        'raw_material_id',
		'amount'
  	];

    public function raw_material_movement()
    {
        return $this->hasOne('App\Models\RawMaterialMovement','id','raw_material_movement_id');
    }

    public function raw_material()
    {
        return $this->hasOne('App\Models\RawMaterial','id','raw_material_id');
    }

    public function raw_material_income_movements()
    {
        return $this->hasMany('App\Models\RawMaterialIncomeMovement');
    }
}
