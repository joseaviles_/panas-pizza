<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokenReset extends Model
{
	protected $table = 'token_resets';
    protected $hash_key = 'kWjFioOmqNraKJWtncef';
    public $incrementing = false;
    public $created_at = false;
    public $updated_at = false;

    protected $fillable = [
        'email',
        'token'
    ];

    public function __construct(array $attributes = array()) 
    {
        parent::__construct($attributes);
        $this->token = $this->createToken();
    }

    public function createToken() 
    {
        return hash_hmac('sha256', \Str::random(40), $this->hash_key);
    }
}
