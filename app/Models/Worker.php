<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    protected $table = 'workers';

    protected $fillable = [
		'first_name',
		'last_name',
		'identity_card',
		'birthdate',
		'job_title'
  	];
}
