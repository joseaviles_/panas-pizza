<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryCompany extends Model
{
    protected $table = 'delivery_companies';

    protected $fillable = [
		'name'
  	];

    public function delivery_company_drivers()
    {
        return $this->hasMany('App\Models\DeliveryCompanyDriver','delivery_company_id','id');
    }

    public function delivery_types()
    {
        return $this->hasMany('App\Models\DeliveryType','delivery_company_id','id');
    }

    public function delivery_sales()
    {
        return $this->hasMany('App\Models\DeliverySale','delivery_company_id','id');
    }
}
