<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliverySale extends Model
{
    protected $table = 'delivery_sales';

    protected $fillable = [
		'delivery_company_id',
		'delivery_type_id',
		'delivery_company_driver_id',
		'amount',
        'price',
		'sale_id',
        'created_at',
  	];

  	public function delivery_company()
    {
        return $this->hasOne('App\Models\DeliveryCompany','id','delivery_company_id');
    }

    public function delivery_type()
    {
        return $this->hasOne('App\Models\DeliveryType','id','delivery_type_id');
    }

    public function delivery_company_driver()
    {
        return $this->hasOne('App\Models\DeliveryCompanyDriver','id','delivery_company_driver_id');
    }

    public function sale()
    {
        return $this->hasOne('App\Models\Sale','id','sale_id');
    }
}
