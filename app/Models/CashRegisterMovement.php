<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashRegisterMovement extends Model
{
    protected $table = 'cash_register_movements';

    protected $fillable = [
		'cash_register_id',
		'type',
		'currency',
		'movement',
		'amount',
		'sale_id',
        'created_at',
  	];

    public function cash_register()
    {
        return $this->hasOne('App\Models\CashRegister','id','cash_register_id');
    }

    public function sale()
    {
        return $this->hasOne('App\Models\Sale','id','sale_id');
    }
}
