<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
		'name',
		'photo',
        'real_name_photo',
		'category_id',
		'price',
		'type',
        'amount'
  	];

    public function category()
    {
        return $this->hasOne('App\Models\Category','id','category_id');
    }

    public function raw_material()
    {
        return $this->belongsToMany('App\Models\RawMaterial','product_raw_material')->withPivot('amount');
    }

    public function unprocessed_products_movements()
    {
        return $this->hasMany('App\Models\UnprocessedProductsMovement');
    }

    public function promotions()
    {
        return $this->belongsToMany('App\Models\Promotion','product_promotion')->withPivot('amount');
    }

    public function sales()
    {
        return $this->belongsToMany('App\Models\Sale','product_sale')->withPivot('amount','price','description');
    }

    public function raw_material_income_movements()
    {
        return $this->hasMany('App\Models\RawMaterialIncomeMovement');
    }
}
