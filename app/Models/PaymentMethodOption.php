<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethodOption extends Model
{
    protected $table = 'payment_method_options';

    protected $fillable = [
		'name',
		'commission',
        'extra_amount',
		'payment_method_id'
  	];

    public function payment_method()
    {
        return $this->hasOne('App\Models\PaymentMethod','id','payment_method_id');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payment','payment_method_option_id','id');
    }

    public function changes()
    {
        return $this->hasMany('App\Models\Payment','change_method_option_id','id');
    }
}
