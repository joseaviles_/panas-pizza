<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'promotions';

    protected $fillable = [
		'name',
		'price',
		'status'
  	];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product','product_promotion')->withPivot('amount');
    }

    public function sales()
    {
        return $this->belongsToMany('App\Models\Sale','promotion_sale')->withPivot('amount','price','description');
    }
}
