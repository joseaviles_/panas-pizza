<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RawMaterialMovement extends Model
{
    protected $table = 'raw_material_movements';

    protected $fillable = [
		'raw_material_id',
		'amount',
        'new_amount',
        'price',
		'price_bss',
		'exchange_rate',
		'concept',
		'type',
        'sale_id',
        'is_deleted',
        'created_at',
  	];

    public function raw_material()
    {
        return $this->hasOne('App\Models\RawMaterial','id','raw_material_id');
    }

    public function sale()
    {
        return $this->hasOne('App\Models\Sale','id','sale_id');
    }

    public function raw_material_income()
    {
        return $this->hasOne('App\Models\RawMaterialIncome');
    }
}
