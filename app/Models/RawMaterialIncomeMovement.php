<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RawMaterialIncomeMovement extends Model
{
    protected $table = 'raw_material_income_movements';

    protected $fillable = [
		'raw_material_income_id',
		'product_id',
        'sale_id',
		'amount',
        'products_quantity',
        'created_at',
  	];

    public function raw_material_income()
    {
        return $this->hasOne('App\Models\RawMaterialIncome','id','raw_material_income_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Product','id','product_id');
    }

    public function sale()
    {
        return $this->hasOne('App\Models\Sale','id','sale_id');
    }
}
