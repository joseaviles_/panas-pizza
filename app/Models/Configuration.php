<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
	protected $table = 'configurations';

    protected $fillable = [
        'exchange_rate',
        'name',
		'logo',
		'real_name_logo',
		'unlock_code'
    ];
}
