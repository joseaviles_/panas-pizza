<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
		'name'
  	];

    public function products()
    {
        return $this->hasMany('App\Models\Product','category_id','id');
    }

    public function raw_material()
    {
        return $this->belongsToMany('App\Models\RawMaterial','category_raw_material','category_id','raw_material_id')->withPivot('amount');
    }
}
