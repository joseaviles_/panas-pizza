<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryType extends Model
{
    protected $table = 'delivery_types';

    protected $fillable = [
		'name',
		'price',
		'delivery_company_id'
  	];

    public function delivery_company()
    {
        return $this->hasOne('App\Models\DeliveryCompany','id','delivery_company_id');
    }

    public function delivery_sales()
    {
        return $this->hasMany('App\Models\DeliverySale');
    }
}
