<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = [
        'sale_id',
        'payment_method_id',
        'payment_method_option_id',
        'payment_amount_usd',
        'payment_amount_bss',
        'payment_exchange_rate',
   		'payment_reference_number',
        'payment_description',
        'change_method_id',
        'change_method_option_id',
        'change_amount_usd',
        'change_amount_bss',
        'change_exchange_rate',
        'change_reference_number',
        'change_description',
        'pay_the_customer',
        'created_at',
  	];

	public function sale()
    {
        return $this->hasOne('App\Models\Sale','id','sale_id');
    }

    public function payment_method()
    {
        return $this->hasOne('App\Models\PaymentMethod','id','payment_method_id');
    }

    public function payment_method_option()
    {
        return $this->hasOne('App\Models\PaymentMethodOption','id','payment_method_option_id');
    }

    public function change_method()
    {
        return $this->hasOne('App\Models\PaymentMethod','id','change_method_id');
    }

    public function change_method_option()
    {
        return $this->hasOne('App\Models\PaymentMethodOption','id','change_method_option_id');
    }
}
