<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = 'sales';

    protected $fillable = [
		'user_id',
		'client_id',
		'description',
		'sale_type',
		'total',
		'exchange_rate',
		'total_bss',
        'discount',
        'total_paid',
        'delivery_amount',
        'dispatched',
  	];

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function client()
    {
        return $this->hasOne('App\Models\Client','id','client_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product','product_sale')->withPivot('amount','price','description');
    }

    public function promotions()
    {
        return $this->belongsToMany('App\Models\Promotion','promotion_sale')->withPivot('amount','price','description');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payment');
    }

    public function cash_register_movements()
    {
        return $this->hasMany('App\Models\CashRegisterMovement');
    }

    public function raw_material_movements()
    {
        return $this->hasMany('App\Models\RawMaterialMovement');
    }

    public function unprocessed_products_movements()
    {
        return $this->hasMany('App\Models\UnprocessedProductsMovement');
    }

    public function raw_material_income_movements()
    {
        return $this->hasMany('App\Models\RawMaterialIncomeMovement');
    }

    public function delivery_sales()
    {
        return $this->hasMany('App\Models\DeliverySale');
    }
}
