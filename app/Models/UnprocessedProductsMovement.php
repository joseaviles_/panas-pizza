<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnprocessedProductsMovement extends Model
{
    protected $table = 'unprocessed_products_movements';

    protected $fillable = [
		'product_id',
		'amount',
        'new_amount',
        'price',
		'price_bss',
		'exchange_rate',
		'concept',
		'type',
        'sale_id',
        'is_deleted',
        'created_at',
  	];

    public function product()
    {
        return $this->hasOne('App\Models\Product','id','product_id');
    }

    public function sale()
    {
        return $this->hasOne('App\Models\Sale','id','sale_id');
    }
}
