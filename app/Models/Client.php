<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';

    protected $fillable = [
		'client_number',
		'first_name',
		'last_name',
		'identity_card',
		'name_business_phone',
		'first_phone',
		'second_phone'
  	];

    public function sales()
    {
        return $this->hasMany('App\Models\Sale');
    }
}
