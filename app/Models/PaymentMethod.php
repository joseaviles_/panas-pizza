<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $table = 'payment_methods';

    protected $fillable = [
		'name',
		'currency',
		'type'
  	];

    public function payment_method_options()
    {
        return $this->hasMany('App\Models\PaymentMethodOption');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payment','payment_method_id','id');
    }

    public function changes()
    {
        return $this->hasMany('App\Models\Payment','change_method_id','id');
    }
}
