<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashRegister extends Model
{
    protected $table = 'cash_registers';

    protected $fillable = [
		'dollar_amount',
		'bolivars_amount'
  	];

    public function cash_register_movements()
    {
        return $this->hasMany('App\Models\CashRegisterMovement');
    }
}
