<?php

namespace App\Console\Commands;

use App\Models\CashRegister;
use App\Models\CashRegisterMovement;
use App\Models\Product;
use App\Models\RawMaterial;
use App\Models\RawMaterialMovement;
use App\Models\UnprocessedProductsMovement;
use DB;
use Illuminate\Console\Command;

class RestartSales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:restart_sales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restart sales';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('sales')->delete();

        $products=Product::all();
        foreach($products as $product) {
            $product->update(['amount'=>0]);
            UnprocessedProductsMovement::where('product_id',$product->id)->delete();
        }

        $raws_material=RawMaterial::all();
        foreach($raws_material as $raw_material) {
            $raw_material->update(['amount'=>0]);
            RawMaterialMovement::where('raw_material_id',$raw_material->id)->delete();
        }

        $cash_register=CashRegister::first();
        $cash_register->update(['dollar_amount'=>0, 'bolivars_amount'=>0]);
        CashRegisterMovement::where('cash_register_id',$cash_register->id)->delete();
    }
}
