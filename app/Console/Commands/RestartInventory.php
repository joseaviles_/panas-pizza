<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\RawMaterial;
use App\Models\RawMaterialMovement;
use App\Models\UnprocessedProductsMovement;
use Illuminate\Console\Command;

class RestartInventory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:restart_inventory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restart inventory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products=Product::all();
        foreach($products as $product) {
            $product->update(['amount'=>0]);
            UnprocessedProductsMovement::where('product_id',$product->id)->delete();
        }

        $raws_material=RawMaterial::all();
        foreach($raws_material as $raw_material) {
            $raw_material->update(['amount'=>0]);
            RawMaterialMovement::where('raw_material_id',$raw_material->id)->delete();
        }
    }
}
