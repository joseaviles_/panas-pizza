<?php

namespace App\Console\Commands;

use App\Models\CashRegister;
use App\Models\CashRegisterMovement;
use Illuminate\Console\Command;

class RestartCashRegister extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:restart_cash_register';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restart cash register';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cash_register=CashRegister::first();
        $cash_register->update(['dollar_amount'=>0, 'bolivars_amount'=>0]);
        CashRegisterMovement::where('cash_register_id',$cash_register->id)->delete();
    }
}
