<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'configuration'], function () {

	// Home
	Route::get('/', 'HomeController@index')->name('home');

	// Authentication
	Route::get('login', 'ProfileController@getLogin')->name('login');
	Route::post('login', 'ProfileController@postLogin')->name('post_login');
	Route::get('logout', 'ProfileController@logout')->middleware('logged')->name('logout');

	// Password reset
	Route::get('password/reset', 'ProfileController@getResetPassword')->name('password_reset');
	Route::post('password/email', 'ProfileController@sendResetLinkEmail')->name('post_password_reset');
	Route::get('password/reset/{token}', 'ProfileController@getResetPasswordToken')->name('password_reset_token');
	Route::post('password/reset', 'ProfileController@resetPassword')->name('post_password_reset_token');

	// Dashboard
	Route::get('dashboard', 'ProfileController@getDashboard')->middleware('logged')->name('dashboard');

	// Admin dashboard
	Route::get('admin/dashboard', 'Admin\AdminController@getAdminDashboard')->middleware('logged')->middleware(['role:admin'])->name('admin_dashboard');
	Route::get('admin/dashboard/elaborable_products','Admin\AdminController@getElaborableProducts')->middleware('logged')->middleware(['role:admin|seller'])->name('get_admin_elaborable_products');
	Route::post('admin/dashboard/exchange_rate','Admin\AdminController@exchangeRate')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_exchange_rate');

	// Seller dashboard
	Route::get('seller/dashboard', 'Seller\SellerController@getSellerDashboard')->middleware('logged')->middleware(['role:seller'])->name('seller_dashboard');

	// Admin - Users
	Route::get('admin/users','Admin\UsersController@getIndex')->middleware('logged')->middleware(['role:admin'])->name('admin_users');
	Route::get('admin/users/get', 'Admin\UsersController@getList')->middleware('logged')->middleware(['role:admin'])->name('get_admin_users');
	Route::get('admin/users/create', 'Admin\UsersController@getCreate')->middleware('logged')->middleware(['role:admin'])->name('admin_users_create');
	Route::post('admin/users/create', 'Admin\UsersController@create')->middleware('logged')->middleware(['role:admin'])->name('admin_users_post_create');
	Route::get('admin/users/edit/{user_id}', 'Admin\UsersController@getEdit')->middleware('logged')->middleware(['role:admin'])->name('admin_users_edit');
	Route::post('admin/users/edit', 'Admin\UsersController@update')->middleware('logged')->middleware(['role:admin'])->name('admin_users_update');
	Route::get('admin/users/trash/{user_id}', 'Admin\UsersController@getTrash')->middleware('logged')->middleware(['role:admin'])->name('admin_users_trash');
	Route::post('admin/users/delete', 'Admin\UsersController@delete')->middleware('logged')->middleware(['role:admin'])->name('admin_users_delete');

	// Admin - Workers
	Route::get('admin/workers','Admin\WorkersController@getIndex')->middleware('logged')->middleware(['role:admin'])->name('admin_workers');
	Route::get('admin/workers/get', 'Admin\WorkersController@getList')->middleware('logged')->middleware(['role:admin'])->name('get_admin_workers');
	Route::get('admin/workers/create', 'Admin\WorkersController@getCreate')->middleware('logged')->middleware(['role:admin'])->name('admin_workers_create');
	Route::post('admin/workers/create', 'Admin\WorkersController@create')->middleware('logged')->middleware(['role:admin'])->name('admin_workers_post_create');
	Route::get('admin/workers/edit/{worker_id}', 'Admin\WorkersController@getEdit')->middleware('logged')->middleware(['role:admin'])->name('admin_workers_edit');
	Route::post('admin/workers/edit', 'Admin\WorkersController@update')->middleware('logged')->middleware(['role:admin'])->name('admin_workers_update');
	Route::get('admin/workers/trash/{worker_id}', 'Admin\WorkersController@getTrash')->middleware('logged')->middleware(['role:admin'])->name('admin_workers_trash');
	Route::post('admin/workers/delete', 'Admin\WorkersController@delete')->middleware('logged')->middleware(['role:admin'])->name('admin_workers_delete');

	// Admin - Clients
	Route::get('admin/clients','Admin\ClientsController@getIndex')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_clients');
	Route::get('admin/clients/get', 'Admin\ClientsController@getList')->middleware('logged')->middleware(['role:admin|seller'])->name('get_admin_clients');
	Route::get('admin/clients/create', 'Admin\ClientsController@getCreate')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_clients_create');
	Route::post('admin/clients/create', 'Admin\ClientsController@create')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_clients_post_create');
	Route::get('admin/clients/edit/{client_id}', 'Admin\ClientsController@getEdit')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_clients_edit');
	Route::post('admin/clients/edit', 'Admin\ClientsController@update')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_clients_update');
	Route::get('admin/clients/trash/{client_id}', 'Admin\ClientsController@getTrash')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_clients_trash');
	Route::post('admin/clients/delete', 'Admin\ClientsController@delete')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_clients_delete');

	// Admin - Categories
	Route::get('admin/categories','Admin\CategoriesController@getIndex')->middleware('logged')->middleware(['role:admin'])->name('admin_categories');
	Route::get('admin/categories/get', 'Admin\CategoriesController@getList')->middleware('logged')->middleware(['role:admin'])->name('get_admin_categories');
	Route::get('admin/categories/create', 'Admin\CategoriesController@getCreate')->middleware('logged')->middleware(['role:admin'])->name('admin_categories_create');
	Route::post('admin/categories/create', 'Admin\CategoriesController@create')->middleware('logged')->middleware(['role:admin'])->name('admin_categories_post_create');
	Route::get('admin/categories/edit/{category_id}', 'Admin\CategoriesController@getEdit')->middleware('logged')->middleware(['role:admin'])->name('admin_categories_edit');
	Route::post('admin/categories/edit', 'Admin\CategoriesController@update')->middleware('logged')->middleware(['role:admin'])->name('admin_categories_update');
	Route::get('admin/categories/trash/{category_id}', 'Admin\CategoriesController@getTrash')->middleware('logged')->middleware(['role:admin'])->name('admin_categories_trash');
	Route::post('admin/categories/delete', 'Admin\CategoriesController@delete')->middleware('logged')->middleware(['role:admin'])->name('admin_categories_delete');

	// Admin - RawMaterial
	Route::get('admin/raw_material','Admin\RawMaterialController@getIndex')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material');
	Route::get('admin/raw_material/get', 'Admin\RawMaterialController@getList')->middleware('logged')->middleware(['role:admin'])->name('get_admin_raw_material');
	Route::get('admin/raw_material/create', 'Admin\RawMaterialController@getCreate')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_create');
	Route::post('admin/raw_material/create', 'Admin\RawMaterialController@create')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_post_create');
	Route::get('admin/raw_material/edit/{raw_material_id}', 'Admin\RawMaterialController@getEdit')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_edit');
	Route::post('admin/raw_material/edit', 'Admin\RawMaterialController@update')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_update');
	Route::get('admin/raw_material/trash/{raw_material_id}', 'Admin\RawMaterialController@getTrash')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_trash');
	Route::post('admin/raw_material/delete', 'Admin\RawMaterialController@delete')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_delete');

	// Admin - Products
	Route::get('admin/products','Admin\ProductsController@getIndex')->middleware('logged')->middleware(['role:admin'])->name('admin_products');
	Route::get('admin/products/get', 'Admin\ProductsController@getList')->middleware('logged')->middleware(['role:admin'])->name('get_admin_products');
	Route::get('admin/products/create', 'Admin\ProductsController@getCreate')->middleware('logged')->middleware(['role:admin'])->name('admin_products_create');
	Route::post('admin/products/create', 'Admin\ProductsController@create')->middleware('logged')->middleware(['role:admin'])->name('admin_products_post_create');
	Route::get('admin/products/edit/{product_id}', 'Admin\ProductsController@getEdit')->middleware('logged')->middleware(['role:admin'])->name('admin_products_edit');
	Route::post('admin/products/edit', 'Admin\ProductsController@update')->middleware('logged')->middleware(['role:admin'])->name('admin_products_update');
	Route::get('admin/products/trash/{product_id}', 'Admin\ProductsController@getTrash')->middleware('logged')->middleware(['role:admin'])->name('admin_products_trash');
	Route::post('admin/products/delete', 'Admin\ProductsController@delete')->middleware('logged')->middleware(['role:admin'])->name('admin_products_delete');
	Route::get('admin/products/get_raw_material/{category_id}','Admin\CategoriesController@getRawMaterial')->middleware('logged')->middleware(['role:admin'])->name('admin_products_get_raw_material');

	// Admin - Promotions
	Route::get('admin/promotions','Admin\PromotionsController@getIndex')->middleware('logged')->middleware(['role:admin'])->name('admin_promotions');
	Route::get('admin/promotions/get', 'Admin\PromotionsController@getList')->middleware('logged')->middleware(['role:admin'])->name('get_admin_promotions');
	Route::get('admin/promotions/create', 'Admin\PromotionsController@getCreate')->middleware('logged')->middleware(['role:admin'])->name('admin_promotions_create');
	Route::post('admin/promotions/create', 'Admin\PromotionsController@create')->middleware('logged')->middleware(['role:admin'])->name('admin_promotions_post_create');
	Route::get('admin/promotions/edit/{promotion_id}', 'Admin\PromotionsController@getEdit')->middleware('logged')->middleware(['role:admin'])->name('admin_promotions_edit');
	Route::post('admin/promotions/edit', 'Admin\PromotionsController@update')->middleware('logged')->middleware(['role:admin'])->name('admin_promotions_update');
	Route::get('admin/promotions/trash/{promotion_id}', 'Admin\PromotionsController@getTrash')->middleware('logged')->middleware(['role:admin'])->name('admin_promotions_trash');
	Route::post('admin/promotions/delete', 'Admin\PromotionsController@delete')->middleware('logged')->middleware(['role:admin'])->name('admin_promotions_delete');

	// Admin - Inventory
	Route::get('admin/inventory','Admin\InventoryController@getIndex')->middleware('logged')->middleware(['role:admin'])->name('admin_inventory');
	Route::get('admin/inventory/get_raw_material', 'Admin\InventoryController@getListRawMaterial')->middleware('logged')->middleware(['role:admin'])->name('get_raw_material_admin_inventory');
	Route::get('admin/inventory/show_raw_material/{raw_material_id}', 'Admin\InventoryController@showRawMaterial')->middleware('logged')->middleware(['role:admin'])->name('admin_inventory_show_raw_material');
	Route::get('admin/raw_material_movements/get/{raw_material_id}', 'Admin\RawMaterialMovementsController@getList')->middleware('logged')->middleware(['role:admin'])->name('get_admin_raw_material_movements');
	Route::post('admin/raw_material_movements/create', 'Admin\RawMaterialMovementsController@create')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_movements_post_create');
	Route::get('admin/raw_material_movements/edit/{raw_material_movement_id}', 'Admin\RawMaterialMovementsController@getEdit')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_movements_edit');
	Route::post('admin/raw_material_movements/edit', 'Admin\RawMaterialMovementsController@update')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_movements_update');
	Route::get('admin/raw_material_movements/trash/{raw_material_movement_id}', 'Admin\RawMaterialMovementsController@getTrash')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_movements_trash');
	Route::post('admin/raw_material_movements/delete', 'Admin\RawMaterialMovementsController@delete')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_movements_delete');
	Route::post('admin/raw_material_movements/adjust_inventory', 'Admin\RawMaterialMovementsController@adjustInventory')->middleware('logged')->middleware(['role:admin'])->name('admin_raw_material_movements_adjust_inventory');
	Route::get('admin/inventory/get_unprocessed_products', 'Admin\InventoryController@getListUnprocessedProducts')->middleware('logged')->middleware(['role:admin'])->name('get_unprocessed_products_admin_inventory');
	Route::get('admin/inventory/show_unprocessed_product/{product_id}', 'Admin\InventoryController@showUnprocessedProduct')->middleware('logged')->middleware(['role:admin'])->name('admin_inventory_show_unprocessed_products');
	Route::get('admin/unprocessed_products/get/{product_id}', 'Admin\UnprocessedProductsMovementsController@getList')->middleware('logged')->middleware(['role:admin'])->name('get_admin_unprocessed_products_movements');
	Route::post('admin/unprocessed_products/create', 'Admin\UnprocessedProductsMovementsController@create')->middleware('logged')->middleware(['role:admin'])->name('admin_unprocessed_products_movements_post_create');
	Route::get('admin/unprocessed_products/edit/{unprocessed_product_movement_id}', 'Admin\UnprocessedProductsMovementsController@getEdit')->middleware('logged')->middleware(['role:admin'])->name('admin_unprocessed_products_movements_edit');
	Route::post('admin/unprocessed_products/edit', 'Admin\UnprocessedProductsMovementsController@update')->middleware('logged')->middleware(['role:admin'])->name('admin_unprocessed_products_movements_update');
	Route::get('admin/unprocessed_products/trash/{unprocessed_product_movement_id}', 'Admin\UnprocessedProductsMovementsController@getTrash')->middleware('logged')->middleware(['role:admin'])->name('admin_unprocessed_products_movements_trash');
	Route::post('admin/unprocessed_products/delete', 'Admin\UnprocessedProductsMovementsController@delete')->middleware('logged')->middleware(['role:admin'])->name('admin_unprocessed_products_movements_delete');
	Route::post('admin/unprocessed_products/adjust_inventory', 'Admin\UnprocessedProductsMovementsController@adjustInventory')->middleware('logged')->middleware(['role:admin'])->name('admin_unprocessed_products_movements_adjust_inventory');

	// Admin - Payment methods
	Route::get('admin/payment_methods','Admin\PaymentMethodsController@getIndex')->middleware('logged')->middleware(['role:admin'])->name('admin_payment_methods');
	Route::get('admin/payment_methods/get', 'Admin\PaymentMethodsController@getList')->middleware('logged')->middleware(['role:admin'])->name('get_admin_payment_methods');
	Route::get('admin/payment_methods/create', 'Admin\PaymentMethodsController@getCreate')->middleware('logged')->middleware(['role:admin'])->name('admin_payment_methods_create');
	Route::post('admin/payment_methods/create', 'Admin\PaymentMethodsController@create')->middleware('logged')->middleware(['role:admin'])->name('admin_payment_methods_post_create');
	Route::get('admin/payment_methods/edit/{payment_method_id}', 'Admin\PaymentMethodsController@getEdit')->middleware('logged')->middleware(['role:admin'])->name('admin_payment_methods_edit');
	Route::post('admin/payment_methods/edit', 'Admin\PaymentMethodsController@update')->middleware('logged')->middleware(['role:admin'])->name('admin_payment_methods_update');
	Route::get('admin/payment_methods/trash/{payment_method_id}', 'Admin\PaymentMethodsController@getTrash')->middleware('logged')->middleware(['role:admin'])->name('admin_payment_methods_trash');
	Route::post('admin/payment_methods/delete', 'Admin\PaymentMethodsController@delete')->middleware('logged')->middleware(['role:admin'])->name('admin_payment_methods_delete');
	
	// Admin - Delivery companies
	Route::get('admin/delivery_companies','Admin\DeliveryCompaniesController@getIndex')->middleware('logged')->middleware(['role:admin'])->name('admin_delivery_companies');
	Route::get('admin/delivery_companies/get', 'Admin\DeliveryCompaniesController@getList')->middleware('logged')->middleware(['role:admin'])->name('get_admin_delivery_companies');
	Route::get('admin/delivery_companies/create', 'Admin\DeliveryCompaniesController@getCreate')->middleware('logged')->middleware(['role:admin'])->name('admin_delivery_companies_create');
	Route::post('admin/delivery_companies/create', 'Admin\DeliveryCompaniesController@create')->middleware('logged')->middleware(['role:admin'])->name('admin_delivery_companies_post_create');
	Route::get('admin/delivery_companies/edit/{delivery_company_id}', 'Admin\DeliveryCompaniesController@getEdit')->middleware('logged')->middleware(['role:admin'])->name('admin_delivery_companies_edit');
	Route::post('admin/delivery_companies/edit', 'Admin\DeliveryCompaniesController@update')->middleware('logged')->middleware(['role:admin'])->name('admin_delivery_companies_update');
	Route::get('admin/delivery_companies/trash/{delivery_company_id}', 'Admin\DeliveryCompaniesController@getTrash')->middleware('logged')->middleware(['role:admin'])->name('admin_delivery_companies_trash');
	Route::post('admin/delivery_companies/delete', 'Admin\DeliveryCompaniesController@delete')->middleware('logged')->middleware(['role:admin'])->name('admin_delivery_companies_delete');

	// Sales
	Route::get('sales','Seller\SalesController@getIndex')->middleware('logged')->middleware(['role:admin|seller'])->name('sales');
	Route::get('sales/history','Seller\SalesController@getIndex')->middleware('logged')->middleware(['role:admin|seller'])->name('sales_history');
	Route::get('sales/get', 'Seller\SalesController@getList')->middleware('logged')->middleware(['role:admin|seller'])->name('get_sales');
	Route::get('sales/history/get', 'Seller\SalesController@getList')->middleware('logged')->middleware(['role:admin|seller'])->name('get_sales_history');
	Route::get('sales/create', 'Seller\SalesController@getCreate')->middleware('logged')->middleware(['role:admin|seller'])->name('sales_create');
	Route::post('sales/create', 'Seller\SalesController@create')->middleware('logged')->middleware(['role:admin|seller'])->name('sales_post_create');
	Route::get('sales/show/{sale_id}', 'Seller\SalesController@getShow')->middleware('logged')->middleware(['role:admin|seller'])->name('sales_show');
	Route::get('sales/edit/{sale_id}', 'Seller\SalesController@getEdit')->middleware('logged')->middleware(['role:admin|seller'])->name('sales_edit');
	Route::post('sales/edit', 'Seller\SalesController@update')->middleware('logged')->middleware(['role:admin|seller'])->name('sales_update');
	Route::get('sales/trash/{sale_id}', 'Seller\SalesController@getTrash')->middleware('logged')->middleware(['role:admin'])->name('sales_trash');
	Route::post('sales/delete', 'Seller\SalesController@delete')->middleware('logged')->middleware(['role:admin'])->name('sales_delete');
	Route::get('sales/delivery_company_drivers/{delivery_company_id}', 'Seller\SalesController@getDeliveryCompanyDrivers')->middleware('logged')->middleware(['role:admin|seller'])->name('sales_delivery_company_drivers');
	Route::get('sales/delivery_types/{delivery_company_id}', 'Seller\SalesController@getDeliveryTypes')->middleware('logged')->middleware(['role:admin|seller'])->name('sales_delivery_types');
	Route::get('sales/payment_method_options/{payment_method_id}', 'Seller\SalesController@getPaymentMethodOptions')->middleware('logged')->middleware(['role:admin|seller'])->name('sales_payment_method_options');
	Route::get('sales/update_dispatched/{sale_id}', 'Seller\SalesController@updateDispatched')->middleware('logged')->middleware(['role:admin|seller'])->name('sales_update_dispatched');
	Route::get('sales/product_sales/{product_id}', 'Seller\SalesController@getProductSales')->middleware('logged')->middleware(['role:admin|seller'])->name('product_sales');
	Route::get('sales/promotion_sales/{promotion_id}', 'Seller\SalesController@getPromotionSales')->middleware('logged')->middleware(['role:admin|seller'])->name('promotion_sales');

	// Discount sales
	Route::get('sales/discount','Seller\SalesController@getDiscountIndex')->middleware('logged')->middleware(['role:admin'])->name('discount_sales');
	Route::get('sales/discount/get', 'Seller\SalesController@getDiscountList')->middleware('logged')->middleware(['role:admin'])->name('get_discount_sales');

	// Payment collections
	Route::get('payment_collections','Seller\PaymentCollectionsController@getIndex')->middleware('logged')->middleware(['role:admin|seller'])->name('payment_collections');
	Route::get('payment_collections/get', 'Seller\PaymentCollectionsController@getList')->middleware('logged')->middleware(['role:admin|seller'])->name('get_payment_collections');
	Route::get('payment_collections/pay/{sale_id}', 'Seller\PaymentCollectionsController@getPay')->middleware('logged')->middleware(['role:admin|seller'])->name('payment_collections_pay');
	Route::post('payment_collections/pay', 'Seller\PaymentCollectionsController@pay')->middleware('logged')->middleware(['role:admin|seller'])->name('payment_collections_post_pay');

	// Cash register
	Route::get('cash_register','Seller\CashRegisterController@getIndex')->middleware('logged')->middleware(['role:admin|seller'])->name('cash_register');
	Route::get('cash_register/get', 'Seller\CashRegisterController@getList')->middleware('logged')->middleware(['role:admin|seller'])->name('get_cash_register');
	Route::get('cash_register/create', 'Seller\CashRegisterController@getCreate')->middleware('logged')->middleware(['role:admin|seller'])->name('cash_register_create');
	Route::post('cash_register/create', 'Seller\CashRegisterController@create')->middleware('logged')->middleware(['role:admin|seller'])->name('cash_register_post_create');
	Route::get('cash_register/withdraw_cash', 'Seller\CashRegisterController@getWithdrawCash')->middleware('logged')->middleware(['role:admin'])->name('cash_register_withdraw_cash');
	Route::post('cash_register/withdraw_cash', 'Seller\CashRegisterController@postWithdrawCash')->middleware('logged')->middleware(['role:admin'])->name('cash_register_post_withdraw_cash');

	// Admin - Reports
	Route::get('admin/reports','Admin\ReportsController@getIndex')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_reports');
	Route::get('admin/new_report','Admin\ReportsController@newReport')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_new_report');
	Route::get('admin/pdf_report/{type}/{initial_date}/{final_date}','Admin\ReportsController@pdfReport')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_pdf_report');
	Route::get('admin/pdf_report/{type}/{initial_date}/{final_date}/{rmp}','Admin\ReportsController@pdfReport')->middleware('logged')->middleware(['role:admin|seller'])->name('admin_pdf_report_rmp');

	// Admin - Trackings
	Route::get('admin/trackings','Admin\TrackingsController@getIndex')->middleware('logged')->middleware(['role:admin'])->name('admin_trackings');
	Route::get('admin/trackings/get', 'Admin\TrackingsController@getList')->middleware('logged')->middleware(['role:admin'])->name('get_admin_trackings');

	// Others
	Route::get('profile', 'ProfileController@getProfile')->middleware('logged')->name('profile');
	Route::post('profile/save', 'ProfileController@saveProfile')->middleware('logged')->name('save_profile');
	Route::get('configuration', 'Admin\AdminController@getConfiguration')->middleware('logged')->middleware(['role:admin'])->name('configuration');
	Route::post('configuration/save', 'Admin\AdminController@saveConfiguration')->middleware('logged')->middleware(['role:admin'])->name('save_configuration');

});